#!bin/bash

function setup_rhel() {
  # setup swapsize to 16GB
}

function stop_firewall() {
  service iptables stop
  chkconfig iptables off  
}

function stop_selinux() {
  # /etc/selinux/config SELINUX=disabled
  sed -i 's/SELINUX=enforcing/SELINUX=disabled/' /etc/selinux/config
  service iptables status
}

function sysctl_conf() {
  # cat >> /etc/sysctl.conf << EOF
  # fs.file-max = 6815744
  # kernel.sem = 250 32000 100 128
  # kernel.shmmni = 4096
  # kernel.shmall = 1073741824
  # kernel.shmmax = 4398046511104
  # net.core.rmem_default = 262144
  # net.core.rmem_max = 4194304
  # net.core.wmem_default = 262144
  # net.core.wmem_max = 1048576
  # fs.aio-max-nr = 1048576
  # net.ipv4.ip_local_port_range = 9000 65500
  # EOF

  /sbin/sysctl -p
}

function add_user() {
  groupadd -g 1000 oinstall
  groupadd -g 1100 dba
  useradd -m -g oinstall -G dba -u 1200 oracle
  id oracle  # show
  # passwd oracle
  echo -n abc123 | passwd --stdin oracle
}

# FIXES display colors check
function xhost() {
  su
  xhost +
  # access control disabled, clients can connect from any host
}

function install_dependencies() {
  # add to /etc/yum.repos.d/
  # [rhel-media]
  # name=Red Hat Enterprise Linux 6.5
  # baseurl=file:///media/RHEL_6.5\ x86_64\ Disc\ 1
  # enabled=1
  # gpgcheck=0
  
  # other can ignore
  yum install cloog-ppl compat-libcap1 compat-libstdc++-33 cpp gcc gcc-c++ \
    glibc-devel glibc-headers kernel-headers libXmu libXt ksh mksh \
    libXxf86misc libXxf86vm libaio-devel libdmx libstdc++-devel \
    mpfr make ppl xorg-x11-utils xorg-x11-xauth libXv libXxf86dga \
    unixODBC unixODBC-devel
}

function run_on_finish() {
  /home/oracle/app/oraInventory/orainstRoot.sh
  /home/oracle/app/oracle/product/11.2.0/dbhome_1/root.sh
}

function lsnr_start() {
  export ORACLE_BASE=/home/oracle/app/oracle
  export ORACLE_HOME=$ORACLE_BASE/product/11.2.0/dbhome_1
  export PATH=$PATH:$ORACLE_HOME/bin
  export ORACLE_SID=orcl 
  #
  # Then run the command with user oracle in terminal
  lsnrctl start
}

function startup() {
  echo "startup" | sqlplus / as sysdba
}

function shutdown() {
  echo "shutdown" | sqlplus / as sysdba
}

function dbconsole_start() { 
  emctl start dbconsole
}

function unlock_account() {
  echo "alter user ${user} account unlock;" | | sqlplus / as sysdba
}

function alter_passwd() {
  echo "alter user ${user} identified by ${passwd}" | | sqlplus / as sysdba
}
