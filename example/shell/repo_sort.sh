#!/bin/sh

main() {
    ls $1 | while read line; do
        echo $line
        repo=$(cd ${line}; git remote -v)
        echo "repo: ${repo}"
        if [ $(echo ${repo} | grep "github.com" | wc -l) -ne 0 ]; then
            mv ${line} ~/usr/src/github.com/
        elif [ $(echo ${repo} | grep "gitlab.com" | wc -l) -ne 0 ]; then
            mv ${line} ~/usr/src/gitlab.com/
        fi
    done
}

main $@
