#!bin/bash

function vmware::version() {
  vmware -v
}

function vmware::vmfs_list() {
  esxcli storage vmfs extent list
}

function vmware::nfs_list() {
  esxcli storage nfs list
}

function vmware::nfs_remove() {
  esxcli storage nfs remove -v zbsnfs
}

function vmware::get_all_vms() {
  vim-cmd vmsvc/getallvms
}

function vmware::punchzero() {
  local disk=$1  # xxx.vmdk
  vmkfstools --punchzero ${disk}
}

function vmware::firewall_rules_list() {
  esxcli network firewall ruleset list
}

function vmware::firewall_sshclient_enable() {
  esxcli network firewall ruleset set --ruleset-id=sshClient --enabled=true
}
