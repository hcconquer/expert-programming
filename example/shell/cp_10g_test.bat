@echo off

echo ######################
echo Make test file
if exist E:\10GB del /q E:\10GB
timeout /t 30 /nobreak > nul
echo Begin_time: %time%
dd if=/dev/random of=E:\10GB bs=1M count=10240
echo End_time  : %time%

echo ######################
echo Copy test file
if exist F:\10GB del /q F:\10GB
timeout /t 30 /nobreak > nul
echo Begin_time: %time%
copy E:\10GB F:\ 
echo End_time  : %time%

pause
