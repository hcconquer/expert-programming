#!/bin/bash
# Copyright (C) 2008, Tencent
# All rights reserved.
#
# Filename   : Check_Alive.sh
# Description: 如果进程数目和指定文件processlist中的个数不一样，重启业务进程。
# Author     : ivanzhang@tencent.com
# Version    : 2.0
# Date       : 2008-10-27
# Last Update: 2009-09-21 support using -s, a custom script to restart program 
#		should be compatible with version 1

export PATH=/sbin:/usr/sbin:/usr/local/sbin:/opt/gnome/sbin:/usr/local/bin:/usr/bin:/usr/X11R6/bin:/bin:/usr/games:/opt/gnome/bin:/usr/lib/mit/bin:/usr/lib/mit/sbin:/usr/local/mysql/bin/
logfile=/tmp/logfile.`basename $0`
tmpfile=/tmp/tmpfile.`basename $0`.$$
tmpfile1=/tmp/tmpfile1.`basename $0`.$$
MYIP=`/sbin/ifconfig eth1| grep 'inet addr' | awk '{print $2}' | awk -F':' '{print $2}'`

show_usage()
{
	echo ""
	echo "用法： "
	echo "	`basename $0` -b 业务进程程序目录 检查进程1 检查进程2 [...]"
	echo "	`basename $0` -b 业务进程程序目录 -f 检查进程文件列表(格式与processlist)相同"
	echo "	`basename $0` -b 业务进程程序目录"
	echo "例子：  "
	echo "	1. `basename $0` -b /home/oicq/drserver ./drserver"
	echo "	2. `basename $0` -b /home/oicq/new_relay/bin  -f /usr/local/agenttools/agent/processlist"
	echo "	3. `basename $0` -b /home/oicq/new_relay/bin 相当于2"
	echo "	4. `basename $0` -b /home/oicq/new_relay/bin \"./SSConfigure ../conf/sessionserver.conf\""
	echo "	5. `basename $0` -s /home/oicq/new_relay/restart.sh [-f /usr/local/agenttools/agent/processlist]"
	echo "注意: 例1,4中的检查进程数目都为1个!"
	echo ""
	exit 1;
}

#重启5分钟后才拉起
check_uptime()
{
	while true;do
		up_min=`awk '{print int($1/60)}' /proc/uptime`		
		if [ ${up_min} -gt 5 ];then
			return
		else
			sleep 120;
			continue
		fi
	done
}

check_uptime
REPORT="/usr/local/agenttools/agent/agentRepStr"

#上报工具不存在，直接退出
if [ ! -x ${REPORT} ];then
	echo "ERROR: ${REPORT} not found"
fi

if [ "${*}x" = "x" ];then
	show_usage;
	exit 1;
fi

#分析传入参数
while getopts \?hb:f:s: flag;do
	case ${flag} in
		\?)show_usage;exit 1 ;;
		h)show_usage;exit 1 ;;
		b)PROG_DIR=${OPTARG} ;;
		f)CONF_FILE=${OPTARG} ;;
		s)RESTART_SH=${OPTARG} ;;
	esac
done

shift `expr ${OPTIND} - 1`

#分析传入参数逻辑是否规范?
if [ "${PROG_DIR}x" = "x" -a "${RESTART_SH}x" = "x" ];then
	show_usage
	exit 1;
fi

if [ "${CONF_FILE}x" = "x" -a "${*}x" != "x" ];then
	for((i=0;i<=$#;i++));do
		PROC_LIST=("${PROC_LIST[@]}" "$1")
		shift 1;
	done
	for idx in $(seq 0 $((${#PROC_LIST[@]} - 1)));do
		PROC_NUM[${idx}]=1
	done
elif [ "${*}x" = "x" ];then
	if [ "${CONF_FILE}x" = "x" ];then
		[ ! -f /usr/local/agenttools/agent/processlist ] && (echo "processlist not exists";exit 1)
		CONF_FILE=/usr/local/agenttools/agent/processlist
	fi

	#重复行只留一行
	awk '!a[$0]++' ${CONF_FILE} > ${tmpfile1}

	sed "s/,[0-9]\{1,\}$//g" ${tmpfile1} > ${tmpfile}
	line_count=`wc -l ${tmpfile} | awk '{print $1}'`
	for((j=1;j<=${line_count};j++));do
		PROC_LIST=("${PROC_LIST[@]}" "`sed -n "${j}p" ${tmpfile}`")
	done

	PROC_NUM=(`awk -F',' '{print $2}' ${CONF_FILE}`)
else
	show_usage;
	exit 1;
	
fi

for((k=0;k<${#PROC_LIST[@]};k++));do
	BIN=`echo "${PROC_LIST[$k]}" | awk '{print $1}' | sed "s/\.\///g"`
	BIN_CONF=`echo "${PROC_LIST[$k]}" | awk '{print $2}' | awk -F',' '{print $1}'`
	PROC_BIN=`echo "${PROC_LIST[$k]}" | awk -F',' '{print $1}' | awk '{print $1}'`
	if [ "${BIN_CONF}x" = "x" ];then
		CUR_PROC_NUM=`ps auxw | grep -P "${PROC_BIN}.*" | grep -v 'grep' | wc -l`
	else
		CUR_PROC_NUM=`ps auxw | grep -P "${PROC_BIN}\s*${BIN_CONF}$" | grep -v 'grep' | wc -l`
	fi

	#wc -l 在slack的机器算出来的数据前面有空格，去掉
	CUR_PROC_NUM=`echo ${CUR_PROC_NUM} | sed 's/ //g'`

	if [ ${CUR_PROC_NUM} -eq ${PROC_NUM[$k]} ];then
		continue;
	else
		if [ "${PROG_DIR}x" != "x" ];then
			cd ${PROG_DIR};
			if [ ! -f ${BIN} ];then
				echo ""
				echo "ERROR: file ${PROC_BIN} not found in ${PROG_DIR}, restart Failed!"
				show_usage
			else
				echo "`date` Progam ${PROC_LIST[$k]} Restart" | tee -a ${logfile}
				${REPORT} 11806 "${PROC_LIST[$k]} restart on $HOSTNAME($MYIP)" 
				killall -9 ${BIN}
				sleep 2;
				${PROC_LIST[$k]} 
				
			fi
		else
			if [ "${RESTART_SH}x" != "x" ];then  
				echo "`date` Progam ${PROC_LIST[$k]} Restart by ${RESTART_SH}" | tee -a ${logfile}
				${REPORT} 11806 "${PROC_LIST[$k]} restart on $HOSTNAME($MYIP)" 
				${RESTART_SH} restart
				break 
			fi
		fi
	fi
done

rm -f ${tmpfile} ${tmpfile1}
