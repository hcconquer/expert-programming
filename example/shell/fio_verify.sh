#!/bin/sh

write_verify_test() {
  if [ $# -lt 2 ]; then
    return 1
  fi

  local filename=$1
  local blocksize=$2
  local size=16M
  local name="write_verify_size${size}_bs${blocksize}"

  fio -name=${name} -rw=write -bs=${blocksize} -size=${size} \
      -filename=${filename} -iodepth=1 -verify=md5  > /dev/null 2>&1

  echo $?
}

for sz in $(seq 63 1048576); do
  blocksize=${sz}B
  ret=$(write_verify_test ./test_file ${blocksize})
  if [ $ret -eq 0 ]; then
    echo "Write verify ${blocksize} succ."
  else
    echo "Write verify ${blocksize} fail."
  fi
done

echo "Done."

