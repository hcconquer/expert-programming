#!/bin/bash
#
# rand to kill zbs

rand_kill() {
  local randnum=$(($(od -An -N1 -i /dev/random)%5))
  echo "random: ${randnum}"
  
  case ${randnum} in
    0)
      echo "service zbs-chunkd restart"
      service zbs-chunkd restart
      ;;
    *)
      echo "do nothing"
      ;;
  esac
}

echo "date: $(date)"
rand_kill
