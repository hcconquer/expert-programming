#!bin/bash

function yum_update_to() {
  local version=$1  # 3.0.5
  yum downgrade *zbs*${version}* *pyzbs*${version}* *smartx*${version}* *fisheye*${version}*
  yum update-to *zbs*3.0.5* *pyzbs*3.0.5* *smartx*3.0.5* *fisheye*3.0.5*
}

function yum_downgrade() {
  local version=$1  # 3.0.5
  yum downgrade *zbs*${version}* *pyzbs*${version}* *smartx*${version}* *fisheye*${version}*
}

function service_add_param() {
  # add /usr/lib/systemd/system/${service}.service
  # systemctl daemon-reload
}