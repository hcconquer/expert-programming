#!/bin/bash

function get_vm_num() {
  local vm_num=$(smartx vms list 2>/dev/null | grep "vm_name" | wc -l)
  echo ${vm_num}
}

function get_vm_name_by_uuid() {
  local vm_uuid=$1
  echo $(smartx vms show ${vm_uuid} 2>/dev/null | grep "vm_name" | 
         awk -F"'" '{print $4}')
}

# full match
function get_vm_uuid_by_name() {
  local vm_name=$1
  # e.g. u'vm_name': u'centos7'},
  local vm_uuid_list=$(smartx vms list 2>/dev/null | 
                       grep -B 2 "vm_name.*'${vm_name}'" | 
                       grep "uuid" | 
                       awk -F"'" '{print $4}')
  local vm_uuid=$(echo ${vm_uuid_list} | awk '{print $1;}')
  echo ${vm_uuid}
}

# wildcard match and rand 1
function get_vm_uuid_by_name_rand1() {
  local vm_name=$1
  local vm_uuid_list=$(smartx vms list 2>/dev/null | 
                       grep -B 2 "vm_name.*${vm_name}" | 
                       grep "uuid" | 
                       awk -F"'" '{print $4}')
  if [ ${#vm_uuid_list} -lt 36 ]; then
    return 1
  fi
  local vm_num=$(echo ${vm_uuid_list} | awk '{print NF}')
  local randnum=$(($(od -An -N2 -i /dev/random)%(vm_num+1)))
  local vm_uuid=$(echo ${vm_uuid_list} | awk -v n=${randnum} '{print $n;}')
  echo ${vm_uuid}
}

function clone_vm_by_uuid() {
  local src_vm_uuid=$1
  local src_vm_name=$(get_vm_name_by_uuid ${src_vm_uuid})
  local dst_vm_name=${src_vm_name}"-clone"
  local eok=$(smartx vms clone --vm_name ${dst_vm_name} --auto_schedule true  ${src_vm_uuid} 2>/dev/null | 
              grep "ec" | grep "EOK" | wc -l)
  if [ ${eok} -ne 1 ]; then
    return 1
  fi
  return 0
}

function delete_vm_by_uuid() {
  local vm_uuid=$1
  local eok=$(smartx vms delete --include_volumes true ${vm_uuid} 2>/dev/null | 
              grep "ec" | grep "EOK" | wc -l)
  if [ ${eok} -ne 1 ]; then
    return 1
  fi
  return 0
}

# wildcard match and rand 1
function get_vm_snapshot_uuid_by_name_rand1() {
  local vm_name=$1
  local vm_snapshot_uuid_list=$(smartx vm_snapshots list 2>/dev/null | 
                                grep -B 3 "vm_name.*${vm_name}" | 
                                grep "uuid" | 
                                awk -F"'" '{print $4}')
  if [ ${#vm_snapshot_uuid_list} -lt 36 ]; then
    return 1
  fi
  local vm_snapshot_num=$(echo ${vm_snapshot_uuid_list} | awk '{print NF}')
  local randnum=$(($(od -An -N2 -i /dev/random)%(vm_snapshot_num+1)))
  local vm_snapshot_uuid=$(echo ${vm_snapshot_uuid_list} | awk -v n=${randnum} '{print $n;}')
  echo ${vm_snapshot_uuid}
}

function create_vm_snapshot_by_vm_uuid() {
  local vm_uuid=$1
  local vm_name=$(get_vm_name_by_uuid ${vm_uuid})
  local snapshot_name=${vm_name}"-snapshot"
  local eok=$(smartx vm_snapshots create ${vm_uuid} ${snapshot_name} 2>/dev/null |
              grep "ec" | grep "EOK" | wc -l)
  if [ ${eok} -ne 1 ]; then
    return 1
  fi
  return 0
}

function delete_vm_snapshot_by_uuid() {
  local snapshot_uuid=$1
  local eok=$(smartx vm_snapshots delete --delete_volumes true ${snapshot_uuid} 2>/dev/null |
              grep "ec" | grep "EOK" | wc -l)
  if [ ${eok} -ne 1 ]; then
    return 1
  fi
  return 0
}

# full match by name
function clone_vm_by_template() {
  local src_vm_name=$1
  local src_vm_uuid=$(get_vm_uuid_by_name ${src_vm_name})
  clone_vm_by_uuid ${src_vm_uuid}
  local ret=$?
  echo "clone vm, result: ${ret}, src_vm_name: ${src_vm_name}, src_vm_uuid: ${src_vm_uuid}"
}

# wildcard match by name and rand 1
function clone_vm_by_template_rand1() {
  local src_vm_name=$1
  local src_vm_uuid=$(get_vm_uuid_by_name_rand1 ${src_vm_name})
  if [ ${#src_vm_uuid} -ne 36 ]; then
    echo "no vm found, name: ${src_vm_name}"
    return 1
  fi
  clone_vm_by_uuid ${src_vm_uuid}
  local ret=$?
  echo "clone vm, result: ${ret}, src_vm_name: ${src_vm_name}, src_vm_uuid: ${src_vm_uuid}"
}

# wildcard match by name and rand 1
function delete_vm_by_name_rand1() {
  local vm_name=$1
  # delete one clone vm
  local vm_uuid_to_delete=$(get_vm_uuid_by_name_rand1 "${vm_name}")
  # uuid len is 36
  if [ ${#vm_uuid_to_delete} -eq 36 ]; then
    delete_vm_by_uuid ${vm_uuid_to_delete}
    local ret=$?
    echo "delete vm, result: ${ret}, vm_uuid: ${vm_uuid_to_delete}, vm_name: ${vm_name}"
  else
    echo "no match vm to delete"
  fi
}

# wildcard match by name and rand 1
function create_vm_snapshot_by_name_rand1() {
  local vm_name=$1
  local vm_uuid=$(get_vm_uuid_by_name_rand1 ${vm_name})
  if [ ${#vm_uuid} -ne 36 ]; then
    echo "no vm found, name: ${vm_name}"
    return 1
  fi
  create_vm_snapshot_by_vm_uuid ${vm_uuid}
  local ret=$?
  echo "create vm snapshot, result: ${ret}, vm_name: ${vm_name}, vm_uuid: ${vm_uuid}"
}

# wildcard match by name and rand 1
function delete_vm_snapshot_by_name_rand1() {
  local vm_name=$1
  local vm_snapshot_uuid_to_delete=$(get_vm_snapshot_uuid_by_name_rand1 ${vm_name})
  # uuid len is 36
  if [ ${#vm_snapshot_uuid_to_delete} -eq 36 ]; then
    delete_vm_snapshot_by_uuid ${vm_snapshot_uuid_to_delete}
    local ret=$?
    echo "delete vm snapshot, result: ${ret}, vm_snapshot_uuid: ${vm_snapshot_uuid_to_delete}, vm_name: ${vm_name}"
  else
    echo "no match vm snapshot to delete"
  fi
}

function main() {
  local vm_num=$(get_vm_num)

  if [ ${vm_num} -lt 50 ]; then
    local randnum=$(($(od -An -N2 -i /dev/random)%10))
    echo "random: ${randnum}"
    case ${randnum} in
      0)
        clone_vm_by_template "centos7"
        ;;
      1)
        clone_vm_by_template_rand1 "centos7-clone"
        ;;
      2)
        create_vm_snapshot_by_name_rand1 "centos7-clone"
        ;;
      3)
        delete_vm_snapshot_by_name_rand1 "centos7-clone"
        ;;
      *)
        echo "do nothing"
        ;;
    esac
  else
    delete_vm_by_name_rand1 "centos7-clone"
  fi
}

main $@
