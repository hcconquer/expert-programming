#!/bin/bash

echo "start"

iscsiadm -m node -u
zbs-iscsi snapshot delete test 25 1
zbs-iscsi snapshot delete test 25 2
zbs-iscsi snapshot delete test 25 3
zbs-iscsi lun delete test 25
sleep 5
zbs-iscsi lun create test 25 25
sleep 10
iscsiadm -m node -l
fio --name test --filename=/dev/sdf --size=20g --bssplit=8k/50:1024k/50 --rw=randrw --ioengine=libaio --direct=1 --iodepth=128 --numjobs=1 --runtime=300 --time_based &
fio --name test --filename=/dev/sdf --size=20g --bssplit=256k --rw=read --ioengine=libaio --direct=1 --iodepth=128 --numjobs=1 --runtime=300 --time_based &
sleep 60
zbs-iscsi snapshot create test 25 1
sleep 60
zbs-iscsi snapshot create test 25 2
sleep 60
zbs-iscsi snapshot create test 25 3

for i in `jobs -p`
do
    wait $i
done

echo "end"
