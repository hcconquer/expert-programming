#!/bin/sh

esxcfg-advcfg -s 30 /NFS/HeartbeatTimeout
esxcfg-advcfg -s 64 /NFS/MaxVolumes
esxcfg-advcfg -s 0 /Misc/APDHandlingEnable
esxcfg-advcfg -s 32 /Net/TcpipHeapSize
esxcfg-advcfg -s 512 /Net/TcpipHeapMax
esxcfg-advcfg -s 1 /UserVars/SuppressShellWarning
