#!/bin/sh

VMK0_IPADDR=$(esxcli network ip interface ipv4 get | grep vmk0 | awk '{print $2}')
VMK1_IPADDR=$(echo ${VMK0_IPADDR} | awk '{print 10"."10"."$3"."$4}' FS=.)
VMK2_IPADDR=192.168.33.1
esxcli network vswitch standard add --ports 128 --vswitch-name SmartXZBS
esxcli network vswitch standard uplink add --uplink-name vmnic2 --vswitch-name SmartXZBS
esxcli network vswitch standard portgroup add --portgroup-name ZBS --vswitch-name SmartXZBS
esxcli network vswitch standard portgroup add --portgroup-name vMotionSmartX --vswitch-name SmartXZBS
esxcli network ip interface add --interface-name vmk1  --portgroup-name vMotionSmartX
esxcli network ip interface ipv4 set --interface-name vmk1 --ipv4 ${VMK1_IPADDR} --netmask 255.255.255.0 --type static
esxcli network ip interface tag add -i vmk1 -t VMotion
esxcli network vswitch standard add --ports 128 --vswitch-name SmartXNFS
esxcli network vswitch standard portgroup add --portgroup-name NFS --vswitch-name SmartXNFS
esxcli network vswitch standard portgroup add --portgroup-name vmk-NFS --vswitch-name SmartXNFS
esxcli network ip interface add --interface-name vmk2  --portgroup-name vmk-NFS
esxcli network ip interface ipv4 set --interface-name vmk2 --ipv4 ${VMK2_IPADDR} --netmask 255.255.255.0 --type static
