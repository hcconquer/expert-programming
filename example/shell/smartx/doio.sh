#!/bin/bash
#
# do fio if no running

get_proc_num() {
  local ps="ps -ef | grep fio | grep -v grep | wc -l"
  local num=$(eval ${ps})
  echo "${num}"
}

do_fio() {
  local num=$(get_proc_num)
  if [ ${num} -ge 1 ]; then
     echo "been has ${num} fio running"
     return
  fi
  fio /root/fio.conf
}

echo "date: $(date)"
do_fio
