finish() {
	echo "exiting"
	iptables -F 
	echo "exited"
	exit
}
trap finish SIGINT 
while true;do
	echo "dropping ..."
	iptables -I INPUT -p tcp --dport 10201 -j DROP
	iptables -I OUTPUT -p tcp --dport 10201 -j DROP
	sleep 30
	cat /var/log/zbs/zbs-chunkd.INFO | grep -oP "Connection still has in progress tasks, id: \K(\d+)" | sort -n | uniq -c
	echo "receiving ..."
	iptables -D INPUT -p tcp --dport 10201 -j DROP
	iptables -D OUTPUT -p tcp --dport 10201 -j DROP
	sleep 30
	cat /var/log/zbs/zbs-chunkd.INFO | grep -oP "Connection still has in progress tasks, id: \K(\d+)" | sort -n | uniq -c
done
