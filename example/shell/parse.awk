#!/bin/awk -f
#      Author: chenhan
# Create-Time: 20120308
# 
# for parse tcpdump and logbuf

function trim(str)
{
    sub(/^[ \t]+/, "", str);
    sub(/[ \t]+$/, "", str);
    return str;
}
function ctos(buf,off)
{
    return lshift(and(buf[off],0xff),8)+and(buf[off+1],0xff);
}
function ctol(buf,off)
{
    return lshift(and(buf[off],0xff),24)+lshift(and(buf[off+1],0xff),16)+lshift(and(buf[off+2],0xff),8)+and(buf[off+3],0xff);
}
function strtobuf(buf,str,num,i)
{
    num=(length(str)+1)/3;
    for(i=0;i<num;i++)
    {
        buf[i]=strtonum("0x" substr(str,i*3+1,2));
    }
    return num;
}
function printbuf(buf,off,len,fmt,max)
{
    #num=(length(str)+1)/3;
    max=off+len;
    for(i=off;i<max;i++)
    {
        if(i<max-1)
        {
            printf("%02x ",buf[i]);
        }else
        {
            printf("%02x",buf[i]);
        }
    }
    printf("\n");
}
function chksum_ip(buf,off,len,cs,sum,num,max,i)
{
    if(len<20)
    {
        return -2;
    }
    for(i=0;i<20;i=i+2)
    {
        if(i!=10&&i!=11)
        {
            sum=sum+ctos(buf,off+i);
        }
    }
#    printf("ip sum = %x\n", sum);
    while(sum>0)
    {
        cs=cs+and(sum,0xffff);
        sum=rshift(sum,16);
    }
    cs=0xffff-cs;
#sum=ctos(buf,10);
#    if(cs!=sum)
#    {
#        printf("chksum_ip fail, %x!=%x\n", cs, sum);
#        return -4;
#    }
#printf("ip cs = %x,%x\n", _cs, 0xffff-_cs);
    return cs;
}
function getiplen(buf,off,len)
{
    return ctos(buf,off+2);
}
function getipdatalen(buf,off,len)
{
    return getiplen(buf,off,len)-20;
}
function chkip(buf,off,len,ret)
{
    if(buf[off]!=0x45)
    {
        return -2;
    }
    #if((length(buf)+1)/3!=getiplen(buf,len))
    if(len!=getiplen(buf,off,len))
    {
#printf("%d!=%d\n",len,getiplen(buf,off,len));
        return -3;
    }
    ret=chksum_ip(buf,off,len);
    if(ret<0)
    {
        printf("chksum_ip fail = %d\n", ret);
    }

    return 0;
}
function chksum_udp(buf,off,len,cs,i,sum,v)
{
    for(i=12;i<20;i=i+2) # source address and dest address
    {
        v=ctos(buf,off+i);
        sum=sum+v;
    }
    sum=sum+strtonum("0x" "00" "11"); # zero and protocol, udp = 17
    sum=sum+(len-20); # TCP/UDP length
    for(i=20;i<26;i=i+2) # source port and dest port and udp length
    {
        v=ctos(buf,off+i);
        sum=sum+v;
    }
    for(i=28;i<len-1;i=i+2) # udp data
    {
        v=ctos(buf,off+i);
        sum=sum+v;
    }
    if(i!=len)
    {
        sum=sum+buf[len-1];
    }
    while(sum>0)
    {
        cs=cs+and(sum,0xffff);
        sum=rshift(sum,16);
    }
    cs=0xffff-cs;
    return cs;
}
function getudplen(buf,off,len)
{
    return ctos(buf,off+24);
}
function getudpdatalen(buf)
{
    return getudplen(buf)-8;
}
function getudpchksum(buf,off,len)
{
    return ctos(buf,off+26);
}
function chkudp(buf,off,len,ulen,ret)
{
    ret=chkip(buf,off,len);
    if(ret!=0)
    {
#printf("chkip fail = %d\n", ret);
        return -2;
    }
    #num=(length(str)+1)/3;
    #len=strtonum("0x" substr(str,24*3+1,2) substr(str,25*3+1,2));
    ulen=getudplen(buf,off,len);
    if(ulen+20!=len) # udp len add ip head equal to packet len
    {
        printf("udp len = %d, packet = %d\n",ulen,len);
        return -5;
    }
    printf("UDP, checksum %04x %04x\n",chksum_udp(buf,0,len),getudpchksum(buf,0,len));
    return 0;
}
BEGIN{
};
{
    if($1~/^[[:space:]]*0x[0-9a-fA-F]*:?$/ || $1~/^[0-9a-fA-F]*:?$/)
    {
        for(i=1;i<=NF;i++)
        {
            s="";
            n=0;
            for(j=i;n<16;j++)
            {
                if($j~/^[0-9a-fA-F]*:?$/)
                {
                    if(length($j)==2) {
                        s=s" "substr($j,1,2);
                        n=n+1;
                    }else if(length($j)==4){
                        s=s" "substr($j,1,2)" "substr($j,3,2);
                        n=n+2;
                    }else{
                        break;
                    }
                }
                else
                {
                    if(n!=0)
                    {
                        break;
                    } else
                    {
                    }
                }
            }
            if(j!=i&&n>0)
            {
                str=trim(str" "trim(s));
                num=num+n;
                break; 
            } 
        }
    }
    else
    {
    }
}
END{
    len=strtobuf(buf,str);
    if(chkudp(buf,0,len)==0)
    {
        printf("UDP, length %d\n", getudpdatalen(buf));
        #print substr(str,28*3+1);
        printbuf(buf,28,len-28);
    } 
    else
    {
        printf("length %d\n", len);
        printbuf(buf,0,len);
    }
}
