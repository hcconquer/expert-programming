#!/bin/sh

#for n in `seq 1`
while : do
    rand=$(od -An -tu -N2 -i /dev/random)
    size=$(echo ${rand} | awk '{print $3}')
    size=$((size/10))
    name=${size}k.sf
    dd if=/dev/zero of=${name} bs=1k count=${size} > /dev/null 2>&1
    md5name=$(md5sum ${name} | awk '{print $2}')
    echo "name: ${name}, md5name: ${md5name}"
    if [ "${name}" != "${md5name}" ]; then
        echo "Read file ${name} fail!"
    fi
    rm -f "${name}"
done
