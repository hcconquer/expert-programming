#!bin/bash

function restart_network() {
  systemctl restart network
}

function get_partition_table() {
  lsblk
}

function get_partition_type() {
  df -hT
}

function get_partition_info() {
  local disk=$1  # /dev/xxx
  sgdisk -p ${disk}
}

function mk_partition() {
  local no=$1
  local begin=$2
  local end=$3
  local type=$4  # e.g. 8300
  local name=$5
  local disk=$6  # e.g. /dev/xxx
  sgdisk -n ${no}:${begin}:${end} -t ${no}:${type} -c ${no}:${name} -g -p ${disk}
}

function mk_rand_file() {
  local dst_file=$1
  local size=$2  # nMB
  dd if=/dev/urandom of=${dst_file} bs=1M count=${size}
}

function qperf::tcp() {
  local ip=$1
  qperf -v ${ip} tcp_bw tcp_lat
}

function qperf::udp() {
  local ip=$1
  qperf -v ${ip} udp_bw udp_lat
}

# https://iperf.fr/
funtion iperf::tcp_server() {
  # -s, --server             run in server mode
  iperf -s
}

funtion iperf::tcp() {
  local ip=$1
  iperf -c ${ip}
}

funtion iperf::udp_server() {
  # -u, --udp                use UDP rather than TCP
  iperf -s -u
}

funtion iperf::udp() {
  local ip=$1
  iperf -c ${ip} -u
}

function rsync_put() {
  local src_file=$1
  local dst_user=$2
  local dst_host=$3
  local dst_path=$4
  rsync -avzP ${src_file} ${dst_user}@${dst_host}:${dst_path}
}

function nfs_cat_exports() {
  cat /etc/exports
}

function nfs_show_mount() {
  local ip=$1
  showmount -e ${ip}
}

function nfs_mount() {
  local ip=$1
  local nfs_dir=$2
  local local_dir=$3
  sudo mount -t nfs -o resvport ${ip}:${nfs_dir} ${local_dir}
}

function wget_get() {
  local url=$1
  # -d --debug Turn on debug output
  # -r --recursive Turn on recursive retrieving. The default maximum depth is 5.
  # -c --continue Continue getting a partially-downloaded file.
  wget -dc ${url}
}

function rpc_service_list() {
  local ip=${1:-"127.0.0.1"}
  rpcinfo -p ${ip}
}

function ipatbles_save() {
  iptables-save > /etc/sysconfig/iptables
}

function ipatbles_reload() {
  iptables-restore < /etc/sysconfig/iptables
}

function firewalld_stop() {
  systemctl stop firewalld
}

# will not run when system restart
function firewalld_disable() {
  systemctl disable firewalld
}

function git::prune() {
  # $() will create a new process
  for d in $(ls); do $(cd ${d}; git gc --prune=now;); done
}

# show pub ip and location
function network::show_pub_ip() {
  curl ip.gs
}

function shell::set_proxy() {
  export ALL_PROXY=socks5://127.0.0.1:1086
}