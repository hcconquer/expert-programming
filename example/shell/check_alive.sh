#!/bin/bash

function strace_check() {
    local dump_dir="$1"
    local pid="$2"
    local dump_file="${dump_dir}/${pid}"
    mkdir -p ${dump_dir}
    timeout 3 strace -p ${pid} -o ${dump_file}
    local lines=$(cat ${dump_file} | grep "accept" | wc -l)
    if [ ${lines} -eq 0 ]; then
        lines=$(cat ${dump_file} | wc -l)
        if [ ${lines} -lt 3 ]; then
            echo "lines: ${lines}, need to kill"
            kill ${pid}
        fi
    fi
}

function check_alive() {
    local name=$1
    ps -ef | grep "${name}" | grep -v "grep" | awk '{print $2}' | while read pid; do
        strace_check "/tmp/${name}" "${pid}"
    done
}

main() {
    local num=0
    while [ true ]; do
        # master name is 'timg-bce', worker name is 'timg-slave'
        num=$(pgrep "timg-bce" | wc -l)
        if [ $num -eq 0 ]; then
            break
        fi
        check_alive "timg-slave"
    done
}

main $@
