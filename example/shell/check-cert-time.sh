#!/bin/sh

check() {
    local crt_file="$1"
    local chk_time="$2"
    local expiry=$(openssl x509 -in ${crt_file} -text -noout 2>/dev/null | grep -A 2 "Validity" | grep "Not After"| sed -e 's/^[ \t]*//')
    if [ -z "${expiry}" ]; then
        return 0
    fi
    local exp_date=$(echo ${expiry} | cut -d ':' -f 2- | sed -e 's/^[ \t]*//')
    local exp_time=""
    if [ "$(date +%N)" = "N" ]; then
        # macOS
        exp_time=$(date -j -f "%b %d %H:%M:%S %Y GMT" "${exp_date}" "+%s")
    else
        # linux
        exp_time=$(date -d "${exp_date}" "+%s")
    fi
    local diff_time=$((exp_time-chk_time))
    local message="Info"
    if [ ${diff_time} -le 0 ]; then
        message="Warn"
    fi
    echo "[${message}] [crt_file=${crt_file}] [expiry=${expiry}] [exp_date=$exp_date] [exp_time=${exp_time}] [chk_time=${chk_time}] [diff_time=${diff_time}]"
}

main() {
    local chk_time="$1"
    if [ -z "${chk_time}" ]; then
        chk_time=$(date "+%s")
    fi
    chk_time=$((chk_time+30*24*60*60))
    find . -name "*pem" | sort | while read line; do
        check ${line} ${chk_time}
    done
}

main $@
