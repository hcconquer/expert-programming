#===============================================================
# gear.sh
# Created: 2010-09-14
# Modify : 2017-08-21
# Version: 11038
#  Author: chenhan
#===============================================================

__GEAR_SH="GEAR_SH"

# add some path
export PATH=$PATH:/usr/bin:/usr/local/bin

# int import(file)
# 0, succ
# 1, fail
# function import()
import() {
    local file=${1:?"no file"}

    file=$(dirname $0)/$file
    if [ -f $file ] && [ -r $file ]; then
        . ${file}
    fi
}

# function log(str)
log(str) {
    local log_str="$1"
    local now_time="$(date '+%Y-%m-%d %H:%M:%S')"
    echo "${now_time}: ${log_str}"
}

# rm no ref shm
rm_no_ref_shm() {
    local os_type=$(get_os_type)
    if [ ${os_type} -eq ${OS_LINUX}; then
        ipcs -m | awk '{if($1 ~ /0x/){print "ipcrm -M "$1}}' | sh
    elif [ ${os_type} -eq ${OS_DARWIN}; then
        ipcs -m | awk '{if($3 ~ /0x/){print "ipcrm -M "$3}}' | sh
    fi	
}

# get_rand_num(min=0, max=2)
# include min and exclude max
# so if no params will return [0, 2)
function get_rand_num() {
    local min=${1:-0}
    local max=${2:-2}
    # /dev/random is used interrupts make source
    # /dev/urandom is PRNG – Pseudo Random Number Generator
    local dev=${3:-"urandom"}
    # -N4 is bytes num
    # -t uI is unsigned
    local randnum=$(($(od -An -N4 -t uI /dev/${dev})%(max-min)+min))
    echo "${randnum}"
}

get_ip_addrs() {
        echo $(/sbin/ifconfig -a | grep inet | grep -v 127.0.0.1 | grep -v inet6 | 
                     awk '{print $2}' | tr -d "addr:")
}

# int ftp_put_file(host, port, user, passwd, src, dst)
# deprecated, replace by ssh
# 0, succ
# 1, fail
# function ftp_put_file() 
ftp_put_file() {
    if [ $# -lt 6 ]; then
        echo "invalid params"
        return 1
    fi

    local host=$1
    local port=$2
    local user=$3
    local passwd=$4
    local src=$5
    local dst=$6

    {
        echo user $user $passwd
        echo bin
        echo put $src $dst
        echo bye
    } | ftp -in $host $port

    return 0
}

# int ftp_get_file(host, port, user, passwd, src, dst)
# deprecated, replace by ssh
# 0, succ
# 1, fail
# function ftp_get_file() 
ftp_get_file() {
    if [ $# -lt 6 ]; then
        echo "invalid params"
        return 1
    fi

    local host=$1
    local port=$2
    local user=$3
    local passwd=$4
    local src=$5
    local dst=$6

    {
        echo user $user $passwd
        echo bin
        echo get $src $dst
        echo bye
    } | ftp -in $host $port

    return 0
}

# int ssh_put(host, port, user, passwd, src, dst, [limit = 10], [timeout = 86400])
# may be you can put file or get file
# limit, default 10M
# 0, succ
# 1, fail
ssh_put_file()
{
    if [ $# -lt 6 ]; then
        echo "invalid params"
    fi

    local host=$1
    local port=$2
    local user=$3
    local passwd=$4
    local src=$5
    local dst=$6
    local limit=$((${7:-10} * 1000)) # default 10M/s
    local timeout=$((${8:-86400})) # default 1 day

    # set timeout to 1 day that enought to send any file
    expect -c "
        set timeout $timeout;
        set flag 0;
        spawn scp -qr -P $port $src $user@$host:$dst;
        expect {
            \"*assword\" { 
                send $passwd\r; 
            }
            \"yes\/no)?\" { 
                set flag 1; 
                send yes\r;
            }
            eof { 
                exit 0; 
            }
        }
        if { \$flag == 1 } {
            expect {
                \"*assword\" { 
                    send $passwd\r; 
                }
            }
        }
        expect {
            \"*assword*\" { 
                puts \"INVALID PASSWD, host = $host, user = $user, passwd = \'$passwd\'\";
                exit 1
            }
            eof {
                exit 0
            }
        }
    "
    ret=$?
    if [ $ret -ne 0 ]; then
        return 1
    fi

    return 0
}

# int ssh_get(host, port, user, passwd, src, dst, [limit = 100M], [timeout = 86400])
# may be you can put file or get file
# limit, default 10M
# 0, succ
# 1, fail
# function ssh_get()
ssh_get()
{
    if [ $# -lt 6 ]; then
        echo "invalid params"
        return 1
    fi

    local host=$1
    local port=$2
    local user=$3
    local passwd=$4
    local src=$5
    local dst=$6
    local limit=$((${7:-100} * 1000)) # default 100M/s
    local timeout=$((${8:-86400})) # default 1 day

    # set timeout to 1 day that enought to send any file
    # may be not need passwd
    expect -c "
        set timeout $timeout;
        set flag 0;
        spawn scp -qr -P $port $user@$host:$src $dst;
        expect {
            \"*assword\" { 
                send $passwd\r; 
            }
            \"yes\/no)?\" { 
                set flag 1; 
                send yes\r;
            }
            eof { 
                exit 0; 
            }
        }
        if { \$flag == 1 } {
            expect {
                \"*assword\" { 
                    send $passwd\r; 
                }
            }
        }
        expect {
            \"*assword*\" { 
            puts \"INVALID PASSWD, host = $host, user = $user, passwd = $passwd\";
                exit 1
            }
            eof {
                exit 0
            }
        }
    "
    ret=$?
    if [ $ret -ne 0 ]; then
        return 5
    fi

    return 0
}

# int ssh_put(host, port, user, passwd, src, dst, [limit = 10], [timeout = 86400])
# may be you can put file or get file
# limit, default 10M
# 0, succ
# 1, fail
# function rsync_put()
rsync_put()
{
    if [ $# -lt 6 ]; then
        echo "invalid params"
    fi

    local host=$1
    local port=$2
    local user=$3
    local passwd=$4
    local src=$5
    local dst=$6
    local limit=$((${7:-10} * 1000)) # default 10M/s
    local timeout=$((${8:-86400})) # default 1 day

    # set timeout to 1 day that enought to send any file
    expect -c "
        set timeout $timeout;
        set flag 0
        # -av --progress, show progress
        spawn rsync -azq -e \"ssh -p $port\" --bwlimit=$limit --partial $src $user@$host:$dst; 
        expect {
            \"*assword\" { 
                send $passwd\r; 
            }
            \"yes\/no)?\" { 
                set flag 1; 
                send yes\r;
            }
            eof { 
                exit 0; 
            }
        }
        if { \$flag == 1 } {
            expect {
                \"*assword\" { 
                    send $passwd\r; 
                }
            }
        }
        expect {
            \"*assword*\" { 
                puts \"INVALID PASSWD, host = $host, user = $user, passwd = \'$passwd\'\";
                exit 1
            }
            eof {
                exit 0
            }
        }
    "	
    ret=$?
    if [ $ret -ne 0 ]; then
        return 5
    fi

    return 0
}

# int rsync_get(host, port, user, passwd, src, dst, [limit = 100M], [timeout = 86400])
# may be you can put file or get file
# limit, default 10M
# 0, succ
# 1, fail
# function rsync_get()
rsync_get()
{
    if [ $# -lt 6 ]; then
        echo "invalid params"
        return 1
    fi

    local host=$1
    local port=$2
    local user=$3
    local passwd=$4
    local src=$5
    local dst=$6
    local limit=$((${7:-100} * 1000)) # default 100M/s
    local timeout=$((${8:-86400})) # default 1 day

    # set timeout to 1 day that enought to send any file
    # may be not need passwd
    expect -c "
        set timeout $timeout;
        set flag 0
        spawn rsync -azq -e \"ssh -p $port\" --bwlimit=$limit $user@$host:$src $dst; 
        expect {
            \"*assword\" { 
                send $passwd\r; 
            }
            \"yes\/no)?\" { 
                set flag 1; 
                send yes\r;
            }
            eof { 
                exit 0; 
            }
        }
        if { \$flag == 1 } {
            expect {
                \"*assword\" { 
                    send $passwd\r; 
                }
            }
        }
        expect {
            \"*assword*\" { 
                puts \"INVALID PASSWD, host = $host, user = $user, passwd = $passwd\";
                exit 1
            }
            eof {
                exit 0
            }
        }
    "
    ret=$?
    if [ $ret -ne 0 ]; then
        return 1
    fi

    return 0
}

# int exec_script(host, port, user, passwd, script, [timeout = 86400])
# 0, succ
# 1, fail
# 2, invalid params
# 3, fail
# 4, invalid script
# 5, login fail
# 6, put file fail
# 7, timeout
# 8, remote execute script fail
# function exec_script()
exec_script()
{
    if [ $# -lt 5 ]; then
        return 2
    fi

    local host=$1
    local port=$2
    local user=$3
    local passwd=$4
    local script=$5
    local timeout=$((${6:-86400})) # default 1 day
    local rscp=$(make_rand_file $(basename $script))
    local ret=0

    if [ ! -f $script ]; then
        return 4
    fi

    echo "script = ${script}, remote_script = ${rscp}"
    ssh_put $host $port $user $passwd $script $rscp 

    expect -c "
        set timeout $timeout
        set flag 0
        spawn ssh -p$port $user@$host;
        expect {
            \"*assword*\" { send $passwd\r; } 
            \"yes\/no)?\" { 
                set flag 1; 
                send yes\r;
            }
            \"Welcome\" { }
        }
        if { \$flag == 1 } {
            expect {
                \"*assword\" { 
                    send $passwd\r; 
                }
            }
        }
        expect {
            \"*assword*\" { 
                puts \"INVALID PASSWD, host = $host, user = $user, passwd = $passwd\";
                exit 1 
            }
            \"#\ \" {} \"$\ \" {} \">\ \" {}
        }
        send $rscp\r;
        expect {
            \"#\ \" {} \"$\ \" {} \">\ \" {}
        }
        send \"rm -f $rscp\r\"
        send exit\r;
        expect eof {
            exit 0
        }
    "
    ret=$?
    if [ $ret -ne 0 ]; then
        return 1
    fi

    return 0
}

# int exec_cmd(host, port, user, passwd, cmd, [timeout = 86400], [rfile = /dev/null])
# 0, succ
# 1, fail
# 2, invalid params
# 3, fail
# 4, invalid script
# 5, login fail
# 6, put file fail
# 7, timeout
# 8, remote execute script fail
# function exec_cmd()
exec_cmd()
{
    if [ $# -lt 5 ]; then
        return 4
    fi

    local host=$1
    local port=$2
    local user=$3
    local passwd=$4
    local cmd=$5
    local timeout=$((${6:-86400})) # default 1 day
    local rfile=${7-"/dev/null"}
    local ret=0

    expect -c "
        set timeout $timeout
        set flag 0
        spawn ssh -p$port $user@$host;
        expect {
            \"*assword*\" { send $passwd\r; } 
            \"yes\/no)?\" { 
                set flag 1; 
                send yes\r;
            }
            \"Welcome\" { }
        }
        if { \$flag == 1 } {
            expect {
                \"*assword\" { 
                    send $passwd\r; 
                }
            
        }
        expect {
            \"*assword*\" { 
                puts \"INVALID PASSWD, host = $host, user = $user, passwd = $passwd\";
                exit 1 
            }
            \"#\ \" {} \"$\ \" {} \">\ \" {}
        }
        log_file ${rfile}
        send \"$cmd \r\";
        expect {
            \"#\ \" {} \"$\ \" {} \">\ \" {}
        }
        send exit\r;
        expect eof {
            exit 0
        }
    "
    cat ${rfile} | sed -n '1,/[#$>].*exit/p' | sed '1d;$d' > ${rfile}
    ret=$?
    if [ $ret -ne 0 ]; then
        return 5
    fi

    return 0
}
