# quick start

## Download

instantclient-basic-macos.x64-11.2.0.3.0.zip
instantclient-sqlplus-macos.x64-11.2.0.3.0.zip
instantclient-sdk-macos.x64-11.2.0.3.0.zip

## install

```shell
# move to lib
# mv all to /usr/local/lib/instantclient_11_2

# ld
ln -s libclntsh.dylib.11.1 libclntsh.dylib

# check ld
pkg-config --list-all | grep oci8

# Install go-oci
go get github.com/mattn/go-oci8
```
