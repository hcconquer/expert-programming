#!/bin/sh

print_usage()
{
    printf "Usage: %s [OPTION]...\n" "$1"
    printf "  %3s %-10s %s\n" "-n,"  "--length"    "password length" 
}

# string mkpasswd(len)
mkpasswd()
{
    local len=$1

    local dict=()
    local dictlen=0
    local use=0
    for i in {0..9} {a..z} {A..Z}
    do
        use=1
        for j in 0 o O 1 l
        do
            if [ "${i}" == "${j}" ]
            then
                use=0
            fi
        done
        if [ "${use}" != "0" ]
        then
            dict[${#dict[@]}]=${i}
        fi
    done
    dictlen=${#dict[@]}
    # echo ${dictlen}
    # echo ${dict[*]}

    passwd=""
    for ((i=0;i<${len};i++))
    do
        rand=$RANDOM
        x=$((${rand}%${dictlen}))
        # c=${dict[@]::${x}}
        passwd=${passwd}${dict[$x]}
        # echo $i $x $c $passwd
    done

    echo ${passwd}
}

len=6

# echo $*
while getopts ":n:" opt
do
    case ${opt} in
        n)  # echo ${OPTARG}
            # echo ${OPTIND}
            len=${OPTARG}
            ;;
        \?) echo "invalid param ${OPTARG}"
            print_usage $0
            exit 1
            ;;
    esac
done

mkpasswd ${len}

