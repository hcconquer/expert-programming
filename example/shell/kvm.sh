#!bin/bash

function get_all_vms() {
  virsh list
}

function set_vnc_port() {
  local name=$1
  # graphics
  virsh edit ${name}
}
