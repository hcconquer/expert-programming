#!bin/bash

function xfs::print_info() {
  local disk=$1
  {
    echo "sb 0"
    echo "print"
    echo "quit"
  } | xfs_db ${disk}
}

function xfs::print_blocksize() {
  $(xfs::print_info | grep "blocksize")
}

function xfs::print_info_2() {
  local disk=$1
  { echo "sb 0"; echo "print"; echo "quit"; } | xfs_db ${disk}
}

function xfs::print_info_2() {
  local disk=$1
  echo -e "sb 0\nprint\nquit\n" | xfs_db ${disk}
}
