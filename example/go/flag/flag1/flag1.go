package main

import (
	"flag"
	"log"
)

var (
	help = flag.Bool("h", false, "help")
	cmd  = flag.String("cmd", "", "command")
	num  = flag.Int("num", 0, "number")
)

// flag param will never be nil
func main() {
	flag.Parse()

	if help == nil {
		log.Fatalf("help is nil")
	} else {
		log.Printf("%v, %v", help, *help)
	}

	if cmd == nil {
		log.Fatalf("cmd is nil")
	} else {
		log.Printf("%v, %v", cmd, *cmd)
	}

	if num == nil {
		log.Fatalf("num is nil")
	} else {
		log.Printf("%v, %v", num, *num)
	}
}
