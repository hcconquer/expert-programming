package main

var ins1 *instance

func GetInstanceLazy() *instance {
	if ins1 == nil {
		ins1 = &instance{} // not thread safe
	}
	return ins1
}
