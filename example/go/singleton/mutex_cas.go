package main

import "sync/atomic"

var ins3 *instance
var initialized uint32

func GetInstanceMutexCas() *instance {
	if atomic.LoadUint32(&initialized) == 1 {
		return ins3
	}

	mu.Lock()
	defer mu.Unlock()

	if initialized == 0 {
		ins3 = &instance{}
		atomic.StoreUint32(&initialized, 1)
	}

	return ins3
}
