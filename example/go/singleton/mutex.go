package main

import "sync"

var ins2 *instance
var mu sync.Mutex

func GetInstanceMutex() *instance {
	mu.Lock()
	defer mu.Unlock()

	if ins2 == nil {
		ins2 = &instance{}
	}
	return ins2
}
