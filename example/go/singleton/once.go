package main

import "sync"

var ins4 *instance
var once sync.Once

func GetInstanceOnce() *instance {
	once.Do(func() {
		ins4 = &instance{}
	})
	return ins4
}
