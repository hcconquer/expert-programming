package main

import "testing"

// BenchmarkGetInstanceLazy-4      2000000000               0.49 ns/op
// BenchmarkGetInstanceMutex-4     30000000                 54.3 ns/op
// BenchmarkGetInstanceCas-4       300000000                5.21 ns/op
// BenchmarkGetInstanceOnce-4      200000000                6.21 ns/op

func BenchmarkGetInstanceLazy(b *testing.B) {
	for i := 0; i < b.N; i++ {
		GetInstanceLazy()
	}
}

func BenchmarkGetInstanceMutex(b *testing.B) {
	for i := 0; i < b.N; i++ {
		GetInstanceMutex()
	}
}

func BenchmarkGetInstanceMutexCas(b *testing.B) {
	for i := 0; i < b.N; i++ {
		GetInstanceMutexCas()
	}
}

func BenchmarkGetInstanceOnce(b *testing.B) {
	for i := 0; i < b.N; i++ {
		GetInstanceOnce()
	}
}
