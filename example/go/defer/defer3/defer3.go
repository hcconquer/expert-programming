package main

import (
	"log"
)

type Person struct {
	name string
}

func (person *Person) Name() {
	log.Printf("name: %s", person.name)
}

func dosth() {
	person := &Person{name: "tom"}
	defer person.Name()
	person = &Person{name: "jack"}
	defer person.Name()
	person = &Person{name: "jerry"}
	defer person.Name()
}

func main() {
	dosth()
}
