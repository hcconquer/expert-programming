package main

import (
	"fmt"
)

/*
 * return 3
 * defer run after set return value and can modify named return result
 */
func f1() (r int) {
	defer func() {
		r = 3
	}()
	return 2
}

/*
 * return 2
 * defer run after set return value
 */
func f2() (int) {
	t := 2
	defer func() {
		t = 3
	}()
	return t
}

/*
 * local param override
 */
func f3() (r int) {
	defer func(r int) {
		r = 3
	}(r)
	return 2
}

/*
 * order:
 * f4-2: 3
 * f4-1: 2
 * 
 * defer will setup params
 */
func f4() (r int) {
	n := 2
	defer fmt.Printf("f4-1: %d\n", n) // 2
	n = 3
	defer func(v int) {
		fmt.Printf("f4-2: %d\n", v) // 3
	}(n)
	n = 4
	return n
}

type count struct {
	value int
}

/*
 * defer will setup params but pointer can change the value
 */
func f5() int {
	pst := new(count)
	pst.value = 2
	defer fmt.Printf("f5-1: %d\n", pst.value) // 2
	pst.value = 3
	defer func(pst *count) {
		defer fmt.Printf("f5-2: %d\n", pst.value) // 4
	}(pst)
	pst.value = 4
	defer func() {
		defer fmt.Printf("f5-3: %d\n", pst.value) // 4
	}()
	return pst.value
}

func main() {
	fmt.Printf("f1: %d\n", f1()) // 3
	fmt.Printf("f2: %d\n", f2()) // 2
	fmt.Printf("f3: %d\n", f3()) // 2
	fmt.Printf("f4: %d\n", f4()) // 4
	fmt.Printf("f5: %d\n", f5()) // 4
}
