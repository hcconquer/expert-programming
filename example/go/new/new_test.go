package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewInt(t *testing.T) {
	pi := new(int)
	assert.NotNil(t, pi)
	assert.Equal(t, 0, *pi)
}
