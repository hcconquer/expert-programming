package client

import (
	"context"
	"io"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"syscall"
	"time"
)

func Do(client *http.Client, req *http.Request) (*http.Response, []byte, error) {
	resp, err := client.Do(req)
	if err != nil {
		log.Printf("GET fail, error: %s", err.Error())
		return nil, nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("read body fail, error: %s", err.Error())
		return resp, nil, err
	}

	return resp, body, nil
}

func Get(client *http.Client, rawurl string) (*http.Response, []byte, error) {
	req, err := http.NewRequest("GET", rawurl, nil)
	if err != nil {
		return nil, nil, err
	}
	return Do(client, req)
}

func Post(client *http.Client, rawurl string, contentType string, body io.Reader) (*http.Response, []byte, error) {
	req, err := http.NewRequest("POST", rawurl, body)
	if err != nil {
		return nil, nil, err
	}
	req.Header.Set("Content-Type", contentType)
	return Do(client, req)
}

func SetTimeout(client *http.Client, timeout int64) {
	client.Timeout = time.Duration(timeout) * time.Millisecond
}

// don't add argument `conn`, because may be hook in DialContext or Resolver in future
func SetAddrCheck(client *http.Client, check func(network, address string) error) {
	transport := &http.Transport{}
	transport.DialContext = func(ctx context.Context, network, address string) (net.Conn, error) {
		log.Printf("DialContext, network: %s, addr: %s", network, address)
		dialer := &net.Dialer{
			Control: func(network, address string, c syscall.RawConn) error {
				ret := check(network, address)
				log.Printf("Control, network: %s, address: %s, check: %v", network, address, ret)
				tcpAddr, err := net.ResolveTCPAddr(network, address)
				if err != nil {
					log.Printf("it's not a tcp addr")
				} else {
					log.Printf("tcp addr, ip: %s, port: %d, zone: %s", tcpAddr.IP.String(), tcpAddr.Port, tcpAddr.Zone)
				}
				return ret
			},
		}
		conn, err := dialer.DialContext(ctx, network, address)
		if err != nil {
			return nil, err
		}
		log.Printf("LocalAddr: %s, RemoteAddr: %s", conn.LocalAddr(), conn.RemoteAddr())
		return conn, err
	}
	client.Transport = transport
}
