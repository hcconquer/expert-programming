package client

import (
	"bytes"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGet(t *testing.T) {
	ts := httptest.NewServer(&MockHttpHandler{})
	defer ts.Close()

	var cases = []struct {
		rawurl string
		status int
	}{
		{fmt.Sprintf("%s%s", ts.URL, "/ok"), http.StatusOK},
	}

	for _, c := range cases {
		client := &http.Client{}
		_, body, err := Get(client, c.rawurl)
		assert.Nil(t, err)
		assert.Equal(t, http.StatusText(c.status), string(body))
	}
}

func TestPost(t *testing.T) {
	ts := httptest.NewServer(&MockHttpHandler{})
	defer ts.Close()

	var cases = []struct {
		rawurl      string
		body        []byte
		contentType string
	}{
		{fmt.Sprintf("%s%s", ts.URL, "/header?key=Content-Type"), []byte(`{name: tom}`), "application/json"},
	}
	for _, c := range cases {
		client := &http.Client{}
		resp, body, err := Post(client, c.rawurl, c.contentType, bytes.NewBuffer(c.body))
		assert.Nil(t, err)
		assert.NotNil(t, body)
		assert.Equal(t, c.contentType, resp.Header.Get(fmt.Sprintf("X-Request-Content-Type")))
	}
}

func TestGetWithHeader(t *testing.T) {
	ts := httptest.NewServer(&MockHttpHandler{})
	defer ts.Close()

	var cases = []struct {
		rawurl string
		key    string
		value  string
	}{
		{fmt.Sprintf("%s/header?key=%s", ts.URL, "name"), "name", "tom"},
	}

	for _, c := range cases {
		req, err := http.NewRequest("GET", c.rawurl, nil)
		req.Header.Add(c.key, c.value)
		client := &http.Client{}
		resp, _, err := Do(client, req)
		assert.Nil(t, err)
		assert.Equal(t, c.value, resp.Header.Get(fmt.Sprintf("X-Request-%s", c.key)))
	}
}

func TestSetTimeout(t *testing.T) {
	ts := httptest.NewServer(&MockHttpHandler{})
	defer ts.Close()

	var cases = []struct {
		rawurl  string
		timeout int64
		result  bool
	}{
		{fmt.Sprintf("%s/delay?time=%d", ts.URL, 1000), 100, false},
		{fmt.Sprintf("%s/delay?time=%d", ts.URL, 1000), 2000, true},
	}

	for _, c := range cases {
		client := &http.Client{}
		SetTimeout(client, c.timeout)
		_, _, err := Get(client, c.rawurl)
		if c.result {
			assert.Nil(t, err)
		} else {
			assert.NotNil(t, err)
			t.Logf("err: %+v", err)
		}
	}
}

func TestSetRedirectCheck(t *testing.T) {
	ts := httptest.NewServer(&MockHttpHandler{})
	defer ts.Close()

	address := ts.URL

	var cases = []struct {
		rawurl string
		num    int
		re     bool
		status int
	}{
		{fmt.Sprintf("%s%s", address, "/ok"), 1, true, http.StatusOK},
		{fmt.Sprintf("%s%s", address, "/ok"), 0, true, http.StatusOK},
		{fmt.Sprintf("%s%s", address, "/ok"), 0, false, http.StatusOK},
		{fmt.Sprintf("%s/redirect?target=%s%s", address, address, "/ok"), 1, true, http.StatusOK},
		{fmt.Sprintf("%s/redirect?target=%s%s", address, address, "/ok"), 0, true, http.StatusTemporaryRedirect},
		{fmt.Sprintf("%s/redirect?target=%s%s", address, address, "/ok"), 0, false, -1},
		{fmt.Sprintf("%s/redirect?target=%s%s", address, address, "/not-exist"), 1, true, http.StatusNotFound}, // follow status code
		{fmt.Sprintf("%s/redirect?target=%s%s", address, "http://must-be-not-exist.com", "/ok"), 1, true, -1},
	}
	for index, c := range cases {
		log.Printf("###################### case %d ######################", index)
		client := &http.Client{}
		client.CheckRedirect = func(req *http.Request, via []*http.Request) error {
			log.Printf("[check] via=%+v", func() string {
				var sb strings.Builder
				for index, r := range via {
					sb.WriteString(fmt.Sprintf("{%d,%s}", index, r.URL.String()))
				}
				return sb.String()
			}())
			// same with num_redirects in curl, return follow resp if out of times
			// golang defaultCheckRedirect is return fail after 10 times
			if c.num < len(via) {
				if !c.re {
					return fmt.Errorf("stopped after %d redirects", len(via))
				}
				return http.ErrUseLastResponse
			}
			return nil
		}
		resp, _, err := Get(client, c.rawurl)
		if c.status < 0 {
			assert.NotNilf(t, err, "case: %+v", c)
		} else {
			assert.Equalf(t, c.status, resp.StatusCode, "case: %+v", c)
		}
	}
}

func TestSetAddrCheck(t *testing.T) {
	ts1 := httptest.NewServer(&MockHttpHandler{})
	defer ts1.Close()

	ts2 := httptest.NewServer(&MockHttpHandler{})
	defer ts2.Close()

	var (
		ErrDeny = errors.New("deny")
	)

	checkAllow := func(network, addr string) error { return nil }
	checkDeny := func(network, addr string) error { return ErrDeny }
	check := func(network, addr string) error {
		u, _ := url.Parse(ts2.URL)
		if u != nil && addr == u.Host {
			return ErrDeny
		}
		return nil
	}

	var cases = []struct {
		rawurl string
		check  func(network, addr string) error
		status int
	}{
		{fmt.Sprintf("%s%s", ts1.URL, "/ok"), checkAllow, http.StatusOK},
		{fmt.Sprintf("%s%s", ts1.URL, "/ok"), checkDeny, -1},
		{fmt.Sprintf("%s%s?target=%s%s", ts1.URL, "/redirect", ts2.URL, "/ok"), checkAllow, http.StatusOK},
		{fmt.Sprintf("%s%s?target=%s%s", ts1.URL, "/redirect", ts2.URL, "/ok"), checkDeny, -1},
		{fmt.Sprintf("%s%s?target=%s%s", ts1.URL, "/redirect", ts2.URL, "/ok"), check, -1},
	}
	for _, c := range cases {
		client := &http.Client{}
		SetAddrCheck(client, c.check)
		resp, _, err := Get(client, c.rawurl)
		if c.status < 0 {
			assert.NotNilf(t, err, "case: %+v", c)
		} else {
			assert.Equalf(t, c.status, resp.StatusCode, "case: %+v", c)
		}
	}
}
