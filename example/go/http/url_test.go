package main

import (
	"net/url"
	"testing"
)

func TestUrlParse(t *testing.T) {
	var cases = []struct {
		url    string
		result bool
	}{
		{"http://localhost:8080/home", true},
		{"http://localhost:8080/home ", true},
		{"http://localhost:8080/ home", true},
		{"http://localhost: 8080/home", false},
		{"http://localhost:80 80/home", false},
		{"http://localhost:8080 /home", false},
	}

	for _, c := range cases {
		u, err := url.Parse(c.url)
		t.Logf("url: %s, err: %s", u, err)
		if c.result {
			if err != nil {
				t.Errorf("err: %s, c: %+v", err, c)
			}
		} else {
			if err == nil {
				t.Errorf("url is bad, url: %s, c: %+v", c.url, c)
			}
		}
	}
}
