package main

import (
	"fmt"
	"log"
	"net/http"
)

type helloHandler struct {
}

func (handler helloHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log.Printf("server http request, UserAgent: %s", r.UserAgent())
	fmt.Fprintf(w, "hello, you've hit %s\n", r.URL.Path)
}

func main4() {
	err := http.ListenAndServe(":8080", helloHandler{})
	log.Fatal(err)
}
