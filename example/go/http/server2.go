package main

import (
	"runtime"
	"context"
	"log"
	"net/http"
	"time"
)

type Handler struct {
}

func (handler *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log.Printf("handler start %p", &r)
	time.Sleep(5 * time.Second)
	log.Printf("handler done %p", &r)
}

func main8() {
	handler := new(Handler)
	srv := &http.Server{
		Addr:           "localhost:8080",
		Handler:        handler,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	// ref to: https://golang.org/pkg/net/http/#Server.Shutdown
	// chan unbuffered and close
	shutdown := make(chan int)

	go func() {
		time.Sleep(10 * time.Second) // shutdown in 10s later
		ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
		defer cancel()
		log.Printf("shutdown, gonum: %d", runtime.NumGoroutine())
		err := srv.Shutdown(ctx)
		if err != nil {
			log.Printf("shutdown err: %v", err)
		}
		close(shutdown)
	}()

	log.Printf("serve start, gonum: %d", runtime.NumGoroutine())
	err := srv.ListenAndServe()
	if err != nil && err != http.ErrServerClosed {
		log.Printf("serve exit err: %s", err)
	}
	<-shutdown
	log.Printf("exit, gonum: %d", runtime.NumGoroutine())
}
