package client

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"
)

type MockHttpHandler struct {
}

func (handler *MockHttpHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log.Printf("path: %s, querys: %+v", r.URL.Path, r.URL.Query())
	switch r.URL.Path {
	case "/ok":
		w.WriteHeader(http.StatusOK)
		fmt.Fprintf(w, http.StatusText(http.StatusOK))
	case "/header":
		key := r.URL.Query().Get("key")
		value := r.Header.Get(key)
		// notice the order, Header().Set() should above WriteHeader(), or the 
		// header will not return
		w.Header().Set(fmt.Sprintf("X-Request-%s", key), value)
		w.WriteHeader(http.StatusOK)
		fmt.Fprintf(w, http.StatusText(http.StatusOK))
	case "/query":
		w.WriteHeader(http.StatusOK)
		fmt.Fprintf(w, "%+v", r.URL.Query())
	case "/delay":
		ptime := r.URL.Query().Get("time")
		tm, err := strconv.Atoi(ptime)
		if err != nil {
			tm = 1000
		}
		time.Sleep(time.Duration(tm) * time.Millisecond)
		w.WriteHeader(http.StatusOK)
		fmt.Fprintf(w, http.StatusText(http.StatusOK))
	case "/redirect":
		target := r.URL.Query().Get("target")
		http.Redirect(w, r, target, http.StatusTemporaryRedirect)
	default:
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprintf(w, http.StatusText(http.StatusNotFound))
	}
}
