package main

import (
	"log"
	"net/http"
	"time"
)

func main2() {
	done := make(chan int, 1)
	go func() {
		for i := 0; i < 30; i++ {
			go func() {
				seq := i
				log.Printf("GET seq[%d]", seq)
				resp, err := http.Get("http://localhost:8080/home")
				if err != nil {
					log.Printf("GET fail, seq[%d], error: %s\n", seq, err)
					return
				}
				log.Printf("Get succ, seq[%d]\n", seq)
				defer resp.Body.Close()
			}()
			time.Sleep(1 * time.Second)
		}
		done<-1
	}()
	// wait all request done
	<-done
}
