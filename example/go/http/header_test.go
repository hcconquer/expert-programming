package http

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHeaderSet(t *testing.T) {
	header := make(http.Header)

	ua, ok := header["User-Agent"]
	assert.False(t, ok)
	assert.Equal(t, 0, len(ua))

	header.Set("User-Agent", "")
	ua, ok = header["User-Agent"]
	assert.True(t, ok)
	assert.Equal(t, 1, len(ua))
	assert.Equal(t, "", ua[0])

	header.Set("User-Agent", "app")
	ua, ok = header["User-Agent"]
	assert.True(t, ok)
	assert.Equal(t, 1, len(ua))
	assert.Equal(t, "app", ua[0])
}

// may is pointer
func TestHeaderCopy(t *testing.T) {
	header := http.Header{
		"Key1": []string{"Value1"},
		"Key2": []string{"Value3", "Value4"},
		"Key3": []string{"Value5"},
	}
	httpRequest, _ := http.NewRequest("GET", "http://localhost", nil)
	httpRequest.Header = header
	assert.EqualValues(t, header, httpRequest.Header)

	header.Set("Key4", "Value7")
	header.Del("Key3")
	assert.EqualValues(t, header, httpRequest.Header)
}
