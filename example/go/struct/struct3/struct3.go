/*
 * support multi embed, but can't has same name
**/

package main

import (
	"fmt"
	"unsafe"
)

type Animal struct {
	// will cause Dog ambiguous selector name
	// name string
	species string
}

type Pet struct {
	name string
	age  int
}

type Dog struct {
	Animal
	Pet
	strain string
}

func (animal *Animal) Move() {
	fmt.Printf("Animal Move, species: %s\n", animal.species)
}

// will cause Dog ambiguous selector Dup
// func (animal *Animal) Dup() {
// }

func (pet *Pet) Dup() {
	fmt.Printf("Pet Dup, name: %s, age: %d\n", pet.name, pet.age)
}

func (pet *Pet) Show() {
	fmt.Printf("Pet Show, name: %s, age: %d\n", pet.name, pet.age)
}

func (dog *Dog) Show() {
	fmt.Printf("Dog Show, name: %s, age: %d, strain: %s\n",
		dog.name, dog.age, dog.strain)
}

func (dog *Dog) Eat() {
	fmt.Printf("Dog Eat, name: %s, age: %d, strain: %s\n",
		dog.name, dog.age, dog.strain)
}

func NewPet(name string, age int, strain string) *Pet {
	dog := new(Dog)
	dog.name = name
	dog.age = age
	dog.strain = strain
	return (*Pet)(unsafe.Pointer(dog))
}

func NewDog(name string, age int, strain string) *Dog {
	dog := new(Dog)
	dog.name = name
	dog.age = age
	dog.strain = strain
	return dog
}

func main() {
	datas := []struct {
		name   string
		age    int
		strain string
	}{
		{"Wang", 18, "Chinese Rural Dog"},
	}

	for _, data := range datas {
		dog := NewDog(data.name, data.age, data.strain)

		dog.Move() // Animal Move, species:
		dog.Animal = Animal{
			species: "wild",
		}
		dog.Move() // Animal Move, species: wild

		dog.Dup()  // Pet Dup, name: Wang, age: 18
		dog.Show() // Dog Show, name: Wang, age: 18, strain: Chinese Rural Dog
		dog.Eat()  // Dog Eat, name: Wang, age: 18, strain: Chinese Rural Dog

		pet := NewPet(data.name, data.age, data.strain)
		pet.Dup()  // Pet Dup, name: Wang, age: 18
		pet.Show() // Pet Show, name: Wang, age: 18
	}
}
