/*
 * 1. embed type of pointer has no diff in use, but pointer need to new and set;
 * 2. if embed pointer, child to father pointer conv only first elem is ok;
**/

package main

import (
	"fmt"
	"unsafe"
	"reflect"
)

type Animal struct {
	// will cause Dog ambiguous selector name
	// name string
	species string
}

type Pet struct {
	name string
	age  int
}

type Dog struct {
	*Animal
	Pet
	strain string
}

type Cat struct {
	Pet
	strain string
	*Animal
}

type Pig struct {
	strain string
	*Animal
	Pet
}

func (animal *Animal) Move() {
	fmt.Printf("Animal Move, species: %s\n", animal.species)
}

// will cause Dog ambiguous selector Dup
// func (animal *Animal) Dup() {
// }

func (pet *Pet) Dup() {
	fmt.Printf("Pet Dup, name: %s, age: %d\n", pet.name, pet.age)
}

func (pet *Pet) Show() {
	fmt.Printf("Pet Show, name: %s, age: %d\n", pet.name, pet.age)
}

func (dog *Dog) Show() {
	fmt.Printf("Dog Show, name: %s, age: %d, strain: %s\n",
		dog.name, dog.age, dog.strain)
}

func (dog *Dog) Eat() {
	fmt.Printf("Dog Eat, name: %s, age: %d, strain: %s\n",
		dog.name, dog.age, dog.strain)
}

func (cat *Cat) Show() {
	fmt.Printf("Cat Show, name: %s, age: %d, strain: %s\n",
		cat.name, cat.age, cat.strain)
}

func (cat *Cat) Eat() {
	fmt.Printf("Cat Eat, name: %s, age: %d, strain: %s\n",
		cat.name, cat.age, cat.strain)
}

func (pig *Pig) Show() {
	fmt.Printf("Pig Show, name: %s, age: %d, strain: %s\n",
		pig.name, pig.age, pig.strain)
}

func (pig *Pig) Eat() {
	fmt.Printf("Pig Eat, name: %s, age: %d, strain: %s\n",
		pig.name, pig.age, pig.strain)
}

func NewDogPet(name string, age int, strain string) *Pet {
	dog := new(Dog)
	dog.name = name
	dog.age = age
	dog.strain = strain
	return (*Pet)(unsafe.Pointer(dog))
}

func NewCatPet(name string, age int, strain string) *Pet {
	cat := new(Cat)
	cat.name = name
	cat.age = age
	cat.strain = strain
	return (*Pet)(unsafe.Pointer(cat))
}

func NewPigPet(name string, age int, strain string) *Pet {
	pig := new(Pig)
	pig.name = name
	pig.age = age
	pig.strain = strain
	return (*Pet)(unsafe.Pointer(pig))
}

func NewDog(name string, age int, strain string) *Dog {
	dog := new(Dog)
	dog.name = name
	dog.age = age
	dog.strain = strain
	return dog
}

func main() {
	datas := []struct {
		name   string
		age    int
		strain string
	}{
		{"Wang", 18, "Chinese Rural Dog"},
	}

	for _, data := range datas {
		dog := NewDog(data.name, data.age, data.strain)

		// invalid memory address or nil pointer dereference
		// dog.Move()
		dog.Animal = &Animal{
			species: "wild",
		}
		dog.Move() // Animal Move, species: wild

		dog.Dup()  // Pet Dup, name: Wang, age: 18
		dog.Show() // Dog Show, name: Wang, age: 18, strain: Chinese Rural Dog
		dog.Eat()  // Dog Eat, name: Wang, age: 18, strain: Chinese Rural Dog

		// bad pointer
		pet1 := NewDogPet(data.name, data.age, data.strain)
		fmt.Printf("pet1 == nil: %t\n", pet1 == nil) // false
		// pet1, type: *main.Pet, kind: ptr
		fmt.Printf("pet1, type: %s, kind: %s\n", reflect.TypeOf(pet1), reflect.TypeOf(pet1).Kind())
		// invalid memory address or nil pointer dereference
		// pet.Dup()  // Pet Dup, name: Wang, age: 18
		// pet.Show() // Pet Show, name: Wang, age: 18

		// right pointer
		pet2 := NewCatPet(data.name, data.age, data.strain)
		fmt.Printf("pet2 == nil: %t\n", pet2 == nil) // false
		// pet2, type: *main.Pet, kind: ptr
		fmt.Printf("pet2, type: %s, kind: %s\n", reflect.TypeOf(pet2), reflect.TypeOf(pet2).Kind())
		pet2.Dup()  // Pet Dup, name: Wang, age: 18
		pet2.Show() // Pet Show, name: Wang, age: 18

		// half right and bad pointer
		pet3 := NewPigPet(data.name, data.age, data.strain)
		fmt.Printf("pet3 == nil: %t\n", pet3 == nil) // false
		// pet3, type: *main.Pet, kind: ptr
		fmt.Printf("pet3, type: %s, kind: %s\n", reflect.TypeOf(pet3), reflect.TypeOf(pet3).Kind())
		pet3.Dup()  // Pet Dup, name: Chinese Rural Dog, age: 0
		pet3.Show() // Pet Show, name: Chinese Rural Dog, age: 0
	}
}
