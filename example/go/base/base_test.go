package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCodeBlock(t *testing.T) {
	num := 1

	{
		num++
	}
	assert.Equal(t, 2, num)

	{
		num++
	}
	assert.Equal(t, 3, num)

	for c := true; c; c = false {
		num++
	}

	assert.Equal(t, 4, num)
}
