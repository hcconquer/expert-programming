package main

import (
	"regexp"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMatchString(t *testing.T) {
	var cases = []struct {
		regex  string
		input  string
		repl   string
		result string
	}{
		{`a(x*)b`, "-ab-axxb-axxxb-azb-", "%", "-%-%-%-azb-"},
		{`a(x*)b`, "-ab-axxb-axxxb-azb-", "$1", "--xx-xxx-azb-"},
		{`.*href=|['"]`, `href='page.html'`, "", "page.html"},
		{`.*href=|['"]`, `javascript:location.href="page.html"`, "%", "%%page.html%"},
		{`.*href=|['"]`, `javascript:location.href="page.html"`, "$1", "page.html"},
		{`.*href=|['"]`, `javascript:location.href="page.html"`, "", "page.html"},
	}

	for _, c := range cases {
		re := regexp.MustCompile(c.regex)
		result := re.ReplaceAllString(c.input, c.repl)
		t.Logf("replace: %s", result)
		assert.Equalf(t, c.result, result, "case: %+v", c)
	}
}
