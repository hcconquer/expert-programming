package main

import (
	"log"
)

type Pet interface {
	Speak()
}

type Dog struct {
	name string
}

func (dog *Dog) Speak() {
	log.Printf("[%s]: hello", dog.name)
}

var dogs = map[string]Pet{
	"tom": &Dog{name: "tom"},
}

// new pointer and add to map
// will be not reclaim
func addMappedPet(name string) {
	dog := new(Dog)
	dog.name = name
	dogs[name] = dog
}

func addPet(name string) Pet {
	dog := new(Dog)
	dog.name = name
	return dog
}

func listPets() {
	for _, pet := range dogs {
		pet.Speak()
	}
}

func main() {
	log.Printf("### add mapped pointer will not be reclaim ###")
	pets := []string{"john", "mary"}
	for _, pet := range pets {
		log.Printf("----------------------")
		addMappedPet(pet)
		listPets()
	}

	log.Printf("### add return pointer will not be reclaim ###")
	pet := addPet("jack")
	pet.Speak()
}
