// go tool vet -all -shadow *
package main

import (
	"log"
)

func max1(num1, num2 int) int {
	for {
		if num1 < num2 {
			return num2
		} else {
			return num1
		}
	}
	// FIXME: unreachable code
	return -1
}

func max2(num1, num2 int) int {
	for {
		if num1 < num2 {
			return num2
		} else {
			return num1
		}
	}
	// will be no error without return
}

func main() {
	num1 := 3
	num2 := 5
	log.Printf("max(%d, %d): %d", max1(num1, num2), num1, num2) // max(5, 3): 5
	log.Printf("max(%d, %d): %d", max2(num1, num2), num1, num2) // max(5, 3): 5
}
