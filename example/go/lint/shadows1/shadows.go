// go tool vet -all -shadow *

package main

import (
	"fmt"
	"log"
)

func dd(flag1, flag2, flag3 bool) error {
	var err error
	if !flag1 {
		_, err := dosth(false, "error 1")
	}
	if err != nil {
		return err
	}
	return nil
}

func dosth(flag bool, msg string) (int, error) {
	if !flag {
		err := fmt.Errorf("Mission Impossible")
		log.Printf("error: %v", err)
		return -1, err
	}
	return 0, nil
}

func perr1(flag1, flag2 bool) error {
	var err error
	if !flag1 {
		err = fmt.Errorf("Minority Report")
	}
	val, err := dosth(flag2) // set return err
	if err != nil {
		log.Printf("val: %v, error: %v", val, err)
	}
	return err
}

func perr2(flag1, flag2 bool) error {
	var err error
	if !flag1 {
		err = fmt.Errorf("Minority Report") // set return err
	}
	if !flag2 {
		// FIXME: declaration of "err" shadows declaration
		_, err := dosth(flag2)
		log.Printf("error: %v", err)
	}
	return err
}

func perr3(flag1, flag2 bool) error {
	var err error
	if !flag1 {
		err = fmt.Errorf("Minority Report")
	}
	if !flag2 {
		_, err = dosth(flag2) // set return err
		log.Printf("error: %v", err)
	}
	return err
}

func perr4(flag1, flag2 bool) error {
	var err error
	if !flag1 {
		err = fmt.Errorf("Minority Report") // set return err
	}
	// FIXME: declaration of "err" shadows declaration
	if val, err := dosth(flag2); err != nil {
		log.Printf("val: %v, error: %v", val, err)
	}
	return err
}

func perr5(flag1, flag2 bool) error {
	var err error
	if !flag1 {
		err = fmt.Errorf("Minority Report") // set return err
	}
	// FIXME: declaration of "err" shadows declaration
	if _, err := dosth(flag2); err != nil {
		log.Printf("error: %v", err)
	}
	return err
}

func main() {
	log.Printf("perr1: %v", perr1(false, false)) // `Mission Impossible`
	log.Printf("perr2: %v", perr2(false, false)) // `Minority Report`
	log.Printf("perr3: %v", perr3(false, false)) // `Mission Impossible`
	log.Printf("perr4: %v", perr4(false, false)) // `Minority Report`
	log.Printf("perr5: %v", perr5(false, false)) // `Minority Report`
}
