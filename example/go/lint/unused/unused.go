// go tool vet -all -shadow *
package main

func main() {
	num := 3    // declared and not used
	var _ = num // fix compile error
}
