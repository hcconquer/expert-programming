package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRef(t *testing.T) {
	s1 := []int{1, 2, 3}
	assert.EqualValues(t, s1, []int{1, 2, 3})

	s2 := append(s1, 4) // not modify slice
	assert.EqualValues(t, s1, []int{1, 2, 3})
	assert.EqualValues(t, s2, []int{1, 2, 3, 4})

	s3 := s1 // s3 is ref to s1
	s3[0] = 6
	assert.EqualValues(t, s1, []int{6, 2, 3})
}

func TestArrayForm(t *testing.T) {
	s1 := []int{0, 2, 3, 4, 5}

	s2 := s1[1:3]
	assert.EqualValues(t, s2, []int{1, 2})
	assert.True(t, len(s2) == 2 && cap(s2) == 5)
	assert.EqualValues(t, cap(s2), 5)

	s3 := s1[1:3:4]
	assert.EqualValues(t, s3, []int{1, 2})
	assert.True(t, len(s2) == 2 && cap(s2) == 3)
}

func TestNewCap(t *testing.T) {
	var cases = []struct {
		len    int
		newcap int
	}{
		{1, 2},
		{7, 14},
		{16, 32},
		{200, 400},
		{300, 608},
		{511, 1024},
		{512, 1024},
		{513, 1184},
		{1000, 2048},
		{1024, 1024 + 1024/4},
		{1025, 1360},
		{2000, 2560},
		{3000, 4096},
		{4000, 5120},
		{5000, 7168},
	}

	for _, c := range cases {
		s := make([]int, c.len)
		s = append(s, 1)
		assert.Equal(t, c.newcap, cap(s))
	}
}

func TestAppend(t *testing.T) {
	a := [...]int{0, 2, 3, 4, 5, 6}
	s0 := a[:]           // len=7 cap=7
	s1 := s0[:]          // len=7 cap=7
	s2 := s1[1:3]        // len=2 cap=6
	s3 := s2[2:6]        // len=4 cap=4
	s4 := s0[3:5]        // len=2 cap=4
	s5 := s0[3:5:5]      // len=2 cap=2
	s6 := append(s4, 77) // len=3 cap=4
	s7 := append(s5, 88) // len=3 cap=4
	s8 := append(s7, 66) // len=4 cap=4
	s3[1] = 99

	assert.EqualValues(t, s0, []int{0, 2, 3, 99, 77, 6})
	assert.True(t, len(s0) == 7 && cap(s0) == 7)
	assert.EqualValues(t, s1, []int{0, 2, 3, 99, 77, 6})
	assert.True(t, len(s1) == 7 && cap(s1) == 7)
	assert.EqualValues(t, s2, []int{1, 2})
	assert.True(t, len(s2) == 2 && cap(s2) == 6)
	assert.EqualValues(t, s3, []int{3, 99, 77, 6})
	assert.True(t, len(s3) == 4 && cap(s3) == 4)
	assert.EqualValues(t, s4, []int{3, 99})
	assert.True(t, len(s4) == 2 && cap(s4) == 4)
	assert.EqualValues(t, s5, []int{3, 99})
	assert.True(t, len(s5) == 2 && cap(s5) == 2)
	assert.EqualValues(t, s6, []int{3, 99, 77})
	assert.True(t, len(s6) == 3 && cap(s6) == 4)
	assert.EqualValues(t, s7, []int{3, 4, 88})
	assert.True(t, len(s7) == 3 && cap(s7) == 4)
	assert.EqualValues(t, s8, []int{3, 4, 88, 66})
	assert.True(t, len(s8) == 4 && cap(s8) == 4)

	// re-uses
	s9 := append(s4, 55) // len=3 cap=4
	assert.EqualValues(t, s9, []int{3, 99, 55})
	assert.True(t, len(s9) == 3 && cap(s9) == 4)
	assert.EqualValues(t, s1, []int{0, 2, 3, 99, 55, 6})
	assert.True(t, len(s1) == 7 && cap(s1) == 7)

	// new
	s10 := append(s4, 101, 102, 103) // len=5 cap=8
	assert.EqualValues(t, s10, []int{3, 99, 101, 102, 103})
	assert.True(t, len(s10) == 5 && cap(s10) == 8)
	assert.EqualValues(t, s1, []int{0, 2, 3, 99, 55, 6})
	assert.True(t, len(s1) == 7 && cap(s1) == 7)
}
