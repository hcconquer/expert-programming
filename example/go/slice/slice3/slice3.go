package main

import (
	"log"
	"time"
)

func makeAndMod(num int) []int {
	slice := make([]int, num)
	for idx := range slice {
		slice[idx] = idx
	}
	go func() {
		for i := 0; i < 5; i++ {
			time.Sleep(time.Second)
			log.Printf("mod %d round", i + 1)
			for idx := range slice {
				slice[idx] = slice[idx] + 1
			}
		}
	}()
	return slice
}

func main() {
	slice := makeAndMod(1)
	for i := 0; i < 8; i++ {
		log.Printf("slice: %v", slice)
		time.Sleep(time.Second)
	}
}
