package main

import (
	"log"
)

func main() {
	s1 := make([]int, 2, 5)
	s1[0] = 0
	s1[1] = 1
	log.Printf("s1: %#v", s1)

	s2 := s1
	for i := 4; i < 10; i++ {
		// modify s2 will not impact s1
		s2 = append(s2, i)
		log.Printf("s1: %#v, s2: %#v", s1, s2)
	}
}
