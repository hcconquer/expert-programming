package main

import (
	"regexp"
	"strings"
)

func matchResources(resource string, resources []string) bool {
	for _, res := range resources {
		if res[0:1] == "/" {
			res = res[1:]
		}
		res = strings.Replace(res, ".", "\\.", -1)
		res = strings.Replace(res, "*", ".*", -1)
		res = strings.Join([]string{"^", res, "$"}, "")
		matched, err := regexp.MatchString(res, resource)
		if err == nil && matched {
			return true
		}
	}
	return false
}
