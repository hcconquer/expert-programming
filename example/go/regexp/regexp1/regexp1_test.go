package main

import (
	"testing"
)

func TestMatchResources(t *testing.T) {
	var cases = []struct {
		resource  string
		resources []string
		matched   bool
	}{
		{"path1", []string{"/path1"}, true},
		{"abc.jpg", []string{"/*.jpg"}, true},
		{"abd.jpg", []string{"/*.png", "/*.jpg"}, true},
		{"abe.jpg", []string{"/*.pg"}, false},
		{"image/abf.jpg", []string{"/*"}, true},
		{"image/abg.jpg", []string{"/image*"}, true},
		{"image/abh.jpg", []string{"/image/*"}, true},
		{"image/abi.jpg", []string{"/image/*.jpg"}, true},
		{"image/abj.jpg", []string{"/image/*.png"}, false},
		{"image/abc.jpg0", []string{"/*.jpg"}, false},
		{"0image/abc.jpg", []string{"/image*.jpg"}, false},
	}

	for _, c := range cases {
		matched := matchResources(c.resource, c.resources)
		if matched != c.matched {
			t.Errorf("test match resource error, expected[%t], real[%t], resource[%s]", c.matched, matched, c.resource)
		}
	}
}
