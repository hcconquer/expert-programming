// go test -benchmem -bench=. -benchtime=5s -cpuprofile=cpuprof.out -memprofile=memprof.out
// go test -benchmem -bench=BenchmarkChan1 -benchtime=5s -cpuprofile=cpuprof1.out -memprofile=memprof1.out
// go test -benchmem -bench=BenchmarkChan2 -benchtime=5s -cpuprofile=cpuprof2.out -memprofile=memprof2.out

package main

import (
	"sync"
	"time"
	"fmt"
	"testing"
)

type blobMsg struct {
	blob []byte
}

const (
	GByte = 1024 * 1024 * 1024
	MByte = 1024 * 1024
	ChanSize = 100
	blobLength = 100 * MByte
)

func BenchmarkChan1(b *testing.B) {
	blobCh := make(chan []byte, ChanSize)
	blobData := make([]byte, blobLength)

	var wg sync.WaitGroup

	go func() {
		start := time.Now()
		length := 0
		wg.Add(1)
		for {
			blob, ok := <-blobCh
			length += len(blob)
			if !ok {
				break
			}
		}
		fmt.Printf("[]byte %s end length:%d\n", time.Now().Sub(start), length)
		wg.Done()
	}()

	offsetMax := blobLength-MByte

	b.ResetTimer()
	for n := 0 ; n < b.N; n++ {
		i := n % offsetMax
		blobCh <- blobData[i: i+MByte]
	}
	b.StopTimer()

	close(blobCh)
	wg.Wait()
}

func BenchmarkChan2(b *testing.B) {
	blobMsgCh := make(chan blobMsg, ChanSize)
	blobData := make([]byte, blobLength)

	var wg sync.WaitGroup

	go func() {
		start := time.Now()
		length := 0
		wg.Add(1)
		for {
			blobMsg, ok := <-blobMsgCh
			length += len(blobMsg.blob)
			if !ok {
				break
			}
		}
		fmt.Printf("blobMsg: %s end length:%d\n", time.Now().Sub(start), length)
		wg.Done()
	}()

	offsetMax := blobLength-MByte

	b.ResetTimer()
	for n := 0 ; n < b.N; n++ {
		i := n % offsetMax
		blobMsgCh <- blobMsg{blobData[i : i+MByte]}
	}

	b.StopTimer()

	close(blobMsgCh)
	wg.Wait()
}
