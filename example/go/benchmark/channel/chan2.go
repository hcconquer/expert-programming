package main

import (
	"flag"
	"fmt"
	"sync"
	"time"
)

type blobMsg struct {
	blob []byte
}

const (
	GByte      = 1024 * 1024 * 1024
	MByte      = 1024 * 1024
	ChanSize   = 100
)

var (
	bufsize = 10 * MByte
)

var (
	seed = flag.Int("seed", 0, "run seed")
	bufcnt = flag.Int("bufcnt", 10, "bufcnt, bufsize=bufcnt*(1-Mbyte)")
)

func test1() {
	blobCh := make(chan []byte, ChanSize)
	blobData := make([]byte, bufsize)

	var wg sync.WaitGroup

	go func() {
		start := time.Now()
		total := 0
		wg.Add(1)
		for {
			blob, ok := <-blobCh
			total += len(blob)
			if !ok {
				break
			}
		}
		fmt.Printf("test1 []byte time %v bufsize %d total %d\n", time.Now().Sub(start).Seconds(), bufsize, total)
		wg.Done()
	}()

	for i := 0; i < bufsize-MByte; i = i + 1 {
		blobCh <- blobData[i : i+MByte]
	}

	close(blobCh)
	wg.Wait()
}

func test2() {
	blobMsgCh := make(chan blobMsg, ChanSize)
	blobData := make([]byte, bufsize)

	var wg sync.WaitGroup

	go func() {
		start := time.Now()
		total := 0
		wg.Add(1)
		for {
			blobMsg, ok := <-blobMsgCh
			total += len(blobMsg.blob)
			if !ok {
				break
			}
		}
		fmt.Printf("test2 blobMsg time %v bufsize %d total %d\n", time.Now().Sub(start).Seconds(), bufsize, total)
		wg.Done()
	}()

	for i := 0; i < bufsize-MByte; i = i + 1 {
		blobMsgCh <- blobMsg{blobData[i : i+MByte]}
	}

	close(blobMsgCh)
	wg.Wait()
}

func main() {
	flag.Parse()

	bufsize = *bufcnt * MByte

	if *seed%2 == 0 {
		test1()
		test2()
	} else {
		test2()
		test1()
	}
}
