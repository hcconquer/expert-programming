// go test -benchmem -bench=.

package channel

import (
	"testing"
)

const (
	BufSize = 32 * 1024 // array can't be greater 64KB
	ChanCap = 256
)

type Object struct {
	data []byte
}

func makeTestString(len int) []byte {
	s := make([]byte, len)
	for i := 0; i < len; i++ {
		s[i] = byte(int('a') + (i % 26))
	}
	return s
}

func BenchmarkChanInt(b *testing.B) {
	ch := make(chan int, ChanCap)

	go func() {
		for {
			<-ch
		}
	}()

	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		ch <- n
	}
	b.StopTimer()
}

func BenchmarkChanInt2Go(b *testing.B) {
	ch := make(chan int, ChanCap)

	for i := 0; i < 2; i++ {
		go func() {
			for {
				<-ch
			}
		}()
	}

	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		ch <- n
	}
	b.StopTimer()
}

func BenchmarkChanInt4Go(b *testing.B) {
	ch := make(chan int, ChanCap)

	for i := 0; i < 4; i++ {
		go func() {
			for {
				<-ch
			}
		}()
	}

	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		ch <- n
	}
	b.StopTimer()
}

func BenchmarkChanSlice(b *testing.B) {
	str := makeTestString(BufSize)
	s := str
	ch := make(chan []byte, ChanCap)

	go func() {
		for {
			<-ch
		}
	}()

	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		ch <- s
	}
	b.StopTimer()
}

func BenchmarkChanStruct(b *testing.B) {
	str := makeTestString(BufSize)
	s := str
	ch := make(chan Object, ChanCap)

	go func() {
		for {
			<-ch
		}
	}()

	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		ch <- Object{data: s}
	}
	b.StopTimer()
}

func BenchmarkChanArray(b *testing.B) {
	str := makeTestString(BufSize)
	s := [BufSize]byte{}
	copy(s[:], str)
	ch := make(chan [BufSize]byte, ChanCap)

	go func() {
		for {
			<-ch
		}
	}()

	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		ch <- s
	}
	b.StopTimer()
}

func BenchmarkChanString(b *testing.B) {
	str := makeTestString(BufSize)
	s := string(str)
	ch := make(chan string, ChanCap)

	go func() {
		for {
			<-ch
		}
	}()

	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		ch <- s
	}
	b.StopTimer()
}
