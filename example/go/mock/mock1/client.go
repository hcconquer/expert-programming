// mockgen -source client.go -destination mock_client.go -package mock1

package mock1

type Message struct {
	a int
	b int
}

type Client interface {
	GetMessage(key string, index int) (*Message, error)
}
