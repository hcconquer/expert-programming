package mock1

import (
	"errors"
	"testing"
	"github.com/golang/mock/gomock"
)

func TestHandleMessage(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	mockClient := NewMockClient(mockCtrl)

	mockClient.EXPECT().GetMessage(gomock.Any(), gomock.Any()).Return(&Message{a: 2, b: 3}, nil)
	val, err := handleMessage(mockClient)
	if err != nil {
		t.Errorf("error: %s", err)
	}
	if val != 5 {
		t.Errorf("bad value: %d", val)
	}

	mockClient.EXPECT().GetMessage(gomock.Any(), gomock.Any()).Return(nil, errors.New("fail"))
	_, err = handleMessage(mockClient)
	if err == nil {
		t.Errorf("should be fail")
	}
}
