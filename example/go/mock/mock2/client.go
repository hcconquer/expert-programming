// mockery -name=Client -inpkg -case=underscore

package mock2

type Message struct {
	a int
	b int
}

type Client interface {
	GetMessage(key string, index int) (*Message, error)
}