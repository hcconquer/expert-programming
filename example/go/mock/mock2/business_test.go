package mock2

import (
	"testing"
	"errors"
	"github.com/stretchr/testify/mock"
)

func TestHandleMessage(t *testing.T) {
	mockClient := new(MockClient)

	mockClient.On("GetMessage", mock.Anything, mock.Anything).Return(&Message{a: 2, b: 3}, nil).Once()
	val, err := handleMessage(mockClient)
	if err != nil {
		t.Errorf("error: %s", err)
	}
	if val != 5 {
		t.Errorf("bad value: %d", val)
	}

	mockClient.On("GetMessage", mock.Anything, mock.Anything).Return(nil, errors.New("fail")).Once()
	_, err = handleMessage(mockClient)
	if err == nil {
		t.Errorf("should be fail")
	}
}
