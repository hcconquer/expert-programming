package mock2

func handleMessage(client Client) (int, error) {
	msg, err := client.GetMessage("test", 1)
	if err != nil {
		return 0, err
	}
	return msg.a + msg.b, nil
}
