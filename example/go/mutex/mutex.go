package main

import (
	"log"
	"sync"
	"time"
)

func t1() {
	var mutex sync.Mutex
	for i := 0; i < 3; i++ {
		go func() {
			mutex.Lock()
			defer mutex.Unlock()
			log.Printf("%d", i)
			time.Sleep(time.Second)
		}()
	}
}

func main() {
	t1()
	log.Printf("e")
	time.Sleep(10*time.Second)
}
