package main

import (
	"io/ioutil"
	"log"
	"encoding/base64"
)

func main() {
	bytes, err := ioutil.ReadFile("../../../image/i-heard-much-loves.jpg")
	if err != nil {
		log.Fatalf("ReadFile err: %s", err)
	}
	base64String := base64.StdEncoding.EncodeToString(bytes)
	log.Printf("base64: <%s>", base64String)
}
