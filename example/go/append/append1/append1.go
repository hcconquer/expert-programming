package main

import "fmt"

func main() {
	s1 := make([]int, 3, 5) // len: 3, cap: 5
	s1[2] = 3
	fmt.Printf("s1: %p, len: %d, cap: %d\n", s1, len(s1), cap(s1))
	fmt.Println("s1:", s1)

	s2 := append(s1, 4, 5) // len: 5, cap: 5
	fmt.Printf("s2: %p, len: %d, cap: %d\n", s2, len(s2), cap(s2))
	fmt.Println("s2:", s2)

	// double mem
	s3 := append(s1, 4, 5, 6) // len: 6, cap: 10
	fmt.Printf("s3: %p, len: %d, cap: %d\n", s3, len(s3), cap(s3))
	fmt.Println("s3:", s3)

	fmt.Printf("s1: %p, len: %d, cap: %d\n", s1, len(s1), cap(s1)) // len: 3, cap: 5
	fmt.Println("s1:", s1)
}
