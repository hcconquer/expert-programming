package main

import (
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUrlResolveReference(t *testing.T) {
	var cases = []struct {
		base   string
		rpath  string
		target string
	}{
		{"http://a.com/dir/page1.html", "page2.html", "http://a.com/dir/page2.html"},
		{"http://a.com/dir/page1.html", "http://b.com/dir/page2.html", "http://b.com/dir/page2.html"},
	}

	for _, c := range cases {
		base, _ := url.Parse(c.base)
		rpath, _ := url.Parse(c.rpath)
		target := base.ResolveReference(rpath)
		assert.Equalf(t, c.target, target.String(), "case: %+v", c)
	}
}
