package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSomething(t *testing.T) {
	assert := assert.New(t)

	var num int = 123
	assert.Equal(num, 123, "they should be equal")
	assert.EqualValues(num, int32(123))
	assert.NotEqual(num, 456, "they should not be equal")

	var p1 *int
	p2 := &num
	assert.Nil(p1)
	assert.NotNil(p2)

	slice := []int{0, 1, 2}
	assert.Equal(slice, []int{0, 1, 2})
	assert.Contains(slice, 1)
	assert.NotContains(slice, -1)

}
