package main

import (
	"log"
	"sync/atomic"
)

func main() {
	log.Println("main begin.")
	coNum := 3 // concurrence number
	coChan := make(chan int, coNum)
	log.Printf("coChan len: %d, cap: %d\n", len(coChan), cap(coChan)) // len: 0, cap 3
	for i := 0; i < coNum; i++ {
		coChan <- 1
	}
	woNum := 10
	done := make(chan int, 1)
	doneCount := int64(0)
	for i := 0; i < woNum; i++ {
		log.Printf("num: %d\n", i)
		<-coChan // get co
		go func(n int) {
			defer func() {
				cnt := atomic.AddInt64(&doneCount, 1)
				if cnt >= int64(woNum) {
					done <- 1 // all done
				}
			}()
			defer func() { coChan <- 1 }() // put co
			log.Printf("handle: %d\n", n)
		}(i)
	}
	log.Printf("coChan len: %d, doneCount: %d\n", len(coChan), doneCount) // 0, 7
	// wait for all done
	<-done
	log.Printf("coChan len: %d, doneCount: %d\n", len(coChan), doneCount) // 3, 10
	log.Println("main exit.")
}
