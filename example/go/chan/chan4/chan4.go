package main

import (
	"time"
	"log"
)

func main() {
	coNum := 3
	ch := make(chan int, coNum)
	for i := 0; i < 10; i++ {
		go func(seq int) {
			v := <-ch
			log.Printf("go seq=%d v=%d", seq, v)
		}(i)
		time.Sleep(100 * time.Millisecond)
	}
	for i := 0; i < 10; i++ {
		ch <- i
	}
	time.Sleep(2000 * time.Millisecond)
}
