package main

import (
	"log"
)

func main() {
	c1 := make(chan int, 1)
	c2 := make(chan int, 1)
	c1 <- 1
	c2 <- 2
	select {
	case <-c1:
		log.Printf("a")
	case <-c2:
		log.Printf("b")
	default:
		log.Printf("c")
	}
}
