package main

import (
	"reflect"
	"strings"
	"testing"
)

func TestSplit(t *testing.T) {
	var cases = []struct {
		str       string
		delimiter string
		fields    []string
	}{
		{"/a/b/c", "/", []string{"", "a", "b", "c"}},
		{"a/b/c", "/", []string{"a", "b", "c"}},
		{"a", "/", []string{"a"}},
		{"", "/", []string{""}},
		{" ", "/", []string{" "}},
		{"/", "/", []string{"", ""}},
		{"//", "/", []string{"", "", ""}},
	}

	for _, c := range cases {
		fields := strings.Split(c.str, c.delimiter)
		if !reflect.DeepEqual(c.fields, fields) {
			t.Errorf("expected: %#v, actual: %#v, case: %#v", c.fields, fields, c)
		}
	}
}

func TestFields(t *testing.T) {
	var cases = []struct {
		str       string
		delimiter rune
		fields    []string
	}{
		{"/a/b/c", '/', []string{"a", "b", "c"}},
		{"a/b/c", '/', []string{"a", "b", "c"}},
		{"a", '/', []string{"a"}},
		{"", '/', []string{}},
		{" ", '/', []string{" "}},
		{"/", '/', []string{}},
		{"//", '/', []string{}},
	}
	for _, c := range cases {
		fields := strings.FieldsFunc(c.str, func(r rune) bool {
			return r == c.delimiter
		})
		if !reflect.DeepEqual(c.fields, fields) {
			t.Errorf("expected: %#v, actual: %#v, case: %#v", c.fields, fields, c)
		}
	}
}
