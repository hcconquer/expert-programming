package main

import (
	"testing"
)

// Equal:   4.75 ns/op
// Compare: 12.0 ns/op
// so `==` may be quick then `compare`
func BenchmarkCompare(b *testing.B) {
	cases := []string{"a", "abc", "hello", "hello world", "yes", "no"}
	num := len(cases)
	for i := 0; i < b.N; i++ {
		p := i % num
		q := (i + 1) % num
		// Equal(cases[p], cases[q])
		Compare(cases[p], cases[q])
	}
}
