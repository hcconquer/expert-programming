package main

import (
	"strings"
)

func Equal(s1, s2 string) bool {
	return s1 == s2
}

func Compare(s1, s2 string) bool {
	return strings.Compare(s1, s2) == 0
}

func main() {
}
