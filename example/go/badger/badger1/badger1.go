// this is a demo for badger kv db from https://github.com/dgraph-io/badger

package main

import (
	"log"

	"github.com/dgraph-io/badger"
)

func main() {
	// Open the Badger database located in the /tmp/badger directory.
	// It will be created if it doesn't exist.
	opts := badger.DefaultOptions
	opts.Dir = "/tmp/badger"
	opts.ValueDir = "/tmp/badger"
	db, err := badger.Open(opts)
	if err != nil {
		log.Fatal(err)
	}

	var cases = []struct {
		database  string
		developer string
	}{
		{"LevelDB", "Google"},
		{"RocksDB", "Facebook"},
		{"Badger", "Dgraph"},
	}

	for _, c := range cases {
		err = db.Update(func(txn *badger.Txn) error {
			err := txn.Set([]byte(c.database), []byte(c.developer))
			return err
		})
	}

	for _, c := range cases {
		err = db.View(func(txn *badger.Txn) error {
			item, err := txn.Get([]byte(c.database))
			if err != nil {
				return err
			}
			val, err := item.Value()
			if err != nil {
				return err
			}
			log.Printf("database: %s, developer: %s", c.database, val)
			return nil
		})
	}

	err = db.View(func(txn *badger.Txn) error {
		opts := badger.DefaultIteratorOptions
		opts.PrefetchSize = 10
		it := txn.NewIterator(opts)
		defer it.Close()
		for it.Rewind(); it.Valid(); it.Next() {
			item := it.Item()
			k := item.Key()
			v, err := item.Value()
			if err != nil {
				return err
			}
			log.Printf("key=%s, value=%s", k, v)
		}
		return nil
	})

	defer db.Close()
}
