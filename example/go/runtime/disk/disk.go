package main;

import (
	"fmt"
	"syscall"
)

func Statfs(path string) {
	statfs := syscall.Statfs_t{}
	err := syscall.Statfs(path, &statfs)
	if err != nil {
		fmt.Printf("Statfs fail, err: %s", err)
		return
	}
	// fmt.Printf("statfs: %+v", statfs)
	fmt.Printf("statfs: %s, Bsize: %v\n", path, statfs.Bsize)
}

func Stat(path string) {
	stat := syscall.Stat_t{}
	err := syscall.Stat(path, &stat)
	if err != nil {
		fmt.Printf("Statfs fail, err: %s", err)
		return
	}
	// fmt.Printf("stat: %+v", stat)
	fmt.Printf("stat: %s, Size: %v\n", path, stat.Size)
}

func main() {
	Statfs("/")
	Stat("disk")
}
