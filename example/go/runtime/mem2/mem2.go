package main

import (
	"fmt"
	"time"
)

func main() {
	// real mem is depend on system hardware
	buf := make([]byte, 1<<30)
	fmt.Printf("cap: %d, len: %d\n", cap(buf), len(buf))
	sum := 0
	// when read will cause sys mem allocate
	for _, b := range buf {
		sum += int(b)
	}
	fmt.Printf("sum: %d\n", sum)
	time.Sleep(30 * time.Second)
}
