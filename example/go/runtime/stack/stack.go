package main

import (
	"log"
	"runtime"
)

func divide(m, n int) {
	log.Printf("%d/%d=%d", m, n, m/n)
}

func unbreakable(m, n int) {
	log.Printf("unbreakable(%d, %d)", m, n)
	// log stack
	defer func() {
		err := recover()
		if err != nil {
			log.Printf("panic")
			buf := make([]byte, 1024*1024)
			buf = buf[:runtime.Stack(buf, true)]
			log.Printf("stack: {%s}", buf)
		}
	}()
	divide(m, n)
}


func main() {
	unbreakable(6, 2)
	unbreakable(6, 0)
	unbreakable(6, 1)
}
