package main

import (
	"fmt"
	"runtime"
)

func MemStats() {
	memStat := new(runtime.MemStats)
	runtime.ReadMemStats(memStat)
	// fmt.Printf("MemStats: %+v", memStat)
	// these are logical memory but not physical memory
	fmt.Printf("Alloc: %v\n", memStat.Alloc)
	fmt.Printf("TotalAlloc: %v\n", memStat.TotalAlloc)
	fmt.Printf("Sys: %v\n", memStat.Sys)
}

func main() {
	MemStats()
}
