package main

import (
	"log"
	"net/rpc"
)

type Args struct {
	Arg1 int
	Arg2 int
}

type Result struct {
	Result1 int
	Result4 int
	Result5 []int
}

type Arith struct {
}

func main() {
	client, err := rpc.DialHTTP("tcp", "localhost:8080")
	if err != nil {
		log.Printf("dail fail, error: %s", err.Error())
		return
	}
	defer client.Close()

	for i := 0; i < 10; i++ {
		// Synchronous call
		args := &Args{32 * (i + 3), i}
		result := Result{}
		err = client.Call("Arith.Divide", args, &result)
		if err != nil {
			log.Printf("RPC fail, error: %s", err.Error())
			return
		}
		log.Printf("Arith.Divide(%+v) = %+v", args, result)
	}
}
