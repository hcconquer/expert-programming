package main

import (
	"log"
	"net"
	"net/http"
	"net/rpc"
)

type Args struct {
	Arg1 int
	Arg2 int
}

type Result struct {
	Result1 int
	Result2 int
	Result3 []int
}

type Arith struct {
}

func (t *Arith) Divide(args *Args, result *Result) error {
	if args.Arg2 != 0 {
		result.Result1 = args.Arg1 / args.Arg2
		result.Result2 = args.Arg1 % args.Arg2
		result.Result3 = make([]int, 2)
		result.Result3[0] = result.Result1
		result.Result3[1] = result.Result2
	}
	log.Printf("Arith.Divide(%+v) = %+v", args, result)
	return nil
}

func main() {
	arith := new(Arith)
	rpc.Register(arith)
	rpc.HandleHTTP()
	listener, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Printf("listen fail, error: %s", err.Error())
		return
	}
	http.Serve(listener, nil)
}
