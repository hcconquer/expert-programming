package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestToString(t *testing.T) {
	toy := Toy{
		Name:  "Lego",
		Price: 128,
	}
	var cases = []struct {
		toy interface{}
	}{
		{"Lego"}, // Toys:[Lego]}
		{toy},    // Toys:[{Name:Lego Price:128}]}
		{&toy},   // Toys:[0xc00000a100]}
	}

	for _, c := range cases {
		dog := Dog{
			Toys: []interface{}{c.toy},
		}
		t.Logf("[Dog] %+v", dog)

		cat := Cat{
			Toys: []interface{}{c.toy},
		}
		t.Logf("[Cat] %+v", cat)
		t.Logf("[*Cat] %+v", &cat)
		assert.Equal(t, fmt.Sprintf("%+v", cat), fmt.Sprintf("%+v", &cat))

		mouse := Mouse{
			Toys: []interface{}{c.toy},
		}
		t.Logf("[Mouse] %+v", mouse)
		t.Logf("[*Mouse] %+v", &mouse)

		pig := Pig{
			Toys: []interface{}{c.toy},
		}
		t.Logf("[Pig] %+v", pig)
	}
}
