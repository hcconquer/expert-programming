package main

import (
	"bytes"
	"encoding/json"
	"fmt"
)

type Toy struct {
	Name  string
	Price float64
}

type Toys []interface{}

type Dog struct {
	Age  int
	Name string
	Toys Toys
}

type Cat struct {
	Age  int
	Name string
	Toys Toys
}

// struct can be used by pointer
func (cat Cat) String() string {
	var buffer bytes.Buffer
	for _, t := range cat.Toys {
		buffer.WriteString(fmt.Sprintf("[%+v]", t))
	}
	return fmt.Sprintf("Cat, toys: %s", buffer.String())
}

type Mouse struct {
	Age  int
	Name string
	Toys Toys
}

// pointer can't used by struct
func (mouse *Mouse) String() string {
	var buffer bytes.Buffer
	for _, t := range mouse.Toys {
		buffer.WriteString(fmt.Sprintf("[%+v]", t))
	}
	return fmt.Sprintf("Mouse, toys: %s", buffer.String())
}

type Pig struct {
	Age  int
	Name string
	Toys Toys
}

func (pig Pig) String() string {
	str, err := json.Marshal(pig)
	if err != nil {
		return "nil"
	}
	return string(str)
}
