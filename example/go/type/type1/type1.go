package main

import (
	"fmt"
)

type st struct {
	n int
}

func fint(n int) {
	n++
	fmt.Printf("int, n: %d\n", n) // 2
}

func fstruct(s st) {
	s.n++
	fmt.Printf("struct, st.n: %d\n", s.n) // 2
}

func fslice(s []int) {
	s[0]++
	fmt.Printf("slice, s[0]: %d\n", s[0]) // 2
}

func farray(a [1]int) {
	a[0]++
	fmt.Printf("array, s[0]: %d\n", a[0]) // 2
}

func fmap(m map[int]int) {
	m[0] = m[0] + 1
	fmt.Printf("map, m[0]: %d\n", m[0]) // 2
}

func main() {
	n := 1
	fint(n)
	fmt.Printf("main, n: %d\n", n) // 1

	st := st{
		n: 1,
	}
	fstruct(st)
	fmt.Printf("main, st.n: %d\n", st.n) // 1

	s := []int{1}
	fslice(s)
	fmt.Printf("main, s[0]: %d\n", s[0]) // 2

	a := [1]int{1}
	farray(a)
	fmt.Printf("main, a[0]: %d\n", a[0]) // 2

	m := make(map[int]int)
	m[0] = 1
	fmap(m)
	fmt.Printf("main, m[0]: %d\n", m[0]) // 2
}
