package main

import (
	"fmt"

	pb "../proto"

	"golang.org/x/net/context"

	"google.golang.org/grpc"
)

func main() {
	conn, err := grpc.Dial("localhost:8080", grpc.WithInsecure())
	if err != nil {
		fmt.Printf("dail fail, error: %s\n", err.Error())
		return
	}
	defer conn.Close()

	client := pb.NewArithClient(conn)

	for i := 0; i < 10; i++ {
		request := &pb.ArithRequest{
			X: uint32(i),
			Y: uint32(i + 1),
		}
		var operator string
		var response *pb.ArithResponse
		if i % 2 == 0 {
			operator = "+"
			response, err = client.Add(context.Background(), request)
		} else {
			operator = "x"
			response, err = client.Multiply(context.Background(), request)
		}
		if err != nil {
			fmt.Printf("RPC fail, error: %s\n", err.Error())
			return
		}
		fmt.Printf("Arith: %d %s %d = %d\n", request.X, operator, request.Y, response.Result)
	}
}
