package main

import (
	"fmt"
	"testing"

	pb "../proto"

	"golang.org/x/net/context"

	"google.golang.org/grpc"
)

func MakeClient() pb.ArithClient {
	conn, err := grpc.Dial("localhost:8080", grpc.WithInsecure())
	if err != nil {
		fmt.Printf("dail fail, error: %s\n", err.Error())
		return nil
	}
	client := pb.NewArithClient(conn)
	return client
}

func MultiplyByOneClient(client pb.ArithClient) {
	for i := 0; i < 10; i++ {
		request := &pb.ArithRequest{
			X: uint32(i),
			Y: uint32(i + 1),
		}
		_, err := client.Multiply(context.Background(), request)
		if err != nil {
			fmt.Printf("RPC fail, error: %s\n", err.Error())
			return
		}
	}
}

func BenchmarkMultiplyByMultiClient(b *testing.B) {
	for i := 0; i < b.N; i++ {
		client := MakeClient()
		MultiplyByOneClient(client)
	}
}

func BenchmarkMultiplyByOneClient(b *testing.B) {
	client := MakeClient()
	for i := 0; i < b.N; i++ {
		MultiplyByOneClient(client)
	}
}
