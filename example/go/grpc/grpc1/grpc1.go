package main

import (
	"fmt"
	"net"

	pb "../proto"

	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

type server struct{}

func (s *server) Add(ctx context.Context, request *pb.ArithRequest) (response *pb.ArithResponse, err error) {
	response = &pb.ArithResponse{Result: request.X + request.Y}
	return
}

func (s *server) Multiply(ctx context.Context, request *pb.ArithRequest) (response *pb.ArithResponse, err error) {
	response = &pb.ArithResponse{Result: request.X * request.Y}
	return
}

func main() {
	listener, err := net.Listen("tcp", ":8080")
	if err != nil {
		fmt.Printf("listen fail, error: %s\n", err.Error())
		return
	}
	gsvr := grpc.NewServer()
	defer gsvr.Stop()
	pb.RegisterArithServer(gsvr, &server{})
	gsvr.Serve(listener)
}
