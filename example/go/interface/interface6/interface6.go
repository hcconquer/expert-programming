package main

import (
	"fmt"
)

type Dog struct {
	name string
}

func (dog *Dog) Speak() string {
	return fmt.Sprintf("Dog(%s)", dog.name)
}

// v must a interface, or 
// invalid type assertion: v.(T) (non-interface type (type of v) on left)
func type1(v interface{}) {
	p, ok := v.(Dog)
	if ok {
		fmt.Printf("%+v is Dog\n", p)
	} else {
		fmt.Printf("%+v is not Dog\n", v)
	}
}

func type2(v interface{}) {
	p, ok := v.(*Dog)
	if ok {
		fmt.Printf("%+v is *Dog\n", p)
	} else {
		fmt.Printf("%+v is not *Dog\n", v)
	}
}

func main() {
	dog1 := &Dog{
		name: "dog1",
	}

	dog2 := Dog{
		name: "dog2",
	}

	type1(dog1) // &{name:dog1} is not Dog
	type1(dog2) // {name:dog2} is Dog

	type2(dog1) // &{name:dog1} is *Dog
	type2(dog2) // {name:dog2} is not *Dog
}
