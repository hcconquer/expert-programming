package main

import (
	"fmt"
)

type Counter interface {
	Add() int
}

type ValueCounter struct {
	counter int
}

func (counter ValueCounter) Add() int {
	counter.counter++
	return counter.counter
}

type PointerCounter struct {
	counter int
}

func (counter *PointerCounter) Add() int {
	counter.counter++
	return counter.counter
}

// value copy, pointer reference
// valueCounter: 101, pointerCounter: 201
// valueCounter: 101, pointerCounter: 202
// valueCounter: 101, pointerCounter: 203
func main() {
	var valueCounter Counter = ValueCounter{counter: 100}
	var pointerCounter Counter = &PointerCounter{counter: 200}
	for i := 0; i < 3; i++ {
		fmt.Printf("valueCounter: %d, pointerCounter: %d\n",
			valueCounter.Add(), pointerCounter.Add())
	}
}
