package main

import (
	"fmt"
)

type Pet interface {
	Speak() string
}

type Dog struct {
	name string
}

type Cat struct {
	name string
}

func (dog *Dog) Speak() string {
	return fmt.Sprintf("Dog(%s)", dog.name)
}

func (cat Cat) Speak() string {
	return fmt.Sprintf("Cat(%s)", cat.name)
}

// v must a interface, or 
// invalid type assertion: v.(T) (non-interface type (type of v) on left)
func type1(v interface{}) {
	p, ok := v.(Pet)
	if ok {
		fmt.Printf("%+v is Pet\n", p)
	} else {
		fmt.Printf("%+v is not Pet\n", v)
	}
}

func type2(v interface{}) {
	p, ok := v.(*Pet)
	if ok {
		fmt.Printf("%+v is *Pet\n", p)
	} else {
		fmt.Printf("%+v is not *Pet\n", v)
	}
}

// T is T, but *T is not *T
// *T is T, but T is not *T
func main() {
	dog1 := &Dog{
		name: "dog1",
	}

	dog2 := Dog{
		name: "dog2",
	}

	cat1 := new(Cat)
	cat1.name = "cat1"

	cat2 := Cat{
		name: "cat2",
	}

	type1(dog1) // &{name:dog1} is Pet
	type1(dog2) // {name:dog2} is not Pet

	type2(dog1) // &{name:dog1} is not *Pet
	type2(dog2) // {name:dog2} is not *Pet

	type1(cat1) // &{name:cat1} is Pet
	type1(cat2) // {name:cat2} is Pet

	type2(cat1) // &{name:cat1} is not *Pet
	type2(cat2) // {name:cat2} is not *Pet
}
