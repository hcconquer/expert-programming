package main

import (
	"time"
	"fmt"
	"sync"
	"sync/atomic"
)

type Account struct {
	balance int
	mutex   *sync.Mutex
}

func (account *Account) add1(num int) {
	account.balance += num
}

// it's golang recommend lock method
// it a little same with RAII
func (account *Account) add2(num int) {
	account.mutex.Lock()
	defer account.mutex.Unlock()
	account.balance += num
}

func test(lock bool) {
	account := new(Account)
	fmt.Printf("begin, balance: %d, lock: %t\n", account.balance, lock)

	if lock {
		account.mutex = new(sync.Mutex)
	}

	time1 := time.Now()

	coNum := 100
	coChan := make(chan int, coNum)
	for i := 0; i < coNum; i++ {
		coChan <- 1
	}
	woNum := 10000000
	done := make(chan int, 1)
	doneCount := int64(0)

	for i := 0; i < woNum; i++ {
		<-coChan
		go func(n int) {
			defer func() {
				cnt := atomic.AddInt64(&doneCount, 1)
				if cnt >= int64(woNum) {
					done <- 1
				}
			}()
			defer func() { coChan <- 1 }()
			if lock {
				account.add2(1)
			} else {
				account.add1(1)
			}
		}(i)
	}

	// wait for all done
	<-done

	time2 := time.Now()

	tm := time2.Sub(time1)

	fmt.Printf("done, balance: %d, time: %v(s)\n", account.balance, tm.Seconds())
}

// 1. has problem without lock;
// 2. mutex is not consume much time;
// begin, balance: 0, lock: false
// done, balance: 9699807, time: 3.769416424(s)
// begin, balance: 0, lock: true
// done, balance: 10000000, time: 3.7828977(s)
func main() {
	test(false)
	test(true)
}
