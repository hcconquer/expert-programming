package main

import (
	"net"
	"testing"

	"github.com/stretchr/testify/assert"
)

func GetIPs(host string) ([]string, error) {
	ips, err := net.LookupIP(host)
	if err != nil {
		return nil, err
	}
	addrs := []string{}
	for _, ip := range ips {
		addrs = append(addrs, ip.String())
	}
	return addrs, nil
}

func TestGetIPs(t *testing.T) {
	var cases = []struct {
		host string
	}{
		{"localhost"},
		{"stackexchange.com"},
	}
	for _, c := range cases {
		ips, err := GetIPs(c.host)
		assert.Nil(t, err)
		assert.NotNil(t, ips)
	}
}
