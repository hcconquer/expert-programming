package main

import (
	"bytes"
	"flag"
	"io"
	"io/ioutil"
	"math/rand"
	"testing"
	"time"
)

// make buf and warm-up
func setupReader(size int64) io.Reader {
	buf := make([]byte, size)
	rander := rand.New(rand.NewSource(time.Now().UnixNano()))
	for i := range buf {
		buf[i] = byte(i * rander.Int())
	}
	reader := bytes.NewReader(buf)
	return reader
}

// read and discard io until EOF
func readAndDiscard(r io.Reader, bufp *[]byte) (sum int64, err error) {
	readSize := 0
	for {
		readSize, err = r.Read(*bufp)
		sum += int64(readSize)
		if err != nil {
			if err == io.EOF {
				return sum, nil
			}
			return
		}
	}
}

// the only diff with readAndDiscard is make buf in func
func readAndDiscard2(r io.Reader, bufsize int64) (sum int64, err error) {
	buf := make([]byte, bufsize)
	readSize := 0
	for {
		readSize, err = r.Read(buf)
		sum += int64(readSize)
		if err != nil {
			if err == io.EOF {
				return sum, nil
			}
			return
		}
	}
}

// go test -v -benchmem -benchtime=60s -test.bench=".*"
// go test -v -benchmem -benchtime=60s -test.bench=".*" -total=33554432 -block=8192

var (
	total = flag.Int("total", 32<<20, "size of total")
	block = flag.Int("block", 1<<20, "size of block")
)

func BenchmarkReadAndDiscard(b *testing.B) {
	flag.Parse()
	reader := setupReader(int64(*total))
	buf := make([]byte, *block)
	for i := 0; i < b.N; i++ {
		readAndDiscard(reader, &buf)
	}
}

func BenchmarkReadAndDiscard2(b *testing.B) {
	flag.Parse()
	reader := setupReader(int64(*total))
	for i := 0; i < b.N; i++ {
		readAndDiscard2(reader, int64(*block))
	}
}

func BenchmarkIOUtilDiscard(b *testing.B) {
	flag.Parse()
	reader := setupReader(int64(*total))
	for i := 0; i < b.N; i++ {
		io.Copy(ioutil.Discard, reader)
	}
}
