package main

import (
	"bytes"
	"errors"
	"io"
	"io/ioutil"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

func TestReadAndDiscardSlice(t *testing.T) {
	var cases = []struct {
		bufSize   int64
		blockSize int64
	}{
		{1 << 20, 1024}, // read 1
		{1 << 20, 1},    // read 1 by 1
		{1 << 20, 128},  // align read
		{1 << 20, 10},   // not align
	}

	for _, c := range cases {
		wbuf := make([]byte, c.bufSize)
		reader := bytes.NewReader(wbuf)
		rbuf := make([]byte, c.blockSize)
		len, err := readAndDiscard(reader, &rbuf)
		if err != nil {
			t.Errorf("err: %s, len: %d, c: %+v", err, len, c)
		}
		if len != int64(c.bufSize) {
			t.Logf("len: %d, bufSize: %d, c: %+v", len, c.bufSize, c)
		}
	}
}

func TestReadAndDiscardHttp(t *testing.T) {
	var cases = []struct {
		bufSize   int64
		blockSize int64
	}{
		{1 << 20, 1024}, // read 1
		{1 << 20, 1},    // read 1 by 1
		{1 << 20, 128},  // align read
		{1 << 20, 10},   // not align
	}

	for _, c := range cases {
		ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(http.StatusOK)
			rbuf := make([]byte, c.blockSize)
			len, err := readAndDiscard(r.Body, &rbuf)
			if err != nil {
				t.Errorf("err: %s, len: %d, c: %+v", err, len, c)
			}
			if len != int64(c.bufSize) {
				t.Logf("len: %d, bufSize: %d, c: %+v", len, c.bufSize, c)
			}
		}))
		defer ts.Close()

		buf := make([]byte, c.bufSize)
		buffer := bytes.NewBuffer(buf)
		response, err := http.Post(ts.URL, "text/plain", buffer)
		if err != nil {
			t.Errorf("err: %s, c: %+v", err, c)
		}
		defer response.Body.Close()
		_, err = ioutil.ReadAll(response.Body)
		if err != nil {
			t.Errorf("err: %s, c: %+v", err, c)
		}
	}
}

func TestReadAndDiscardTimeout(t *testing.T) {
	var cases = []struct {
		bufSize   int64
		blockSize int64
		timeo     int64
		err       error
	}{
		{1 << 20, 1024, 1000, nil},                // read 1
		{1 << 20, 1, 1000, nil},                   // read 1 by 1
		{1 << 20, 128, 1000, nil},                 // align read
		{1 << 20, 10, 1000, nil},                  // not align
		{1 << 20, 0, 1000, errors.New("timeout")}, // read 0, so must timeout
	}

	for _, c := range cases {
		wbuf := make([]byte, c.bufSize)
		reader := bytes.NewReader(wbuf)
		rbuf := make([]byte, c.blockSize)
		len, err := readAndDiscardTimeout(reader, &rbuf, time.Duration(c.timeo)*time.Millisecond)
		if c.err != nil {
			if err == nil {
				t.Errorf("should be fail, c: %+v", c)
			}
		} else {
			if err != nil {
				t.Errorf("err: %s, len: %d, c: %+v", err, len, c)
			}
			if len != int64(c.bufSize) {
				t.Logf("len: %d, bufSize: %d, c: %+v", len, c.bufSize, c)
			}
		}
	}
}

func setupReader(size int64) io.Reader {
	buf := make([]byte, size)
	rander := rand.New(rand.NewSource(time.Now().UnixNano()))
	for i := range buf {
		buf[i] = byte(i * rander.Int())
	}
	reader := bytes.NewReader(buf)
	return reader
}

// go test -v -test.bench=".*"

// BenchmarkReadAndDiscard-4        2000000               962 ns/op
func BenchmarkReadAndDiscard(b *testing.B) {
	reader := setupReader(32 << 20)
	buf := make([]byte, 8192)
	for i := 0; i < b.N; i++ {
		readAndDiscard(reader, &buf)
	}
}

// BenchmarkIOUtilDiscard-4        50000000                26.9 ns/op
func BenchmarkIOUtilDiscard(b *testing.B) {
	reader := setupReader(32 << 20)
	for i := 0; i < b.N; i++ {
		io.Copy(ioutil.Discard, reader)
	}
}
