package main

import (
	"errors"
	"io"
	"time"
)

// read and discard io until EOF
func readAndDiscard(r io.Reader, bufp *[]byte) (sum int64, err error) {
	readSize := 0
	for {
		readSize, err = r.Read(*bufp)
		sum += int64(readSize)
		if err != nil {
			if err == io.EOF {
				return sum, nil
			}
			return
		}
	}
}

// read and discard io until EOF or timeout
func readAndDiscardTimeout(reader io.Reader, bufp *[]byte, timeo time.Duration) (int64, error) {
	done := make(chan struct {
		len int64
		err error
	}, 1)
	go func() {
		len, err := readAndDiscard(reader, bufp)
		done <- struct {
			len int64
			err error
		}{
			len: len,
			err: err,
		}
	}()

	select {
	case res := <-done:
		return res.len, res.err
	case <-time.After(timeo):
		return -1, errors.New("timeout")
	}
}
