package main

import (
	"log"
	"sync"
	"time"
)

func main() {
	log.Println("main start")
	var wg sync.WaitGroup
	for i := 0; i < 10; i++ {
		wg.Add(1)
		log.Printf("go: %d", i)
		go func(n int) {
			defer wg.Done()
			log.Printf("start: %d", n)
			time.Sleep(time.Second)
		}(i)
	}
	wg.Wait()
	log.Printf("main exit")
}
