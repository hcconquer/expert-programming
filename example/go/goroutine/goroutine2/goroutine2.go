package main

import (
	"log"
	"sync"
	"time"
)

func main() {
	log.Println("main start")
	var wg sync.WaitGroup
	ch := make(chan int, 3)
	for i := 0; i < 10; i++ {
		ch <- 1
		wg.Add(1)
		// log.Printf("go: %d", i)
		go func(n int) {
			defer func() {
				<-ch
				wg.Done()
			}()
			log.Printf("start: %d", n)
			time.Sleep(time.Second)
			log.Printf("done: %d", n)
		}(i)
	}
	wg.Wait()
	log.Printf("main exit")
}
