package main

import (
	"flag"

	"github.com/gographics/gmagick"
)

func resize(orig string, dest string) {
	mw := gmagick.NewMagickWand()
	defer mw.Destroy()
	mw.ReadImage(orig)
	filter := gmagick.FILTER_LANCZOS
	w := mw.GetImageWidth()
	h := mw.GetImageHeight()
	mw.ResizeImage(w/2, h/2, filter, 1)
	mw.WriteImage(dest)
}

// ./gmagick1 -from=../images/winter_is_coming.jpg -to=a.jpg
func main() {
	f := flag.String("from", "", "original image file ...")
	t := flag.String("to", "", "target file ...")
	flag.Parse()

	gmagick.Initialize()
	defer gmagick.Terminate()

	resize(*f, *t)
}
