package main

import (
	"fmt"
)

func main() {
	type Weekday int

	const (
		Sunday    Weekday = iota // 0
		Monday                   // 1
		Tuesday                  // 2
		Wednesday                // 3
		Thursday                 // 4
		Friday                   // 5
		Saturday                 //6
	)

	fmt.Printf("Sunday: %d, Monday: %d, Saturday: %d\n", Sunday, Monday, Saturday)

	type Flags uint

	const (
		FlagUp           Flags = 1 << iota // 1
		FlagBroadcast                      // 2
		FlagLoopback                       // 4
		FlagPointToPoint                   // 8
		FlagMulticast                      // 16
	)

	fmt.Printf("FlagUp: %d, FlagBroadcast: %d, FlagMulticast: %d\n", FlagUp, FlagBroadcast, FlagMulticast)

	const (
		_   = 1 << (10 * iota)
		KiB // 1024
		MiB // 1048576
		GiB // 1073741824
		TiB // 1099511627776             (exceeds 1 << 32)
		PiB // 1125899906842624
		EiB // 1152921504606846976
		ZiB // 1180591620717411303424    (exceeds 1 << 64)
		YiB // 1208925819614629174706176
	)

	fmt.Printf("KiB: %d, MiB: %d, GiB: %d\n", KiB, MiB, GiB)
}
