package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestArrayCopy(t *testing.T) {
	a1 := [...]int{0, 1, 2}
	a2 := a1
	a1[1] = 9
	assert.EqualValues(t, 1, a2[1])

	s1 := []int{0, 1, 2}
	s2 := s1
	s1[1] = 9
	assert.EqualValues(t, 9, s2[1])
}
