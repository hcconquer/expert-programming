// go test -bench=. -benchtime=5s -run=none

// BenchmarkCopy-4            200000000        8.70 ns/op
// BenchmarkBuffer-4          100000000        12.9 ns/op
// BenchmarkStringBuilder-4   100000000        27.1 ns/op
// BenchmarkConcat-4             100000       77679 ns/op

package main

import (
	"bytes"
	"strings"
	"testing"
)

var MagicString = "DJBX33A"

func BenchmarkCopy(b *testing.B) {
	bs := make([]byte, b.N*len(MagicString))
	bl := 0

	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		bl += copy(bs[bl:], MagicString)
	}
	b.StopTimer()

	if s := strings.Repeat(MagicString, b.N); string(bs) != s {
		b.Errorf("unexpected result; got=%s, want=%s", string(bs), s)
	}
}

func BenchmarkBuffer(b *testing.B) {
	var buffer bytes.Buffer

	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		buffer.WriteString(MagicString)
	}
	b.StopTimer()

	if s := strings.Repeat(MagicString, b.N); buffer.String() != s {
		b.Errorf("unexpected result; got=%s, want=%s", buffer.String(), s)
	}
}

func BenchmarkStringBuilder(b *testing.B) {
	var builder strings.Builder

	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		builder.WriteString(MagicString)
	}
	b.StopTimer()

	if s := strings.Repeat(MagicString, b.N); builder.String() != s {
		b.Errorf("unexpected result; got=%s, want=%s", builder.String(), s)
	}
}

func BenchmarkConcat(b *testing.B) {
	var str string

	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		str += MagicString
	}
	b.StopTimer()

	if s := strings.Repeat(MagicString, b.N); str != s {
		b.Errorf("unexpected result; got=%s, want=%s", str, s)
	}
}
