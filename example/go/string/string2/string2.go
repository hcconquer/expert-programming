package main

import (
	"log"
)

// https://golang.org/ref/spec#String_literals
func main() {
	// non-octal character in escape sequence: '
	array := []byte{/*'\0', */'\010', '\x10'}
	for index, value := range array {
		log.Printf("index: %v, value: %v", index, value)
	}
}
