package main

import (
	"fmt"
)

func main() {
	s1 := "12345"
	c1 := []byte(s1) // string --> []bytes
	c1[0] = 'c'
	s2 := string(c1)     // []bytes --> string
	s3 := string(c1[2:]) // []bytes --> string

	fmt.Printf("s1: %s\n", s1) // 12345
	fmt.Printf("s2: %s\n", s2) // c2345
	fmt.Printf("s3: %s\n", s3) // 345
}
