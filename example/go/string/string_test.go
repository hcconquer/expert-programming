package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSlice2String(t *testing.T) {
	slice1 := []byte{'0', '1', '2'}
	str1 := string(slice1)
	assert.Equal(t, str1, "012")
	slice1[1] = '9'
	t.Logf("%s", str1)
}
