package main

import (
	"reflect"
	"log"
	"time"
)

type Database interface {
	Commit()
}

type Mysql struct {
	Name string
}

func (mysql *Mysql) Commit() {
	log.Printf("[%s] Commit", mysql.Name)
}

type Oracle struct {
	Version uint32
	Name    string
	Build   time.Time
	Authors []string
}

func (oracle *Oracle) Commit() {
	log.Printf("[%s] Commit", oracle.Name)
}

func Info(object interface{}) {
	val := reflect.ValueOf(object)
	if val.Kind() == reflect.Ptr {
		log.Printf("it's ptr")
		val = reflect.Indirect(val)
	}
	t := val.Type()
	log.Printf("type: %v.%v", t.PkgPath(), t.Name())
	log.Printf("fields: %d", t.NumField())
	for i := 0; i < t.NumField(); i++ {
		f := t.Field(i)
		v := val.Field(i).Interface()
		log.Printf("field[%d], name: %s, type: %v, value: %v", i, f.Name, f.Type, v)
	}
}

func SetValue(object interface{}, field string, value interface{}) {
	val := reflect.ValueOf(object)
	val = reflect.Indirect(val)
	f := val.FieldByName(field)
	if !f.CanSet() {
		log.Printf("can't set")
	}

	// f.SetUint(uint64(10))
	v := reflect.ValueOf(value)
	v = reflect.Indirect(v)
	switch f.Kind() {
	case reflect.Int, reflect.Int32:
		rv := v.Convert(reflect.TypeOf(int64(0)))
		f.SetInt(rv.Int())
	case reflect.Uint, reflect.Uint32:
		rv := v.Convert(reflect.TypeOf(uint64(0)))
		f.SetUint(rv.Uint())
	case reflect.String:
		rv := v.Convert(reflect.TypeOf(""))
		f.SetString(rv.String())
	}
}

func main() {
	mysql := new(Mysql)
	mysql.Name = "MySQL Enterprise Edition"
	Info(mysql)

	oracle := new(Oracle)

	oracle.Version = 11
	oracle.Name = "Oracle Database 11g Release 1"
	oracle.Build = time.Now()
	oracle.Authors = []string{"Jack", "Ray"}
	Info(oracle)

	SetValue(oracle, "Name", "Oracle Database 12c")
	SetValue(oracle, "Version", uint64(12))
	oracle.Authors = []string{"Tom", "Jerry"}
	Info(oracle)
}
