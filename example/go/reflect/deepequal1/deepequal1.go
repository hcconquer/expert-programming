package main

import (
	"fmt"
	"reflect"
)

func main() {
	array1 := [...]string{"1", "2", "3"}
	array2 := [...]string{"1", "2", "3"}
	array3 := [...]string{"1", "3", "2"}
	slice1 := []string{"1", "2", "3"}

	fmt.Printf("equal(array1, array2) = %t\n", reflect.DeepEqual(array1, array2)) // true
	fmt.Printf("equal(array1, array3) = %t\n", reflect.DeepEqual(array1, array3)) // false
	fmt.Printf("equal(array1, slice1) = %t\n", reflect.DeepEqual(array1, slice1)) // false
}
