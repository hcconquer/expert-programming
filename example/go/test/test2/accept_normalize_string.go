package test

import (
	"strings"
)

// AcceptNormalize return normalized value for http header `Accept`, now just
// support image MIME types
func AcceptNormalize(s string) string {
	mimes := []string{"image/webp"}
	for _, mime := range mimes {
		if strings.Index(s, mime) >= 0 {
			return mime
		}
	}
	return ""
}
