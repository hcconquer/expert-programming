package test

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAcceptNormalize(t *testing.T) {
	var cases = []struct {
		acceptHeaderOrigin        string
		acceptHeaderNormalization string
	}{
		{"", ""},
		{"image/webp", "image/webp"},
		{"image/*", ""},
		{"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8", "image/webp"}, // Chrome
		{"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8", ""},                                 // Safari
		{"image/png,image/svg+xml,image/*;q=0.8,*/*;q=0.5", ""},                                                 // IE9
	}

	for _, c := range cases {
		acceptHeaderNormalization := AcceptNormalize(c.acceptHeaderOrigin)
		assert.Equal(t, c.acceptHeaderNormalization, acceptHeaderNormalization)
	}
}
