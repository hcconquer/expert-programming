package main

import (
	"flag"
	"log"
	"net/http"
	_ "net/http/pprof"
	"runtime"
	"time"
)

const (
	OP_SLEEP_WAIT   = 1
	OP_SLEEP_NOWAIT = 2
)

// go run time2.go -op=1
// Goroutine increase
// visit http://localhost:6060/debug/pprof/goroutine?debug=2 by browser
//
// can see many goroutine like this:
// 
// goroutine 8 [chan receive]:
// main.SleepWait(0x3c)
// ../go/src/time/time2/time2.go:21 +0x4e
// created by main.GoSleep
// ../go/src/time/time2/time2.go:42 +0xac
func SleepWait(timeSec int) {
	select {
	case <-time.After(time.Duration(timeSec) * time.Second):
		log.Printf("timeout")
	}
}

// go run time2.go -op=2
// Goroutine keep but timer leak
// `go tool pprof -top http://localhost:6060/debug/pprof/heap`
// 
// can see many timer like this:
// 
// Showing nodes accounting for 2054.75kB, 100% of 2054.75kB total
//       flat  flat%   sum%        cum   cum%
//  1536.10kB 74.76% 74.76%  1536.10kB 74.76%  time.NewTimer
//   518.65kB 25.24%   100%   518.65kB 25.24%  time.Sleep
//          0     0%   100%  1536.10kB 74.76%  main.SleepNoWait
//          0     0%   100%   518.65kB 25.24%  main.SleepNoWait.func1
//          0     0%   100%  1536.10kB 74.76%  time.After
func SleepNoWait(timeSec int) {
	ch := make(chan int, 1)
	go func() {
		time.Sleep(10 * time.Millisecond)
		ch <- 1
	}()
	select {
	case <-time.After(time.Duration(timeSec) * time.Second):
		log.Printf("timeout")
	case <-ch:
		log.Printf("done")
	}
}

func GoSleep(timeSec, op int) {
	switch op {
	case OP_SLEEP_WAIT:
		go SleepWait(timeSec)
	case OP_SLEEP_NOWAIT:
		go SleepNoWait(timeSec)
	default:
		log.Printf("not-implement")
	}
}

func main() {
	op := flag.Int("op", 0, "op")
	flag.Parse()

	log.Printf("op: %d", *op)

	go func() {
		err := http.ListenAndServe("localhost:6060", nil)
		if err != nil {
			log.Fatalf("ListenAndServe err: %s", err)
		}
	}()

	for {
		GoSleep(3600, *op)
		log.Printf("NumGoroutine: %d", runtime.NumGoroutine())
		time.Sleep(20 * time.Millisecond)
	}
}
