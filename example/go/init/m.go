package main

import (
	"log"
)

func init() {
	log.Printf("m.go:init 1st")
}

func dosth1() error {
	log.Printf("m.go:dosth1")
	return nil
}

func dosth2() error {
	log.Printf("m.go:dosth1")
	return nil
}

func main() {
	dosth1()
	dosth2()
}