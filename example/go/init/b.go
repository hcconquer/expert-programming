package main

import (
	"log"
)

func init() {
	log.Printf("b.go:init 1st")
}

func init() {
	log.Printf("b.go:init 2nd")
}
