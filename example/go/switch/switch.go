package main

import (
	"strings"
)

// Bin2Str show traditional switch
func Bin2Str(num int) string {
	switch num {
	case 0:
		return "0"
	case 1:
		return "1"
	default:
		return ""
	}
}

// Str2Bin show string switch
func Str2Bin(str string) int {
	switch str {
	case "0":
		return 0
	case "1":
		return 1
	default:
		return -1
	}
}

// GetLevel show range
func GetLevel(num int) byte {
	switch {
	case 90 <= num:
		return 'A'
	case 76 <= num && num < 90:
		return 'B'
	case 60 <= num && num < 76:
		return 'C'
	default:
		return 'D'
	}
}

// ParseArgs show break
func ParseArgs(arg, val string) string {
	switch arg {
	case "one":
		if len(val) >= 1 {
			break
		}
		return ""
	case "two":
		if len(val) >= 2 {
			break
		}
		return ""
	default:
		return ""
	}
	return "ok"
}

// GetType show type switch from value
func GetType(v interface{}) string {
	switch v.(type) {
	case bool:
		return "bool"
	case int:
		return "int"
	case float32:
		return "float"
	case float64:
		return "float"
	case string:
		return "string"
	case map[int]int:
		return "map"
	default:
		return ""
	}
}

// GetProvince show multiple cases
func GetProvince(city string) string {
	switch city {
	case "Beijing":
		return "Beijing"
	case "Shanghai":
		return "Shanghai"
	case "Guangzhou", "Shenzhen":
		return "Guangdong"
	default:
		return "unknown"
	}
}

// Decode show fallthrough
func Decode(packet string) string {
	index := strings.IndexByte(packet, ':')
	if index < 0 {
		return ""
	}
	protocol := packet[0:index]
	index = 0
	switch protocol {
	case "ip":
		index+=3
		fallthrough
	case "tcp", "udp":
		index+=4
		fallthrough
	case "http":
		index+=5
		fallthrough
	default:
		return packet[index:]
	}
}

func Plural(word string) string {
	switch word {
	case "man":
		return "men"
	case "woman":
		return "women"
	case "people", "fish", "sheep":
		return word
	default:
		return word + "s"
	}
}
