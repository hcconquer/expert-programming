package main

import "testing"

func TestBin2Str(t *testing.T) {
	var cases = []struct {
		num int
		str string
	}{
		{0, "0"},
		{1, "1"},
		{2, ""},
	}

	for _, c := range cases {
		str := Bin2Str(c.num)
		if str != c.str {
			t.Errorf("actual: %v, case: %+v", str, c)
		}
	}
}

func TestStr2Bin(t *testing.T) {
	var cases = []struct {
		str string
		num int
	}{
		{"0", 0},
		{"1", 1},
		{"2", -1},
	}

	for _, c := range cases {
		num := Str2Bin(c.str)
		if num != c.num {
			t.Errorf("actual: %v, case: %+v", num, c)
		}
	}
}

func TestGetLevel(t *testing.T) {
	var cases = []struct {
		num   int
		level byte
	}{
		{100, 'A'},
		{90, 'A'},
		{85, 'B'},
		{75, 'C'},
		{60, 'C'},
		{59, 'D'},
	}

	for _, c := range cases {
		level := GetLevel(c.num)
		if level != c.level {
			t.Errorf("actual: %v, case: %+v", level, c)
		}
	}
}

func TestParseArgs(t *testing.T) {
	var cases = []struct {
		arg string
		val string
		str string
	}{
		{"one", "1", "ok"},
		{"one", "", ""},
		{"two", "12", "ok"},
		{"two", "1", ""},
		{"three", "123", ""},
	}

	for _, c := range cases {
		str := ParseArgs(c.arg, c.val)
		if str != c.str {
			t.Errorf("actual: %s, case: %+v", str, c)
		}
	}
}

func TestGetType(t *testing.T) {
	b := false
	if GetType(b) != "bool" {
		t.Errorf("type fail, val: %+v", b)
	}

	n := 100
	if GetType(n) != "int" {
		t.Errorf("type fail, val: %+v", n)
	}

	s := "hello"
	if GetType(s) != "string" {
		t.Errorf("type fail, val: %+v", s)
	}

	f := 1.0
	if GetType(f) != "float" {
		t.Errorf("type fail, val: %+v", f)
	}

	a := [...]int{1, 2, 3}
	if GetType(a) != "" {
		t.Errorf("type fail, val: %+v", a)
	}

	m := map[int]int{1: 11, 2: 12, 3: 13}
	if GetType(m) != "map" {
		t.Errorf("type fail, val: %+v", m)
	}
}

func TestGetProvinces(t *testing.T) {
	var cases = []struct {
		city string
		province string
	}{
		{"Beijing", "Beijing"},
		{"Shanghai", "Shanghai"},
		{"Guangzhou", "Guangdong"},
		{"Shenzhen", "Guangdong"},
	}

	for _, c := range cases {
		province := GetProvince(c.city)
		if province != c.province {
			t.Errorf("actual: %s, case: %+v", province, c)
		}
	}
}

func TestDecode(t *testing.T) {
	var cases = []struct {
		packet string
		content string
	}{
		{"http:h1", "h1"},
		{"tcp:http:t2", "t2"},
		{"ip:tcp:http:i3", "i3"},
		{"ip:udp:http:i4", "i4"},
	}

	for _, c := range cases {
		content := Decode(c.packet)
		if content != c.content {
			t.Errorf("actual: %s, case: %+v", content, c)
		}
	}
}
