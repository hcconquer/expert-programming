package main

import (
	"testing"
	"fmt"
	"database/sql"

	"gopkg.in/DATA-DOG/go-sqlmock.v1"
)

func getRowNum(rows *sql.Rows) int {
	rowNum := 0
	for rows.Next() {
		err := rows.Scan()
		if err != nil {
			rowNum++
		}
	}
	return rowNum
}

func TestMultiSelect(t *testing.T) {
	db, sqlMock, err := sqlmock.New()
	if err != nil {
		t.Errorf("An error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	columns := []string{"id", "status"}
	sqlRegexStr := "SELECT (.+) FROM orders"

	sqlMock.ExpectQuery(sqlRegexStr).WillReturnRows(sqlmock.NewRows(columns).AddRow(1, 1))
	rows, err := db.Query("SELECT id, status FROM orders WHERE id=$1", 1)
	defer rows.Close()
	rowNum := getRowNum(rows)
	if rowNum != 1 {
		t.Errorf("row num mismatch, expect: %d, real: %d", 1, rowNum)
	}

	sqlMock.ExpectQuery(sqlRegexStr).WillReturnRows(sqlmock.NewRows(columns).AddRow(1, 2).AddRow(1, 3))
	rows, err = db.Query("SELECT id, status FROM orders WHERE id=$1", 1)
	rowNum = getRowNum(rows)
	if rowNum != 2 {
		t.Errorf("row num mismatch, expect: %d, real: %d", 2, rowNum)
	}

	sqlMock.ExpectQuery(sqlRegexStr).WillReturnError(fmt.Errorf("failed"))
	_, err = db.Query("SELECT id, status FROM orders WHERE id=$1", 1)
	if err != nil {
		t.Errorf("error: %v", err)
	} else {
		t.Logf("error should be not nil")
	}
}

func TestMultiSelectSqlNotMatch(t *testing.T) {
	db, sqlMock, err := sqlmock.New()
	if err != nil {
		t.Errorf("An error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	columns := []string{"id", "status"}
	sqlRegexStr := "SELECT id, status FROM orders"

	// FIXME: sql not match
	sqlMock.ExpectQuery(sqlRegexStr).WillReturnRows(sqlmock.NewRows(columns).AddRow(1, 1))
	_, err = db.Query("SELECT id FROM orders WHERE id=$1", 1)
	if err != nil {
		t.Errorf("err: %v", err)
	}
}

func TestMultiSelectBadResueQuery(t *testing.T) {
	db, sqlMock, err := sqlmock.New()
	if err != nil {
		t.Errorf("An error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	columns := []string{"id", "status"}
	sql := "SELECT id, status FROM orders WHERE"
	mockQuery := sqlMock.ExpectQuery(sql)

	mockQuery.WillReturnRows(sqlmock.NewRows(columns).AddRow(1, 1))
	_, err = db.Query("SELECT id, status FROM orders WHERE id=$1", 1)
	if err != nil {
		t.Errorf("Query error: %v", err)
	}

	// FIXME: can't reuse ExpectQuery
	mockQuery.WillReturnRows(sqlmock.NewRows(columns).AddRow(1, 2))
	_, err = db.Query("SELECT id, status FROM orders WHERE id=$1", 1)
	if err != nil {
		t.Errorf("Query error: %v", err)
	}
}

func TestMultiSelectBadReuseRows(t *testing.T) {
	db, sqlMock, err := sqlmock.New()
	if err != nil {
		t.Errorf("An error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	columns := []string{"id", "status"}
	sqlRegexStr := "SELECT id, status FROM orders WHERE"

	mockRows := sqlmock.NewRows(columns).AddRow(1, 1).AddRow(1, 2)
	sqlMock.ExpectQuery(sqlRegexStr).WillReturnRows(mockRows)
	rows, err := db.Query("SELECT id, status FROM orders WHERE id=$1", 1)
	defer rows.Close()
	rowNum := getRowNum(rows)
	if rowNum != 2 {
		t.Errorf("row num mismatch, expect: %d, real: %d", 1, rowNum)
	}

	sqlMock.ExpectQuery(sqlRegexStr).WillReturnRows(sqlmock.NewRows(columns).AddRow(1, 3))
	rows, err = db.Query("SELECT id, status FROM orders WHERE id=$1", 1)
	rowNum = getRowNum(rows)
	if rowNum != 1 {
		t.Errorf("row num mismatch, expect: %d, real: %d", 2, rowNum)
	}

	// FIXME: can't reuse rows
	mockRows = mockRows.AddRow(1, 4)
	sqlMock.ExpectQuery(sqlRegexStr).WillReturnRows(mockRows)
	rows, err = db.Query("SELECT id, status FROM orders WHERE id=$1", 1)
	rowNum = getRowNum(rows)
	if rowNum != 1 && rowNum != 3 {
		t.Errorf("row num mismatch, expect: %d, real: %d", 3, rowNum)
	}
}
