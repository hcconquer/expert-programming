package main

import (
	"encoding/json"
	"log"
)

type Person struct {
	Name string `json:"name"`
	Age  int
}

type Student struct {
	Major string
	Grade int `json:"grade"`
}

type Employee struct {
	Title  string
	Salary float64 `json:"salary"`
}

type Man struct {
	Person
	Student `json:"stu"`
	Emp     Employee
}

func main() {
	man := Man{
		Person: Person{
			Name: "tom",
			Age : 30,
		},
		Student: Student{
			Major: "Software",
			Grade: 3,
		},
		Emp: Employee{
			Title: "Senior Manager",
			Salary: 2.7182,
		},
	}
	log.Printf("man: %+v", man)

	jsonBytes, _ := json.Marshal(man)
	log.Printf("bytes: %s", jsonBytes)
}

// cannot use promoted field Person.Name in struct literal of type Student
