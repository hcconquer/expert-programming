package main

import (
	"encoding/json"
	"fmt"
)

type Pet struct {
	Name string `json:"name"`
}

type Student struct {
	Name   string  `json:"name"`
	Age    int     `json:"age"`
	Zodiac int     `json:"zodiac,omitempty"`
	Weight float64 `json:"weight"`
	Scores []int   `json:"scores"`
	Dog    Pet     `json:"dog"`
	Cat    *Pet    `json:"cat"`
	Pets   []Pet   `json:"pets"`
}

func main() {
	var stu1 Student = Student{
		Name:   "tom",
		Age:    22,
		Scores: []int{100, 99, 98},
		Dog: Pet{
			"dog",
		},
		Cat: &Pet{
			"cat",
		},
		Pets: []Pet{
			Pet{"pig"},
		},
	}
	str, err := json.Marshal(stu1)
	if err != nil {
		fmt.Println(err)
	}
	// {"name":"tom","age":22,"weight":0,"scores":[100,99,98],"dog":{"name":"dog"},"cat":{"name":"cat"},"pets":[{"name":"pig"}]}
	fmt.Println(string(str))

	var stu2 Student
	str = []byte(`{"name":"tom","age":22,"scores":[100,99,98],"dog":{"name":"dog"},"cat":{"name":"cat"},"pets":[{"name":"pig"}]}`)
	err = json.Unmarshal(str, &stu2)
	if err != nil {
		fmt.Printf("Unmarshal fail, error: %s\n", err)
	}
	// {Name:tom Age:22 Weight:0 Scores:[100 99 98] Dog:{Name:dog} Cat:0xc42007c450 Pets:[{Name:pig}]}
	fmt.Printf("%+v\n", stu2)
}
