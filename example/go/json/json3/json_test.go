package main

import (
	"bytes"
	"encoding/json"
	"testing"
)

func TestInvalidJsonToEmptyObject(t *testing.T) {
	var cases = []struct {
		input  []byte
		result bool
	}{
		{nil, false},
		{[]byte(""), false},
		{[]byte("{}"), true},             // object
		{[]byte(`{"name":"tom"}`), true}, // object
		{[]byte("[]"), true},             // array
		{[]byte("1"), true},              // value
	}

	var empty interface{}
	for _, c := range cases {
		err := json.Unmarshal(c.input, &empty)
		if c.result {
			if err != nil {
				t.Errorf("json.Unmarshal error: %s, c: %+v", err, c)
			}
		} else {
			if err != nil {
				t.Logf("json.Unmarshal error: %s, c: %+v", err, c)
			} else {
				t.Errorf("json.Unmarshal success from invalid json, c: %+v", c)
			}
		}
	}
}

type Student struct {
	Name   string  `json:"name,omitempty"`
	Age    int     `json:"age"`
	Zodiac int     `json:"zodiac,omitempty"`
	Weight float64 `json:"weight,omitempty"`
}

func TestJsonOmitEmpty(t *testing.T) {
	var cases = []struct {
		student   Student
		jsonBytes []byte
	}{
		{Student{Zodiac: 0}, []byte(`{"age":0}`)},
	}

	// var empty interface{}
	for _, c := range cases {
		jsonBytes, err := json.Marshal(c.student)
		if err != nil {
			t.Errorf("json.Marshal error: %s, c: %+v", err, c)
		}
		if !bytes.Equal(jsonBytes, c.jsonBytes) {
			t.Errorf("json.Marshal not equal, c: %+v, real: %s", c, jsonBytes)
		}
	}
}
