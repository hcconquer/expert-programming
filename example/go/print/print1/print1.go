package main

import (
	"fmt"
)

type Pet struct {
	Name string
}

type Student struct {
	Name   string
	Age    int
	Weight float64
	Scores []int
	Dog    Pet
	Cat    *Pet
	Pets   []Pet
}

func main() {
	var stu Student = Student{
		Name:   "tom",
		Age:    22,
		Scores: []int{100, 99, 98},
		Dog: Pet{
			"dog",
		},
		Cat: &Pet{
			"cat",
		},
		Pets: []Pet{
			Pet{"pig"},
		},
	}
	// {tom 22 0 [100 99 98] {dog} 0xc42007c450 [{pig}]}
	fmt.Printf("%v\n", stu)
	// {Name:tom Age:22 Weight:0 Scores:[100 99 98] Dog:{Name:dog} Cat:0xc42007c450 Pets:[{Name:pig}]}
	fmt.Printf("%+v\n", stu)
	// main.Student{Name:"tom", Age:22, Weight:0, Scores:[]int{100, 99, 98}, Dog:main.Pet{Name:"dog"}, Cat:(*main.Pet)(0xc42007c450), Pets:[]main.Pet{main.Pet{Name:"pig"}}}
	fmt.Printf("%#v\n", stu)
	// main.Student
	fmt.Printf("%T\n", stu)
}
