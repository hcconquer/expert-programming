#include <stdint.h>
#include <stdio.h>

#define STACK_SIZE 80

#define OFFSET(p, b) ((ssize_t)((size_t)(p) - (size_t)(b)))
#define OFFOF(p, n) ((typeof(p))((char *)(p) + (n)))

int *pa, *pb, *pn;
uint8_t *p;

int midx = 0;
uint8_t mem[STACK_SIZE];

void pstack(void *base) {
    printf("*%p(%zd): %d\n", base, sizeof(int), *(int *)base);
    base = OFFOF(base, sizeof(int));
    printf("*%p(%zd): %d\n", base, sizeof(int), *(int *)base);
    base = OFFOF(base, sizeof(int));
    printf("*%p(%zd): %d\n", base, sizeof(int), *(int *)base);
    base = OFFOF(base, sizeof(int));
    // printf("*%p(%zd): %zx, p-ps: %zd\n", base, sizeof(size_t), *(size_t
    // *)base,
    //        *(size_t *)base - (size_t)ps);
    base = OFFOF(base, sizeof(size_t));
    printf("*%p(%zd): %zx\n", base, sizeof(size_t), *(size_t *)base);
    base = OFFOF(base, sizeof(size_t));
    printf("*%p(%zd): %zx\n", base, sizeof(size_t), *(size_t *)base);
    base = OFFOF(base, sizeof(size_t));
    printf("*%p(%zd): %zx\n", base, sizeof(size_t), *(size_t *)base);
    base = OFFOF(base, sizeof(size_t));
    printf("*%p(%zd): %zx\n", base, sizeof(size_t), *(size_t *)base);
}

int sum(int a, int b) {
    pa = &a;
    pb = &b;
    // while (num-- >= 0) {
    //     printf("p: %p, *p: %d, %x\n", pa + num, *(pa + num), *(pa + num));
    // }
    int n = 0x6167;
    pn = &n;
    // printf("a: %d, &a: %p, b: %d, &b: %p, s: %d, &s: %p\n", a, &a, b, &b, n,
    // pn); pstack(pn);
    return n;
}

// int empty() {
//     return 11;
// }

/*
0x00 0x12345678

0x00 0x78
0x01 0x56
0x02 0x34
0x03 0x12
*/

int main() {
    printf("main: %p, &main: %p\n", main, &main);
    printf("pstack: %p, &pstack: %p\n", pstack, &pstack);
    // printf("empty: %p, &empty: %p\n", empty, &empty);
    printf("sum: %p, &sum: %p\n", sum, &sum);
    int s = 0x3741;
    // ps = (uint8_t *)&s;
    // for (midx = 0; midx < STACK_SIZE; midx++) {
    //     mem[midx] = 0x00;
    // }
    s = sum(0x1317, 0x2329);

    p = (uint8_t *)pn;
    for (midx = 0; midx < STACK_SIZE; midx++) {
        mem[midx] = p[midx];
    }

    // ps++;
    // for (midx = 0; midx < STACK_SIZE; midx++) {
    //     mem[midx] = *ps;
    //     ps--;
    // }
    // printf("s: %d, &s: %p\n", s, &s);
    // printf("pa-pb: %zd\n", OFFSET(pa, pb));
    // printf("ps-pb: %zd\n", OFFSET(&s, pb));
    // ps = (uint8_t *)&s;
    // ps++;
    p = (uint8_t *)pn;
    printf("pn: %p, *pn: %08x\n", pn, *pn);
    for (midx = 0; midx < STACK_SIZE; midx++) {
        printf("*%p(%zd): %d[0x%02x]\n", p + midx, sizeof(uint8_t), mem[midx],
               mem[midx]);
    }
    // return empty();
    return 0;
}

/*
(int)s=5

(ptr)
(int)a=2
(int)b=3
(int)n=5
*/