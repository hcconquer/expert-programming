/*
 * glog_sighandler_fix.cc
 *
 *  Created: May 23, 2017
 *      Author: hanchen
 */

#include <glog/logging.h>
#include <glog/raw_logging.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    google::InitGoogleLogging(argv[0]);
    google::SetStderrLogging(google::INFO);

    LOG(FATAL) << "fatal error!";

    LOG(INFO) << "Exit!";

    return EXIT_SUCCESS;
}
