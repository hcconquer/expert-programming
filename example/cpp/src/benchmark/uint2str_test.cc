#include <benchmark/benchmark.h>
#include <sstream>

std::string uint2str(unsigned int num) {
    std::ostringstream oss;
    oss << num;
    return oss.str();
}

static void Benchmark_uint2str(benchmark::State& state) {
    unsigned int num = 1234;
    while (state.KeepRunning()) {
        (void)uint2str(num);
    }
}

// Register the function as a benchmark
BENCHMARK(Benchmark_uint2str);

BENCHMARK_MAIN();
