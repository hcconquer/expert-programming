// #include <gtest/gtest.h>
#include <cassert>
#include <iostream>

#include <benchmark/benchmark.h>

class ScopeGuard {
  public:
    template <class Callable>
    ScopeGuard(Callable&& fn) : _fn(std::forward<Callable>(fn)) {
    }

    ScopeGuard(ScopeGuard&& other) : _fn(std::move(other._fn)) {
        other._fn = nullptr;
    }

    ~ScopeGuard() {
        if (_fn) _fn();
    }

    ScopeGuard(const ScopeGuard&) = delete;
    void operator=(const ScopeGuard&) = delete;

  private:
    std::function<void()> _fn;
};

static void Benchmark_no_defer(benchmark::State& state) {
    unsigned int num = 0;
    while (state.KeepRunning()) {
        [&num](...) { num++; }();
    }
    assert(num > 0);
}

static void Benchmark_raii_defer(benchmark::State& state) {
    unsigned int num = 0;
    while (state.KeepRunning()) {
        ScopeGuard __defer0 = [&num](...) { num++; };
    }
}

static void Benchmark_shared_ptr_defer(benchmark::State& state) {
    unsigned int num = 0;
    while (state.KeepRunning()) {
        std::shared_ptr<void> __defer0(nullptr, [&num](...) { num++; });
    }
}

static void Benchmark_unique_ptr_defer(benchmark::State& state) {
    unsigned int num = 0;
    while (state.KeepRunning()) {
        auto deleter = [&num](...) { num++; };
        std::unique_ptr<void, decltype(deleter)> __defer0(nullptr, deleter);
    }
}

// Register the function as a benchmark
BENCHMARK(Benchmark_no_defer);
BENCHMARK(Benchmark_raii_defer);
BENCHMARK(Benchmark_shared_ptr_defer);
BENCHMARK(Benchmark_unique_ptr_defer);

BENCHMARK_MAIN();
