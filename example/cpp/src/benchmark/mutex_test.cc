#include <pthread.h>

#include <benchmark/benchmark.h>

pthread_mutex_t mutex;

int mutex_init() {
    return pthread_mutex_init(&mutex, NULL);
}

int mutex_lock() {
    return pthread_mutex_lock(&mutex);
}

int mutex_unlock() {
    return pthread_mutex_unlock(&mutex);
}

static void Benchmark_mutex(benchmark::State& state) {
    mutex_init();
    while (state.KeepRunning()) {
        mutex_lock();
        mutex_unlock();
    }
}

// Register the function as a benchmark
BENCHMARK(Benchmark_mutex);

BENCHMARK_MAIN();
