#include <iostream>

int main() {
    int* array = new int[3];
    std::cout << "num: " << *array << std::endl;
    delete[] array;
    // FIXME: AddressSanitizer: heap-use-after-free
    std::cout << "num: " << *array << std::endl;
    return 0;
}
