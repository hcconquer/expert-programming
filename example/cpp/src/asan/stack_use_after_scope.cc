// it's seem not work in macOS
// it will cause `Segmentation fault: 11`

#include <iostream>

int main(int argc, char** argv) {
    int *pnum = NULL;
    if (argc < 1) {
        int num = 3;
        pnum = &num;
        std::cout << "num: " << *pnum << std::endl;
    }
    // FIXME: AddressSanitizer: SEGV on unknown address
    std::cout << "num: " << *pnum << std::endl;

    return 0;
}
