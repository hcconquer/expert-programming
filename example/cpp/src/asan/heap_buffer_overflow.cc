#include <iostream>

int main() {
    int* array = new int[3];
    std::cout << "num: " << array[0] << std::endl;
    // FIXME: AddressSanitizer: heap-buffer-overflow
    std::cout << "num: " << array[5] << std::endl;
    return 0;
}
