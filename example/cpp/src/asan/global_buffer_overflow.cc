#include <iostream>

int array[] = {1, 2, 3};

int main() {
    std::cout << "num: " << array[0] << std::endl;
    // FIXME: AddressSanitizer: global-buffer-overflow
    std::cout << "num: " << array[5] << std::endl;
    return 0;
}
