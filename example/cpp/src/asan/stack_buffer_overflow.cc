#include <iostream>

int main() {
    int array[] = {1, 2, 3};
    std::cout << "num: " << array[0] << std::endl;
    // FIXME: AddressSanitizer: stack-buffer-overflow
    std::cout << "num: " << array[5] << std::endl;
    return 0;
}
