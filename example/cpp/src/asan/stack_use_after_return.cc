// does not attempt to detect these bugs by default, need to run the test with
// ASAN_OPTIONS
//
// export ASAN_OPTIONS=detect_stack_use_after_return=1; ./stack_use_after_return
//
// it's seem not work in macOS

#include <iostream>

int *g_array;

void new_array(int **parray) {
    int array[] = {1, 2, 3};

    // TODO: address of stack memory associated with local variable 'array'
    // returned return array;

    *parray = array;
    g_array = array;
}

int main() {
    int *array = NULL;
    new_array(&array);
    std::cout << "num: " << array[1] << std::endl;
    std::cout << "num: " << g_array[1] << std::endl;
    return 0;
}
