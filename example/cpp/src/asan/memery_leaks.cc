// LeakSanitizer is enabled by default in ASan builds of x86_64 Linux, and can
// be enabled with `ASAN_OPTIONS=detect_leaks=1` on x86_64 macOS

// export ASAN_OPTIONS=detect_leaks=1; ./memery_leaks
// but it's seem not work in macOS

#include <iostream>

void new_array() {
    int* array = new int[3];
    array[0] = 1;
}

int main(int argc, char** argv) {
    new_array();

    return 0;
}
