// gcc -E macro.cpp

#define FLOG1(level)  LOG(level) << #level << " [" << __FILE__ << ":" << __LINE__ << "] "

#define FLOG2(level, fmt_str)  FLOG1(level) << boost::format(fmt_str)

#define GetFLogger(_1, _2, NAME, ...) NAME

#define XLOG(...) GetXLogger(__VA_ARGS__, FLOG2, FLOG1, ...)(__VA_ARGS__)

#define FLOG(...) GetFLogger(__VA_ARGS__, FLOG2, FLOG1, ...)(__VA_ARGS__)

int main() {
    XLOG(DEBUG) << "test";
    XLOG(DEBUG, INFO) << "test";
    XLOG(DEBUG, INFO, FATAL) << "test";

    FLOG(DEBUG) << "test";
    FLOG(DEBUG, INFO) << "test";
    FLOG(DEBUG, INFO, FATAL) << "test";
}
