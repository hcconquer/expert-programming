#include <iostream>
#include <memory>

class Test;
std::shared_ptr<Test> gptr;

class Test {
public:
    ~Test() {
        std::cout << "destroy" << std::endl;
        auto ptr = gptr;
    }
};

int main(int argc, char* argv[]) {
    gptr = std::make_shared<Test>();
    return 0;
}
