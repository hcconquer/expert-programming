#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>

int SetNonBlock(int fd) {
    int flags = fcntl(fd, F_GETFL, 0);
    if (flags < 0) {
        return -1;
    }
    flags = flags | O_NONBLOCK | O_NDELAY;
    flags = fcntl(fd, F_SETFL, flags);
    if (flags < 0) {
        return -1;
    }
    return 0;
}

int main(int argc, char *argv[]) {
    // if not set nonblocking, loopcnt must be 1
    // if set nonblocking, loopcnt must be great then 1
    // SetNonBlock(STDIN_FILENO);
    int loopcnt = 0;
    char buf[16];
    while (1) {
        loopcnt++;
        int nbytes = read(STDIN_FILENO, buf, sizeof(buf));
        if (nbytes > 0) {
            break;
        }
    }
    printf("input: %s, loopcnt: %d\n", buf, loopcnt);
    return 0;
}
