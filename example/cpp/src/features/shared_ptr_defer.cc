#include <iostream>
#include <memory>

void shared_ptr_defer(int* num) {
    std::shared_ptr<void> __defer0(nullptr, [&num](...) { (*num)++; });
}

int main(int argc, char* argv[]) {
    int num = 0;

    shared_ptr_defer(&num);
    std::cout << "num: " << num << std::endl;
}
