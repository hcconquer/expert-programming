#include <iostream>

void print(const char* name, int* arr, int len) {
    std::cout << name << ":";
    while (len-- > 0) {
        std::cout << " " << *arr;
        arr++;
    }
    std::cout << std::endl;
}

#define NEW_NUM 7

int main(int argc, char* argv[]) {
    int* p1 = new int(NEW_NUM);  // alloc an integer and init with param
    int* p2 = new int[NEW_NUM];  // alloc a memory with NEW_NUM len
    print("p1", p1, NEW_NUM);
    print("p2", p2, NEW_NUM);
    return 0;
}
