#include <math.h>   /* sin */
#include <stdio.h>  /* printf, fgets */
#include <stdlib.h> /* atof */
#include <string>   /* std::stof */

int main(int argc, char* argv[]) {
    char strs[][32] = {
        "3.1415926535",  // 3.141593, pos: 12
        "3.14",          // 3.140000, pos: 4
        "a.14",          // 0.000000, fail
        "3.a4",          // 3.000000, pos: 2
        "3.1a",          // 3.100000, pos: 3
    };
    int len = sizeof(strs) / sizeof(strs[0]);
    std::size_t pos = 0;
    for (int i = 0; i < len; i++) {
        printf("atof(%s) = %f\n", strs[i], atof(strs[i]));
        float value = 0;
        try {
            value = std::stof(strs[i], &pos);
        } catch (...) {
            printf("std::atof(%s) fail\n", strs[i]);
            continue;
        }
        printf("std::atof(%s) = %f, pos: %zu\n", strs[i], value, pos);
    }
    return 0;
}
