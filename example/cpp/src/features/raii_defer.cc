#include <iostream>
#include <memory>

class Defer {
  public:
    template <class Callable>
    Defer(Callable&& fn) : fn_(std::forward<Callable>(fn)) {
    }

    Defer(Defer&& other) : fn_(std::move(other.fn_)) {
        other.fn_ = nullptr;
    }

    ~Defer() {
        if (fn_) fn_();
    }

    Defer(const Defer&) = delete;
    void operator=(const Defer&) = delete;

  private:
    std::function<void()> fn_;
};

void raii_defer(int* num) {
    std::shared_ptr<void> __defer0(nullptr, [&num](...) { (*num)++; });
}

int main(int argc, char* argv[]) {
    int num = 0;

    raii_defer(&num);
    std::cout << "num: " << num << std::endl;
}
