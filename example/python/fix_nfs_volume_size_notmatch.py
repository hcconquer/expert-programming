# -*- coding: utf-8 -*-

from zbs.lib import logger
from zbs.lib.utils import get_meta_ip
from zbs.lib.parser import NFSParser
from zbs.nfs.client import ZbsNFS
from zbs.proto.meta_pb2 import FILE
from zbs.proto.meta_pb2 import DIR
from zbs.deps.tabulate import tabulate


def get_volume_size_min(filesize):
    if filesize <= 0:
        return 0
    VOLUME_SIZE_MIN = (1 << 28)  # 256M
    num = int((filesize - 1) / VOLUME_SIZE_MIN) + 1
    return num * VOLUME_SIZE_MIN


def volume_resize(client, pool_name, volume_name, nsize):
    client.volume_resize(pool_name, volume_name, nsize)


def dir_check_size(client, pool_name, dir_id, fix=False):
    inodes = client.inode_list(dir_id)
    for inode in inodes.inodes:
        if inode.attr.type == FILE:
            sizeinfo = client.size_info_get(inode.id)
            volume_size_min = get_volume_size_min(sizeinfo.size)
            if volume_size_min > sizeinfo.volume_size:
                print tabulate([[pool_name, inode.id, inode.name,
                                 str(sizeinfo.size), str(sizeinfo.volume_size),
                                 str(volume_size_min)]],
                               tablefmt='orgtbl')
                if fix:
                    volume_name = inode.volume_id[:18]
                    volume_resize(client, pool_name, volume_name,
                                  volume_size_min)
        elif inode.attr.type == DIR:
            dir_check_size(client, pool_name, inode.id, fix)


def volume_check_size(args):
    client = ZbsNFS(meta_ip=get_meta_ip(args.config_file, args.meta_ip),
                    meta_port=int(args.meta_port))
    print tabulate([['pool_name', 'inode_id', 'inode_name',
                     'file_size', 'volume_size', 'volume_size_min']],
                   tablefmt='grid')
    fix = (args.fix == str(True))
    exports = client.export_list()
    for export in exports.pools:
        dir_id = export.id[:18]
        dir_check_size(client, export.name, dir_id, fix)


def add_volume_cmd(parser):
    cmd = parser.add_cmd('volume', help='Commands to manage volume.')
    subcmd = cmd.add_subcmd('check_size', volume_check_size,
                            help="Check volume size.")
    subcmd.add_argument('--fix', default=False, help='Fix volume size.')


def create_parser():
    parser = NFSParser()
    add_volume_cmd(parser)

    return parser


def main():
    parser = create_parser()
    args = parser.parse()

    logger.init_logger(args.verbose)

    parser.run()


if __name__ == "__main__":
    main()
    