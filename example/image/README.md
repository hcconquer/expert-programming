# image

## catalog

[TOC]

## image info

| image                        | 特点              | 大小(KB) | 分辨率    |
| ---------------------------- | ----------------- | -------- | --------- |
| scarlett-johansson.jpg       | 人像              | 80       | 700x934   |
| plumbaginoides.jpg           | 植物, 超大图      | 5508     | 4288x2848 |
| panda-face.png               | 透明通道, 头像    | 36       | 316x336   |
| bicycle.gif                  | 动图              | 768      | 1280x720  |
| animal-emoji-ceramic-mug.jpg | iphone, 旋转，GPS | 1344     | 3024x4032 |
