package com.zheezes.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;

/**
 * for catch exception
 * 
 * @author hcconquer@gmail.com
 */
public class ZBufferedReader extends BufferedReader {
	private Exception exception = null;

	public ZBufferedReader(Reader in) {
		super(in);
	}

	public Exception getException() {
		return exception;
	}

	public Exception takeException() {
		Exception e = exception;
		exception = null;
		return e;
	}

	@Override
	public String readLine() {
		String line = null;
		try {
			line = super.readLine();
		} catch (IOException e) {
			exception = e;
		}
		return line;
	}
}
