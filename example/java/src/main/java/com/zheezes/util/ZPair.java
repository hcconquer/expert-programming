package com.zheezes.util;

/**
 * @author hcconquer@gmail.com
 *
 * @param <K>
 * @param <V>
 */
public class ZPair <K, V> {
	private K k;
	private V v;

	public ZPair() {
	}

	public ZPair(K k, V v) {
		this.k = k;
		this.v = v;
	}
	
	@SuppressWarnings("unchecked")
	private <T> T get(int index, T t) {
		if (index == 0) {
			return (T) k;
		} if (index == 1) {
			return (T) v;
		}
		return null;
	}
	
	private static <K, V, T> ZPair<K, V> find(ZPair<K, V>[] pairs, T t, int index) {
		if (pairs == null) {
			return null;
		}
		for (ZPair<K, V> pair: pairs) {
			T p = pair.get(index, t);
			if (p == t) {
				return pair;
			}
		}
		return null;
	}
	
	public static <K, V> ZPair<K, V> findByKey(ZPair<K, V>[] pairs, K k) {
		return find(pairs, k, 0);
	}
	
	public static <K, V> ZPair<K, V> findByValue(ZPair<K, V>[] pairs, K k) {
		return find(pairs, k, 1);
	}

	public K getKey() {
		return k;
	}
	
	public K setKey(K k) {
		K old = this.k;
		this.k = k;
		return old;
	}

	public V getValue() {
		return v;
	}
	
	public V setValue(V v) {
		V old = this.v;
		this.v = v;
		return old;
	}
	
	public static void main(String[] args) {
		new ZPair<String, Integer>();
	}
}
