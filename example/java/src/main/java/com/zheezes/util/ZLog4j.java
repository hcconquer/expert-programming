package com.zheezes.util;

import java.util.Properties;

/**
 * @author hcconquer@gmail.com
 */
public class ZLog4j {
	public static int configConsole(String level, String pattern) {
		if (level == null) {
			level = "DEBUG";
		}
		if (pattern == null) {
			pattern = "[%-5p] [%t] %d{yyyy-MM-dd HH:mm:ss.SSS} %c:%L - %m%n";
		}
		Properties props = new Properties();
		props.put("log4j.rootLogger", "DEBUG, C");
		props.put("log4j.appender.C", "org.apache.log4j.ConsoleAppender");
		props.put("log4j.appender.C.Threshold", level);
		props.put("log4j.appender.C.layout", "org.apache.log4j.PatternLayout");
		props.put("log4j.appender.C.layout.ConversionPattern", pattern);
		
		@SuppressWarnings("rawtypes")
		Class clazz = ZClass.clazz("org.apache.log4j.PropertyConfigurator");
		if (clazz == null) {
			return -2;
		}
		ZClass.call(clazz, "configure");
		// PropertyConfigurator.configure(props);
		return 0;
	}
	
	public static int configConsole() {
		return configConsole(null, null);
	}
}
