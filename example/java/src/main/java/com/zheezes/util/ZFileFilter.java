package com.zheezes.util;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * @author hcconquer@gmail.com
 */
public class ZFileFilter extends FileFilter {
	private String[] suffix = { };
	private String desc = "";
	
	public ZFileFilter(String[] suffix) {
		this.suffix = suffix;
		StringBuilder sb = new StringBuilder();
		for (String s: suffix) {
			sb.append(s);
			sb.append(";");
		}
		sb.deleteCharAt(sb.length() - 1);
		desc = sb.toString();
	}

	@Override
	public String getDescription() {
		return desc;
	}
	
	@Override
	public boolean accept(File f) {
		for (String s: suffix) {
			if (f.isDirectory() || f.getName().endsWith(s)) {
				return true;
			}
		}
		return false;
	}
}
