package com.zheezes.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author hcconquer@gmail.com
 */
public class ZFile {
	public static String suffix(File file) {
		String name = file.getName();
		int begin = name.lastIndexOf('.');
		if (begin < 0) {
			return null;
		}
		return name.substring(begin + 1);
	}
	
	public static String suffix(String path) {
		return suffix(new File(path));
	}
	
	/**
	 * @param file
	 *         read file
	 * @return the file content
	 * @throws IOException
	 */
	public static final String readFile(File file) throws IOException {
		InputStream input = new FileInputStream(file);
		return new String(ZInputStream.read(input));
	}
	
	/**
	 * @see ZFile#readFile
	 * 
	 * @param file
	 *         read file
	 * @return the file content
	 */
	public static final String readFileEx(File file) {
		try {
			return readFile(file);
		} catch (IOException e) {
		}
		return null;
	}
}
