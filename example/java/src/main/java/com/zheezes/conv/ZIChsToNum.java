package com.zheezes.conv;

/**
 * @author hcconquer@gmail.com
 */
public interface ZIChsToNum {
	public Integer conv(char[] chs);
}
