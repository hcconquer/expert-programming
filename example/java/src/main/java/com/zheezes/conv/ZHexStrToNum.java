package com.zheezes.conv;

/**
 * @author hcconquer@gmail.com
 */
public class ZHexStrToNum implements ZIStrToNum {
	/**
	 * String("12") --> Integer(12)
	 */
	@Override
	public Integer conv(String str) {
		if (str == null || str.length() != 2) {
			return null;
		}
		
		Integer num = null;
		
		try {
			num = Integer.parseInt(str);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}

		return num;
	}
}
