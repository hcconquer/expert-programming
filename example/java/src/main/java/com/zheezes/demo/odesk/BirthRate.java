package com.zheezes.demo.odesk;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class BirthRate {
	private int scale = 6;
	
	public void setScale(int scale) {
		this.scale = scale;
	}
	
	public BigDecimal diffBirthRate(int pnum, int days) {
		BigDecimal rate = new BigDecimal(1);
		MathContext mc = new MathContext(scale, RoundingMode.HALF_UP);
		if (pnum > 1) {
			rate = new BigDecimal(days - pnum + 1).divide(new BigDecimal(days), mc);
			rate = diffBirthRate(pnum - 1, days).multiply(rate);
		}
		return rate;
	}

	public static void main(String[] args) {
		BirthRate br = new BirthRate();
		for (int n = 1; n < 365; n++) {
			BigDecimal dr = br.diffBirthRate(n, 365);
			dr = dr.setScale(10, RoundingMode.HALF_UP);
			String s = dr.movePointRight(2).toPlainString();
			System.out.println(String.format("%d: %s", n, s));
		}
	}
}
