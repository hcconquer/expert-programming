package com.zheezes.demo.odesk;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/*
Write a program which will accept a single pair of strings separated by a comma; the program should calculate the sum of ascii values of the characters of each string. The program should then subtract the sum of the ascii values of the second string from the sum of the ascii values of the first string.

Suppose the following input is given to the program: 

123ABC,456DEF

Then the sum of the ascii values of the characters in '123ABC' is 348 and in '456DEF' it is 366. The Difference between these numbers is 348 – 366 = -18 
The corresponding output to be printed by the program is: 

-18
**/

public class SumStr {
	private static String[] getInput(int num) {
		List<String> list =new ArrayList<String>();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					System.in));
			int count = 0;
			String s = null;
			while ((s = reader.readLine()) != null) {
				list.add(s);
				count++;
				if (count >= num) {
					break;
				}
			}
		} catch (IOException e) {
		}
		if (list.size() != num) {
			return null;
		}
		return list.toArray(new String[0]);
	}
	
	private static int sum(String str) {
		int sum = 0;
		for (char c : str.toCharArray()) {
			sum += (int) c;
		}
		return sum;
	}

	public static void main(String[] args) {
		String[] input = getInput(1);
		if (input == null) {
			return;
		}
		String[] ss = input[0].split(",");
		if (ss.length != 2) {
			return;
		}
		System.out.println(sum(ss[0]) - sum(ss[1]));
	}
}
