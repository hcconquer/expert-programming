package com.zheezes.demo.lang.intmet;

///*
// * 1.interface can define default method
// */
//interface Inter1 {
//	default void show() {
//		System.out.println(Inter1.class.getSimpleName());
//	}
//}
//
//interface Inter2 {
//	default void show() {
//		System.out.println(Inter2.class.getSimpleName());
//	}
//}
//
///*
// * 1.interface can extends from another
// * 2.interface can override parent's method
// */
//interface Inter3 extends Inter1 {
//	@Override
//	default void show() {
//		System.out.println(Inter3.class.getSimpleName());
//	}
//}
//
//interface Inter4 {
//	default void showlo() {
//		System.out.println(Inter4.class.getSimpleName());
//	}
//}
//
///*
// * 1.class can implements more then 1 interface
// * 2.class can't implements some interface which has same method
// * or you will get error: "can't implements two interface which has same method, or you will get
// * "Duplicate default methods named show with the parameters () and () 
// * are inherited from the types Inter2 and Inter3"
// */
//class Clazz1 implements Inter3/* , Inter2 */, Inter4 {
//	public void test() {
//		Clazz1 demo = new Clazz1();
//		demo.show(); // print "Inter3"
//		demo.showlo(); // print "Inter4"
//	}
//}
//
//class Clazz2 implements Inter1 {
//	/*
//	 * 1.class can override interface's default method
//	 * 2.implement method must be public
//	 * or you will get error: "Cannot reduce the visibility of the inherited method from Inter1"
//	 */
//	@Override
//	public/* private */void show() {
//		System.out.println(Clazz2.class.getSimpleName());
//	}
//
//	public void test() {
//		Clazz2 demo = new Clazz2();
//		demo.show(); // print "Clazz2"
//	}
//}
//
public class Demo1 {
	public static void main(String[] args) {
//		new Clazz1().test();
//		new Clazz2().test();
	}
}
