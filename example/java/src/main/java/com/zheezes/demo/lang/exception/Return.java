package com.zheezes.demo.lang.exception;

/**
 * Created on 2013-1-11
 * 
 * @author hanchen
 */
public class Return {
	/**
	 * @return be 1, set in finally
	 */
	public static int test() {
		int ret = 0;
		try {
			Integer.parseInt(null);
		} catch (NumberFormatException e) {
		} finally {
			ret = 1;
		}
		return ret;
	}

	public static void main(String[] args) {
		System.out.printf("result: %d\n", test());
	}
}
