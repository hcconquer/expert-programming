package com.zheezes.demo.odesk;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
/*
Write a program which will accept three sentences (one sentence per line) and print the words having Initial Caps within the sentences. Below is an example.

Here is an example. If the below three sentences are given to the program as input,

This is a Program
Coding test of Initial Caps
the program Will Test You

Then, the output would look like:

This
Program
Coding
Initial
Caps
Will
Test
You
**/

public class UpperCase {
	private static String[] getInput(int num) {
		List<String> list =new ArrayList<String>();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					System.in));
			int count = 0;
			String s = null;
			while ((s = reader.readLine()) != null) {
				list.add(s);
				count++;
				if (count >= num) {
					break;
				}
			}
		} catch (IOException e) {
		}
		if (list.size() != num) {
			return null;
		}
		return list.toArray(new String[0]);
	}
	
	public static void main(String[] args) {
		String[] input = getInput(3);
		if (input == null) {
			return;
		}
		for (String s : input) {
			String[] words = s.split(" ");
			for (String word : words) {
				word = word.trim();
				if (word.length() <= 0) {
					continue;
				}
				if (Character.isUpperCase(word.charAt(0))) {
					System.out.println(word);
				}
			}
		}
	}
}

