package com.zheezes.demo.lang.type;

/**
 * different with {@link GenericTypes1}, this class can compiled and run 
 * It will print "ok"
 * 
 * Created on 2013-10-30
 * 
 * @author hcconquer@gmail.com
 */
public class GenericTypes2 {
//	public static String method(List<String> list) {
//		System.out.println("invoke string method!");
//	}
//
//	public static int method(List<Integer> list) {
//		System.out.println("invoke Integer method!");
//	}

	public static void main(String[] args) {
		// List<Integer> list = new ArrayList<Integer>();
		// List<String> list2 = new ArrayList<String>();
		// // System.out.println(GenericTypes.method(list));
		// // System.out.println(GenericTypes.method(list2));
		System.out.println("ok");
	}
}
