package com.zheezes.demo.odesk;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/*

 Write a program which will take the year (yyyy) and the numeric sequence of the month (0-11) as its input. The program will return the day on which the 28th of that particular month and year falls. The input can consist of two year-month combinations, one combination per line.

 The numeric sequence of months is as follows:

 0 – Jan
 1 – Feb
 2 – March
 and so on......

 The format for supplying the input is:

 1999-5

 Where 1999 is the year and 5 is the numeric sequence of the month (corresponding to June). The program should display the day on which June 28, 1999 fell, and in this case the output will be MONDAY. 

 The output should be displayed in uppercase letters.

 Suppose the following INPUT sequence is given to the program:

 1999-5
 1998-6

 Then the output should be:

 MONDAY
 TUESDAY

 **/

public class Weeks {
	private static String[] getInput(int num) {
		List<String> list =new ArrayList<String>();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					System.in));
			int count = 0;
			String s = null;
			while ((s = reader.readLine()) != null) {
				list.add(s);
				count++;
				if (count >= num) {
					break;
				}
			}
		} catch (IOException e) {
		}
		if (list.size() != num) {
			return null;
		}
		return list.toArray(new String[0]);
	}
	
	public static String get28th(String str) {
		if (str == null) {
			return null;
		}
		String[] ss = str.split("-");
		int year = 0;
		int month = 0;
		if (ss != null && ss.length == 2) {
			try {
				year = Integer.parseInt(ss[0]);
				month = Integer.parseInt(ss[1]);
			} catch (NumberFormatException e) {
				return null;
			}
		} else {
			return null;
		}
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, month);
		cal.set(Calendar.DAY_OF_MONTH, 28);
		final String[] weeks = new String[] { "SUNDAY", "MONDAY", "TUESDAY",
				"WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY" };
		int week = cal.get(Calendar.DAY_OF_WEEK) - 1;
		if ((week < 0) && (week >= weeks.length)) {
			return null;
		}
		return weeks[week];
	}

	public static void main(String[] args) {
		String[] input = getInput(2);
		if (input == null) {
			return;
		}
		for (String s : input) {
			String value = get28th(s);
			if (value != null) {
				System.out.println(value);
			}
		}
	}
}

