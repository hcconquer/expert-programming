package com.zheezes.demo.lang.exception;

/**
 * Created on 2013-1-11
 * 
 * @author hcconquer@gmail.com
 */
public class Break {
	/**
	 * print 0, 1, 2
	 * continue in finally will continue the loop
	 */
	@SuppressWarnings("finally")
	public static void test() {
		for (int i = 0; i < 3; i++) {
			System.out.println(i);
			try {
				break;
			} catch (Exception e) {
				
			} finally {
				continue;
			}
		}
	}

	public static void main(String[] args) {
		test();
	}
}
