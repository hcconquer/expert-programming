package com.zheezes.sudoku;

/**
 * Created on 2013-1-11
 * 
 * @author hcconquer@gmail.com
 *
 * @param <T>
 */
@SuppressWarnings("unused")
public class Square<T> {
	private int size;

	private T[][] data;

	public Square(int size) {
		this.size = size;
	}
}
