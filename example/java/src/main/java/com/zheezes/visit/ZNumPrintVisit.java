package com.zheezes.visit;

/**
 * @author hcconquer@gmail.com
 */
public class ZNumPrintVisit implements ZINumVisit {
	@Override
	public int visit(int c) {
		System.out.print(c);
		return 0;
	}
}
