package com.zheezes.protocol;

public abstract class ZAbsFixedUnit extends ZAbsUnit {
	public abstract int length();

	public int length(int len) {
		return -1;
	}

	public int parse(char[] chs) {
		return parse(chs, 0, chs.length);
	}

	public abstract int parse(char[] chs, int off, int len);

	public int pack(char[] chs) {
		return pack(chs, 0);
	}

	public abstract int pack(char[] chs, int off);
}
