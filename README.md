# expert-programming

## catalog

[TOC]

## knowledge graph

## paper

### communication

| File                 | Name                                     |
| :------------------- | :--------------------------------------- |
| timestamps-in-message-passing-systems-that-preserve-the-partial-ordering.pdf | Timestamps in Message-Passing Systems That Preserve the Partial Ordering |
| jmlc06.pdf           | Event-Based Programming without Inversion of Control |
| chubby-osdi06.pdf    | The Chubby lock service for loosely-coupled distributed systems |
| LITE-sosp17.pdf      | LITE Kernel RDMA Support for Datacenter Applications |
| TCPRST.pdf           | TCP RST: Calling close() on a socket with data in the receive queue |

### transactions & consensus

| File                                | Name                                                                                |
| :---------------------------------- | :---------------------------------------------------------------------------------- |
| consensus-on-transaction-commit.pdf | Consensus on Transaction Commit                                                     |
| raft.pdf                            | In Search of an Understandable Consensus Algorithm                                  |
| raft-atc14.pdf                      | In Search of an Understandable Consensus Algorithm                                  |
| Peng.pdf                            | Large-scale Incremental Processing Using Distributed Transactions and Notifications |
| lamport-time.pdf                    | Time, Clocks, and the Ordering of Events in a Distributed System                    |
| cockroach-hybrid-logical-clock.pdf  | Logical Physical Clocks and Consistent Snapshots in Globally Distributed Databases  |
| atc12-final181_0.pdf                | Erasure Coding in Windows Azure Storage                                             |

### storage

| File                                          | Name                                                                                       |
| :-------------------------------------------- | :----------------------------------------------------------------------------------------- |
| lsmtree.pdf                                   | The Log-Structured Merge-Tree (LSM-Tree)                                                   |
| fast16-papers-lu.pdf                          | WiscKey: Separating Keys from Values in SSD-conscious Storage                              |
| bigtable-osdi06.pdf                           | Bigtable: A Distributed Storage System for Structured Data                                 |
| gfs-sosp2003.pdf                              | The Google File System                                                                     |
| mapreduce-osdi04.pdf                          | MapReduce: Simplified Data Processing on Large Clusters                                    |
| spanner-osdi2012.pdf                          | Spanner: Google’s Globally-Distributed Database                                            |
| spanner-becoming-a-sql-system.pdf             | Spanner: Becoming a SQL System                                                             |
| f1-a-distributed-sql-database-that-scales.pdf | F1: A Distributed SQL Database That Scales                                                 |
| p1041-verbitski.pdf                           | Amazon Aurora: Design Considerations for High Throughput Cloud-Native Relational Databases |
| weil-osdi06.pdf                               | Ceph: A Scalable, High-Performance Distributed File System                                 |
| weil-crush-sc06.pdf                           | CRUSH: Controlled, Scalable, Decentralized Placement of Replicated Data                    |
| atc17-balmau.pdf                              | Creating Synergies Between Memory, Disk and Log in Log Structured Key-Value Stores         |
| hstore.pdf                                    | The End of an Architectural Era (It’s Time for a Complete Rewrite)                         |
| hstore-demo.pdf                               | H-Store: A High-Performance, Distributed Main Memory Transaction Processing System         |
| beaver.pdf                                    | Finding a needle in Haystack: Facebook’s photo storage                                     |
| f4-osdi14.pdf                                 | f4: Facebook’s Warm BLOB Storage System                                                    |
| amazon-dynamo-sosp2007.pdf                    | Dynamo: Amazon’s Highly Available Key-value Store                                          |
| p51-ramakrishnan.pdf                          | Azure Data Lake Store                                                                      |
| Hekaton-Sigmod2013-final.pdf                  | Hekaton: SQL Server’s Memory-Optimized OLTP Engine                                         |
| mclock.pdf                                    | mClock: Handling Throughput Variability for Hypervisor IO Scheduling                       |

### cache

| File                                          | Name                                                                                       |
| :-------------------------------------------- | :----------------------------------------------------------------------------------------- |
| leases.pdf                                    | Lease: An Efficient Fault-Tolerant Mechanism for Distributed File Cache Consistency        |

### graphs

| File                        | Name                                                                                               |
| :-------------------------- | :------------------------------------------------------------------------------------------------- |
| TransactionalMemory-MSF.pdf | An Efficient Transactional Memory Algorithm for Computing Minimum Spanning Forest of Sparse Graphs |

### image

| File                                                              | Name                                                          |
| :---------------------------------------------------------------- | :------------------------------------------------------------ |
| feature-based-image-set-compression.pdf                           | FEATURE-BASED IMAGE SET COMPRESSION                           |
| rate-distortion-based-sparse-coding-for-image-set-compression.pdf | Rate-Distortion Based Sparse Coding for Image Set Compression |
| tpg-image-compression-technology.pdf                              | TPG image compression technology                              |

### ico

| File                                                     | Name                                                 |
| :------------------------------------------------------- | :--------------------------------------------------- |
| bitcoin.pdf                                              | Bitcoin: A Peer-to-Peer Electronic Cash System       |
| a-fair-payment-system-with-online-anonymous-transfer.pdf | A Fair Payment System with Online Anonymous Transfer |

## rfc

| File                          | Name                                                                        |
| :---------------------------- | :-------------------------------------------------------------------------- |
| rfc2616.txt                   | Hypertext Transfer Protocol -- HTTP/1.1                                     |
| rfc7230.txt                   | Hypertext Transfer Protocol (HTTP/1.1): Message Syntax and Routing          |
| rfc6749.txt                   | The OAuth 2.0 Authorization Framework                                       |
| IEEE830-1998.pdf              | IEEE Recommended Practice for Software Requirements Specifications          |
| DC-008-Translation-2016-E.pdf | Exchangeable image file format for digital still cameras: Exif Version 2.31 |

## ref

| File                                                        | Name                                                     |
| :---------------------------------------------------------- | :------------------------------------------------------- |
| APUE-3rd.pdf                                                | Advanced Programming in the UNIX Environment 3rd Edition |
| raftuserstudy2013.pdf                                       | Raft: A Consensus Algorithm for Replicated Logs          |
| craftconf2014.pdf                                           | An Introduction to Consensus with Raft                   |
| raft-the-understandable-distributed-consensus-protocol.pdf  | Raft - The Understanable Distributed Consensus Protocol  |
| P6-Spanner.pdf                                              | Spanner: Google’s Globally-Distributed Database          |
| spanner-truetime-the-cap-theorem.pdf                        | Spanner, TrueTime & The CAP Theorem                      |
| OReilly.Designing.Data-Intensive.Applications.pdf           | Designing Data-Intensive Applications                    |
| distributed-transaction-processing-the-xa-specification.pdf | Distributed Transaction Processing: The XA Specification |
| NutanixBible.pdf                                            | The Nutanix Bible                                        |
| taobao-leveldb.pdf                                          | leveldb实现解析                                           |
| cellar-kv-storage.pdf                                       | Cellar分布式KV存储                                         |
| leveldb-lsm-tree.pdf                                        | LevelDB存储引擎                                           |
| ExpressivenessOfGo-2010.pdf                                 | The Expressiveness of Go                                 |
| sockets-rdma-interface-over-10-gigabit-networks.pdf         | Sockets vs. RDMA Interface over 10-Gigabit Networks      |

### QCon-2016

| File                                | Name                        |
| :---------------------------------- | :-------------------------- |
| meituan-kv-cellar.pdf               | 美团分布式KV存储Cellar演进之路  |
| metuan-object-storage-mangix.pdf    | Mangix: 美团云分布式对象存储系统 |
| qiniu-microservice-architecture.pdf | 七牛服务治理                   |
| tencent-block-storage.pdf           | 腾讯云弹性块存储系统实践         |

## Donating

[![Support via PayPal](https://cdn.rawgit.com/twolfson/paypal-github-button/1.0.0/dist/button.svg)](https://www.paypal.me/hcconquer/0.99)
