# compare

## Regular Expression Match

ref to [Regular Expression](http://tldp.org/LDP/abs/html/bashver3.html#REGEXMATCHREF):

```shell
// right
if [[ "abcdefg" =~ "cde" ]]; then echo "ok"; fi
if [[ "abcdefg" =~ b*f ]]; then echo "ok"; fi
// wrong
if [[ "abcdefg" =~ "b*f" ]]; then echo "ok"; fi
```

