# shell-in-action

## catalog

[TOC]

## symbol

| symbol | function                                         |
| ------ | ------------------------------------------------ |
| `'`    | define string, inlcude space                     |
| `"`    | define string, inlcude space, exclude `$` and \` |
| `()`   | nested sub-shell                                 |
| `$()`  | non-named command group                          |

```shell
# copy all '*.h"
cp -R /usr/include/*.h /tmp
# but can't
cp -R "/usr/include/*.h" /tmp
```

## base

### bc

```shell
# scale
echo "scale=4;100/3" | bc

# dec2hex
echo "obase=16;ibase=10;31" | bc

# hex2dec, hex should be uppercase
echo "obase=10;ibase=16;1F" | bc

# pow(m, n)
echo "3^2" | bc

# square root sqrt(n)
echo "sqrt(100)" | bc

# exponentiation
echo "e(2)" | bc -l

# natural logarithm ln(n) or loge(n)
echo "l(4)" | bc -l
```

### rand

```shell
# rand select
# gshuf in mac and shuf in linux
# shuf can avoid random conflict
for i in $(echo -e "a\nb\nc\nd"); do echo $i ${RANDOM}; done | gshuf | sort -nk2
```

### date

```shell
# conv date to unix second
date -d '2006-01-02 15:04:05' +%s
# conv unix second to date
date -r'${sec}' '+%Y-%m-%d %H:%M:%S %Z'
```

### grep

```shell
zgrep -a ${key} ${file}.tar.gz
```

### find

```shell
# -H show file
# -n show line no
# -s not show error, e.g. file not found
find . | xargs -I{} grep -Hns "yum" {}

# find macOS ".DS_Store" file and rm it
find . -name ".DS_Store" -depth | xargs -I{} rm {}
```

### xargs

```shell
# for Linux only
ls | xargs -i echo {}
# for Linux and macOS
ls | xargs -I{} echo {}
```

### editor

```shell
# replace tab by 4 space
expand -t 4 pom.xml | tee pom.xml

# replace text
sed -i "s/${src}/${dst}/g" ${file}

# trim empty line
sed '/^[[:space:]]*$/d' ${file}

# trim last char
sed 's/.$//' ${file}

# merge per 2 line
# { echo a; echo b; echo c; echo d; echo e; } | sed '$!N;s/\n/ /'
echo $str | sed '$!N;s/\n/ /'

# truncate path from src, used for git or svn find base path
# `echo "/home/srcs/abc/src/efg/srcs/xyz" | sed 's/\/src\/.*$//'` -> `/home/srcs/abc`
echo $(pwd) | sed 's/\/src\/.*$//'

# show hex
# -C Canonical hex+ASCII display.
hexdump -C -s ${offset} -n ${len} ${file}
```

### loop

```shell
# update all git repo
ls | while read line; do
    (cd ${line}; if [ -d .git ]; then echo ${line}; git pull; fi)
done
```

## network

### netstat

```shell
# list tcp listen on linux
netstat -lpnt

# list all tcp listen on macOS
# -nP, not convert ip to host name
# -i, protocol name, e.g. -iTCP -iUDP
# -s, state name
lsof -nP -iTCP -sTCP:LISTEN
# list tcp listen by port on macOS
lsof -nP -iTCP:${port} -sTCP:LISTEN
```

## disk

### 

```shell
# macOS, 2097152 * 512B = 1024MB
diskutil erasevolume HFS+ 'ram' `hdiutil attach -nomount ram://2097152`

# mount tmpfs
sudo mount -t tmpfs -o size=1024m tmpfs /mnt/ram
```

## proc

### cpu

```shell
# physical cpu
cat /proc/cpuinfo| grep "physical id"| sort| uniq| wc -l

# physical core per cpu
cat /proc/cpuinfo| grep "cpu cores"| uniq

# logical cpu
cat /proc/cpuinfo| grep "processor"| wc -l
```

|              | example1   | example2            |
| ------------ | ---------- | ------------------- |
| physical cpu | 2          | 2                   |
| cpu cores    | 6          | 8                   |
| processor    | 12         | 32                  |
| HT           | 2*6=12, No | 2*8=16 = 32/12, Yes |



### pid

```shell
# get pid by proc name, maybe resule is array
pidof ${proc_name}
```

### top

```shell
# top by pid on linux
top -p ${pid}
# on macOS
top -pid ${pid}

# top by proc name
top -p $(pidof ${proc_name})
```

## ssl

### scan


## run-time

```shell
# show all boot info
who --all
who -a

# show boot time
who --boot
who -b

# show runlevel
who --runlevel
who -r

# show reboot history
last reboot

# show run time by top
# top - 19:37:47 up 19 days
top

# show run time by w
# 19:37:58 up 19 days,  4:20
w

# show run time by uptime
# 19:38pm  up 19 days  4:20,  1 user,  load average: 0.46, 0.54, 0.46
uptime

# by second, e.g. 1657401.00 18665690.69
cat /proc/uptime
# by CST time
date -d "`cut -f1 -d. /proc/uptime` seconds ago"
# by human time
date -d "$(awk -F. '{print $1}' /proc/uptime) second ago" +"%Y-%m-%d %H:%M:%S"
```

## other

```shell
pstack
pmap
ptree
lsof
```