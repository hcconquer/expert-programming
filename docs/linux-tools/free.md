# free

## 简介

```
$ free
             total       used       free     shared    buffers     cached
Mem:      49395740   49001616     394124          0     977660   35943928
-/+ buffers/cache:   12080028   37315712
Swap:            0          0          0
```

默认单位为KB。

| item    | meaning                                              | value    |
| ------- | ---------------------------------------------------- | -------- |
| total   | 物理内存总量                                         | 49395740 |
| used    | 已使用                                               | 49001616 |
| free    | 交换区                                               | 394124   |
| shared  | 进程之间共享的内存，现在已经deprecated，linux上总是0 | 0        |
| buffers | 写文件缓存                                           | 915      |
| cached  | 读文件缓存                                           | 35153    |

$total=user+free=49001616+394124=49395740$。

$buffer+cache=977660+35943928=36921588$

$used-(buffer+cache)=49001616-36921588=12080028$，用户层认为使用的内存。

$free+(buffer+cache)=37315712+36921588=37315712$，用户层认为剩余的内存。

## 单位修改为MB

```
$ free -m
             total       used       free     shared    buffers     cached
Mem:         48238      47998        239          0        915      35153
-/+ buffers/cache:      11929      36308
Swap:            0          0          0
```

## buffer和cache

>     A buffer is something that has yet to be "written" to disk. 
>     A cache is something that has been "read" from the disk and stored for later use.

buffer是用于存放要输出到disk（块设备）的数据的，而cache是存放从disk上读出的数据。这二者是为了提高IO性能的，并由OS管理。
