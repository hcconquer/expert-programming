# coroutine

## catalog

[TOC]

## longjmp & swapcontext



## coroutine & thread

| keywords    | 线程                         | 协程                         |
| ----------- | --------------------------- | ---------------------------    |
| 任务调度     | 抢占式，不受应用程序控制        | 非抢占式，应用程序可以主动切换     |
| 调度级别     | 内核                         | 用户态，换速度要远快于内核调度    |
| 资源占用     | MB级别                       | KB级别                        |

## 1:N & M:N

协程调度分为两种，一是单线程无锁协程，也就是1:N模型，二是多线程协程，也就是M:N模型。

单线程无锁协程的设计思想是避免使用锁，是传统多进程状态机模型的演化，所有协程都在一个线程中运行，对于多核CPU，需要启动多个进程或线程监听不同的端口或事件，主要代表为C/C++的协程库，例如[云风-C协程库](https://github.com/cloudwu/coroutine/), [Tencent libco](https://github.com/Tencent/libco)等。

多线程协程的设计思想是尽量利用多核，是传统多线程模型的演化，当多协程同步数据时，也避免使用锁，而是通过管道或IPC来通信，代表为goroutine，但部分C/C++的协程库也是这种思路的实现，例如[bthread](https://github.com/brpc/brpc/blob/master/docs/cn/bthread.md)，[libgo](https://github.com/yyzybb537/libgo)。

## stackful & stackless

## contiguous stack 

http://www.d-kai.me/golang%E5%87%BD%E6%95%B0%E8%B0%83%E7%94%A8stack%E5%88%86%E5%B8%83/

## references

- [Contiguous stacks](https://docs.google.com/document/d/1wAaf1rYoM4S4gtnPh0zOlGzWtrZFQ5suE8qr2sD8uWQ/pub)


https://swtch.com/libtask/
