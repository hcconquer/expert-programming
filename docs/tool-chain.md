# tool chain

## catalog

[TOC]

## theme

- [dracula-theme](https://draculatheme.com/terminal/): A dark theme for 50+ apps
- [osx-terminal-themes](https://github.com/Dwarven/osx-terminal-themes): color schemes for default Mac OS X Terminal.app

## font

- `Monaco`
- `Courier New`
