# Predicate Pushdown Rules

学术理论上，底层的文件系统或DB不应该理解业务层的逻辑，但某些场景下，这样做会导致业务层对底层的读取放大。

具体例子：假设某Key-Value存储系统X，某业务系统Y需要读取特定的Prefix开头的Key，如果X不支持对Key进行Filter的话，那么Y需要读取所有的Key，然后在业务层做Filter，这导致Y对X的读放大了。X支持通用的计算方法，称之为谓词下推(Predicate Pushdown Rules)。
