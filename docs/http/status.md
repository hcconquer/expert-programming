# status

## catalog

[TOC]

## Informational 1xx

### 1xx

1xx（临时响应）,表示临时响应并需要请求者继续执行操作的状态代码。

> The 1xx (Informational) class of status code indicates an interim
> response for communicating connection status or request progress
> prior to completing the requested action and sending a final
> response.

| code | text                | define                                                                             |
| ---- | ------------------- | ---------------------------------------------------------------------------------- |
| 100  | Continue            | 继续。请求者应当继续提出请求。服务器返回此代码表示已收到请求的第一部分，正在等待其余部分。      |
| 101  | Switching Protocols | 切换协议。服务器根据客户端的请求切换协议。只能切换到更高级的协议，例如，切换到HTTP的新版本协议。 |

## Successful 2xx

### 2xx

2xx（成功）,表示成功处理了请求的状态代码。

> The 2xx (Successful) class of status code indicates that the client's
> request was successfully received, understood, and accepted.

| code | text            | define                                                   |
| ---- | --------------- | -------------------------------------------------------- |
| 200  | OK              | 成功。服务器已成功处理了请求。通常，这表示服务器提供了请求的网页。 |
| 201  | Created         | 已创建。请求成功并且服务器创建了新的资源。                     |
| 202  | Accepted        | 已接受。服务器已接受请求，但尚未处理。常见于异步请求场景。       |
| 206  | Partial Content | 服务器已经成功处理了部分请求。常见于Range请求。                |

### 206 Partial Content

Range，是在HTTP/1.1里新增的一个请求头字段域。迅雷等支持多线程下载以及断点下载的核心也是基于此特性。现在视频播放中也常使用Range将视频分段加载。

Range请求有三种状态：

1. 在请求成功的情况下，服务器会返回`206 Partial Content`状态码。
2. 在请求的范围越界的情况下（范围值超过了资源的大小），服务器会返回`416 Requested Range Not Satisfiable`（请求的范围无法满足）状态码。
3. 在不支持范围请求的情况下，服务器会返回`200 OK`状态码。

参考资料：

- [206 Partial Content](https://developer.mozilla.org/zh-CN/docs/Web/HTTP/Status/206)
- [HTTP range requests](https://developer.mozilla.org/en-US/docs/Web/HTTP/Range_requests)

## Redirection 3xx

### 3xx

3xx（重定向）,表示要完成请求，需要进一步操作。 通常，这些状态代码用来重定向。

> The 3xx (Redirection) class of status code indicates that further
> action needs to be taken by the user agent in order to fulfill the
> request.

| code | text               | define                                                       |
| ---- | ------------------ | ------------------------------------------------------------ |
| 300  | Multiple Choices   | 多种选择。针对请求，服务器可执行多种操作。                         |
| 301  | Moved Permanently  | 永久移动。请求的资源已被永久的移动到新URI，返回信息会包括新的URI，浏览器会自动定向到新URI。今后任何新的请求都应使用新的URI代替。 |
| 302  | Found              | 临时移动。与301类似。但资源只是临时被移动。客户端应继续使用原有URI。  |
| 304  | Not Modified       | 未修改。所请求的资源未修改，不会返回任何资源。                      |
| 305  | Use Proxy          | 使用代理。被请求的资源必须通过指定的代理才能被访问。                 |
| 307  | Temporary Redirect | 临时重定向。服务器目前从不同位置的网页响应请求，但请求者应继续使用原有位置来进行以后的请求。 |

### 302 vs 307

## Client Error 4xx

### 4xx

4xx（请求错误）,客户端的错误。

> The 4xx (Client Error) class of status code indicates that the client
> seems to have erred.

| code | text                            | define                                                        |
| ---- | ------------------------------- | ------------------------------------------------------------- |
| 400  | Bad Request                     | 错误请求。包含服务器不理解请求的语法。                              |
| 401  | Unauthorized                    | 未授权。请求要求身份验证。 对于需要登录的网页，服务器可能返回此响应。常见于浏览器401弹窗验证。 |
| 403  | Forbidden                       | 禁止。服务器已经理解请求，但拒绝请求。                              |
| 404  | Not Found                       | 不存在。请求所希望得到的资源未被在服务器上发现，但允许用户的后续请求。   |
| 405  | Method Not Allowed              | 方法被禁止。请求行中指定的请求方法不能被用于请求相应的资源。该响应必须返回一个Allow头信息用以表示出当前资源能够接受的请求方法的列表。 |
| 406  | Not Acceptable                  | 无法接受。请求的资源的内容特性无法满足请求头中的条件，因而无法生成响应实体，该请求不可接受。 |
| 408  | Request Timeout                 | 请求超时。服务器等候请求时发生超时。                                |
| 409  | Conflict                        | 请求冲突。表示因为请求存在冲突无法处理该请求，例如多个同步更新之间的编辑冲突。 |
| 410  | Gone                            | 不可用。表示所请求的资源不再可用，将永远不再可用。                    |
| 413  | Request Entity Too Large        | 请求实体过大。服务器无法处理请求，因为请求实体过大，超出服务器的处理能力。 |
| 414  | Request-URI Too Long            | 表示请求的URI长度超过了服务器能够解释的长度，因此服务器拒绝对该请求提供服务。 |
| 415  | Unsupported Media Type          | 不支持的媒体类型。请求的格式不受请求页面的支持。                     |
| 416  | Requested Range Not Satisfiable | 客户端已经要求文件的一部分，但服务器不能提供该部分，比如超出文件大小。   |
| 424  | Failed Dependency               | 依赖失败。                                                      |
| 429  | Too Many Requests               | 请求太多。用户在给定的时间内发送了太多的请求。                        |
| 431  | Request Header Fields Too Large | 请求头字段过大。因为一个或多个头字段过大，服务器无法处理请求。          |

### 404 vs 410

`404`表示请求所希望得到的资源未被在服务器上发现，但允许用户的后续请求。没有信息能够告诉用户这个状况到底是暂时的还是永久的。假如服务器知道情况的话，应当使用`410`状态码来告知旧资源因为某些内部的配置机制问题，已经永久的不可用，而且没有任何可以跳转的地址。`404`这个状态码被广泛应用于当服务器不想揭示到底为何请求被拒绝或者没有其他适合的响应可用的情况下。

`410`表示所请求的资源不再可用，将不再可用。当资源被有意地删除并且资源应被清除时，应该使用这个。在收到`410`状态码后，用户应停止再次请求资源。但大多数服务端不会使用此状态码，而是直接使用`404`状态码。

### 400 vs 422

`400`在[rfc2616](https://tools.ietf.org/html/rfc2616#section-10.4.1)中定义的错误，`422`是[rfc2518](https://tools.ietf.org/html/rfc2518#section-10.3)引入的扩展错误。

rfc对`400`的定义如下：

> The 400 (Bad Request) status code indicates that the server cannot or
> will not process the request due to something that is perceived to be
> a client error (e.g., malformed request syntax, invalid request
> message framing, or deceptive request routing).

rfc对`422`的定义如下：

> The 422 (Unprocessable Entity) status code means the server
> understands the content type of the request entity (hence a
> 415(Unsupported Media Type) status code is inappropriate), and the
> syntax of the request entity is correct (thus a 400 (Bad Request)
> status code is inappropriate) but was unable to process the contained
> instructions.  For example, this error condition may occur if an XML
> request body contains well-formed (i.e., syntactically correct), but
> semantically erroneous XML instructions.

也就是语法正确(syntactically correct)，但(but)语义(semantically incorrect)错误时，应该返回`422`。

但由于`422`实际使用并不广泛，大多数服务对语义错误返回了`400`。

相关讨论：

- [400 vs 422 response to POST of data](https://stackoverflow.com/questions/16133923/400-vs-422-response-to-post-of-data)
- [github client errors](https://developer.github.com/v3/#client-errors)

## Server Error 5xx

### 5xx

5xx（服务器错误）,服务器在处理请求的过程中发生错误或者异常，无法处理请求。

| code | text                       | define                                                   |
| ---- | -------------------------- | -------------------------------------------------------- |
| 500  | Internal Server Error      | 通用错误消息。服务器遇到错误，无法完成请求。                    |
| 501  | Not Implemented            | 尚未实现。服务器不支持当前请求所需要的某个功能。                 |
| 502  | Bad Gateway                | 错误网关。服务器作为网关或代理，从上游服务器收到无效响应。        |
| 503  | Service Unavailable        | 服务不可用。由于临时的服务器维护或者过载，服务器当前无法处理请求。 |
| 504  | Gateway Timeout            | 网关超时。服务器作为网关或代理，但是没有及时从上游服务器收到请求。 |
| 505  | HTTP Version Not Supported | HTTP版本不受支持。服务器不支持请求中所用的HTTP协议版本。        |

### 502 vs 504

`502`与`504`的区别：

1. `502`是接收到了无效响应比如`Connection Refused`；
2. `504`是响应超时，响应超时可能是后端处理超时，也有可能是网络原因请求丢失了；

## refer

- [Hypertext Transfer Protocol -- HTTP/1.1](https://tools.ietf.org/html/rfc2616)
- [HTTP Extensions for Distributed Authoring -- WEBDAV](https://tools.ietf.org/html/rfc2518)
- [HTTP Extensions for Web Distributed Authoring and Versioning (WebDAV)](https://tools.ietf.org/html/rfc4918)
- [Hypertext Transfer Protocol (HTTP/1.1): Semantics and Content](https://tools.ietf.org/html/rfc7231)
- [HTTP STATUS DOGS](https://httpstatusdogs.com/422-unprocessable-entity)
