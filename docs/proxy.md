# proxy

## catalog

[TOC]

## git

ref [git httpproxy](https://git-scm.com/docs/git-config#Documentation/git-config.txt-httpproxy):

```
Override the HTTP proxy, normally configured using the http_proxy, https_proxy, and all_proxy environment variables.
```

## curl

man curl `ENVIRONMENT`:

```
ENVIRONMENT
       The  environment  variables  can  be specified in lower case or upper case. The lower case version has precedence. http_proxy is an exception as it is only
       available in lower case.

       Using an environment variable to set the proxy has the same effect as using the -x, --proxy option.

       http_proxy [protocol://]<host>[:port]
              Sets the proxy server to use for HTTP.

       HTTPS_PROXY [protocol://]<host>[:port]
              Sets the proxy server to use for HTTPS.

       [url-protocol]_PROXY [protocol://]<host>[:port]
              Sets the proxy server to use for [url-protocol], where the protocol is a protocol that curl supports and as specified in a  URL.  FTP,  FTPS,  POP3,
              IMAP, SMTP, LDAP etc.

       ALL_PROXY [protocol://]<host>[:port]
              Sets the proxy server to use if no protocol-specific proxy is set.

       NO_PROXY <comma-separated list of hosts>
              list of host names that shouldn't go through any proxy. If set to a asterisk '*' only, it matches all hosts.
```

## wget

```
ENVIRONMENT
       Wget supports proxies for both HTTP and FTP retrievals.  The standard way to specify proxy location, which Wget recognizes, is using the following
       environment variables:

       http_proxy
       https_proxy
           If set, the http_proxy and https_proxy variables should contain the URLs of the proxies for HTTP and HTTPS connections respectively.

       ftp_proxy
           This variable should contain the URL of the proxy for FTP connections.  It is quite common that http_proxy and ftp_proxy are set to the same URL.

       no_proxy
           This variable should contain a comma-separated list of domain extensions proxy should not be used for.  For instance, if the value of no_proxy is
           .mit.edu, proxy will not be used to retrieve documents from MIT.
```

## recommend

```shell
export http_proxy=http://127.0.0.1:1087
export https_proxy=http://127.0.0.1:1087
export HTTPS_PROXY=http://127.0.0.1:1087
```

## ref

- [righth format for proxy environment](https://unix.stackexchange.com/questions/212894/whats-the-right-format-for-the-http-proxy-environment-variable-caps-or-no-ca)
