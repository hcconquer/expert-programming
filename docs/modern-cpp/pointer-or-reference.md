# pointer-or-reference

## 本质区别

引用是变量别名，引用和原变量是相同的内存，引用是编译时决定的。

指针是变量的索引，先读索引的内容，然后根据索引读取原变量，指针是运行时决定的。

一般来说，两者没有性能上的差别。

## 使用区别

引用和指针使用区别如下；

- 引用声明时赋值，指针任意时刻赋值；
- 不可以改变引用关系，指针可以；
- 引用不可以为空，指针可以；

## 使用场景

如下场景应使用指针：

- 允许指向为空；
- 允许动态改变指向；
- 类型循环引用的数据结构，比如链表；
- 返回值尽量使用指针，避免不必要的clone；
- C语言接口；

## 函数参数

很多材料提出：不可修改的变量使用引用，可修改的变量使用指针。

其出处来源于[Google_Style_Reference_Arguments](https://google.github.io/styleguide/cppguide.html#Reference_Arguments)。


```c++
void Foo(const string &in, string *out);
```

但要注意的是，

## RAII与智能指针

RAII(Resource acquisition is initialization)，建议通过变量的生命周期来管理内存。

这里的智能指针主要指`std::unique_ptr`，符合RAII规范，并且和裸指针几乎相同的性能。对于`std::shared_ptr`或其他智能指针，适用于不同的场景，而且性能会低1-2个数量级，无法对等比较。

由于`std::unique_ptr`不能复制，不能在参数中直接传递，需要借助引用或者`move`语义实现，所以，不如直接使用引用更简洁。

## 其他使用建议

参考[C++ FAQ Lite](https://isocpp.org/wiki/faq/references#refs-vs-ptrs)：

> Use references when you can, and pointers when you have to.

参考[C++ pointers](https://hownot2code.com/2017/08/10/c-pointers-why-we-need-them-when-we-use-them-how-they-differ-from-accessing-to-object-itself/)：

> always prefer the alternatives unless you really need pointers.

尽量使用引用，必要时使用指针。
