# graphdb

## catalog

[TOC]

## 图数据库产品

|                | HugeGraph                                               | JanusGraph                                 | Neo4J     |
| -------------- | ------------------------------------------------------- | ------------------------------------------ | --------- |
| 集群           | 支持                                                    | 支持                                       | 单机      |
| 数据库查询语言 | Gremlin                                                 | Gremlin                                    | Cypher    |
| 后端数据库     | 内存，RocksDB，Cassandra，ScyllaDBMySQL，正在支持 HBase | 内存，Cassandra，HBase，BerkeleyDB，Oracle | 内嵌 bolt |
| 支持事务       | OLTP & OLAP                                             | OLTP & OLAP                                |           |
| 实现语言       | Java                                                    | Java                                       | Java      |
| 访问数据库方式 | HugeApi(http)，HugeClient                               | Http，WebSocket                            |           |
| 大数据分析     | spark                                                   | spark，hadoop                              |           |
| 全文搜索       | **目前不支持**                                          | ElasticSearch                              |           |
| 图存储模型     | 链接表                                                  | 链接表                                     |           |
| 数据模型       | Property Graph（属性图）                                | Property Graph（属性图）                   |           |
| 集群分区方式   | 边分割存储                                              | 边分割存储                                 |           |

## 参考资料

- [Property Graphs Explained](http://graphdatamodeling.com/Graph%20Data%20Modeling/GraphDataModeling/page/PropertyGraphs.html)
