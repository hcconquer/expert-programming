# golang-gui

## fyne

| name | about                                            | advantage   | disadvantage                       |
| ---- | ------------------------------------------------- | ----------- | ---------------------------------- |
| [fyne](https://github.com/fyne-io/fyne) | Cross platform GUI in Go based on Material Design | pure go<br> | OpenGL will be deprecated in macOS |
|      |                                                   |             |                                    |
|      |                                                   |             |                                    |
