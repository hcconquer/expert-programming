# memory

## 内存管理

内存管理可以分为三个层次，自底向上分别是：

- 操作系统内核的内存管理，为用户态提供的接口主要为`mmap`；
- glibc层使用系统调用维护的内存管理算法，主要接口为`malloc/free`；
- 进程从glibc动态分配内存后进行的封装，比如`std::shared_ptr`；

## tcmalloc

## arena

## ref 

- [TCMalloc : Thread-Caching Malloc](https://gperftools.github.io/gperftools/tcmalloc.html)
