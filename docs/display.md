# display

## catalog

[TOC]

## 基本概念

### DPI

DPI(Dots Per Inch)，图像每英寸面积内的像素点(dot)数。

### PPI

PPI(Pixels Per Inch)，每英寸所拥有的像素(pixel)数目。两块面积相同而PPI不同的屏幕，自然是PPI大的那块总像素数量多，其展示细节的潜力和能力也越大，只用4x4个像素来表达一条曲线，自然不如8x8个像素更加细腻平滑。后者的PPI是前者的2倍，后者像素数量则是前者的4倍。

其计算公式为：$$PPI=\sqrt{X^2+Y^2}/Z$$

| 参数 | 意义              |
| ---- | ----------------- |
| X    | 长度像素数(pixel) |
| Y    | 宽度像素数(pixel) |
| Z    | 屏幕大小(inch)    |

也可以使用[DPI Calculator / PPI Calculator](https://www.sven.de/dpi/)来计算。

| Display                      | Resolution | X    | Y    | Z    | PPI    |
| ---------------------------- | ---------- | ---- | ---- | ---- | ------ |
| DELL U2417H IPS              | 1080P      | 1920 | 1080 | 23.8 | 92.56  |
| AOC Q2490PXQ IPS             | 2K         | 2560 | 1440 | 23.8 | 123.41 |
| SAMSUNG U28E590D             | 4K         | 3840 | 2160 | 28   | 157.35 |
| Apple Retina MacBook Pro 13" | Retina     | 2560 | 1600 | 13.3 | 226.98 |

但PPI有几个层次：

- 显示器物理属性，也就是硬件本身的特质；
- 操作系统PPI，通过操作系统调节分辨率，根据公式，X和Y发生变化，Z不变，PPI必然会发生变化；

### Retina

[Retina](http://www.wikiwand.com/en/Retina_display)是一种显示标准，是把更多的像素点压缩至一块屏幕里，从而达到更高的分辨率并提高屏幕显示的细腻程度。由摩托罗拉公司研发。最初该技术是用于Moto Aura上。这种分辨率在正常观看距离下足以使人肉眼无法分辨其中的单独像素。也被称为视网膜显示屏。

- 移动电话显示器的像素密度达到或高于300ppi就不会再出现颗粒感；
- 手持平板类电器显示器的像素密度达到或高于260ppi就不会再出现颗粒感；
- 电脑显示器像素密度只要超过200ppi就无法区分出单独的像素；

### HiDPI

普通显示器默认分辨率下，`dot`和`pixel`是等同的，也就是DPI和PPI相等。但由于普通显示器的PPI小余200，所以肉眼看能看到像素点。

HiDPI(High Dots Per Inch)，也就是将多个`pixel`显示一个`dot`，显示同样的面积，需要的`pixel`变多了。

假设2个`pixel`显示一个`dot`，操作系统变换之后的分辨率X和Y减半，原来显示4个`dot`，所需要的像素为$$4\times4$$，现在变为了$$8\times8$$，也就是对应的PPI倍数变化为$$PPI1/PPI0=\sqrt{8^2+8^2}\div\sqrt{4^2+4^2}=2$$，也就是PPI变为原来的2倍了。

对于4K/2K/1980P等16:9的显示器通过HiDPI技术，分辨率和PPI变化如下：

| 倍率 | 原分辨率  | 原PPI  | 新分辨率  | 新PPI  |
| ---- | --------- | ------ | --------- | ------ |
| 1.3  | 2560x1440 | 123.41 | 1920x1080 | 164.54 |
| 1.7  | 2560x1440 | 123.41 | 1600x900  | 197.46 |
| 2.0  | 2560x1440 | 123.41 | 1280x720  | 246.82 |
| 1.5  | 3840x2160 | 157.35 | 2560x1440 | 236.02 |
| 2.0  | 3840x2160 | 157.35 | 1920x1080 | 314.70 |

Macbook默认显示器也使用了HiDPI技术，对于`Apple Retina MacBook Pro 13"`默认分辨率为1280x800，PPI的倍率为2，也就是实际PPI达到了$$226.98\times2=453.96$$，这已经远超出几倍普通显示器了。

## Macbook外接显示器

使用Macbook外接显示器(分辨率<4K)时，会发现字体非常小，而且没有Macbook显示器那么清晰，通过`Display`工具降低分辨率，字体会变大，但并没有变得清晰。

由于上述原因，是外接显示器的分辨率<4K时，macOS没有开启HiDPI，外接显示器的PPI远低于Macbook显示器，导致对比效果显得很模糊。

建议按照以下步骤操作，不建议使用收费的`SwitchResX`。

### 开启HiDPI

通过命令开启macOS的HiDPI。

```shell
# get DisplayResolutionEnabled value
sudo defaults read /Library/Preferences/com.apple.windowserver.plist DisplayResolutionEnabled

# set DisplayResolutionEnabled on
sudo defaults write /Library/Preferences/com.apple.windowserver.plist DisplayResolutionEnabled -bool true
```

### RDM软件

[avibrazil/RDM](https://github.com/avibrazil/RDM)是一个为macOS的Retina效果设计的分辨率调节软件，这个软件是免费的。

进入[RDM List](http://avi.alkalay.net/software/RDM/)页面，选择最新的版本下载并安装，由于macOS的保护机制，可能要进入`System Preferences`->`Security & Privacy`->`General`确认授权安装。

安装完成后，在`Application`中点击`RDM`启动软件。在`Display`中可以看到有很多分辨率，带有闪电标识⚡️的分辨率为支持HiDPI模式的，可以对比同样分辨率下支持和不支持HiDPI的显示效果。

对HiDPI模式的分辨率，分辨率越低，转换的PPI越高。要达到Retina效果，PPI需要大于200。

对于2K的显示器，建议分辨率为1600x900，对于4K显示器，建议分辨率为2560x1440。

有几种异常的情况：

- 某些支持HiDPI的分辨率无法被选中生效，这是macOS系统的限制，只能选择相似的分辨率替代；
- 某些分辨率没有HiDPI模式，这是macOS系统的限制，可以通过修改显示器参数开启；

### 增加HiDPI配置

#### 获取显示器信息

运行命令：

```shell
ioreg -lw0 | grep IODisplayPrefsKey
```

结果一般为：

```shell
$ ioreg -lw0 | grep IODisplayPrefsKey
    | |   | | |       "IODisplayPrefsKey" = "IOService:/AppleACPIPlatformExpert/PCI0@0/AppleACPIPCI/IGPU@2/AppleIntelFramebuffer@0/display0/AppleBacklightDisplay-610-a029"
    | |   | | |       "IODisplayPrefsKey" = "IOService:/AppleACPIPlatformExpert/PCI0@0/AppleACPIPCI/IGPU@2/AppleIntelFramebuffer@2/display0/AppleDisplay-5e3-2490"
```

AppleBacklightDisplay为Macbook自带的显示器，而AppleDisplay为外置显示器。

取最后两个字段分别为`DisplayVendorId`和`DisplayProductID`，如上述例子，DisplayVendorId为1507(0x5e3)，DisplayProductID为9360(0x2490)。

然后通过`About This Mac`->`Displays`获得显示器名称`DisplayProductName`。

#### 获取显示器配置

进入[SCALED RESOLUTIONS FOR YOUR MACBOOKS EXTERNAL MONITOR](https://comsysto.github.io/Display-Override-PropertyList-File-Parser-and-Generator-with-HiDPI-Support-For-Scaled-Resolutions/)，填入显示器的`DisplayProductName`,`DisplayProductID`,`DisplayVendorID`几个信息。

然后配置期望的HiDPI分辨率，每个分辨率要多配置一条两倍的分辨率，如期望`1920x1080`，需要先配置`3840x2160`，而且都需要勾选上`hidpi`选项。

一般而言，可以删除掉默认的配置，只加入希望的几个HiDPI分辨率即可。

这是`AOC Q2490PXQ IPS`的配置模板，加入了`1920x1080`, `1600x900`, `1280x720`几种HiDPI的配置。

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
    <dict>
        <key>DisplayVendorID</key>
        <integer>1507</integer>
        <key>DisplayProductID</key>
        <integer>9360</integer>
        <key>DisplayProductName</key>
        <string>AOC Q2490PXQ</string>
        <key>scale-resolutions</key>
        <array>
            <data>AAAPAAAACHAAAAABACAAAA==</data>
            <data>AAAHgAAABDgAAAABACAAAA==</data>
            <data>AAAMgAAABwgAAAABACAAAA==</data>
            <data>AAAGQAAAA4QAAAABACAAAA==</data>
            <data>AAAKAAAABaAAAAABACAAAA==</data>
            <data>AAAFAAAAAtAAAAABACAAAA==</data>
        </array>
    </dict>
</plist>
```

保存为`DisplayProductID-${DisplayProductID}.plist`，对于`AOC Q2490PXQ IPS`为`DisplayProductID-2490`。

#### 关闭SIP

因为增加显示器配置，需要修改`/System/Library`目录，因而需要关闭macOS的SIP(System Integrity Protection)。

1. 在终端输入`csrutil status`查看SIP的状态，如果为`enabled`则需要关闭；
2. 重启Macbook，在开机的时候按command+R进入恢复模式；
3. 在终端输入`csrutil disable`；
4. 重启Macbook，再次查看SIP的状态，如果为`disabled`说明已经关闭了SIP；

分辨率调整好之后，可以在恢复模式通过`csrutil enable`重新开启SIP。

#### 生效显示器配置

使用下列命令，将下载的plist文件拷贝到对应目录，主要路径中的ID都是16进制小写格式的。

```shell
# syntax
sudo mkdir /System/Library/Displays/Contents/Resources/Overrides/DisplayVendorID-${DisplayVendorId}
sudo cp DisplayProductID-${DisplayProductID}.plist /System/Library/Displays/Contents/Resources/Overrides/DisplayVendorID-${DisplayVendorId}/DisplayProductID-${DisplayProductID}

# example
sudo mkdir /System/Library/Displays/Contents/Resources/Overrides/DisplayVendorID-5e3
sudo cp DisplayProductID-2490.plist /System/Library/Displays/Contents/Resources/Overrides/DisplayVendorID-5e3/DisplayProductID-2490
```

拷贝完成后，重启Macbook，然后再次打开RDM软件，可以看到新增的分辨率支持HiDPI了。
