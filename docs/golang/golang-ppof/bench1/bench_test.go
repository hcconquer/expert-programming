package channel

import (
	"testing"
)

func DoubleByMulti(v int) int {
	return v * 2
}

func DoubleByBitShift(v int) int {
	return v << 1
}

func BenchmarkDoubleByMulti(b *testing.B) {
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		DoubleByMulti(n)
	}
	b.StopTimer()
}

func BenchmarkDoubleByBitShift(b *testing.B) {
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		DoubleByBitShift(n)
	}
	b.StopTimer()
}
