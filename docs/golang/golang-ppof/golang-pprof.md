# golang prof

## catalog

[TOC]

## benchmark with prof

```shell
# -benchmem, show mem report
# -cpuprofile, save cpu prof data
# -memprofile, save mem prof data
go test -bench=. -benchmem -cpuprofile=cpuprof.out -memprofile=memprof.out
```

you can see report like this:

```
BenchmarkDoubleByMulti-4        2000000000               0.36 ns/op            0 B/op          0 allocs/op
BenchmarkDoubleByBitShift-4     2000000000               0.34 ns/op            0 B/op          0 allocs/op
```

and will create :

- xxx.test
- cpuprof.out
- memprof.out

## pprof tool

### install pprof

```shell
# if go version < 1.11, get pprof tool
# and use `pprof`
go get -u github.com/google/pprof
# else use `go tool pprof`
```

```shell
# if need visualization should install graphviz
# on centos
yum install graphviz
# on macos
brew install graphviz
```

### pprof console

```shell
go tool pprof xxx.test cpuprof.out
```

### pprof visualization

show pprof svg in browser

```shell
pprof -http=":8080" xxx.test cpuprof.out
pprof -http=":8080" xxx.test memprof.out
```

#### top n

#### pprof graph

#### flamegraph

深度解析。

## flamegraph

## other

```shell
export GOROOT=/tmp/go
export GOPATH=/tmp/gopath
```

```shell
go tool pprof http://localhost:8888/m/pprof/goroutine
```

```shell
top 10
```

prof tool: https://cizixs.com/2017/09/11/profiling-golang-program/

save prof data: https://www.kancloud.cn/cattong/go_command_tutorial/261357