# golang-share

[TOC]

## golang起源

> "When the three of us got started, it was pure research. The three of us got together
and decided that we **hated C++**.  We
started off with the idea that all three of us had to be talked into every
feature in the language, so there was no extraneous **garbage** put into the
language for any reason." (Ken Thompson)

- developed at Google
  - Robert Griesemer
  - Rob Pike
  - Ken Thompson
- open source
- compiled, statically typed
  - very fast compilation
- C-like syntax
- garbage collection
- built-in concurrency
- no classes or type inheritance or overloading or generics

## golang关键字

A list and count of keywords in programming languages.

```
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                             ...... │
│                                                                             ...... │
│                                                                      ...... ...... │
│                                                                      ...... ...... │
│                                                               ...... ...... ...... │
│                                                               ...... ...... ...... │
│                                                               ...... ...... ...... │
│                                                               ...... ...... ...... │
│                                                               ...... ...... ...... │
│                                                 ...... ...... ...... ...... ...... │
│                                   ...... ...... ...... ...... ...... ...... ...... │
│                                   ...... ...... ...... ...... ...... ...... ...... │
│                     ...... ...... ...... ...... ...... ...... ...... ...... ...... │
│       ...... ...... ...... ...... ...... ...... ...... ...... ...... ...... ...... │
│...... ...... ...... ...... ...... ...... ...... ...... ...... ...... ...... ...... │
│...... ...... ...... ...... ...... ...... ...... ...... ...... ...... ...... ...... │
│..25.. ..26.. ..32.. ..35.. ..36.. ..49.. ..51.. ..52.. ..54.. ..89.. .100.. .109.. │
│Go     Erlang C      Python Ruby   JS     Java   Rust   Dart   Swift  C#     C++    │
└────────────────────────────────────────────────────────────────────────────────────┘
```

[leighmcculloch/keywords](https://github.com/leighmcculloch/keywords)

## nil

`nil`类似于C/C++的`nullptr`和Java的`null`，[官方描述](https://golang.org/pkg/builtin/#pkg-variables)如下：

```
nil is a predeclared identifier representing the zero value for a pointer, channel, func, interface, map, or slice type.
```

```go
var nil Type // Type must be a pointer, channel, func, interface, map, or slice type
```

- `nil`不是关键字，而是builtin的变量；
- `nil`表示`*`、`chan`、`func`、`interface`、`map`、`slice`的0，且这些类型默认初始化为`nil`；
- `nil`是有类型的，不同类型的`nil`不能比较，部分类型相同的`nil`也不能比较，比如`[]int`、`map`；

## array & slice

### array & slice define

array是定长的序列，slice是变长序列，array必须指定长度，或通过`[...]int{1, 2}`的方式推断出长度。

```go
SliceType = []ElementType{}
ArrayType = [ArrayLength]ElementType{}
```

```go
// len = high - low
// cap = max - low
slice[low:high]     // two-index form, 0 <= low <= high <= cap(slice), max = cap(slice)
slice[low:high:max] // three-index form, 0 <= low <= high <= max <= cap(slice)
```

### slice layout

![slice-layout](image/slice-layout.png)

参考[gccgo-type](https://golang.org/doc/install/gccgo#Types)或者[gcc-go](https://github.com/gcc-mirror/gcc/blob/master/libgo/runtime/runtime.h)中的定义：

```c
struct __go_slice {
  void *__values;
  intptr_t __count;
  intptr_t __capacity;
};
```

### slice and array

[Arrays](https://golang.org/doc/effective_go.html#arrays):

- Arrays are **values**. Assigning one array to another **copies** all the elements.
- In particular, if you pass an array to a function, it will receive a **copy** of the array, not a pointer to it.
- The size of an array is part of its type. The types [10]int and [20]int are distinct.

[Slices](https://golang.org/doc/effective_go.html#slices):

Slices hold **references** to an underlying array, and if you assign one slice to another, both refer to the same array. If a function takes a slice argument, changes it makes to the elements of the slice will be visible to the caller, analogous to passing a pointer to the underlying array.

```go
func TestArrayCopy(t *testing.T) {
	a1 := [...]int{0, 1, 2}
	a2 := a1
	a1[1] = 9
	assert.EqualValues(t, 1, a2[1])

	s1 := []int{0, 1, 2}
	s2 := s1
	s1[1] = 9
	assert.EqualValues(t, 9, s2[1])
}
```

### slice append

```go
append(s S, x ...T) S  // T is the element type of S
```

```go
s0 := []int{0, 0}
s1 := append(s0, 2)                // append a single element     s1 == []int{0, 0, 2}
s2 := append(s1, 3, 5, 7)          // append multiple elements    s2 == []int{0, 0, 2, 3, 5, 7}
s3 := append(s2, s0...)            // append a slice              s3 == []int{0, 0, 2, 3, 5, 7, 0, 0}
s4 := append(s3[3:6], s3[2:]...)   // append overlapping slice    s4 == []int{3, 5, 7, 2, 3, 5, 7, 0, 0}

var t []interface{}
t = append(t, 42, 3.1415, "foo")   //                             t == []interface{}{42, 3.1415, "foo"}

var b []byte
b = append(b, "bar"...)            // append string contents      b == []byte{'b', 'a', 'r' }
```

参考[Appending to and copying slices](https://golang.org/ref/spec#Appending_and_copying_slices):

```
If the capacity of s is not large enough to fit the additional values, append allocates a new, sufficiently large underlying array that fits both the existing slice elements and the additional values. Otherwise, append re-uses the underlying array.
```

### append example

```go
func TestAppend2(t *testing.T) {
	a := [...]int{0, 1, 2, 3, 4, 5, 6}
	s0 := a[:]           // len=7 cap=7
	s1 := s0[:]          // len=7 cap=7
	s2 := s1[1:3]        // len=2 cap=6
	s3 := s2[2:6]        // len=4 cap=4
	s4 := s0[3:5]        // len=2 cap=4
	s5 := s0[3:5:5]      // len=2 cap=2
	s6 := append(s4, 77) // len=3 cap=4
	s7 := append(s5, 88) // len=3 cap=4
	s8 := append(s7, 66) // len=4 cap=4
	s3[1] = 99

	assert.EqualValues(t, s0, []int{0, 1, 2, 3, 99, 77, 6})
	assert.True(t, len(s0) == 7 && cap(s0) == 7)
	assert.EqualValues(t, s1, []int{0, 1, 2, 3, 99, 77, 6})
	assert.True(t, len(s1) == 7 && cap(s1) == 7)
	assert.EqualValues(t, s2, []int{1, 2})
	assert.True(t, len(s2) == 2 && cap(s2) == 6)
	assert.EqualValues(t, s3, []int{3, 99, 77, 6})
	assert.True(t, len(s3) == 4 && cap(s3) == 4)
	assert.EqualValues(t, s4, []int{3, 99})
	assert.True(t, len(s4) == 2 && cap(s4) == 4)
	assert.EqualValues(t, s5, []int{3, 99})
	assert.True(t, len(s5) == 2 && cap(s5) == 2)
	assert.EqualValues(t, s6, []int{3, 99, 77})
	assert.True(t, len(s6) == 3 && cap(s6) == 4)
	assert.EqualValues(t, s7, []int{3, 4, 88})
	assert.True(t, len(s7) == 3 && cap(s7) == 4)
	assert.EqualValues(t, s8, []int{3, 4, 88, 66})
	assert.True(t, len(s8) == 4 && cap(s8) == 4)

	// re-uses
	s9 := append(s4, 55) // len=3 cap=4
	assert.EqualValues(t, s9, []int{3, 99, 55})
	assert.True(t, len(s9) == 3 && cap(s9) == 4)
	assert.EqualValues(t, s1, []int{0, 1, 2, 3, 99, 55, 6})
	assert.True(t, len(s1) == 7 && cap(s1) == 7)

	// new, cap is double by 4
	s10 := append(s4, 101, 102, 103) // len=5 cap=8
	assert.EqualValues(t, s10, []int{3, 99, 101, 102, 103})
	assert.True(t, len(s10) == 5 && cap(s10) == 8)
	assert.EqualValues(t, s1, []int{0, 1, 2, 3, 99, 55, 6})
	assert.True(t, len(s1) == 7 && cap(s1) == 7)
}
```

- if cap enough will re-use;
- if cap is not enough will new;

### slice和string转换会产生拷贝

slice和string的转换是会产生拷贝的。

src/runtime/string.go: 

```go
func slicebytetostring(buf *tmpBuf, b []byte) (str string) {
	// ...
	memmove(p, (*(*slice)(unsafe.Pointer(&b))).array, uintptr(len(b)))
	// ...
	return
}
```

```go
func stringtoslicebyte(buf *tmpBuf, s string) []byte {
	var b []byte
	if buf != nil && len(s) <= len(buf) {
		*buf = tmpBuf{}
		b = buf[:len(s)]
	} else {
		b = rawbyteslice(len(s))
	}
	copy(b, s)
	return b
}
```

## string

### string layout

```c
struct __go_string {
  const unsigned char *__data;
  intptr_t __length;
};
```

## array & slice & string

### benchmark

```go
package main

import "testing"

func makeTestString(len int) []byte {
	s := make([]byte, len)
	for i := 0; i < len; i++ {
		s[i] = byte(int('a') + (len % 26))
	}
	return s
}

const (
	BufSize = 32768
	ChanCap = 128
)

func BenchmarkChanSlice(b *testing.B) {
	d := makeTestString(BufSize)
	s := d
	ch := make(chan []byte, ChanCap)

	go func() {
		for {
			<-ch
		}
	}()

	b.ResetTimer()
	n := 0
	for ; n < b.N; n++ {
		s1 := s
		ch <- s1
	}
	b.StopTimer()
}

func BenchmarkChanArray(b *testing.B) {
	d := makeTestString(BufSize)
	s := [BufSize]byte{}
	copy(s[:], d)
	ch := make(chan [BufSize]byte, ChanCap)

	go func() {
		for {
			<-ch
		}
	}()

	b.ResetTimer()
	n := 0
	for ; n < b.N; n++ {
		s1 := s
		ch <- s1
	}
	b.StopTimer()
}

func BenchmarkChanString(b *testing.B) {
	d := makeTestString(BufSize)
	s := string(d)
	ch := make(chan string, ChanCap)

	go func() {
		for {
			<-ch
		}
	}()

	b.ResetTimer()
	n := 0
	for ; n < b.N; n++ {
		s1 := s
		ch <- s1
	}
	b.StopTimer()
}
```

测试结果如下：

```
BenchmarkChanSlice-4    	20000000	       112 ns/op	       0 B/op	       0 allocs/op
BenchmarkChanArray-4    	  100000	     12070 ns/op	       0 B/op	       0 allocs/op
BenchmarkChanString-4   	20000000	        93.4 ns/op	       0 B/op	       0 allocs/op
```

- string和slice都是struct拷贝；
- array因为copy value所以远远慢于slice和string；

## map

### map define

[Go maps in action](https://blog.golang.org/go-maps-in-action)：

```
Go provides a built-in map type that implements a hash table.
```

```
Maps are not safe for concurrent use: it's not defined what happens when you read and write to them simultaneously. If you need to read from and write to a map from concurrently executing goroutines, the accesses must be mediated by some kind of synchronization mechanism. One common way to protect maps is with sync.RWMutex.
```

- map是hash table的实现；
- map不是协程安全的，如果有并发读写需要加锁；

对于内建`map`协程不安全的问题，在1.9之后增加了`sync.map`，查询、存储和删除都是平均常数时间，并且可以并发访问。

### map crush when read and write

如下示例，两个协程一个写一个读，可能会导致crush。

```go
package main

import (
	"fmt"
	"sync"
)

type UserTable struct {
	m map[int]string
	sync.Mutex
}

func (this *UserTable) Add(id int, name string) {
	this.Lock()
	defer this.Unlock()
	this.m[id] = name
}

func (this *UserTable) Get(id int) string {
	if name, ok := this.m[id]; ok {
		return name
	}
	return ""
}

func main() {
	table := UserTable{m: map[int]string{}}
	count := 10000
	var wg sync.WaitGroup
	wg.Add(2)
	go func() {
		defer wg.Done()
		for i := 1; i < count; i++ {
			table.Add(i, fmt.Sprintf("id_%d", i))
		}
	}()
	go func() {
		defer wg.Done()
		for i := 1; i < count; i++ {
			table.Get(i)
		}
	}()
	wg.Wait()
}
```

## channel

### channel deinfe

chan是channel的简写，主要语法：

```go
ch <- v       // Send v to channel ch.
v := <-ch     // Receive from ch, and assign value to v.
v, ok := <-ch // Receive from ch, and assign ok to false if closed.
```

> By default, sends and receives block until the other side is ready. This allows goroutines to synchronize **without explicit locks or condition variables**.

- chan的send和receive可能会造成阻塞；
- chan是协程安全的，在不同的goroutine中调用不需要锁来保护；

### chan layout

src/pkg/runtime/chan.h:

```go
type hchan struct {
	qcount   uint           // total data in the queue
	dataqsiz uint           // size of the circular queue
	buf      unsafe.Pointer // points to an array of dataqsiz elements
	elemsize uint16
	closed   uint32
	elemtype *_type // element type
	sendx    uint   // send index
	recvx    uint   // receive index
	recvq    waitq  // list of recv waiters
	sendq    waitq  // list of send waiters

	// lock protects all fields in hchan, as well as several
	// fields in sudogs blocked on this channel.
	//
	// Do not change another G's status while holding this lock
	// (in particular, do not ready a G), as this can deadlock
	// with stack shrinking.
	lock mutex
}

type waitq struct {
	first *sudog
	last  *sudog
}
```

![channel-layout.png](image/channel-layout.png)

### send & receive普通流程

假设send前没有receive，发起send，send的值会写入buf，goroutine不会阻塞。

![chan-send-1](image/chan-send-1.png)

当buf写满之后，再次send，当前goroutine G1将进入sendx队列，并且G1阻塞。

![chan-send-full](image/chan-send-full.png)

如果此时goroutine G2发起receive，会从buf头读出一个数据。

![chan-recv-1](image/chan-recv-1.png)

G2的receive调用完成前，G2会将G1状态设为Runnable，G1从sendx中脱离，将数据写入buf尾。

![chan-send-resume](image/chan-send-resume.png)

### direct send

假设G2发起receive时buf中没有数据，那么G2阻塞，并进入recvq。

![chan-recv-2](image/chan-recv-2.png)

然后，G1发起send，会直接将数据拷贝给G2，然后G2进入Runnable状态。

```go
func chansend(c *hchan, ep unsafe.Pointer, block bool, callerpc uintptr) bool {
	// ...
	if sg := c.recvq.dequeue(); sg != nil {
		// Found a waiting receiver. We pass the value we want to send
		// directly to the receiver, bypassing the channel buffer (if any).
		send(c, sg, ep, func() { unlock(&c.lock) }, 3)
		return true
	}
	// ...
}
```

### select is random

以下代码运行结果随机为`a`和`b`。

```go
package main

import (
	"log"
)

func main() {
	c1 := make(chan int, 1)
	c2 := make(chan int, 1)
	c1 <- 1
	c2 <- 2
	select {
	case <-c1:
		log.Printf("a")
	case <-c2:
		log.Printf("b")
	default:
		log.Printf("c")
	}
}
```

## goroutine

### 协程本质

参考[wiki-Coroutine](https://en.wikipedia.org/wiki/Coroutine):

> Coroutines are computer program components that generalize **subroutines** for non-preemptive multitasking, by allowing execution to be **suspended** and **resumed**.

协程的本质是一组泛化的子程序，允许执行被挂起和恢复。

![coroutine-state](image/coroutine-state.jpg)

```
var q := new queue

coroutine produce
    loop
        while q is not full
            create some new items
            add the items to q
        yield to consume

coroutine consume
    loop
        while q is not empty
            remove some items from q
            use the items
        yield to produce
```

### 实现原理

有两组API接口，可以切换当前进程的上下文。

```c
#include <setjmp.h>
// save stack context for non-local goto
int setjmp(jmp_buf env);
// jump to a saved stack context
void longjmp(jmp_buf env, int val);
```

```c
#include <ucontext.h>
void makecontext(ucontext_t *ucp, void (*func)(), int argc, ...);
// saves the current context in the structure pointed to by oucp, and then activates the context pointed to by ucp.
int swapcontext(ucontext_t *oucp, ucontext_t *ucp);
```

### 简单的协程实现

参考[c_coroutine](https://blog.codingnow.com/2012/07/c_coroutine.html):

```c
void coroutine_resume(struct schedule *S, int id) {
    struct coroutine *C = S->co[id];
    int status = C->status;
    switch (status) {
        case COROUTINE_READY:
            C->status = COROUTINE_RUNNING;
            uintptr_t ptr = (uintptr_t)S;
            makecontext(&C->ctx, (void (*)(void))mainfunc, 2, (uint32_t)ptr,
                        (uint32_t)(ptr >> 32));
            swapcontext(&S->main, &C->ctx);
            break;
        case COROUTINE_SUSPEND:
            memcpy(S->stack + STACK_SIZE - C->size, C->stack, C->size);
            C->status = COROUTINE_RUNNING;
            swapcontext(&S->main, &C->ctx);
            break;
        default:
            assert(0);
    }
}
```

```c
void coroutine_yield(struct schedule *S) {
    int id = S->running;
    struct coroutine *C = S->co[id];
    assert((char *)&C > S->stack);
    _save_stack(C, S->stack + STACK_SIZE);
    C->status = COROUTINE_SUSPEND;
    S->running = -1;
    swapcontext(&C->ctx, &S->main);
}
```

### coroutine & threads

![coroutine-and-threads](image/coroutine-and-threads.png)

- 协程是用户态调度的，线程是内核态调度的；

### G-P-M Model

![g-p-m](image/g-p-m.png)

- G：代表一个goroutine对象，每次go调用的时候，都会创建一个G对象，它包括栈、指令指针等;
- P：代表一个处理器，每一个运行的M都必须绑定一个P，就像线程必须在么一个CPU核上执行一样，由P来调度G在M上的运行，P的个数就是GOMAXPROCS；
- M：代表一个内核线程，所有的G最终在M上执行；

参阅[Scalable Go Scheduler Design Doc](https://docs.google.com/document/d/1TTj4T2JO42uD5ID9e89oa0sLKhJYD0Y_kqxDv3I3XMw/)。

### goroutine生命周期

![gsm-2](image/gsm-2.png)

### 协程切换的时机

goroutine切换(Context Switching)的时机：

- `go`创建新协程；
- syscall；
- 阻塞的调用，包括`chan`、`mutex`操作阻塞时；
- GC(Garbage collection)；
- 调用协程切换接口`runtime.Gosched()`；

### 抢占式调度

原理是在每个函数或方法的入口，加上一段额外的代码，让runtime有机会检查是否需要执行抢占调度。这种解决方案只能说局部解决了“饿死”问题，对于没有函数调用，纯算法循环计算的G，scheduler依然无法抢占。

### 阻塞式系统调用可能造成P迁移

正常运行时，每个P对应一个M，P对G队列调度在M上运行。

![local_run_queue](image/local_run_queue.png)

当G1中出现阻塞系统调用时，底层的M1被阻塞了，这样P1上所有的G都无法运行，CPU出现了空闲。因此有一个监控线程`sysmon`会创建一个新的M2，并将让P1迁移到M2上运行。

![thread_is_blocked](image/thread_is_blocked.png)

在G1的系统调用结束后，G1没有P关联了，会选择一个队列未满的P关联，如果所有P队列都满了，则放入Global队列。

### work stealing

当一个G在M里执行结束后，P会从队列中把该G取出，如果P的队列为空，即没有其他G可以执行，再尝试从Global队列中获取G，如果Global队列为空，M就随机选择另外一个P，从其可执行的G队列中取走一半。

![scheduler-concepts](image/scheduler-concepts.png)

![scheduler-stealing](image/scheduler-stealing.png)

## gomock

```go
// mockgen -source client.go -destination mock_client.go -package mock1

package mock1

type Message struct {
	a int
	b int
}

type Client interface {
	GetMessage(key string, index int) (*Message, error)
}
```

```go
package mock1

func handleMessage(client Client) (int, error) {
	msg, err := client.GetMessage("test", 1)
	if err != nil {
		return 0, err
	}
	return msg.a + msg.b, nil
}
```

```go
package mock1

import (
	"errors"
	"testing"
	"github.com/golang/mock/gomock"
)

func TestHandleMessage(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	mockClient := NewMockClient(mockCtrl)

	mockClient.EXPECT().GetMessage(gomock.Any(), gomock.Any()).Return(&Message{a: 2, b: 3}, nil)
	val, err := handleMessage(mockClient)
	if err != nil {
		t.Errorf("error: %s", err)
	}
	if val != 5 {
		t.Errorf("bad value: %d", val)
	}

	mockClient.EXPECT().GetMessage(gomock.Any(), gomock.Any()).Return(nil, errors.New("fail"))
	_, err = handleMessage(mockClient)
	if err == nil {
		t.Errorf("should be fail")
	}
}
```

## testify/assert

```go
package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSomething(t *testing.T) {
	assert := assert.New(t)

	var num int = 123
	assert.Equal(num, 123, "they should be equal")
	assert.EqualValues(num, int32(123))
	assert.NotEqual(num, 456, "they should not be equal")

	var p1 *int
	p2 := &num
	assert.Nil(p1)
	assert.NotNil(p2)

	slice := []int{0, 1, 2}
	assert.Equal(slice, []int{0, 1, 2})
	assert.Contains(slice, 1)
	assert.NotContains(slice, -1)
}
```
