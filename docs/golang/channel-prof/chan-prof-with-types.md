# chan prof with types

对于golang的channel进行性能测试，本章主要测试不同类型对channel性能的影响。

测试设计：

1. 保证使用到的内存都经过统一的随机初始化，保证真实得到分配；
2. 使用buffered channel，但测试次数超过cap的1000倍以上；
3. 空读，减少结果的拷贝；
4. 去除逻辑校验代码；
5. 每个测试时间5s，减少系统波动影响；

```go
// go test -benchmem -bench=. -benchtime=5s

package channel

import (
	"testing"
)

const (
	BufSize = 32 * 1024 // array can't be greater 64KB
	ChanCap = 256
)

type Object struct {
	data []byte
}

func makeTestString(len int) []byte {
	s := make([]byte, len)
	for i := 0; i < len; i++ {
		s[i] = byte(int('a') + (i % 26))
	}
	return s
}

func BenchmarkChanInt(b *testing.B) {
	ch := make(chan int, ChanCap)

	go func() {
		for {
			<-ch
		}
	}()

	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		ch <- n
	}
	b.StopTimer()
}

func BenchmarkChanSlice(b *testing.B) {
	str := makeTestString(BufSize)
	s := str
	ch := make(chan []byte, ChanCap)

	go func() {
		for {
			<-ch
		}
	}()

	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		ch <- s
	}
	b.StopTimer()
}

func BenchmarkChanStruct(b *testing.B) {
	str := makeTestString(BufSize)
	s := str
	ch := make(chan Object, ChanCap)

	go func() {
		for {
			<-ch
		}
	}()

	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		ch <- Object{data: s}
	}
	b.StopTimer()
}

func BenchmarkChanArray(b *testing.B) {
	str := makeTestString(BufSize)
	s := [BufSize]byte{}
	copy(s[:], str)
	ch := make(chan [BufSize]byte, ChanCap)

	go func() {
		for {
			<-ch
		}
	}()

	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		ch <- s
	}
	b.StopTimer()
}

func BenchmarkChanString(b *testing.B) {
	str := makeTestString(BufSize)
	s := string(str)
	ch := make(chan string, ChanCap)

	go func() {
		for {
			<-ch
		}
	}()

	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		ch <- s
	}
	b.StopTimer()
}
```

on Linux, Intel(R) Xeon(R) Gold 6148 CPU @ 2.40GHz。

```
BenchmarkChanInt-2              100000000               96.6 ns/op             0 B/op          0 allocs/op
BenchmarkChanSlice-2            100000000              111 ns/op               0 B/op          0 allocs/op
BenchmarkChanStruct-2           100000000              112 ns/op               0 B/op          0 allocs/op
BenchmarkChanArray-2             1000000             10200 ns/op               0 B/op          0 allocs/op
BenchmarkChanString-2           100000000              110 ns/op               0 B/op          0 allocs/op
```

主要结论：

1. channel耗时大约是在100ns左右，查阅其他测试，极限大约是50ns左右；
2. int内存拷贝消耗最低，`chan int`可以看做是channel的极限；
3. slice和string本质是一个`struct{buf,len,cap}`的结构，channel传递是浅拷贝；
4. struct要看具体的结构大小，本测试中只包含一个slice，所以和slice的性能接近；
5. array传递是深拷贝，不应该使用channel传递；
6. 除去array的情况，不同类型的channel性能差异不大，说明channel主要耗时并不在内存拷贝；
