# golang-http-close

## catalog

[TOC]

## client

[Client](https://golang.org/pkg/net/http/#Client)

```
If the returned error is nil, the Response will contain a non-nil Body which the user is expected to close. If the Body is not closed, the Client's underlying RoundTripper (typically Transport) may not be able to re-use a persistent TCP connection to the server for a subsequent "keep-alive" request.

The request Body, if non-nil, will be closed by the underlying Transport, even on errors.

On error, any Response can be ignored. A non-nil Response with a non-nil error only occurs when CheckRedirect fails, and even then the returned Response.Body is already closed.

Generally Get, Post, or PostForm will be used instead of Do.
```

```
When err is nil, resp always contains a non-nil resp.Body. Caller should close resp.Body when done reading from it.
```

client should to execute `resp.Body.Close()`。

below is right：

```go
client := http.DefaultClient
resp, err := client.Do(req)
if err != nil {
	return nil, err
}
defer resp.Body.Close()
```

but not need to check `res != nil`，below is wrong：

```go
client := http.DefaultClient
resp, err := client.Do(req)
defer func() {
	if resp != nil {
		resp.Body.Close()
	}
}()
if err != nil {
	return nil, err
}
```

## server

[Request](https://golang.org/pkg/net/http/#Request)

```go
// Body is the request's body.
//
// For client requests a nil body means the request has no
// body, such as a GET request. The HTTP Client's Transport
// is responsible for calling the Close method.
//
// For server requests the Request Body is always non-nil
// but will return EOF immediately when no body is present.
// The Server will close the request body. The ServeHTTP
// Handler does not need to.
Body io.ReadCloser
```

server auto close `request.Body`，not need program to execute.

## ref

- [What could happen if I don't close response.Body in golang?](https://stackoverflow.com/questions/33238518/what-could-happen-if-i-dont-close-response-body-in-golang/33238755)
- [When using go http client Do method, can the httpResponse and the error be not nil at the same time?](https://stackoverflow.com/questions/46423385/when-using-go-http-client-do-method-can-the-httpresponse-and-the-error-be-not-n)
