# timer

## syntax

```golang
func After(d Duration) <-chan Time
```

## example

```golang
package main

import (
	"fmt"
	"time"
)

var c chan int

func handle(int) {}

func main() {
	select {
	case m := <-c:
		handle(m)
	case <-time.After(5 * time.Minute):
		fmt.Println("timed out")
	}
}
```

## After memory leak

See [time.After](https://golang.org/pkg/time/#After), running timer will cause mem leak and no goroutine leak should stop the timer when not use.

> After waits for the duration to elapse and then sends the current time on the returned channel. It is equivalent to NewTimer(d).C. The underlying Timer is not recovered by the garbage collector until the timer fires. If efficiency is a concern, use NewTimer instead and call Timer.Stop if the timer is no longer needed.
