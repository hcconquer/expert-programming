# golang-style

## Naming

### package

use noun but not verb as package name.

```go
// good: noun
package config
package configuration
package management
package logging

// bad: verb
package configure
package log
```
