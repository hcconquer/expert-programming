# golang-dep

## 目录

[TOC]

## 背景

在大多数项目中，都引入了外部依赖库，对于管理外部库，有几种不同的方式：

| 方案                         | 优点 | 缺点 | 示例 |
| ---------------------------- | ---- | ---- | ---- |
| 将外部库安装到系统库路径中 |  | 1.侵入系统；2.程序编译依赖系统环境； |  `yum install ImageMagick-devel`    |
| 将外部库拷贝到项目中如lib目录 | 1.不侵入系统；2.编译不依赖环境； | 1.会导致项目文件增多；2.不方便多项目统一升级； | lib/im4java-1.2.0.jar |
| 通过配置文件管理依赖 | 1.不侵入系统；2.编译不依赖环境；3.外部库版本容易管理； | 1.需要部署库仓库或者依赖公网； | maven |
