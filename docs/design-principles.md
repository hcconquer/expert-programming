# Design Principles

## 组合优于继承

[组合优于继承(Composition over inheritance)](https://en.wikipedia.org/wiki/Composition_over_inheritance)。

### 多重继承问题

假设，一辆高级汽车Limo，自身是一辆Car，又安装了空调Airer，那Limo和Car、Airer的关系是怎样的呢？

出于代码实现方便的考虑，如果`Limo extends Car, Airer`，这样Limo就直接获得了Car和Airer的方法，代码实现是最快的，逻辑上可能也没有问题。

但随着业务变复杂，Car和Airer都增加stop()方法，并都有内部标识的字段`_state`，因为两个都有`stop()`，Limo需要对`stop()`和`_state`重载，内部调用`Car.stop()`和`Airer.stop()`，否则会`ambiguous name lookup`，但如果Car和Airer的`_state`有不同的定义值，就有可能会导致Car的行为出现异常，想要解决的话，只能重载`Car::print()`，这就需要去了解`Car::print()`的实现，违背了面向对象封装的初衷。

这种问题会随着继承的父类数量、继承层数、逻辑相关性，变得越来越复杂。

```c++
# include <iostream>

using namespace std;

class Car {
public:
    int _state;
    Car() {
        set_state(STATE_RUNNING);
    }
    virtual void stop() {
        std::cout << "Car stop" << std::endl;
        set_state(STATE_STOPPED);
    }
    virtual int get_state() {
        return _state;
    }
    virtual void set_state(int state) {
        _state = state;
    }
    virtual void print() {
        int state = get_state();
        switch(state) {
        case STATE_RUNNING:
            std::cout << "Car running" << std::endl;
            break;
        case STATE_STOPPED:
            std::cout << "Car stopped" << std::endl;
            break;
        default:
            std::cout << "Car unknown state: " << state << std::endl;
        }
    }
    static const int STATE_RUNNING = 1;
    static const int STATE_STOPPED = 2;
};

class Airer {
public:
    int _state;
    Airer() {
        set_state(STATE_RUNNING);
    }
    virtual void cooling() {
        std::cout << "Airer cooling" << std::endl;
    }
    virtual void stop() {
        set_state(STATE_STOPPED);
        std::cout << "Airer stop" << std::endl;
    }
    virtual int get_state() {
        return _state;
    }
    virtual void set_state(int state) {
        _state = state;
    }
    static const int STATE_RUNNING = 3;
    static const int STATE_STOPPED = 4;
};

class Limo : public Car, Airer  {
public:
#if defined A
    int _state;
#endif
    virtual void stop() {
        Car::stop();
        Airer::stop();
    }
#if defined A
    virtual int get_state() {
        return _state;
    }
    virtual void set_state(int state) {
        _state = state;
    }
#endif
};

int main(int argc, char *argv[]) {
    Limo *limo = new Limo();
    limo->stop();
    // if !define A: Car stopped
    // if defined A: Car unknown state: 4
    limo->print();

    return 0;
}
```

```shell
$ clang++ -o car car.cc 
$ ./car 
Car stop
Airer stop
Car stopped
```

```shell
$ clang++ -o car car.cc -DA
$ ./car 
Car stop
Airer stop
Car unknown state: 4
```

### is or has

对上述问题，本质原因是Limo is a Car，但Limo is not a Airer，Limo has a Airer，因此，Limo不应该继承Airer，而是应该通过组合的方式加入Airer。

如下示例，增加IArier接口，Limo实现接口IArier，并且通过`set_airer`允许组合不同的IAirer实现，同时，成员变量`_state`不会再存在歧义。

```c++
# include <iostream>

using namespace std;

class IAirer {
public:
    virtual void cooling() = 0;
    virtual void stop() = 0;
};

class Car {
public:
    int _state;
    Car() {
        set_state(STATE_RUNNING);
    }
    virtual void stop() {
        std::cout << "Car stop" << std::endl;
        set_state(STATE_STOPPED);
    }
    virtual int get_state() {
        return _state;
    }
    virtual void set_state(int state) {
        _state = state;
    }
    virtual void print() {
        int state = get_state();
        switch(state) {
        case STATE_RUNNING:
            std::cout << "Car running" << std::endl;
            break;
        case STATE_STOPPED:
            std::cout << "Car stopped" << std::endl;
            break;
        default:
            std::cout << "Car unknown state: " << state << std::endl;
        }
    }
    static const int STATE_RUNNING = 1;
    static const int STATE_STOPPED = 2;
};


class Airer : public IAirer{
public:
    int _state;
    Airer() {
        set_state(STATE_RUNNING);
    }
    virtual void cooling() {
        std::cout << "Airer cooling" << std::endl;
    }
    virtual void stop() {
        set_state(STATE_STOPPED);
        std::cout << "Airer stop" << std::endl;
    }
    virtual int get_state() {
        return _state;
    }
    virtual void set_state(int state) {
        _state = state;
    }
    static const int STATE_RUNNING = 3;
    static const int STATE_STOPPED = 4;
};

class Fan : public IAirer {
public:
    virtual void cooling() {
        std::cout << "Fan cooling" << std::endl;
    }
    virtual void stop() {
        std::cout << "Fan stop" << std::endl;
    }
};

class Limo : public Car, IAirer {
public:
    IAirer *_airer;
    virtual void stop() {
        Car::stop();
        _airer->stop();
    }
    void set_airer(IAirer *airer) {
        _airer = airer;
    }
    virtual void cooling() {
        _airer->cooling();
    }
};

int main(int argc, char *argv[]) {
    Limo *limo = new Limo();

    limo->set_airer(new Airer());
    limo->cooling();
    limo->stop();
    limo->print();

    limo->set_airer(new Fan());
    limo->cooling();
    limo->stop();
    limo->print();

    return 0;
}
```

```shell
$ clang++ -o car car.cc
$ ./car 
Airer cooling
Car stop
Airer stop
Car stopped
Fan cooling
Car stop
Fan stop
Car stopped
```

## 面向接口编程

[面向接口编程(Interface-based programming)](https://en.wikipedia.org/wiki/Interface-based_programming)。

### duck typing

[Duck typing](https://en.wikipedia.org/wiki/Duck_typing)。

> Duck typing is concerned with establishing the suitability of an object for some purpose, using the principle, "If it walks like a duck and it quacks like a duck, then it must be a duck."

如下示例，Limo组合了Car，并且通过`(*Limo) cooling()`和`(*Car) stop()`实现了IAirer所有的方法，因而Limo成为了IAirer的实现。

```go
package main

import (
	"fmt"
)

type IAirer interface {
	cooling()
	stop()
}

type Car struct {
}

func (car *Car) stop() {
	fmt.Printf("Car stop\n")
}

type Limo struct {
	Car
}

func (limo *Limo) cooling() {
	fmt.Printf("Limo cooling\n")
}

func run(airer IAirer) {
	airer.cooling()
	airer.stop()
}

func main() {
	limo := new(Limo)
	run(limo)
}
```

```shell
$ ./car
Limo cooling
Car stop
```

C++示例中Limo通过实现接口IAirer，golang示例中通过duck type实现IAirer，都是面向接口编程。

## 里氏替换原则

[里氏替换原则(Liskov substitution principle)](https://en.wikipedia.org/wiki/Liskov_substitution_principle)。

> Substitutability is a principle in object-oriented programming stating that, in a computer program, if S is a subtype of T, then objects of type T may be replaced with objects of type S (i.e. an object of type T may be substituted with any object of a subtype S) without altering any of the desirable properties of the program (correctness, task performed, etc.).
