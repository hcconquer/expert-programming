# image

## catalog

[TOC]

## format

jpg webp

[Webp](https://developers.google.com/speed/webp/)

## compression

### google/guetzli

offical-site: https://github.com/google/guetzli

### dropbox/lepton

offical-site: https://github.com/dropbox/lepton

[Lepton image compression: saving 22% losslessly from images at 15MB/s](https://blogs.dropbox.com/tech/2016/07/lepton-image-compression-saving-22-losslessly-from-images-at-15mbs/)
