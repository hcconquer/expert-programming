# bin format

## linux

### object

### archive

```shell
# list
ar t libx.a
# printf
ar t libx.a
# add
ar r libx.a x.o
# delete
ar d x.o libx.a
```

### information

```shell
# get compiler info
objdump -s --section .comment ${bin}
```

result:

```
output/bin/gm:     file format elf64-x86-64

Contents of section .comment:
 0000 4743433a 2028474e 55292034 2e382e33  GCC: (GNU) 4.8.3
 0010 00474343 3a202847 4e552920 342e382e  .GCC: (GNU) 4.8.
 0020 3200                                 2.             
```
