# Distributed Storage

## 目录

[TOC]

## 概述

### 需求和挑战

应用程序对存储系统的要求：

- 分布式(Distributed)：部署在不同地区，尽可能的减少数据传输时间；
- 一致性(Consistency)：所有节点在同一时间的数据完全一致；
- 大规模(Massive)：十万级别的集群，PB级别的数据；
- 高并发(High Concurrent)：每秒千万以上的请求；
- 高性能(High Performance)：尽可能的利用硬件资源；
- 高可用(High Availability)：永远可用或者故障时间很短，平均无故障时间(MTTF)长；
- 高可靠(High Reliability)：能长期运行无故障，平均故障间隔时间(MTBF)长；
- 扩展性(Scalability)：能线性伸缩；
- 低成本(Low Cost)：尽可能的使用底层本的硬件；
- 完整性(Completeness)：数据完整不会缺失；
- 正确性(Correctness)：逻辑正确，如满足ACID约束；
- 安全性(Security)：不会被攻击和窃取；

但物理世界有很多约束和限制：

- 节点宕机(fail-stop): 节点持续宕机，而且无法恢复；
- 节点宕机恢复(fail-recover): 节点宕机一段时间后恢复；
- 消息传递异步无序(asynchronous): 物理网络是不可靠的，存在消息延时和丢失，节点间消息传递做不到同步有序(synchronous)；
- 网络分区(network partition): 网络链路出现问题，将节点隔离成多个部分；
- 拜占庭将军问题(byzantine failure): 节点宕机或逻辑失败，甚至出现错误的干扰信息；

### CAP

#### 定义

CAP定理说，下面三个期望的属性中，你最多只能同时达到两个：

- C：一致性(Consistency)

> all nodes see the same data at the same time.

所有节点在同一时间的数据完全一致，也就是**强一致性(Strong Consistency)**。也叫全认同(agreement)，即所有N个节点都认同一个结果。

- A：读取和更新的100％可用性(Availability)

> Reads and writes always succeed.

所有读写请求在一定时间内得到非错的响应。也叫可结束(termination)，决议过程在一定时间内结束，不会无休止地进行下去。

- P：对网络分区(Partitions)的容忍

> the system continues to operate despite arbitrary message loss or failure of part of the system.

在网络分区的情况下，被分隔的节点仍能正常对外服务，**P是物理世界本身的局限**。

#### CAP的取舍

CAP不能同时达到，对其中一项进行舍弃：

- CA without P

  P是分布式系统所受到的物理限制，如果不要求P，则是一个单机系统。

  这种系统的代表是IBM中高端服务器，已逐渐被分布式系统取代。

  CA系统的缺点：成本过高，而且无法大规模平行扩展。

- CP without A

  如果不要求A，任意P出现的时候，所有分区都停止服务，网络恢复后，所有节点同步数据再恢复服务。

  传统的数据库分布式事务都属于这种模式，银行、电商等涉及到钱的业务都是CP系统。

  CP系统的特点：保证了正确性，但服务不可用会对用户造成影响。

- AP wihtout C

  如果不要求C，任意P出现的时候，所有分区继续提供服务，网络恢复后，所有节点合并数据或主节点强制覆盖从节点数据。

  现在众多的NoSQL都属于此类，论坛、社交软件等不追求严格正确性的业务都是AP系统。

  AP系统的特点：保证了高可用，但舍弃了正确性，**有损服务，允许万有一失**。

#### 相关算法

- Paxos

  Paxos是一种基于消息传递且具有高度容错特性的一致性算法。

- 2PC

  二阶提交协议（Two Phase Commitment Protocol），是为了使基于分布式系统架构下的所有节点在进行事务提交时保持一致性而设计的一种算法。

- Gossip

  Gossip算法又被称为反熵（Anti-Entropy），熵是物理学上的一个概念，代表杂乱无章，而反熵就是在杂乱无章中寻求一致，Gossip是一个最终一致性算法。

![cap_paxos_2pc_gossip](distributed-storage/image/cap_paxos_2pc_gossip.jpg)

#### 工程量化

对CAP一直有很多误解，主要是CAP只是一个定性的理论，**在强一致(C)的情况下100%的可用(A)**，但实际上并不需要：

- 可用性并不总要求100%

  常见云服务可靠性`P(A)`为99.99%，如果网络分区的概率`P(P)`小于系统不可靠的概率`1-P(A)`，那么系统实际上是同时满足了CAP。

- 很多场景并不需要强一致性

  比如现实生活中A转账给B，允许在A扣款后经过一定的延迟B才收到。

关于量化这一点，在“[CAP理论十二年回顾：”规则”变了](http://www.infoq.com/cn/articles/cap-twelve-years-later-how-the-rules-have-changed)” 这篇文章中也有讨论。

### ACID

**定义：** 所谓事务，它是一个操作序列，这些操作要么都执行，要么都不执行，它是一个不可分割的工作单位。事务(Transaction)应该具有四个特性：

- 原子性（Atomicity）
- 一致性（Consistency）
- 隔离性（Isolation）
- 持久性（Durability）

#### 原子性(Atomicity)

> 原子性是指事务是一个不可再分割的工作单位，事务中的操作要么都发生，要么都不发生。

在事务中的扣款和加款两条语句，要么都执行，要么就都不执行。否则如果只执行了扣款语句，就提交了，此时如果突然断电，A账号已经发生了扣款，B账号却没收到加款，就会引起纠纷。

#### 一致性(Consistency)

> 一致性是指在事务开始之前和事务结束以后，数据库的完整性约束没有被破坏。

> The [consistency](https://en.wikipedia.org/wiki/Consistency_(database_systems)) property ensures that any transaction will bring the database from one valid state to another. Any data written to the database must be valid according to all defined rules, including [constraints](https://en.wikipedia.org/wiki/Integrity_constraints), [cascades](https://en.wikipedia.org/wiki/Cascading_rollback), [triggers](https://en.wikipedia.org/wiki/Database_trigger), and any combination thereof. This does not guarantee correctness of the transaction in all ways the application programmer might have wanted (that is the responsibility of application-level code), but merely that any programming errors cannot result in the violation of any defined rules.

ACID定义的Consistency跟现在大家常说的一致性(CAP的C)不一样。区别是，ACID把C限制在一个事务单元内，强调事务必须保证数据从一个一致性状态进入另一个一致性状态，事务的结束和数据的一致性之间没有时差。

**常见的误区**：

- CAP的C和ACID的C是完全无关的两个概念，两者并不会产生影响；

- "对银行A->B转帐事务，应该保证事务结束前后A+B账户总额相同"，这并不是Consistency的约束，这个错误来自于对[wikipedia-ACID](https://en.wikipedia.org/wiki/ACID)的片面解读，原文中表定义有`CHECK`，这类似于`unique index`的约束。

```sql
CREATE TABLE acidtest (A INTEGER, B INTEGER, CHECK (A + B = 100));
```

#### 隔离性(Isolation)

> 多个事务并发访问时，事务之间是隔离的，一个事务不应该影响其它事务运行效果。

这指的是在并发环境中，当不同的事务同时操纵相同的数据时，每个事务都有各自的完整数据空间。由并发事务所做的修改必须与任何其他并发事务所做的修改隔离。事务查看数据更新时，数据所处的状态要么是另一事务修改它之前的状态，要么是另一事务修改它之后的状态，事务不会查看到中间状态的数据。

事务之间的相互影响分为几种：

- 脏读(Dirty reads)

  脏读意味着一个事务读取了另一个事务未提交的数据，而这个数据是有可能回滚的。

- 不可重复读(Non-repeatable reads)

  不可重复读意味着，在数据库访问中，一个事务范围内两个相同的查询却返回了不同数据。

- 幻读(Phantom reads)

  是指当事务不是独立执行时发生的一种现象，例如第一个事务对一个表中的数据进行了修改，这种修改涉及到表中的全部数据行，同时第二个事务也修改这个表中的数据，这种修改是向表中插入一行新数据，那么以后就会发生操作第一个事务的用户发现表中还有没有修改的数据行，就好象发生了幻觉一样。

- 丢失更新

  两个事务同时读取同一条记录(n)，A先修改记录(n+1)，B也修改记录(n+2)，B提交数据后(n+2)B的修改结果覆盖了A的修改结果(n+1)，与期望不符合(n+3)。

ANSI SQL isolation有四种隔离级别，由弱到强分别是：

- 读未提交(Read Uncommitted)

  允许脏读，也就是可能读取到其他会话中未提交事务修改的数据，业务的中间状态对外可见，不需要事务隔离。

- 读提交(Read Committed, RC)

  **也叫不重复读(Non-Repeatable Read)**，只能读取到已经提交的数据，业务的中间状态对外不可见，但长事务可能会受到短事务提交的影响，多次读取同一个数据得到不同的结果，**Oracle等多数数据库默认都是该级别**。

- 可重复读(Repeatable Read, RR)

  在同一个事务内的查询都是事务开始时刻一致的，**InnoDB默认级别**。在SQL标准中，该隔离级别消除了不可重复读，但是还存在幻象读。

  例如，事务A为`update xxx set value=value+1`，而事务B为`insert into xxx value=2`，如果A早于B开始但晚于B结束，那由于A可重复读，必然无法观察到B的修改，也就无法修改到B新增的数据。如果此时value有唯一性约束，那可能会导致事务A失败。

- 串行化(Serializable)

  完全串行化的读，每次读都需要获得表级共享锁，读写相互都会阻塞。

隔离级别与事务影响([wiki](https://en.wikipedia.org/wiki/Isolation_(database_systems)))：

| Isolation level  | Dirty reads | Non-repeatable reads | Phantoms    |
| ---------------- | ----------- | -------------------- | ----------- |
| Read Uncommitted | *may occur* | *may occur*          | *may occur* |
| Read Committed   | don't occur | *may occur*          | *may occur* |
| Repeatable Read  | don't occur | don't occur          | *may occur* |
| Serializable     | don't occur | don't occur          | don't occur |

#### 持久性(Durability)

> 持久性，意味着在事务完成以后，该事务所对数据库所作的更改便持久的保存在数据库之中，并不会被回滚。

即使出现了任何事故比如断电等，事务一旦提交，则持久化保存在数据库中。

### BASE

BASE是Basically Available（基本可用）、Soft state（软状态）和Eventually consistent（最终一致性）三个短语的缩写。BASE理论是对CAP中一致性和可用性权衡的结果，其来源于对大规模互联网系统分布式实践的总结，是基于CAP定理逐步演化而来的。

BASE理论的核心思想是：**即使无法做到强一致性，但每个应用都可以根据自身业务特点，采用适当的方式来使系统达到最终一致性**。

#### 基本可用

基本可用是指分布式系统在出现故障的时候，允许损失部分可用性。比如：

- 响应时间上的损失

  正常情况下，微信转账可能需要1秒完成，当出现故障时，提示用户稍后查看结果。

- 系统功能上的损失

  淘宝在双十一关闭退货和退款功能。

**有损服务，柔性可用**：重点接口重点保障，次要接口有损保障，并提供紧急时刻**服务降级**的能力。即不保证完美体验，对非关键路径进行有损，提升系统的可用性。

#### 软状态

软状态指允许系统中的数据存在中间状态，并认为该中间状态的存在不会影响系统的整体可用性，即允许系统在不同节点的数据副本之间进行数据同步的过程存在延时。

#### 最终一致性

最终一致性强调的是所有的数据副本，在经过一段时间的同步之后，最终都能够达到一个一致的状态。因此，最终一致性的本质是需要系统保证最终数据能够达到一致，而不需要实时保证系统数据的强一致性。

总的来说，BASE理论面向的是大型高可用可扩展的分布式系统，和传统的事物ACID特性是相反的，**它完全不同于ACID的强一致性模型，而是通过牺牲强一致性来获得可用性，并允许数据在一段时间内是不一致的，但最终达到一致状态**。但同时，在实际的分布式场景中，不同业务单元和组件对数据一致性的要求是不同的，因此在具体的分布式系统架构设计过程中，ACID特性和BASE理论往往又会结合在一起。

### CAP/ACID/BASE的关系

- ACID放弃了分区容忍(P)
- BASE放弃了强一致性(C)

![cap_acid_base](distributed-storage/image/cap_acid_base.jpg)

### 多版本并发控制(MVCC)

多版本并发控制（Multiversion concurrency control，MVCC），是一种并发控制方法，在数据库管理系统中用来提供并发访问数据库的能力，在编程语言中实现事务内存。

MVCC最大的特点是**读不阻塞写，写不阻塞读**。

### 纠删码(EC)

纠删码(Erasure Code, EC)，是一种前向错误纠正技术(Forward Error Correction, FEC)，主要应用在网络传输中避免包的丢失， 存储系统利用它来提高 存储 可靠性。相比多副本复制而言， 纠删码能够以更小的数据冗余度获得更高数据可靠性， 但编码方式较复杂，需要大量计算 。纠删码只能容忍数据丢失，无法容忍数据篡改，纠删码正是得名与此。

EC可以将n份原始数据，增加m份数据，并能通过n+m份中的任意n份数据，还原为原始数据。即如果有任意小于等于m份的数据失效，仍然能通过剩下的数据还原出来。

目前，纠删码技术在分布式存储 系统中的应用主要有几类：

- Array Code，阵列纠删码，RAID5、RAID6等
- RS(Reed-Solomon)，里德-所罗门类纠删码
- LDPC(LowDensity Parity Check Code)，低密度奇偶校验纠删码

RAID是EC的特殊情况。在传统的RAID中，仅支持有限的磁盘失效，RAID5只支持一个盘失效，RAID6支持两个盘失效，而EC支持多个盘失效。

## 全局时间

### 全局时间的重要性

分布式系统的工程实践中，由于物理设备的限制，无法实现最理想的强一致性，因而考虑降级实现线性一致性，全局的时间是非常重要的：

- 外部逻辑的因果关系
  如果A完成后才会触发B，那系统所有模块看到A的时间一定要早于B，否则B可能因缺乏条件失败，或产生问题数据。
- ​分布式系统基于日志复制
  AB操作和BA操作可能会产生不同的结果，如果不同副本操作的顺序不一致，那副本间的数据可能会不一致。
- 多版本并发控制(MVCC)的使用
  当前流行的数据库(Oracle等)都是用MVCC，避免使用锁以提高性能，如果A在B之前完成，那B一定要能读取到A修改的数据，否则就产生了脏读。

### Logic Clock

### Vector Clock

### True Time

### Hybrid Time

#### cockroachdb HLC

> Each cockroach node maintains a hybrid logical clock (HLC) as discussed in the [Hybrid Logical Clock paper](http://www.cse.buffalo.edu/tech-reports/2014-04.pdf). HLC time uses timestamps which are composed of a physical component (thought of as and always close to local wall time) and a logical component (used to distinguish between events with the same physical component). It allows us to track causality for related events similar to vector clocks, but with less overhead. In practice, it works much like other logical clocks: When events are received by a node, it informs the local HLC about the timestamp supplied with the event by the sender, and when events are sent a timestamp generated by the local HLC is attached.

## 锁

### 类型

#### 悲观锁

悲观锁(*Pessimistic Locking*)，指的是对数据被修改持保守态度(悲观)，因此，在整个数据处理过程中，将数据处于锁定状态。

> *Pessimistic locking*: a user who reads a record with the intention of updating it places an exclusive lock on the record to prevent other users from manipulating it. This means no one else can manipulate that record until the user releases the lock. The downside is that users can be locked out for a very long time, thereby slowing the overall system response and causing frustration.

#### 乐观锁

乐观锁(*Optimistic Locking*)，指的是对数据被修改持激进态度(乐观)，假设数据一般情况下不会造成冲突，所以在数据进行提交更新的时候，才会正式对数据的冲突与否进行检测，如果发现冲突了，则让返回用户错误的信息，让用户决定如何去做。

> *Optimistic locking*: this allows multiple concurrent users access to the database whilst the system keeps a copy of the initial-read made by each user. When a user wants to update a record, the application determines whether another user has changed the record since it was last read. The application does this by comparing the initial-read held in memory to the database record to verify any changes made to the record. Any discrepancies between the initial-read and the database record violates concurrency rules and hence causes the system to disregard any update request. An error message is generated and the user is asked to start the update process again. It improves database performance by reducing the amount of locking required, thereby reducing the load on the database server. It works efficiently with tables that require limited updates since no users are locked out. However, some updates may fail. The downside is constant update failures due to high volumes of update requests from multiple concurrent users - it can be frustrating for users.

## 分布式事务

### 2PC

### 3PC

### Paxos Commit

## 单机存储

### Rollback Journal

### WAL

WAL(Write-Ahead Logging)是一种高效的日志算法，几乎是所有非内存数据库提升写性能的不二法门，基本原理是在数据写入之前首先顺序写入日志，然后再写入缓存，等到缓存写满之后统一落盘。之所以能够提升写性能，是因为WAL将一次随机写转化为了一次顺序写加一次内存写。提升写性能的同时，WAL可以保证数据的可靠性，即在任何情况下数据不丢失。假如一次写入完成之后发生了宕机，即使所有缓存中的数据丢失，也可以通过恢复日志还原出丢失的数据。

WAL （参看 [sqlite 相关文献](http://www.sqlite.org/wal.html)）常常是与 rollback journal 相比较的，区别在于 rollback journal 是将原始 page 写入到 journal 中然后将新内容覆盖原先的 page，这样在 rollback 的时候将 journal 中的 page 恢复即可；WAL 恰好相反，原始内容仍然放在原先的 page 里，新的内容首先写入到单独的 WAL 文件里

### LSM

#### 原理

LSM的基本思想是将修改的数据保存在内存，达到一定数量后在将修改的数据批量写入磁盘，在写入的过程中与之前已经存在的数据做合并。同B树存储模型一样，LSM存储模型也支持增、删、读、改以及顺序扫描操作。**LSM模型利用批量写入解决了随机写入的问题，虽然牺牲了部分读的性能，但是大大提高了写的性能。**

#### 整体结构

典型的LSM本由MemTable、Immutable MemTable、SSTable等多个部分组成。

如图是google bigtable的IO流程：

![big_table_memtable_sstable](distributed-storage/image/big_table_memtable_sstable.jpg)

如图是leveldb数据在内存和磁盘的分布：

![lsm-leveldb](distributed-storage/image/lsm-leveldb.png)

#### MemTable

MemTable 和 Immutable MemTable两者都位于内存，属于内存的数据结构，两者是一样的数据结构，区别在于MemTable可读可写，而Immutable MemTable是只读的，不允许写入。MemTable承接来自客户的key-value请求，当写入的数据占用内存到指定门限，就会自动转成Immutable Memtable，等待Dump到磁盘中。

MemTable的实现有很多种不同的方式，主要是SkipList和HashTable，需要range查询用SkipList，不需要range查询用HashTable。

#### SSTable

## 分布式事务

### XA规范

Open Group定义了一套[DTP](https://en.wikipedia.org/wiki/Distributed_transaction)(Distributed Transaction Processing)分布式模型，主要含有：

- 应用程序(AP, Application)，一般是业务应用；
- 事务管理器(TM, Transaction Manager)；
- 资源管理器(RM, Resource Manager)，一般是数据库
- 通讯资源管理器(CRM, Communication Resource Manager)，一般是消息中间件；

[XA](https://en.wikipedia.org/wiki/X/Open_XA)是DTP模型定义TM和RM之前通讯的接口规范。XA接口函数由数据库厂商提供。TM交易中间件用它来通知数据库事务的开始、结束以及提交、回滚等。

二阶段提交(2PC)和三阶段提交(3PC)就是根据这种思想衍生而来。

### 二阶段提交(2PC)

二阶段提交(2PC, Two-phase Commit Protocol)，顾名思义它分成两个阶段：
- 准备阶段(Commit-Request Phase)，节点称为协调者(coordinator)进行提议(propose)并收集其他节点(participants)的反馈(vote)；
- 提交阶段(Commit Phase)，协调者根据反馈决定提交(commit)或中止(abort)事务；

#### 准备阶段(Commit-Request Phase)

在阶段1中，coordinator发起一个提议，分别问询各participant是否接受。

#### 提交阶段(Commit Phase)

coordinator根据participant的反馈，提交或中止事务，如果participant全部同意则提交，只要有一个participant不同意就中止。

#### 缺陷

在2PC下，如果coordinator在发起提议后宕机，那么participant将进入阻塞(block)状态、一直等待coordinator回应以完成该次决议。

### 三阶段提交(3PC)

为了解决2PC下单点(特别是coordinator)的问题，将原来的Commit-Request拆分为CanCommit和PreCommit，即为3PC。

![three-phase_commit_diagram](distributed-storage/image/three-phase_commit_diagram.png)

#### CanCommit

协调者向参与者发送commit请求，参与者如果可以提交就返回Yes响应，否则返回No响应。(如何判断是否可以提交不同的算法有不同的机制，但主要作用是确定Cohort具备基本的完成Commit条件，并不会执行事务操作)

事务询问。Coordinator向Cohort发送CanCommit请求。询问是否可以执行事务提交操作。然后开始等待参与者的响应。

响应反馈。Cohort接到CanCommit请求之后，正常情况下，如果其自身可以顺利执行事务，则返回Yes响应，并进入预备状态。否则反馈No。

#### PreCommit

Coordinator根据Cohort的反应情况来决定是否可以继续事务的PreCommit操作。根据响应情况，有以下两种可能。假如Coordinator从所有的Cohort获得的反馈都是Yes响应，那么就会进行事务的预执行：

发送预提交请求。Coordinator向Cohort发送PreCommit请求，并进入Prepared阶段。

事务预提交。Cohort接收到PreCommit请求后，会执行事务操作，并将undo和redo信息记录到事务日志中。

响应反馈。如果Cohort成功的执行了事务操作，则返回ACK响应，同时开始等待最终指令。

假如有任何一个Cohort向Coordinator发送了No响应，或者等待超时之后，Coordinator都没有接到Cohort的响应，那么就中断事务：

发送中断请求。Coordinator向所有Cohort发送abort请求。

中断事务。Cohort收到来自Coordinator的abort请求之后（或超时之后，仍未收到Cohort的请求），执行事务的中断。

#### DoCommit

该阶段进行真正的事务提交，也可以分为以下两种情况。

执行提交

发送提交请求。Coordinator接收到Cohort发送的ACK响应，那么他将从预提交状态进入到提交状态。并向所有Cohort发送doCommit请求。
事务提交。Cohort接收到doCommit请求之后，执行正式的事务提交。并在完成事务提交之后释放所有事务资源。
响应反馈。事务提交完之后，向Coordinator发送ACK响应。
完成事务。Coordinator接收到所有Cohort的ACK响应之后，完成事务。
中断事务

Coordinator没有接收到Cohort发送的ACK响应（可能是接受者发送的不是ACK响应，也可能响应超时），那么就会执行中断事务。

发送中断请求。Coordinator向所有Cohort发送abort请求
事务回滚。Cohort接收到abort请求之后，利用其在阶段二记录的undo信息来执行事务的回滚操作，并在完成回滚之后释放所有的事务资源。
反馈结果。Cohort完成事务回滚之后，向Coordinator发送ACK消息
中断事务。Coordinator接收到参与者反馈的ACK消息之后，执行事务的中断。

在doCommit阶段，如果Cohort无法及时接收到来自Coordinator的doCommit或者rebort请求时，会在等待超时之后，会继续进行事务的提交。（其实这个应该是基于概率来决定的，当进入第三阶段时，说明参与者在第二阶段已经收到了PreCommit请求，那么Coordinator产生PreCommit请求的前提条件是他在第二阶段开始之前，收到所有参与者的CanCommit响应都是Yes。一旦参与者收到了PreCommit，意味他知道大家其实都同意修改了。所以，一句话概括就是，当进入第三阶段时，由于网络超时等原因，虽然参与者没有收到commit或者abort响应，但是他有理由相信：成功提交的几率很大。）

#### 异常处理

- 阶段1: coordinator或watchdog未收到宕机participant的vote，直接中止事务；宕机的participant恢复后，读取logging发现未发出赞成vote，自行中止该次事务。
- 阶段2: coordinator未收到宕机participant的precommit ACK，但因为之前已经收到了宕机participant的赞成反馈(不然也不会进入到阶段2)，coordinator进行commit；watchdog可以通过问询其他participant获得这些信息，过程同理；宕机的participant恢复后发现收到precommit或已经发出赞成vote，则自行commit该次事务。
- 阶段3: 即便coordinator或watchdog未收到宕机participant的commit ACK，也结束该次事务；宕机的participant恢复后发现收到commit或者precommit，也将自行commit该次事务。

#### 总结

因为有了准备提交(prepare to commit)阶段，3PC的事务处理延时也增加了1个RTT，变为3个RTT(propose+precommit+commit)，但是它防止participant宕机后整个系统进入阻塞态，增强了系统的可用性，对一些现实业务场景是非常值得的。

最本质来讲，3PC避免了状态停滞，在2PC有可能因为各种原因，产生状态停滞。但是3PC会让状态继续下去，虽然有可能继续下去是错的。

3PC的问题是明显的（并没有完全解决2PC的问题3）：

即如果进入PreCommit后，Coordinator发出的是abort请求，如果只有一个Cohort收到并进行了abort操作，而其他对于系统状态未知的Cohort会根据3PC选择继续Commit，那么系统的不一致性就存在了。所以无论是2PC还是3PC都存在问题，后面会继续了解那个传说中唯一的一致性算法Paxos Commit。

### Paxos Commit

### 算法对比

#### 特性对比

![distributed-transaction-comparison](distributed-storage/image/distributed-transaction-comparison.jpg)

#### 2PC与3PC的区别

相对于2PC，3PC主要解决的单点故障问题，并减少阻塞，因为一旦参与者无法及时收到来自协调者的信息之后，他会默认执行commit。而不会一直持有事务资源并处于阻塞状态。但是这种机制也会导致数据一致性问题，因为，由于网络原因，协调者发送的abort响应没有及时被参与者接收到，那么参与者在等待超时之后执行了commit操作。这样就和其他接到abort命令并执行回滚的参与者之间存在数据不一致的情况。

## 思考讨论

### 什么是分布式系统的一致性？

`一致性`是分布式系统中很常见的术语，但描述的内涵是不一样的：

- 一致性Hash的Consistent

  指的是节点变化前后的数据映射的一致。

- CAP的Consistence

  指的是多副本数据(A1, A2, A3)之间的一致，如果写成功了，那必然可以读到最新的数据，也就是(A1, A2, A3)的数据要么不可用，要么是一致的。

  > Every read receives the most recent write or an error.

- ACID的Consistency

  指的是数据库的状态是一致的，所有状态都符合数据库的约束。

  > The consistency property ensures that any transaction will bring the database from one valid state to another. Any data written to the database must be valid according to all defined rules, including constraints, cascades, triggers, and any combination thereof. This does not guarantee correctness of the transaction in all ways the application programmer might have wanted (that is the responsibility of application-level code), but merely that any programming errors cannot result in the violation of any defined rules.

  中文网站上有一种错误的解释：

  > A给B转账前后，(A.value+B.value)的值保持不变。

  这是对[wikipedia-ACID](https://en.wikipedia.org/wiki/ACID)断章取义的理解，因为wiki的例子中包含`CHECK (A + B = 100)`约束，是约束一致性导致的限制。

  ```sql
  CREATE TABLE acidtest (A INTEGER, B INTEGER, CHECK (A + B = 100));
  ```

- 分布式事务的Consensus

  指的是如果某个事务操作的资源分布在不同的资源组(A, B, C)上，要么(A, B, C)都提交，要么都不提交，不会出现部分提交而部分失败的情况。

  > Consistency It is impossible for one RM to be in the committed state and another to be in the aborted state.

这几者之间的关系：

- 一致性Hash的Consistent是相对独立的概念，与其他没有太大关联；

- ACID的Consistency依赖于分布式事务的Consensus；

  假设，分布式数据库涉及到资源`A`和`B`，而数据库有约束`A<B`，原有数据`A=2`和`B=3`，符合ACID的Consistency，如果`T(value*=2)`提交成功之后，`A=4`，但`B`修改失败，`B=3`。
  此时，那对于ACID的Consistency是不正确的，因为违反了约束`A<B`。

- 分布式事务的Consensus依赖于CAP的Consistence；

  假设，分布式数据库涉及到资源`A`和`B`，而数据库有约束`A<B`，原有数据`A(A1, A2, A3)=2`和`B(B1, B2, B3)=3`，符合ACID的Consistency，如果`T(value*=2)`提交成功之后，`A(A1, A2, A3)=4`，`B(B1, B2)=6`，但`B3`失败出现不一致`B3=3`。

  此时，如果读取到了`B=3`，对于分布式事务的Consensus是不正确的，因为看起来资源`B`提交失败了，对于ACID的Consistency也是不正确的，因为违反了约束`A<B`。

### 在一个事务内，写是否对后续的读可见？

在Spanner中是不可见的：

> 4.2.1 Read-Write Transactions
> Like Bigtable, writes that occur in a transaction are
> buffered at the client until commit. As a result, reads
> in a transaction do not see the effects of the transaction’s
> writes.

但在mysql中是可见的：

```sql
create database t;
create table t(id int);

begin;
insert into t(id) value(1);
select * from t; // has data in current transaction, not in other
commit;
```

### 2PC和Paxos都用来解决一致性问题，两者之间什么关系？

在分布式数据库中，一个分布式事务T可能会涉及到两类节点：

- 不同数据的分片节点，记为(A, B, C)；
- 同一份数据的多个副本节点(A1, A2, A3)；

两者解决的问题不一样：

- Paxos要解决的问题：当网络分区时，A的副本(A1, A2, A3)数据是一致的，也就是同副本的一致性；
- 2PC要解决的问题：当网络正常的情况下，或者基于Paxos等同正常的情况下，要符合事务ACID的特性，对于A而言，就是(A, B, C)上的数据要么都提交成功，要么都提交失败，其实是分片数据对同一个事务处理的一致性。

所以，2PC的一致性和Paxos的一致性描述的内容是不同的，如果说有关系的话：

- 部分2PC的实现是基于Paxos的；
- Paxos Commit algorithm也可以实现分布式事务的一致性；

## 分布式数据库产品

### TiDB

#### 主要模块

| 模块   | 功能                                       |
| ---- | ---------------------------------------- |
| TiDB | 主要是负责 SQL 的解析器和优化器，它相当于计算执行层，同时也负责客户端接入和交互。 |
| TiKV | 是一套分布式的 Key-Value 存储引擎，它承担整个数据库的存储层，数据的水平扩展和多副本高可用特性都是在这一层实现。TiKV是可替换的，早期的版本中使用的是HBase。 |
| PD   | 根据数据分布状况以及各个存储节点的负载来采取合适的调度策略，维持整个系统的平衡与稳定。 |

#### 关键技术

- Google F1/Spanner的实现；
- 协议与MySQL兼容；
- 内部通信使用gRPC；
- 使用Raft保证一致性;

## Alibaba

### 组件体系

| 组件   | 作用   |
| ---- | ---- |
| 夸父   | 网络   |
| 女娲   | 协同   |
| 伏羲   | 调度   |
| 盘古   | 存储   |
| 神农   | 监控   |

### 盘古
| 组件     | 作用   |
| ------ | ---- |
| Client | 客户端  |
| Master | 元数据  |
| Chunk  | 真实数据 |

#### Volume

Volume，文件？

#### 一致性

三副本强一致。

#### 完整性

- 端到端的数据校验，主要是CRC校验；
- 静默数据检查；

#### 性能

读:150K QPS
写: 50K QPS
如何高性能？

#### 成本

纠删码

#### 面向容错的设计思想

- Checksum和数据自愈；
- 服务监控、环境检查、故障报警；
- 误删保护，比如回收站和Read-Only属性；

#### Chunk

- SSD作为Cache，然后转储到机械盘；
- 随机写转为顺序写；

#### 协程

- C++的协程是单线程的，不能使用Sleep这样的阻塞操作；
- C++的异常可能会导致协程出现问题；

#### 工程实践

- 持续集成，问题代码回退；
- 没有专职测试，端到端的全栈研发；

### Tair

#### ConfigServer

ConfigServer获取数据分布的对照表，类似于Ceph的CRUSH算法，Client将会获取和缓存对照表。

对照表有一个版本号，当数据节点发生增删时，对照表更新，版本号增加，然后通过心跳同步给数据节点。当客户端请求数据节点或者数据节点之间通信时，如果发现版本号不相同，则会用新版本的对照表覆盖旧版本。

#### 存储引擎

Tair默认包含两个存储引擎：mdb和fdb。

mdb是一个高效的缓存存储引擎，它有着和memcached类似的内存管理方式。mdb支持使用share memory，这使得我们在重启Tair数据节点的进程时不会导致数据的丢失，从而使升级对应用来说更平滑，不会导致命中率的较大波动。

fdb是一个简单高效的持久化存储引擎，使用树的方式根据数据key的hash值索引数据，加快查找速度。索引文件和数据文件分离，尽量保持索引文件在内存中，以便减小IO开销。使用空闲空间池管理被删除的空间。

mdb的区别在于，mdb将数据也存储在内存，而fdb只将索引存储在内存，存储在内存中的数据都使用`share memory`。

## Google

### 整体架构

| 服务      | 功能                      |
| ------- | ----------------------- |
| F1      | SQL前端                   |
| Spanner | 带事务的分布式存储引擎             |
| CFS     | Colossus ，分布式文件系统，GFS二代 |

![google-f1-spanner](distributed-storage/image/google-f1-spanner.png)

### F1

#### 设计目标

- 系统可以添加资源进行纵向扩展
- 无需改变应用程序就具备数据分片及均衡的能力
- 对事务支持ACID特性
- SQL的全支持，同时支持索引

#### 特性

- 在整体数据上分配SQL查询，并提供join能力
- 索引的事务一致性
- 异步模式转变
- 使用新的ORM库

### Spanner

#### 设计目标

- 跨数据中心的管理及复制数据
- 数据的重分片及均衡能力
- 主机间数据的自动迁移

#### 特性
- 分布事务间（2PC）提供强一致性
- 基于时间戳的整体排序
- 通过Paxos进行同步复制、容错、数据的自动均衡等

#### 事务

Spanner对于不同的事务有不同的锁策略：

| Operation                                | ConcurrencyControl | Replica Required                  |
| ---------------------------------------- | ------------------ | --------------------------------- |
| Read-Write Transaction                   | pessimistic        | leader                            |
| Read-Only Transaction                    | lock-free          | leader for timestamp, any forread |
| Snapshot Read, client-provided timestamp | lock-free          | any                               |
| Snapshot Read, client-provided bound     | lock-free          | any                               |

- 写事务
  需要leader加悲观锁，这是最常见的数据库写操作的锁模型。

- 只读事务
  需要leader提供收到请求时最新数据的时间$t$，然后在任意副本服务器中读取，当$t <= t_{safe}$时，本地副本数据一定是事务发起时最新的。
  > A replica can satisfy a read at a timestamp t if $t <= t_{safe}$.

- 快照读事务
  读快照数据任何时候都不需要加锁，并由客户端提供时间$t$。

## TiDB

TiDB 采用松散耦合的方法，由一个 MySQL Server 层和 SQL 层组成，其基础是开源分布式事务键-值数据库 TiKV。 TiKV 是另一个 PingCAP 项目，使用 Rust 编程语言和分布式协议Raft ，而 TiDB 用的是 Go 编程语言。 TiKV 项目内含 MVCC（多版本并发控制）、 Raft 以及使用 RocksDB 的本地键值存储等功能，它同样使用 Spark Connector 。

### 主要模块

| 模块   | 功能                                       |
| ---- | ---------------------------------------- |
| TiDB | 主要是负责 SQL 的解析器和优化器，它相当于计算执行层，同时也负责客户端接入和交互。 |
| TiKV | 是一套分布式的 Key-Value 存储引擎，它承担整个数据库的存储层，数据的水平扩展和多副本高可用特性都是在这一层实现。TiKV是可替换的，早期的版本中使用的是HBase。 |
| PD   | 根据数据分布状况以及各个存储节点的负载来采取合适的调度策略，维持整个系统的平衡与稳定。 |

### 关键技术

- Google F1/Spanner的实现；
- 协议与MySQL兼容；
- 内部通信使用gRPC；
- 使用Raft保证一致性;

### Transaction

Spanner 默认将数据使用 range 的方式切分成不同的 splits，就跟 TiKV 里面 region 的概念比较类似。每一个 Split 都会有多个副本，分布在不同的 node 上面，各个副本之间使用 Paxos 协议保证数据的一致性。

Spanner 对外提供了 read-only transaction 和 read-write transaction 两种事物，这里简单的介绍一下，主要参考 Spanner 的[白皮书](https://link.jianshu.com?t=https://cloud.google.com/spanner/docs/whitepapers/LifeCloudSpannerReadWrite.pdf)。

#### Single Split Write

假设 client 要插入一行数据 Row 1，这种情况我们知道，这一行数据铁定属于一个 split，spanner 在这里使用了一个优化的 1PC 算法

#### Multi Split Write

如果一个 read-write 事务要操作多个 Split 了，那么我们就只能使用 2PC 了。

硬盘（Volume）为主机提供块存储设备，它独立于主机的生命周期而存在，可以被连接到任意运行中的主机上。注意，硬盘附加到主机上后，您还需要登录到您的主机的操作系统中去加载该硬盘。当然，也可以从主机上卸载硬盘、并转至其他主机。注意，请先在您的主机的操作系统中卸载硬盘，然后再在 QingCloud 系统中卸载。

你可以在 QingCloud 上快速搭建基于 iSCSI 的 Virtual SAN 服务，在你的应用系统之间共享数据。注意，如果将一块硬盘通过 Virtual SAN 服务挂载到多台主机上，那么必须通过共享型文件系统（Shared File System）的支持才可以并行读写。

NAS 是支持基于 NFS 和 Samba(CIFS) 协议的网络共享存储服务。你可以将硬盘挂载到 NAS 服务器上，让多个客户端通过网络连接进行共享，同时支持可访问服务的账号的控制管理。

## 参考资料

### transaction

- [Transactions Across datacenters](http://snarfed.org/transactions_across_datacenters_io.html)

## TODO

第二是内存和NVM，很早就有memsql，配合使用新的数据结构，例如最近几年出现的succinct data structures, masstree等。

作者：李晨曦
链接：https://www.zhihu.com/question/52629893/answer/131405608
来源：知乎
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。

内存数据库领域用的很多1. MemSQL用Lock Free Skip List做索引：The Story Behind MemSQL’s Skiplist Indexes2. SQL SERVER内存存储引擎Hekaton用Lock Free Bw-Tree做索引：https://www.microsoft.com/en-us/research/wp-content/uploads/2016/02/bw-tree-icde2013-final.pdf3. HyPer的并行查询引擎大量的应用了Lock Free数据结构，如使用Lock Free的Hash Table实现Hash Join：http://db.in.tum.de/~leis/papers/morsels.pdf4. DB2 BLU的并行查询引擎：http://db.disi.unitn.eu/pages/VLDBProgram/pdf/industry/p773-barber.pdf5. OceanBase也大量的使用Lock Free为什么传统数据库不用呢？因为瓶颈在磁盘，Lock Free增加的性能几乎可以忽略不计，但是在内存数据库中是可以大幅度提高性能的。


2014算是最大改变当属hekaton了，内存hash表 + native proc，这点对于写并发有数量级的提高！凭借这个累计前几年的改进，SQL server才可以名正言顺的称为企业级数据库，才有底气拿到oracle一起站队比拼，之前的企业级都只能说是小企业级。 不过MS这么多年一直有一个非常非常傻逼的东西一直存在，就是在线log文件不自动归档，一直扩大（如果不备份、截取、这点oracle一直都是循环使用，归档到别处），在线日志一直写下去给一些没dba的公司造成很多的隐患，同时也给一些TB级大量写的数据库带来维护上困难！！强烈要求微软正视这个问题，不要抄sybase ，抄了几十年也不变！  大致如此，有些东西的年份可能会有记忆差错，主要的特点就这些。

作者：樱木花
链接：https://www.zhihu.com/question/28590134/answer/107595287
来源：知乎
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。

数据库MVCC中，如何判断并发事务能否Commit?
https://www.zhihu.com/question/53617964/answer/148111721