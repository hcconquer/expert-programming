# storage-system

## 存储类型

| 存储类型 | 英文全称                 | 典型产品      |
| ---- | -------------------- | --------- |
| 块存储  | Block Storage        | GFS       |
| 对象存储 | Object-Based Storage | S3, OSS   |
| 文件存储 | File Storage         |           |
| 键值存储 | Key-Value Storage    | memcached |
| 文档型数据库 | Document | MongoDB |
| 关系型数据库   | Database             | mysql     |
| NewSQL  | NewSQL             | F1, TiDB  |
| 消息队列 | Message Queue        | kafka     |

### 块存储

块存储对应着传统硬盘。iSCSI，NFS。

### 对象存储

### 文件存储

### 键值存储

### 文档型数据库

文档型数据库是nosql的一种，是KV结构的nosql与关系型数据库的折衷形态，它结合了两者的优点：

- 每一行都是json或者等同的数据结构，没有严格的shcema，让它可以方便的增删字段，同时还支持数组、对象的嵌套，这是传统关系型数据库不支持的；
- 通过一个或多个字段作为key来分区和索引，保留了KV nosql可平行扩展的优点；

### SAN

集群管理软件确保DBMS服务器只在连接到共享磁盘上的一个节点上运行，节点之间通常使用存储区域网络（SAN）互联。如果当前活动节点崩溃，强制卸载该服务，并在不同的节点上启动服务器。标准日志恢复过程可确保在磁盘上的数据的一致性，因此SDF适用任何DBMS。

Shared Disk Failover (SDF)

## AWS

S3

DynamoDB，类似于MongoDB。

## Azure

## other

MongoDB

Cassandra

