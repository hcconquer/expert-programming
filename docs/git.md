# git

## catalog

[TOC]

## command

### config

```shell
git config --global user.email ${email}
git config --global user.name ${name}
git config user.email ${email}
git config user.name ${name}
```

### clone

```shell
# clone
git clone ${path}

# clone without ssl verify, used for proxy
git -c http.sslVerify=false clone ${path}

# clone submodules which has .git in sub dir
git clone --recurse-submodules ${path.git}
```

### list change

```shell
# show change file list
git status

# show file changes
git diff ${filename}
```

### log

```shell
# show logs
git log

# show lastest 2 changes details
git log -p -2

# show special commit changs
git show ${commit_id}

# show change file list
git log --name-only
```

### commit

```shell
# add files
git add ${files}

# remote files
git reset ${files}

# commit with signoff
git commit -s -m "${msg}"

# modify commit info, also recreating change-id if remove old
git commit --amend

# signoff last commit
git commit --amend -s --no-edit
```

### branch

```shell
# list branch
git branch

# show local branch
git branch -v

# show local branch with upstream
git branch -vv

# switch branch
git checkout ${local_branch}

# create local branch
git checkout -b ${local_branch}

# checkout branch from tag
git checkout -b ${local_branch} ${tag_name}

# push branch to remote
git push -u origin ${branch_name}

# checkout remote branch
git checkout -b ${local_branch} origin/${remote_branch}
# or
git checkout ${branch_name}

# delete branch
git branch -d ${local_branch}
git branch --delete ${local_branch}

# delete branch force
git branch -D ${local_branch}
```

## merge

```shell
# merge current branch to spec branch
git merge origin/master
```

### ls-remote

```shell
# ls remote, allow unmerged commit, most used for gerrit
# result is ref list: `${commit} ${refspec}`
# can use pull to get unmerged commit
git ls-remote | grep ${commit}
```

### fetch

```shell
# git pull = git fetch + merge to local
# fetch all data
git fetch --all
```

### pull

```shell
# pull remote repo branch
git pull origin ${branch}

# pull remote spec ref
git pull origin ${refspec}
```

### push

```shell
# push master to remote, default is not `origin` but first remote
git push ${remote}

# push branch to remote
git push origin ${local_branch}

# set branch upstream
git branch --set-upstream-to ${remote_branch}
git branch --set-upstream-to gitlab/master

# temporary push upstream
git push --set-upstream ${remote_branch} ${local_branch}

# push branch
git push origin HEAD:refs/for/${remote_branch}
# or
git push origin ${local_branch}:refs/for/${remote_branch}
```

### rebase

```shell
# if has modified files, need to commit or stash at first
git rebase -i origin/master
```

### stash

```shell
# saved working directory and index state
git stash

# list stash
git stash list

# show stash changes
git stash show

# show stash details
git stash show -p

# show diff stash 1
git stash show -p stash@{1}

# recover latest stash
git stash apply

# recover stash 1
git stash apply stash@{1}

# drop latest stash
git stash drop

# drop stash 1
git stash drop stash@{1}
```

### reset

```shell
# delete untracked files
git clean -df

# to delete directories with .git sub directory or file
git clean -dff

# reset to local last commited version, discard local files
git reset --hard

# reset to remote version, discard local files
git reset --hard origin/master

# reset to spec commited version
git checkout ${commit}

# reset to spec commited version, force local to old commit
git reset --hard ${commit}

# reset special file
git checkout ${file}
```

### patch

```shell
# create patch after commit, and the commit not include
git format-patch ${commit_since}

# create patch after master, which use for brach patch to master
git format-patch ${commit_since} master

# create patch and print to stdout
git format-patch --stdout ${commit_since}

# create patch with rename file
git format-patch -M ${commit_since}

# create patch from commit and more 2(3-1) commit
git format-patch -3 ${commit_since}

# stat patch info
git apply --stat patch.diff

# check patch can be apply
git apply --check patch.diff

# apply patch
git apply patch.diff
```

## diff

```shell
git diff --staged
```

### clean

```shell
git count-objects -v
git gc --prune=now
```

## action

### repo sync

```shell
git remote -v
git remote rm origin
git remote add origin ${remote_repo}

# if remote match
git push -u origin --all
git push -u origin --tags

# else if remote mismatch
git push -f -u origin --all
git push -f -u origin --tags

# pull all
git remote | xargs -I{} git pull {} ${local_branch}
git remote | xargs -I{} git push {}
```

### gerrit

#### commit to master

```shell
git branch -a
git checkout -b ${branch}
git commit -a -m "Fix ${issue} ${msg}"
git commit --amend
git review -t ${topic}
```

#### commit to branch

```shell
git branch -a
git checkout -b ${branch} remotes/origin/${branch}
git review -d ${change-id}
git cherry-pick ${commit}
git review -t ${topic} ${branch}
```

## development model

### trunk-based

`trunk-based` work like this:

```shell
git checkout master
git checkout -b ${branch}
git commit
# rebase commit
git rebase
# push to master
git push
```

- [What is Trunk-Based Development?](https://paulhammant.com/2013/04/05/what-is-trunk-based-development/)

### Pull Request

`Pull Request` work like this:

```shell
git checkout master
git checkout -b ${branch}
git commit
# push to remote branch
git push
# merge branch to master
git merge origin/master
```

### git flow

support:

- [OneFlow - a Git branching model and workflow](https://www.endoflineblog.com/oneflow-a-git-branching-model-and-workflow)
- [Learn Version Control with Git](https://www.git-tower.com/learn/git/ebook/cn/command-line/advanced-topics/git-flow)

oppose:

- [gitflow-consider-harmful](http://insights.thoughtworkers.org/gitflow-consider-harmful/)

### principle

- long lived branch is evil;

### reference

- [git-workflow-tutoria](https://github.com/xirong/my-git/blob/master/git-workflow-tutorial.md)
