# kubernetes

## 基础概念

### 集群

Kubernetes将底层的计算资源连接在一起对外体现为一个高可用的计算机集群。Kubernetes将资源高度抽象化，允许将容器化的应用程序部署到集群中。为了使用这种新的部署模型，需要将应用程序和使用环境一起打包成容器。与过去的部署模型相比，容器化的应用程序更加灵活和可用，在新的部署模型中，应用程序被直接安装到特定的机器上，Kubernetes能够以更高效的方式在集群中实现容器的分发和调度运行。

Kubernetes集群包括两种类型资源：

- Master节点：协调控制整个集群。
- Nodes节点：运行应用的工作节点。

![k8s_cluster](kubernetes/image/k8s_cluster.svg)

Master 负责集群的管理。Master 协调集群中的所有行为/活动，例如应用的运行、修改、更新等。

Node节点作为Kubernetes集群中的工作节点，可以是VM虚拟机、物理机。每个node上都有一个Kubelet，用于管理node节点与Kubernetes Master通信。每个Node节点上至少还要运行container runtime（比如docker或者rkt）。

Kubernetes上部署应用程序时，会先通知master启动容器中的应用程序，master调度容器以在集群的节点上运行，node节点使用master公开的Kubernetes API与主节点进行通信。最终用户还可以直接使用Kubernetes API与集群进行交互。

Kubernetes集群可以部署在物理机或虚拟机上。使用Kubernetes开发时，你可以采用Minikube。Minikube可以实现一种轻量级的Kubernetes集群，通过在本地计算机上创建虚拟机并部署只包含单个节点的简单集群。Minikube适用于Linux，MacOS和Windows系统。Minikube CLI提供集群管理的基本操作，包括 start、stop、status、 和delete。

### node

Node是Kubernetes中的工作节点，可以是虚拟机或物理机。每个Node由 Master管理，Node上可以有多个pod，Kubernetes Master会自动处理群集中Node的pod调度，同时Master的自动调度会考虑每个Node上的可用资源。

每个Kubernetes Node上至少运行着：

- Kubelet，管理Kubernetes Master和Node之间的通信，管理机器上运行的Pods和containers容器；
- container runtime，如Docker、rkt；

![k8s_nodes](kubernetes/image/k8s_nodes.svg)

### pod

Pod是Kubernetes中一个抽象化概念，由一个或多个容器组合在一起得共享资源。这些资源包括：

- 共享存储，如 Volumes 卷
- 网络，唯一的集群IP地址
- 每个容器运行的信息，例如:容器镜像版本
- Pod模型是特定应用程序的“逻辑主机”，并且包含紧密耦合的不同应用容器。

Pod中的容器共享IP地址和端口。

Pod是Kubernetes中的最小单位，当在Kubernetes上创建Deployment时，该Deployment将会创建具有容器的Pods（而不会直接创建容器），每个Pod将被绑定调度到Node节点上，并一直保持在那里直到被终止（根据配置策略）或删除。在节点出现故障的情况下，群集中的其他可用节点上将会调度之前相同的Pod。

![k8s_pods](kubernetes/image/k8s_pods.svg)

### services

Pod是有生命周期的。当一个工作节点(Node)销毁时，节点上运行的Pod也会销毁，然后通过ReplicationController动态创建新的Pods来保持应用的运行。作为另一个例子，考虑一个图片处理 backend，它运行了3个副本，这些副本是可互换的，前端不需要关心它们调用了哪个 backend 副本。也就是说，Kubernetes集群中的每个Pod都有一个独立的IP地址，甚至是同一个节点上的Pod，因此需要有一种方式来自动协调各个Pod之间的变化，以便应用能够持续运行。

可以通过type在ServiceSpec中指定一个需要的类型的 Service，Service的四种type:
- ClusterIP（默认） - 在集群中内部IP上暴露服务。此类型使Service只能从群集中访问。
- NodePort - 通过每个 Node 上的 IP 和静态端口（NodePort）暴露服务。NodePort 服务会路由到 ClusterIP 服务，这个 ClusterIP 服务会自动创建。通过请求 <NodeIP>:<NodePort>，可以从集群的外部访问一个 NodePort 服务。
- LoadBalancer - 使用云提供商的负载均衡器（如果支持），可以向外部暴露服务。外部的负载均衡器可以路由到 NodePort 服务和 ClusterIP 服务。
- ExternalName - 通过返回 CNAME 和它的值，可以将服务映射到 externalName 字段的内容，没有任何类型代理被创建。这种类型需要v1.7版本或更高版本kube-dnsc才支持。

## setup

### client

```shell
# install kubernetes client
brew install kubectl
# or
brew install kubernetes-cli
```

### minikube

```shell
# install minikube
brew cask install minikube

# show version
minikube version
```

#### minikube drivers
```
Minikube uses Docker Machine to manage the Kubernetes VM so it benefits from the driver plugin architecture that Docker Machine uses to provide a consistent way to manage various VM providers. Minikube embeds VirtualBox and VMware Fusion drivers so there are no additional steps to use them. However, other drivers require an extra binary to be present in the host PATH.
```

minikube支持的[Driver plugin](https://github.com/kubernetes/minikube/blob/master/docs/drivers.md)。

```
Docker for Windows uses Microsoft Hyper-V for virtualization, and Hyper-V is not compatible with Oracle VirtualBox.
Docker for Mac uses HyperKit, a lightweight macOS virtualization solution built on top of the Hypervisor.framework in macOS 10.10 Yosemite and higher.
```

简单来说，如果是Windows用户，建议使用`Hyper-V`，如果是Mac用户，建议使用`HyperKit`，Mac用户也可以使用`brew`安装`xhyve`驱动。

```shell
# install docker machine driver hyperkit
curl -LO https://storage.googleapis.com/minikube/releases/latest/docker-machine-driver-hyperkit \
&& chmod +x docker-machine-driver-hyperkit \
&& sudo mv docker-machine-driver-hyperkit /usr/local/bin/ \
&& sudo chown root:wheel /usr/local/bin/docker-machine-driver-hyperkit \
&& sudo chmod u+s /usr/local/bin/docker-machine-driver-hyperkit
```

```shell
# install docker machine driver xhyve
brew install docker-machine-driver-xhyve

# docker-machine-driver-xhyve binary needs root owner and uid
sudo chown root:wheel $(brew --prefix)/opt/docker-machine-driver-xhyve/bin/docker-machine-driver-xhyve
sudo chmod u+s $(brew --prefix)/opt/docker-machine-driver-xhyve/bin/docker-machine-driver-xhyve
```

#### run and check
```shell
minikube start --vm-driver=hyperkit
# or
minikube start --vm-driver=xhyve
```

#### start echoserver pod
```shell
# start echoserver pod
kubectl run hello-minikube --image=gcr.io/google_containers/echoserver:1.4 --port=8080

# expose echoserver by NodePort
kubectl expose deployment hello-minikube --type=NodePort
```

```shell
# show cluster info
kubectl config use-context minikube
kubectl cluster-info

# show pods states
kubectl get pods

# show nodes
kubectl get nodes 

# show all name spaces
kubectl get pod -o wide --all-namespaces
```
