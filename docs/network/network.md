# network

## catalog

[TOC]

## 基本概念

### 术语

| 名称  | 含义                        | 说明                            |
| ----- | --------------------------- | ------------------------------- |
| ToR   | Top of Rack                 | 三层交换，一个 ToR 对应一个网段 |
| VxLAN | Virtual Extensible LAN      | 虚似扩展局域网                  |
| SDN   | Software Defined Networking | 软件定义网络                    |
| VPC   | Virtual Private Cloud       | 专有网络                        |
| EIP   | Elastic IP                  | 弹性公网 IP                     |

### OSI 7层网络

## TCP/IP 4层网络

### MSL

[MSL](https://en.wikipedia.org/wiki/Maximum_segment_lifetime)是Maximum Segment Lifetime英文的缩写，中文可以译为“报文最大生存时间”，他是任何报文在网络上存在的最长时间，超过这个时间报文将被丢弃。

```shell
$ sysctl net.inet.tcp | grep msl
net.inet.tcp.msl: 15000
```

### TTL

ip头中有一个TTL域，TTL是 time to live的缩写，中文可以译为“生存时间”，这个生存时间是由源主机设置初始值但不是存的具体时间，而是存储了一个ip数据报可以经过的最大路由数，每经 过一个处理他的路由器此值就减1，当此值为0则数据报将被丢弃，同时发送ICMP报文通知源主机。RFC 793中规定MSL为2分钟，实际应用中常用的是30秒，1分钟和2分钟等。
TTL最常见的用途是`traceroute`路由探测。
TTL与MSL是有关系的但不是简单的相等的关系，MSL要大于等于TTL。

### RTT

RTT是客户到服务器往返所花时间(round-trip time，简称RTT)，TCP含有动态估算RTT的算法。TCP还持续估算一个给定连接的RTT，这是因为RTT受网络传输拥塞程序的变化而变化。

### 2MSL

2MSL即两倍的MSL，TCP的TIME_WAIT状态也称为2MSL等待状态，当TCP的一端发起主动关闭，在发出最后一个ACK包后，即第3次握 手完成后发送了第四次握手的ACK包后就进入了TIME_WAIT状态，必须在此状态上停留两倍的MSL时间，等待2MSL时间主要目的是怕最后一个ACK包对方没收到，那么对方在超时后将重发第三次握手的FIN包，主动关闭端接到重发的FIN包后可以再发一个ACK应答包。在TIME_WAIT状态时两端的端口不能使用，要等到2MSL时间结束才可继续使用。当连接处于2MSL等待阶段时任何迟到的报文段都将被丢弃。不过在实际应用中可以通过设置SO_REUSEADDR选项达到不必等待2MSL时间结束再使用此端口。

## TCP

### connect-timeout

127s

http://www.chengweiyang.cn/2017/02/18/linux-connect-timeout/

### time-wait

30s/60s/120s

http://blog.qiusuo.im/blog/2014/06/11/tcp-time-wait/

## QoS

## 物理网络

### VPC

### EIP

弹性公网IP是可以独立申请的公网IP地址。EIP实例为用户提供一个单独公网IP和一份带宽，为用户访问公网提供服务。EIP实例可与任意虚拟机、NAT网关、VPN网关等实例绑定，并支持随时升级和降级带宽，灵活匹配用户不同业务场景。

## 典型数据

各类网线的性能。

## 网络安全

### 防御层级

L3：开发特定端口、限速
L4：SYN Proxy
L7：统计/恶意访问识别，封禁、跳转
旁路：BCS/911，检测+流量清洗

## DPDK
