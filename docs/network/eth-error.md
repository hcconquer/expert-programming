# eth-error

## background

found a server http request is very slow or can't finish until timeout.

## research

### ifconfig

```shell
ifconfig xgbe0
xgbe0     Link encap:Ethernet  HWaddr 6C:92:BF:3A:4D:30  
          inet addr:10.123.87.24  Bcast:10.123.87.255  Mask:255.255.255.0
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:347932542793 errors:941839722 dropped:0 overruns:0 frame:941839722
          TX packets:420532701003 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000 
          RX bytes:384370057621317 (349.5 TiB)  TX bytes:267164635806300 (242.9 TiB)
```

this show too much RX errors.

### ethtool

```shell
# view eth details
ethtool xgbe0
Settings for xgbe0:
        Supported ports: [ FIBRE ]
        Supported link modes:   10000baseT/Full 
        Supported pause frame use: No
        Supports auto-negotiation: No
        Advertised link modes:  10000baseT/Full 
        Advertised pause frame use: Symmetric
        Advertised auto-negotiation: No
        Speed: 10000Mb/s
        Duplex: Full
        Port: Other
        PHYAD: 0
        Transceiver: external
        Auto-negotiation: off
        Supports Wake-on: d
        Wake-on: d
        Current message level: 0x00000007 (7)
                               drv probe link
        Link detected: yes
```

```shell
# change eth ring buffer
ethtool -g xgbe0
Ring parameters for xgbe0:
Pre-set maximums:
RX:             4096
RX Mini:        0
RX Jumbo:       0
TX:             4096
Current hardware settings:
RX:             512
RX Mini:        0
RX Jumbo:       0
TX:             512

# change rx and tx ring buffer
ethtool -G xgbe0 rx 2048 tx 2048
```

try add ring buffer but it not works.

```shell
ethtool -S xgbe0 | grep "error"
     rx_errors: 941843663
     tx_errors: 0
     rx_over_errors: 0
     rx_crc_errors: 941567806
     rx_frame_errors: 0
     rx_fifo_errors: 0
     rx_missed_errors: 0
     tx_aborted_errors: 0
     tx_carrier_errors: 0
     tx_fifo_errors: 0
     tx_heartbeat_errors: 0
     rx_long_length_errors: 0
     rx_short_length_errors: 0
     rx_csum_offload_errors: 191
```

it show most is crc errors.

## eth means

`RX` stands for received and `TX` stands for transmitted. Documentation for the fields that follow is sparse and only long-deserted ghost-town forums popped up in my searches. 

### rx

| key      | reamrks                                                      |
| -------- | ------------------------------------------------------------ |
| packets  | total number of packets received.                            |
| errors   | an aggregation of the total number of packets received with errors. This includes too-long-frames errors, ring-buffer overflow errors, crc errors, frame alignment errors, fifo overruns, and missed packets. |
| dropped  | counts things like unintended VLAN tags or receiving IPv6 frames when the interface is not configured for IPv6. |
| overruns | fifo overruns, which are caused by the rate at which the ring-buffer is drained being higher that the kernel being able to handle IO. |
| frame    | counts for the incoming frames that were misaligned. all frames should end on an 8-bit boundary, but problems on the network could cause the number of bits to deviate from the multiple of 8. |
