# tool chain

## catalog

[TOC]

## brew

### repo

```shell
cd "$(brew --repo)"; git remote -v
origin    https://github.com/Homebrew/brew (fetch)
origin    https://github.com/Homebrew/brew (push)

$ cd "$(brew --repo)/Library/Taps/homebrew/homebrew-core"; git remote -v
origin    https://github.com/Homebrew/homebrew-core (fetch)
origin    https://github.com/Homebrew/homebrew-core (push)
```

```shell
# official
cd "$(brew --repo)"
git remote set-url origin https://github.com/Homebrew/brew.git
cd "$(brew --repo)/Library/Taps/homebrew/homebrew-core"
git remote set-url origin https://github.com/Homebrew/homebrew-core.git
brew update

# tsinghua.edu
cd "$(brew --repo)"
git remote set-url origin https://mirrors.tuna.tsinghua.edu.cn/git/homebrew/brew.git
cd "$(brew --repo)/Library/Taps/homebrew/homebrew-core"
git remote set-url origin https://mirrors.tuna.tsinghua.edu.cn/git/homebrew/homebrew-core.git
brew update
```

### tab

```shell
# list tap
brew tap
# remote tap
brew untap
```

### cask

```shell
brew cask search ${keyword}

# list installed apps
brew cask list

# install app
brew cask install ${app}

# install apps
echo "google-chrome
thunder
visual-studio-code
vmware-fusion" | while read app; do brew cask install ${app}; done
```

### special version

```shell
# protobuf2
brew install protobuf@2.5
```

upgrade

https://segmentfault.com/a/1190000004353419

brew cask outdated

greedy

https://github.com/Homebrew/homebrew-cask/issues/48002


brew --cache
