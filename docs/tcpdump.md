# tcpdump

## catalog

[TOC]

## har

### har intro

[HAR](https://en.wikipedia.org/wiki/.har)(HTTP Archive format), is a JSON-formatted archive file format for logging of a web browser's interaction with a site. The common extension for these files is .har. 

## write

```shell
tcpdump -w ${file.cap}
```

## filter

### http get

```shell
# filter http GET
# 'GET'==0x47455420
tcpdump 'tcp[((tcp[12:1]&0xf0)>>2):4]=0x47455420'
# 'HT'==0x4854
# 'GE'==0x4745
# 'PO'==0x504f
tcpdump 'tcp[20:2]=0x4745 or tcp[20:2]=0x4854'
```



## parameters

| parameter | abbr | function                            |
| --------- | ---- | ----------------------------------- |
| count     | -c   | exit after receiving count packets. |
| write     | -w   | write the raw packets to file       |

```shell
# tcp dump
# -l: Make stdout line buffered
# -p: Don't  put  the  interface into promiscuous mode.
# -n: Don't convert addresses
# -nn: Don't convert all
tcpdump -i${eth} port ${port} -lpnn
# dump and print by ascii, most for http
# s0: print not length limited
tcpdump -i${eth} port ${port} -Alpnns0
# dump and print by hex, most for binary protocol
tcpdump -i${eth} port ${port} -Xlpnns0
# dump protocol, host, port
tcpdump -i${eth} tcp and dst host ${host} and port ${port} -Xlpns0

# show tcp rst
tcpdump 'tcp[tcpflags] & (tcp-rst) != 0' -lpn

# write to file
# -w: Write  the raw packets to file rather than parsing and printing them out.
tcpdump -lpn -w ${file.log}



```

https://danielmiessler.com/study/tcpdump/

https://www.cnblogs.com/ggjucheng/archive/2012/01/14/2322659.html

https://danielmiessler.com/study/tcpdump/

## tools

conv between ox and str: [ox2str](https://www.bejson.com/convert/ox2str/)

tcpdump tcp[((tcp[12:1]&0xf0)>>2):4]=0x47455420 -c 100000 -Alpnns0 -w /tmp/bos-http-get.cap

/usr/sbin/tcpdump -tt -ieth1 host 10.23.0.237 or 10.23.0.231 -lpnn -w /tmp/ntpsr.cap
/usr/sbin/tcpdump -i eth1 -n -s 500 port domain -w /tmp/dns.cap


/usr/sbin/tcpdump -tt -ieth1 host 172.22.1.253 or 172.22.1.254
