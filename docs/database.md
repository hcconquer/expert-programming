# database

## catalog

[TOC]

## database分类

| **分类**              | **Examples举例**                               | 典型应用场景                                                 | 数据模型                                                     | 优点                                                         | 缺点                                                         |
| --------------------- | ---------------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| **键值(KV)**          | DynamoDB, Azure Table, LevelDB, RocksDB, Redis | 可以通过key快速查询到其value。一般来说，存储不管value的格式，照单全收。 | Key 指向 Value 的键值对，通常用hash table或者LSM(需要Range)来实现。 | 查找速度快                                                   | 数据无结构化，通常只被当作字符串或者二进制数据。             |
| **列存储数据库**      | Cassandra, Scylla, HBase, Riak                 | 是按列存储数据的。最大的特点是方便存储结构化和半结构化数据，方便做数据压缩，对针对某一列或者某几列的查询有非常大的IO优势。 | 以列簇式存储，将同一列数据存在一起。                         | 查找速度快，可扩展性强，更容易进行分布式扩展                 | 功能相对局限。                                               |
| **文档型数据库**      | CouchDB, MongoDB                               | 文档存储一般用类似json的格式存储，存储的内容是文档型的。这样也就有有机会对某些字段建立索引，实现关系数据库的某些功能。 | Key-Value对应的键值对，Value为结构化数据。                   | 数据结构要求不严格，表结构可变，不需要像关系型数据库一样需要预先定义表结构 | 查询性能不高，而且缺乏统一的查询语法。                       |
| **NewSQL**            | Google Spanner, CockroachDB, TiDB              | 用于需要关系型数据库并且需要服务能力可水平扩展的业务。       | 以nosql为底层，前端兼容SQL协议。                             | 标准的SQL查询，并拥有nosql可扩展的特性。                     |                                                              |
| **图形(Graph)数据库** | Neo4J, InfoGrid, Infinite Graph, TITAN, Dgraph                | 图形关系的最佳存储。使用传统关系数据库来解决的话性能低下，而且设计使用不方便。专注于构建关系图谱，如社交网络，推荐系统等。 | 图结构。                                                     | 利用图结构相关算法。比如最短路径寻址，N度关系查找等          | 很多时候需要对整个图做计算才能得出需要的信息，而且这种结构不太好做分布式的集群方案。 |

### 要点

#### select for update

假设有两个线程并发执行如下操作，由于`val1=val1+1`这个操作出现了值缓存，那么可能会出现值被覆盖的问题。

```sql
begin;
select @val1=val where id=1;
val1=val1+1
update t set val1=val where id=1;
commit;
```

增加`for update`，第一个`select`将产生行锁，之后的`select`将会阻塞。这样解决了并发修改的问题，但同时会导致性能下降。

```sql
begin;
/* will lock until tansaction done */
select @val1=val where id=1 for update;
val1=val1+1
update t set val1=val where id=1;
commit;
```

## 参考资料

- [NoSQL](http://nosql-database.org/)
