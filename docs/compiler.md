# compiler

## catalog

[TOC]

## gcc

### link

| args    | syntax          | example |
| ------- | --------------- | ------- |
| -L      | `-L${lib_path}` |         |
| -l      | `-l${lib_name}` |         |
| -static |                 |         |
