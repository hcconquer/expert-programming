# app

## catalog

[TOC]

## program editor

|                    |      |      |
| ------------------ | ---- | ---- |
| visual-studio-code | free |      |
| eclipse-*          | free |      |

## markdown editor

|         |      |      |
| ------- | ---- | ---- |
| typora  | free |      |
| notable | free |      |

## hex editor

| name      | fee    | remarks |
| --------- | ------ | ------- |
| hex-fiend | free   |         |

## http debug

| name    | fee    | remarks |
| ------- | ------ | ------- |
| postman | free   |         |
| paw     | $49.99 |         |

## media

| name    | fee    | remarks |
| ------- | ------ | ------- |
| mpv     | free   |         |
