# IO

## catalog

[TOC]

## IO模型

### 阻塞和非阻塞

参考[APUE 3rd](http://www.codeman.net/wp-content/uploads/2014/04/APUE-3rd.pdf)中`14.2 Nonblocking I/O`的解释：

Blocking I/O的特性：

- Reads that can block the caller forever if data isn’t present with certain file types (pipes, terminal devices, and network devices);
- Writes that can block the caller forever if the data can’t be accepted immediately by these same file types (e.g., no room in the pipe, network flow control);
- Opens that block until some condition occurs on certain file types (such as an open of a terminal device that waits until an attached modem answers the phone, or an open of a FIFO for writing only, when no other process has the FIFO open for reading);
- Reads and writes of files that have mandatory record locking enabled;

Nonblocking I/O的特性：

> Nonblocking I/O lets us issue an I/O operation, such as an open, read, or write, and not have it block forever. If the operation cannot be completed, the call returns immediately with an error noting that the operation would have blocked.

Nonblocking要么返回数据，要么返回错误，一定不会阻塞(block)。

假设通过标准输入读取15B的数据，程序形式为`read(STDIN_FILENO, buf, 15)`，那么：

|           | Blocking             | Nonblocking     |
| --------- | -------------------- | --------------- |
| 没有数据  | 阻塞直到有数据(>=1B) | EAGAIN          |
| 有10B数据 | 立刻返回10B数据      | 立刻返回10B数据 |

### 同步与异步

参考[Wikipedia Asynchronous I/O](https://en.wikipedia.org/wiki/Asynchronous_I/O)的解释：

> In computer science, asynchronous I/O (also non-sequential I/O) is a form of input/output processing that permits other processing to continue before the transmission has finished.

参考[Microsoft Synchronous and Asynchronous I/O](https://docs.microsoft.com/en-us/windows/desktop/FileIO/synchronous-and-asynchronous-i-o)的解释：

> In synchronous file I/O, a thread starts an I/O operation and immediately enters a wait state until the I/O request has completed. A thread performing asynchronous file I/O sends an I/O request to the kernel by calling an appropriate function. If the request is accepted by the kernel, the calling thread continues processing another job until the kernel signals to the thread that the I/O operation is complete. It then interrupts its current job and processes the data from the I/O operation as necessary.

参考[Oracle Differences Between Synchronous and Asynchronous I/O](https://docs.oracle.com/cd/E36784_01/html/E36860/character-11.html)的解释：

> Data transfers can be synchronous or asynchronous. The determining factor is whether the entry point that schedules the transfer returns immediately or waits until the I/O has been completed.

参考[APUE 3rd](http://www.codeman.net/wp-content/uploads/2014/04/APUE-3rd.pdf)中`14.5 Asynchronous I/O`的解释：

> Using select and poll, as described in the previous section, is a synchronous form of notification. The system doesn’t tell us anything until we ask (by calling either select or poll). As we saw in Chapter 10, signals provide an asynchronous form of notification that something has happened. All systems derived from BSD and System V provide some form of asynchronous I/O, using a signal (SIGPOLL in System V; SIGIO in BSD) to notify the process that something of interest has happened on a descriptor. As mentioned in the previous section, these forms of asynchronous I/O are limited: they don’t work with all file types and they allow the use of only one signal. If we enable more than one descriptor for asynchronous I/O, we cannot tell which descriptor the signal corresponds to when the signal is delivered.

可以看到对`Synchronous`和`Asynchronous`的概念是存在差异的：

| 参考      | Synchronous                                                 | Asynchronous                             |
| --------- | ----------------------------------------------------------- | ---------------------------------------- |
| Wikipedia | 较早的请求完成之前，其后的不会被执行。                           | 较早的请求完成之前，允许执行后续的请求。 |
| Microsoft | 等待直到IO完成。                                             | 立刻返回，然后等待kernel通知返回结果。   |
| Oracle    | 等待直到IO完成。                                             | 立刻返回。                               |
| APUE      | 需要向系统询问资源状态(select和epoll也属于同步IO)。              | 发起后等待system通知，主要如Linux AIO或者Windows IOCP |

相对来说APUE和Microsoft的解释更为精准：

| 概念         | 含义                                                             |
| ------------ | --------------------------------------------------------------- |
| Synchronous  | IO发起后，用户进程等待kernel处理完成。                               |
| Asynchronous | IO发起后，用户进程可以处理其他任务，kernel处理完成之后会主动通知用户进程。 |

但要注意，内核通知并不局限于回调函数的形式，如Linux AIO需要通过`aio_error()`获取IO执行状态，需要通过`WSAGetLastError()`获得IO执行状态。

从用户进程的调用过程来看，AIO不需要等待立刻返回，可以减少用户应用的IO调用时间，这也可以在用户态模拟实现，比如[glibc-AIO](https://www.gnu.org/software/libc/manual/html_node/Asynchronous-I_002fO.html)，但本质只是将IO调用的等待从应用程序转移到了库线程。

### 非阻塞与AIO

参考[APUE 3rd](http://www.codeman.net/wp-content/uploads/2014/04/APUE-3rd.pdf)中`16.8 Nonblocking and Asynchronous I/O`的解释：

> Normally, the recv functions will block when no data is immediately available. Similarly, the send functions will block when there is not enough room in the socket’s output queue to send the message. This behavior changes when the socket is in nonblocking mode. In this case, these functions will fail instead of blocking, setting errno to either EWOULDBLOCK or EAGAIN. When this happens, we can use either poll or select to determine when we can receive or transmit data.
> The Single UNIX Specification includes support for a general asynchronous I/O mechanism (recall Section 14.5). The socket mechanism has its own way of handling asynchronous I/O, but this isn’t standardized in the Single UNIX Specification. Some texts refer to the classic socket-based asynchronous I/O mechanism as "signal-based I/O" to distinguish it from the general asynchronous I/O mechanism found in the Single UNIX Specification.

主要含义：

- Nonblocking在于如果资源不可用立即返回失败(返回EAGAIN)，但如果资源可用，用户进程需要等待kernel处理完成；
- Asynchronous则分为两种：基于信号(signal-based)的称为经典(classic)模型，其他的称为普通(general)模型；

更具体来说，非阻塞(Nonblocking)与AIO都有不阻塞的含义，但行为处于不同的阶段：

- 非阻塞(Nonblocking)是IO不可完成的时候不阻塞，但如果IO可以执行，那系统调用(read/write)还是会阻塞，等待内核读取并返回数据，而AIO不会阻塞，调用一定立刻返回；
- 对于非阻塞`read`而言，如果资源可读，同步IO将阻塞在系统调用上直到数据读完并拷贝到用户空间，而AIO则立刻返回，操作系统异步的读取数据；
- 对于非阻塞`write`而言，如果资源可写，同步IO将阻塞在系统调用上直到数据从用户空间拷贝到内核空间，而AIO则立刻返回，操作系统异步的将数据从用户空间拷贝到内核空间；

假设进程的业务逻辑如下：

1. 从磁盘读取100M的数据，假设磁盘的吞吐率为100M/s，读取100M数据需要1s；
2. 对数据进行MD5校验，假设MD5(100M)需要的时间为200ms；

同步和异步IO的处理流程：

- 如果是同步IO，无论是阻塞还是非阻塞模式，进程`read`需要阻塞1s的时间，阻塞的这段时间内，进程无法处理业务，操作系统会调度进程会让出CPU，导致CPU空闲利用率低，响应时间(RT)为1.2s，吞吐率(Throughput)为100M/1.2s=83M/s；
- 如果是异步IO，进程`aio_read`会立刻返回，直到kernel从磁盘读完所有数据，并从内核空间拷贝到用户空间，在等待kernel处理IO的时间内，可以对前次读取的数据进行MD5计算，响应时间(RT)为1.2s，吞吐率(Throughput)为100M/s；

### 典型的IO模型

|              | Blocking   | Nonblocking                                        |
| ------------ | ---------- | -------------------------------------------------- |
| Synchronous  | read/write | read/write(O_NONBLOCK), multiplexing(select/epoll) |
| Asynchronous | -          | AIO                                                |

要注意，多工IO(select/epoll)是不算Asynchronous的，虽然`epoll_wait`是内核通知的形式，但真实的IO操作都是会阻塞的。

## Cache

### Page Cache

VFS的Cache，大小为4K。

### Buffer Cache

### Page和Buffer区别

- Page和Buffer处于IO不同的阶段，write先到Page，刷新磁盘的时候会到Buffer；
- Buffer会复用Page的内存，但inode不属于文件内容，并不会在Page中，是独立的Buffer；

## Direct IO

## AIO

### glibc AIO

[glibc Asynchronous I/O](https://www.gnu.org/software/libc/manual/html_node/Asynchronous-I_002fO.html)是glibc在`librt`中实现的异步IO，用于减少应用程序等待IO处理的时间，主要通过多线程(libpthread)来实现。

其主要接口如下：

```c++
#include <aio.h>

int aio_read(struct aiocb *aiocbp);
int aio_write(struct aiocb *aiocbp);
int lio_listio(int mode, struct aiocb *const aiocb_list[], int nitems, 
               struct sigevent *sevp);
```

### Linux Kernel AIO

[Kernel Asynchronous I/O (AIO) Support for Linux](http://lse.sourceforge.net/io/aio.html)是Linux内核提供的异步IO系统调用(system call)，在内核层面实现异步通知，是真正的Asynchronous I/O。

其主要接口如下：

```c++
#include <libaio.h>

struct iocb {
  void     *data;
  unsigned key;
  short    aio_lio_opcode;
  short    aio_reqprio;
  int      aio_fildes;
};

typedef void (*io_callback_t)(io_context_t ctx, struct iocb *iocb, long res, long res2);

inline void io_prep_pwrite(struct iocb *iocb, int fd, void *buf, size_t count, long long offset);
inline void io_prep_pread(struct iocb *iocb, int fd, void *buf, size_t count, long long offset);
static inline void io_prep_fsync(struct iocb *iocb, int fd);
static inline void io_set_callback(struct iocb *iocb, io_callback_t cb);
int io_submit(io_context_t ctx, long nr, struct iocb *iocbs[]);
```

但使用存在一些限制：

- AIO read and write on raw (and O_DIRECT on blockdev)；
- AIO read and write on files opened with O_DIRECT on ext2, ext3, jfs, xfs；

对于Linux AIO的实现细节，可以参看设计文档：[Design Notes on Asynchronous I/O (aio) for Linux](http://lse.sourceforge.net/io/aionotes.txt)。

## SPDK

## 参考文档

- [Asynchronous I/O and event notification on linux](http://davmac.org/davpage/linux/async-io.html)
