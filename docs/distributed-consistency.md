# distributed-consistency

## 一致性模型(Consistency Models)

### 基础模型

#### 强一致性(Strong)

新的数据一旦写入，在任意副本任意时刻都能读到新值，CAP默认的C就是强一致性。比如：文件系统，RDBMS都是强一致性的。

<!-- 强一致模型有时也被称之为可线性化（Linearizability）。Maybe ? -->

#### 弱一致性(Weak)

当你写入一个新值后，读操作在数据副本上可能读出来，也可能读不出来。比如：某些cache系统，搜索引擎。

#### 最终一致性(Eventually)

当你写入一个新值后，有可能读不出来，但在某个时间窗口之后保证最终能读出来。比如：DNS，电子邮件、Amazon S3。

最终一致性是弱一致性的特殊形式，也分布式系统中弱一致性最广泛的实现形式。

### 细分模型

对于一致性模型，在不同的文档中略有差异，中英文wikipedia的说法是互相冲突的，这里采用[wiki-en](https://en.wikipedia.org/wiki/Consistency_model)的说法。

以下一致性模型中，严格一致性和线性一致性可以认为是强一致性模型，其他都是弱一致性模型。

#### 严格一致性

严格一致性(Strict Consistency)：任何读操作都能读取到最新的修改，要求任何写操作都立刻同步到其他所有进程。这里强调的是绝对时钟上的立刻同步，而任何信息的同步都需要延迟，因此在分布式系统下无法严格实现。

> Strict consistency is the strongest consistency model. Under this model, a write to a variable by any processor needs to be seen instantaneously by all processors. The Strict model diagram and non-Strict model diagrams describe the time constraint – **instantaneous**. It can be better understood as though a global clock is present in which every write should be reflected in all processor caches by the end of that clock period. The next operation must happen only in the next clock period.

不考虑系统的性能，一种可能的替代办法就是：让整个系统只在单个节点的单个线程运行，或者，在系统粒度上，读写操作操作被读写锁保护起来，但这另一种形式的单机系统。

严格一致性下，在时间轴上(从左到右)，一旦有进程写了x，则其他进程立刻就能读到最新的值。

![consistency-strict](distributed-storage/image/consistency-strict.png)

#### 线性一致性

[线性一致性](https://en.wikipedia.org/wiki/Linearizability)(Linearizability Consistency)，是比严格一致性要弱一点的一致性模型，主要是去掉了受物理世界限制而又非必须的实时性(instantaneously)。

线性一致性的定义：

1. 所有进程都执行相同的操作序列，并且得到相同的结果；
2. 如果操作a在b开始前完成(a happended before b, `a->b`)，a在操作序列中一定早于b；

> A history σ is *linearizable* if there is a linear order of the completed operations such that:
>
> 1. For every completed operation in σ, the operation returns the same result in the execution as the operation would return if every operation was completed one by one in order σ.
> 2. If an operation op1 completes (gets a response) before op2 begins (invokes), then op1 precedes op2 in σ.

在分布式系统中，要实现线性一致性必须要有**全局统一时间**，才能让在不同分区发生的操作按相同的时钟排列。因为光速的限制，全局统一时间无法通过NTP等中心化的时间服务实现，只能使用原子钟或者GPS时钟这样独立的绝对时钟。

#### 顺序一致性

[顺序一致性](https://en.wikipedia.org/wiki/Sequential_consistency)(Sequential Consistency)，摒弃了线性一致性对绝对时钟的要求(real-time constraint)，而是使用分布式逻辑时钟，将所有事件定一个全序，每个进程看到的都是这个全序。

顺序一致性的定义：

1. 所有进程都执行相同的操作序列，并且得到相同的结果；
2. 同一处理器（或线程）中程序的执行顺序与程序顺序相同，不同处理器（或线程）之间的程序执行顺序未定义；

> As defined by Lamport(1979), Sequential Consistency is met if "the result of any execution is the same as if the operations of all the processors were executed in some sequential order, and the operations of each individual processor appear in this sequence in the order specified by its program."

顺序一致性最大的问题在于进程之间操作的顺序可能会违背操作的绝对时间序，进而违反操作的因果关系，例如，假设a操作完成才触发b操作，但定序时b操作反而在a操作之前。

如图，W1,W2,W3实际发生顺序是W1<W2<W3，但是被定序后，可能是W3<W2<W1。但只要每个进程看到的定序结果是一致的，如P4和P5的读的顺序和结果都是一致的，系统就是满足Sequential Consistency的。

![consistency-sequential](distributed-storage/image/consistency-sequential.png)

#### 因果一致性

[因果一致性](https://en.wikipedia.org/wiki/Causal_consistency)(Causal Consistency)，是一种弱化的顺序一致性模型，但要求存在因果关系的操作必须要符合因果顺序。

因果一致性的定义：

1. 有因果关系的操作序列，在所有进程都以相同的顺序执行；
2. 并发操作可以以任何顺序排列，不同的进程可以以不同顺序执行；

因果关系的定义：设(a happended before b)为`a->b`，分布式系统中事件集合满足如下条件：

1. 如果a和b是同一个进程内的事件，且a在b之前发生，那么`a->b`；
2. 如果a是一个进程发送消息事件，b是另一个进程接收该消息的事件，那么`a->b`；
3. 满足传递性，`a->b ∧ b->c ⇒ a->c`。

如果`a->b`与`b->a`都不成立，那么ab是并发的。

> Causal consistency can be defined as a model that captures the causal relationships between operations in the system and guarantees that each process can observe those causally related operations in common causal order. In other words, all processes in the system agree on the order of the causally related operations.

如图，对y的操作没有因果序，出现了P3读到R(y)2 R(y)1，而P4读到R(y)1 R(y)2的现象，因为并发的操作在不同的进程可以以不同的顺序执行，这在Sequential Consistency是不允许的。

![consistency-casual](distributed-storage/image/consistency-casual.png)

#### 处理器一致性

[处理器一致性](https://en.wikipedia.org/wiki/Processor_consistency)(Processor Consistency)，是一种更为弱化的因果一致性模型。

处理器一致性的定义：

1. 在一个处理器上完成的所有写操作，将会被以它实际执行的顺序通知给所有其它的处理器；
2. 在不同处理器上对同一个地址的写操作，将会被以它实际执行的顺序被所有处理器所见；
3. 在不同处理器上对不同地址的写操作，也许会被其它处理器以不同于实际执行的顺序所看到；

> All processors need to be consistent in the order in which they see writes done by one processor and in the way they see writes by different processors to the same location (coherence is maintained). However, they do not need to be consistent when the writes are by different processors to different locations.

处理器一致性是先入先出一致性与缓存一致性的结合，先入先出一致性舍约束2，缓存一致性舍弃了约束1，先入先出一致性与缓存一致性相比，是两个维度的约束，这两个一致性是无法比较的(incomparable)。

![consistency-processor](distributed-storage/image/consistency-processor.png)

#### 先入先出一致性

先入先出一致性(FIFO Consistency)，也叫PRAM一致性(Pipelined-Random-Access-Memory Consistency)，舍弃了处理器一致性中对同一地址操作顺序的一致性约束。

先入先出一致性的定义：

1. 在一个处理器上完成的所有写操作，将会被以它实际执行的顺序通知给所有其它的处理器；
2. 在不同处理器上对不同地址的写操作，也许会被其它处理器以不同于实际执行的顺序所看到；

> All processes see memory writes from one process in the order they were issued from the process.
> Writes from different processes may be seen in a different order on different processes.

![fifo](distributed-storage/image/consistency-fifo.png)

#### 缓存一致性

缓存一致性(Cache Consistency)，舍弃了处理器一致性中单个处理器内操作顺序的一致性约束。

> Cache consistency requires that all write operations to the same memory location are performed in some sequential order. Cache consistency is weaker than process consistency and incomparable with PRAM consistency.

如图，P3,P4看到的x,y分别是一致的。但是x,y并不是一致的。

![cache](distributed-storage/image/consistency-cache.png)

#### 最终一致性

最终一致性(Eventual Consistency)，也称为乐观复制，是一种非常弱的一致性模型。

最终最终一致性没有标准的定义，主流的认识：
1. 如果没有新的写操作，所有副本都会逐渐收敛，最终达成一致；
2. 在收敛之前，所有副本保持为上一次收敛时的值，这个值可能任何历史值；

为了确保副本收敛，系统必须协调多个副本之间的差异，主要包括：
- 在服务器之间交换数据的版本或更新，称为反熵(anti-entropy)；
- 在发生并发更新时选择适当的最终状态，称为调和(reconciliation)；

最终一致性在设计领域也有一些争议，随着业务复杂性的提升，弱一致性确实带给业务一些问题和不便，这可能也是近几年强一致性的关系型数据库又复苏的一个原因。

### 读一致性

#### 任何条件下强一致

任何时候多从多个节点读取，如果有超过1/2的节点数据一致，那么数据在集群内必然是强一致的。

#### 部分条件下强一致

永远从leader读写，但如果网络发生分区，出现了两个leader，此时可能会产生脏读。(旧leader不会自己回退)

#### 允许脏读

[Consistent reads are not consistent](https://github.com/coreos/etcd/issues/741)

可以从任意节点读取，很容易出现脏读。

## 一致性算法

### WARO

WARO(Write All Read one)是一种简单的副本控制协议，当Client请求向某副本写数据时，只有当所有的副本都写成功之后，这次写操作才算成功，否则视为失败。

WARO的特点：
* 写操作很脆弱，因为只要有一个副本更新失败，此次写操作就视为失败了。
* 读操作很简单，因为所有的副本更新成功才视为更新成功，从而保证所有的副本一致，读操作只需要读任何一个副本上的数据即可。

WARO牺牲了写的可用性，最大程度地增强了读的可用性：**假设有N个副本，N-1个都宕机了，剩下的那个副本仍能提供读服务；但是只要有一个副本宕机了，写服务就不会成功。**

### Quorum

Quorum机制是”抽屉原理“的一个应用。定义如下：假设有N个副本，更新操作wi在W个副本中更新成功之后，才认为此次更新操作wi成功。称成功提交的更新操作对应的数据为：“成功提交的数据”。对于读操作而言，至少需要读R个副本才能读到此次更新的数据。**其中，W+R>N ，即W和R有重叠。**

假设系统中有5个副本，W=3，R=3。初始时数据为(V1，V1，V1，V1，V1），当某次更新操作在3个副本上成功后，就认为此次更新操作成功，数据变成：(V2，V2，V2，V1，V1）。
最多只需要读3个副本，一定能够读到V2。而在后台，可对剩余的V1同步到V2，而不需要让Client知道。

WARO是Quorum的一个特例(W=N, R=1)。

#### 强一致性

仅通过R+W>N，是无法保证强一致性的。

例如，上面的V2成功提交后（已经写入W=3份），尽管读取3个副本时一定能读到V2，如果刚好读到的是(V2，V2，V2），则此次读取的数据是最新成功提交的数据，因为W=3，而此时刚好读到了3份V2。如果读到的是（V2，V1，V1），则无法确定是一个成功提交的版本，还需要继续再读，直到读到V2的达到3份为止，这时才能确定V2就是已经成功提交的最新的数据。

#### 实践方案

- 假设(W=N，R=1)，就是写入所有的节点是全部节点，那么读取任何一个节点就可以最新的数据，也就是WARO模型。
- 假设(W=1，R=N)，那么写的效率就非常高，读取的效率比较低，但可能会因为宕机而丢数据。
- 假设(W<N，R=1)，那么是一个弱一致的系统，这种模型常用于搜索、消息类的应用中。
- 如果(R=N/2+1, W=N/2)，读写平衡，兼顾了性能和可用性，是比较常见的方案。

## 参考文档

- [Consistency in Distributed Systems](https://mwhittaker.github.io/consistency_in_distributed_systems/1_baseball.html)
