# Typical Performance

## time

|                 | nanosecond(ns) | millisecond(µs) | microsecond(ms) | second(s)     |
| --------------- | -------------- | --------------- | --------------- | ------------- |
| nanosecond(ns)  | 1              | 0.001           | 0.000,001       | 0.000,000,001 |
| microsecond(us) | 1,000          | 1               | 0.001           | 0.000,001     |
| millisecond(ms) | 1,000,000      | 1,000           | 1               | 0.001         |
| second(s)       | 1,000,000,000  | 1,000,000       | 1,000           | 1             |

## Speed of light

### Exact values

c = 299792458(m/s) = 300,000(km/s)

sources: [Speed_of_light](https://en.wikipedia.org/wiki/Speed_of_light)

### Approximate light signal travel times

| typical instruction               | length    | ns  | µs  | ms   | second |
| --------------------------------- | --------- | --- | --- | ---- | ------ |
| 1 metre                           |           | 3.3 |     |      |        |
| the max length of Beging          | 200km     |     | 670 | 0.7  |        |
| from Beijing to Shanghai          | 1084km    |     |     | 3.6  |        |
| from Beijing to Shenzhen          | 2372km    |     |     | 7.9  |        |
| from Shanghai to New York         | 11,875km  |     |     | 39.6 |        |
| from geostationary orbit to Earth | 35,786km  |     |     | 119  |        |
| the length of Earth's equator     | 40,075km  |     |     | 134  |        |
| from Moon to Earth                | 405,400km |     |     | 1300 | 1.3    |

ref to [wiki Fiber optics](https://en.wikipedia.org/wiki/Latency_(engineering)#Fiber_optics), latency should multiple 1.5 in fibre.

> The [index of refraction](https://en.wikipedia.org/wiki/Index_of_refraction) of most fibre optic cables is about 1.5, meaning that light travels about 1.5 times as fast in a vacuum as it does in the cable. This works out to about 5.0 µs of latency for every kilometer.

## Typical Latency

| typical instruction                | ns          | µs      | ms  | Comparison          |
| ---------------------------------- | ----------- | ------- | --- | ------------------- |
| L1 cache reference                 | 0.5         |         |     |                     |
| Branch mispredict                  | 5           |         |     |                     |
| L2 cache reference                 | 7           |         |     | 14x L1 cache        |
| Mutex lock/unlock                  | 25          |         |     |                     |
| Main memory reference              | 100         |         |     | 15x L2, 200x L1     |
| Compress 1K bytes with Zippy       | 3,000       | 3       |     |                     |
| Send 1K bytes over 1 Gbps network  | 10,000      | 10      |     |                     |
| Read 4K randomly from SSD          | 150,000     | 150     |     |                     |
| Read 1 MB sequentially from memory | 250,000     | 250     |     |                     |
| Round trip within same datacenter  | 500,000     | 500     |     |                     |
| Read 1 MB sequentially from SSD    | 1,000,00    | 1,000   | 1   | 4x memory           |
| Disk seek                          | 10,000,000  | 10,000  | 10  |                     |
| Read 1 MB sequentially from disk   | 20,000,000  | 20,000  | 20  | 80x memory, 20x SSD |
| Send packet CA->Netherlands->CA    | 150,000,000 | 150,000 | 150 |                     |

sources: from Jeff Dean at 2001, first publicized by Peter Norvig in [article](http://norvig.com/21-days.html#answers).

| typical instruction         | ns  | µs  | ms  | Comparison |
| --------------------------- | --- | --- | --- | ---------- |
| TCMalloc                    | 50  |     |     |            |
| glibc 2.3 malloc(ptmalloc2) | 300 |     |     |            |

sources: from [TCMalloc : Thread-Caching Malloc](https://gperftools.github.io/gperftools/tcmalloc.html).

some data has been out-of-date, e.g. Read 4K from SSD from S3710-400G can up to 85000 IOPS, it seems 12(us) latency.

## SSD

### Intel

#### S3710 400GB

| Item                   | S3710-400G | P3710-1.6T |
| ---------------------- | ---------- | ---------- |
| Capacity(GB)           | 400        | 1600       |
| Sequential Read(MB/s)  | 550        | 2800       |
| Sequential Write(MB/s) | 470        | 1900       |
| Random Read(IOPS)      | 85000      | 450000     |
| Random Write(IOPS)     | 43000      | 150000     |
| Latency Read(µs)       | 55         | 20         |
| Latency Write(µs)      | 66         | 20         |
