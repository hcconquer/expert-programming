# ssl

## catalog

[TOC]

## https基本原理

### 公钥基础架构(PKI)

公钥基础设施(Public Key Infrastructure, PKI)，，是一组由硬件、软件、参与者、管理政策与流程组成的基础架构，其目的在于创造、管理、分配、使用、存储以及撤销数字证书。

![Public-Key-Infrastructure.svg.png](https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/Public-Key-Infrastructure.svg/1052px-Public-Key-Infrastructure.svg.png)

### HTTPS工作流

HTTPS其实是有两部分组成：HTTP+SSL/TLS，也就是在HTTP上又加了一层处理加密信息的模块。服务端和客户端的信息传输都会通过TLS进行加密，所以传输的数据都是加密后的数据。

具体流程：

1. 客户端发起HTTPS请求；
2. 服务端根据配置读取CRT证书（公钥）；
3. 服务端响应CRT证书给客户端；
4. 客户端解析证书，如果无效则展示警告，如果有效则产生随机key，使用CRT证书加密key；
5. 客户端将key加密后发送给服务端；
6. 服务端使用CRT私钥解密信息获得key，并用key加密数据；
7. 服务端响应加密后的数据给客户端；
8. 客户端使用key解密信息；

![ssl.png](ssl.png)

## key类型

### pem

PEM(Privacy Enhanced Mail)是一种数据格式，使用base64编码，Apache和Nginx等常用这种格式。

对于PEM的规范可以参考[RFC 1421: Privacy Enhancement for Internet Electronic Mail](https://tools.ietf.org/html/rfc1421)。

PEM内容有明显的特征：以`-----BEGIN-----`开头，以`-----END——`结尾。

> This document defines message encryption and authentication procedures, in order to provide privacy-enhanced mail (PEM) services for electronic mail transfer in the Internet. It is intended to become one member of a related set of four RFCs.

`.cert`和`.key`都是PEM格式的，只是用途不一样。

### Private Key

私钥(Private Key)，后缀名一般为`*.key`。

私钥用于签发证书和后续解密客户端请求。

### Certificate

证书(Certificate)，后缀名一般为`*.cer`和`*.crt`。

证书一般是公钥，用于客户端获取之后，加密信息发送给服务端。

### Certificate signing request

证书签名请求(Certificate signing request)，后缀名一般为` *.csr`。

证书申请者在申请数字证书时，由CSP(加密服务提供者)在生成私钥(.key)的同时也生成证书请求文件CSR，证书申请者把CSR文件提交给证书颁发机构，证书颁发机构(CA)使用其根证书私钥签名生成证书公钥文件，也就是颁发给用户的SSL证书(.cer)。

### Intermediate Certificate

每次签发时，域名的私钥和证书其实并不需要改变，而中级证书会改变，因为中级证书记录了域名私钥的授权信息，包括：有效期，域名等。

## nginx config

参考[Configuring HTTPS servers](http://nginx.org/en/docs/http/configuring_https_servers.html):

> The server certificate is a public entity. It is sent to every client that connects to the server.

这里再次说明，证书是一个公共资源，用于客户端向服务端请求时加密。

### digicert

参考[digicert config on nginx server](https://www.digicert.com/csr-ssl-installation/nginx-openssl.htm)指引文档。

涉及到两个文件：一个是私钥(.key)，一个证书(.csr)，而证书是通过私钥生成的。

- **Private-Key File**: Used to generate the CSR and later to secure and verify connections using the certificate.
- **Certificate Signing Request (CSR) file**: Used to order your SSL certificate and later to encrypt messages that only its corresponding private key can decrypt.

证书(.csr)需要和中级证书(intermediate certificate)合并产生最终证书(bundle.crt)，证书一般是pem格式的。

```shell
cat your_domain_name.crt DigiCertCA.crt >> bundle.crt
```

## command

### nmap

```shell
# scan ssl
nmap -A baidu.com -p 443 -v
```

### openssl

参考[OpenSSL Essentials](https://www.digitalocean.com/community/tutorials/openssl-essentials-working-with-ssl-certificates-private-keys-and-csrs)：

```shell
# Generate a Private Key and a CSR
openssl req -newkey rsa:2048 -nodes -keyout domain.key -out domain.csr

# Verify a Private Key
openssl rsa -check -in domain.key

# Generate a CSR from an Existing Private Key
openssl req -key domain.key -new -out domain.csr

# Generate a CSR from an Existing Certificate and Private Key
openssl x509 -in domain.crt -signkey domain.key -x509toreq -out domain.csr

# View CSR Entries
openssl req -text -noout -verify -in domain.csr

# View Certificate Entries
openssl x509 -text -noout -in domain.crt

# Verify a Private Key Matches a Certificate and CSR
# Use to verify if a private key (domain.key) matches a certificate (domain.crt) and CSR (domain.csr)
openssl rsa -noout -modulus -in domain.key | openssl md5
openssl x509 -noout -modulus -in domain.crt | openssl md5
openssl req -noout -modulus -in domain.csr | openssl md5
```

### example

| name       | remarks            |
| ---------- | ------------------ |
| server.key | 私钥               |
| server.cer | CA签名生产的公钥   |
| DG-OV.cer  | digicert的中级证书 |
| server.pem | 最终证书           |

```shell
# verify private key
openssl rsa -check -in server.key

# view certificate(pubkey)
openssl x509 -text -noout -in server.cer

# verify certificate by CA
openssl verify -verbose -CAFile DG-OV.cer server.cer

# create final certificate file server.pem
cat server.cer DG-OV.cer >> server.pem

# view certificate
openssl x509 -in server.pem -text -noout
# show time
openssl x509 -in server.pem -text -noout | grep -A 2 "Validity"
# show domains
openssl x509 -in server.pem -text -noout | grep "Subject:"

# verify if equal
openssl rsa -noout -modulus -in server.key | openssl sha256
openssl x509 -noout -modulus -in server.cer | openssl sha256
openssl x509 -noout -modulus -in server.pem | openssl sha256

# view pubkey with domain name
openssl s_client -servername s3.bj.bcebos.com -connect s3.bj.bcebos.com:443 | openssl x509 -pubkey -noout
openssl s_client -servername s3.bj.bcebos.com -connect 10.59.27.12:8443 | openssl x509 -pubkey -noout

# view certificate valid dates
openssl s_client -servername s3.bj.bcebos.com -connect s3.bj.bcebos.com:443 | openssl x509 -dates -noout
openssl s_client -servername s3.bj.bcebos.com -connect 10.59.27.12:8443 | openssl x509 -dates -noout

# https test
curl -v 'https://bj.bcebos.com:443'
curl -v --resolve 'bj.bcebos.com:443:10.59.27.12' 'https://bj.bcebos.com:443'
```
