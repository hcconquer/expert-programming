# Content-MD5

## Content-MD5 Header

ref to [RFC-1864 The Content-MD5 Header Field](https://tools.ietf.org/html/rfc1864):

> The output of the MD5 algorithm is a 128 bit digest. When viewed in network byte order (big-endian order), this yields a sequence of 16 octets of binary data. These 16 octets are then encoded according to the base64 algorithm in order to obtain the value that is placed in the Content-MD5 field.

## MD5 algorithm

MD5 is 128(bit) or 16(Byte) in binary, conv to hex text will double, which len is 32(Byte).

```shell
# conv to hex text is 32(Byte)
# --binary, read in binary mode
$ echo -n 'Check Integrity!' | md5sum --binary
9f0aaae9beae6bfb530e4ec1e4cd7ce3 *-
```

## base64 algorithm

ref [Base4](https://en.wikipedia.org/wiki/Base64), conv 3 8-bit bytes to 4 6-bit digits.

```shell
# as rfc1864 example, if MD5 raw data is `Check Integrity!` which make up by people
$ echo -n 'Check Integrity!' | base64
Q2hlY2sgSW50ZWdyaXR5IQ==
```

## MD5 to base64

if conv binary MD5 to base64, `ceil(16/3)*4=6*4=24`, result len is 24(Byte).

```shell
# binary is 16(Byte), conv to hex text is 32(Byte)
$ echo -n 'Check Integrity!' | openssl md5 -binary | hexdump -C
00000000  9f 0a aa e9 be ae 6b fb  53 0e 4e c1 e4 cd 7c e3  |......k.S.N...|.|

# binary is 16(Byte), conv to base64 is 24(Byte)
$ echo -n 'Check Integrity!' | openssl md5 -binary | base64
nwqq6b6ua/tTDk7B5M184w==
```

### base64 to MD5 text

```shell
$ echo "nwqq6b6ua/tTDk7B5M184w==" | base64 -d | hexdump -C
00000000  9f 0a aa e9 be ae 6b fb  53 0e 4e c1 e4 cd 7c e3  |......k.S.N...|.|
00000010
```

## python example

```python
import hashlib
import base64

# Q2hlY2sgSW50ZWdyaXR5IQ==
print base64.standard_b64encode('Check Integrity!')
# nwqq6b6ua/tTDk7B5M184w==
print base64.standard_b64encode(hashlib.md5('Check Integrity!').digest())
```

## refer

- [ceil](https://en.wikipedia.org/wiki/Floor_and_ceiling_functions)
