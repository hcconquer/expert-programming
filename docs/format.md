# format

## catalog

[TOC]

## Google Styles



## clang-format

### install

```shell
brew install clang-format
```

### quick-start

```shell
# format by style config .clang-format, default style is `file`
clang-format main.cpp -style=file
# format by style name
clang-format main.cpp -style=LLVM
# format by style name and addition parmas
clang-format main.cpp -style="{BasedOnStyle: Google, IndentWidth: 4}"

# replace the origin file
clang-format main.cpp -i

# format files
find . -name "*.c" -o -name "*.cc" | xargs clang-format -i
find . -name "*.c" -o -name "*.cc" | xargs -I{} clang-format {} -i

# dump style config
clang-format -style=file -dump-config > .clang-format
clang-format -style=Google -dump-config > .clang-format
```

### format-off

```c++
// clang-format off
std::cout << "hello"
          << std::endl;
// clang-format on
```

### reference

- [homepage](https://clang.llvm.org/docs/ClangFormat.html)
