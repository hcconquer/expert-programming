# HBase

## HBase数据模型

![img](hbase/image/hbase-data-model.jpg)

### Row Key

行键，Table的主键，用来检索记录。访问HBase table中的行，只有三种方式：

- 通过单个row key访问
- 通过row key的range
- 全表扫描

row key可以是任意字符串（最大长度是 64KB，实际应用中长度一般为 10-100bytes左右），在HBase内部，row key保存为字节数组。数据按照row key的字典序（byte order）排序存储，因此设计key时要充分排序存储这个特性，将经常一起读取的行存储放到一起。

HBase支持单行事务，行的一次读写是原子操作 （不论一次读写多少列），这个设计决策能够使用户很容易的理解程序在对同一个行进行并发更新操作时的行为。

### Column Family

列簇，Table在水平方向有一个或者多个Column Family组成，一个Column Family中可以由任意多个Column组成，即Column Family支持动态扩展，无需预先定义Column的数量以及类型，所有Column均以二进制格式存储，用户需要自行进行类型转换。

列名都以列族作为前缀。例如’courses:history’，’courses:math’，都属于courses 这个列族。
访问控制、磁盘和内存的使用统计都是在列族层面进行的。实际应用中，列族上的控制权限能帮助我们管理不同类型的应用：我们允许一些应用可以添加新的基本数据、一些应用可以读取基本数据并创建继承的列族、一些应用则只允许浏览数据（甚至可能因为隐私的原因不能浏览所有数据）。

