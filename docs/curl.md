# curl

## output

```shell
# write output <file>
curl -o file "<url>"

# write output to null
curl -o /dev/null "<url>"
```

## header

```shell
# -H, --header <header>
curl -H "User-Agent: Mozilla/5.0" "<url>"
```

## referer

```shell
# -e, --referer <URL>
curl --referer "www.baidu.com" "<url>"
```

## post

```shell
# post json
# -X, --request <command>
# -d, --data <data>
curl -X POST -d "{"id":100}" "<url>"
curl -X POST -H "Accept: application/json" -H "Content-type: application/json" -d '{"id":100}' "<url>"

# post file
curl -T <file> "<url>"
curl -T a.txt "http://databin.bj.bcebos.com/"

# post file by form
# -F, --form <name=content>
curl -X POST -F "<filename>=@<file>" "<url>"
```

## verbose

```shell
# show header
curl --verbose "<url>"

# show time
curl --verbose --trace-time "<url>"
```

## compress

```shell
# add head `Accept-Encoding: deflate, gzip`
curl --verbose --compress "<url>"
```

## host

```shell
# --connect-to <HOST1:PORT1:HOST2:PORT2>
curl -v --connect-to "www.baidu.com:80:220.181.57.216:80" "http://www.baidu.com"
# --resolve <host:port:address>, less param then --connect-to
curl -v --resolve "www.baidu.com:80:220.181.57.216" "http://www.baidu.com"
```

## proxy

```shell
# -x, --proxy [protocol://]host[:port]
curl -x "<ip>:<port>" "<url>"
curl --resolve "www.baidu.com:80:220.181.57.216" "www.baidu.com"

```

## location

```shell
# -L, --location
# --max-redirs <num>, if 0 will not follow
curl -L --max-redirs 1 "<url>"
```

## parameters

| parameter     | abbr | function                           |
| ------------- | ---- | ---------------------------------- |
| --compress    |      |                                    |
| --connnect-to |      | Send request to the given host     |
| --data        | -d   | Sends the specified data in a POST |
| --form        | -F   | Emulate a filled-in form.          |
| --header      | -H   | Extra header.                      |
| --location    | -L   | follow 3xx                         |
| --max-redirs  |      | redirects to follow max            |
| --output      | -o   |                                    |
| --proxy       | -x   |                                    |
| --resolve     |      | custom address for a specific host |
| --request     | -X   | Specifies a custom request method. |
| --referer     | -e   | Referrer  Page.                    |
| --trace-time  |      | Prepends a time stamp.             |
| --upload-file | -T   | upload file                        |
| --verbose     | -v   | Show header data.                  |
