# Google Spanner

## 概念

**shard**，一个表水平分为很多的分区，每个分区叫shard，也有一些其他系统叫partition，segment，slice等。

## 查询重启

分布式系统里面会碰到各种错误，比如网络分割，机器重启，进程崩溃，文中还提到了distributed wait和data movement。Distributed wait可能的原因是因为机器忙导致paxos group里面follower跟不上leader。Data movement是某些数据（好像叫Dictionary）可能正在被动态迁移到其他可用区，比如从美国迁移到欧洲，这个Spanner 2012里面有论述。

上面的错误都可能导致某些查询执行中断，此时有两个选择，一个是让用户重试，比如著名的指数退避原则，但是作者认为这样难用，而且容易导致query长尾，同时也影响了架构的灵活性（对动态负载均衡的约束）。

另一个是系统内部重试掉，这个符合作者的意图，其认为该方法有很多好处，包括：简化编程、降低请求毛刺、让长查询能顺利执行、利于升级、简化Spanner内部处理。在论文下一节里面，作者描述了做到重启的技术，其实就是查询的checkpoint，restart后从哪里开始，这个checkpoint的设计要考虑兼容、高效编解码等方面。

这里的观点我非常同意，系统的接口越简单，内部自动化的机会就越多，同时用户也越爽。

## TrueTime

## 部署

Google将广告系统使用的F1和Spanner集群部署在美国的5个数据中心，东海岸两个，西海岸两个，中间一个。相当于每份数据5个副本，其中东海岸一个数据中心被作为leader数据中心。在spanner的paxos实现中，5个副本中有一个leader副本，所有的对这个副本的读写事务都经过它，这个leader副本一般就存在leader数据中心中。由于paxos协议的运行只需要majority响应即可，那么一次paxos操作的延时基本取决于东海岸的leader数据中心和东海岸另外一个数据中心，和中间那个数据中心之间的延时。从这里也可以看出，对于写比较多的F1 Client来说，F1 Client和F1 Server都部署在leader数据中心性能最好。在这个配置下，F1用户的commit延时大概在50ms到150ms之间。读延时大约5～10ms。



