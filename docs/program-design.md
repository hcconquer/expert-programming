# program-design

## 原则

### 里氏替换原则

里氏替换原则（Liskov Substitution principle）是对子类型的特别定义。它由芭芭拉·利斯科夫（Barbara Liskov）在1987年在一次会议上名为“数据的抽象与层次”的演说中首先提出。

里氏替换原则：**派生类（子类）对象能够替换其基类（超类）对象被使用。**

> Let q(x) be a property provable about objects x of type T. Then q(y) should be true for objects y of type S where S is a subtype of T.
