package leetcode

import (
	"log"
)

/*
 * A cards heap has {1, 2, ..., n} with unknown order
 * scatter steps:
 * 1.move top 1 card from heap to desk cards bottom;
 * 2.move top 1 card from heep to heep cards bottom;
 * do until heap is empty
 * in final, desk cards is asc order from 1 to n
 * what the origin cards heap order?
**/

/**
 * scatter cards as rules
**/
func scatter(cards []int) []int {
	num := len(cards)
	src := make([]int, num)
	copy(src, cards)
	srcNum := num
	dst := make([]int, num)
	dstNum := 0

	gindex := 0
	for srcNum > 0 {
		num := srcNum
		harf := srcNum / 2
		rem := srcNum % 2
		if gindex%2 == 0 {
			for index := 0; index < harf; index++ {
				dst[dstNum+index] = src[index*2]
				src[index] = src[index*2+1]
			}
			if rem != 0 {
				dst[dstNum+harf] = src[srcNum-1]
				dstNum = dstNum + harf + 1
			} else {
				dstNum = dstNum + harf
			}
			srcNum = harf
		} else {
			for index := 0; index < harf; index++ {
				src[index] = src[index*2]
				dst[dstNum+index] = src[index*2+1]
			}
			if rem != 0 {
				src[harf] = src[srcNum-1]
				srcNum = harf + 1
			} else {
				srcNum = harf
			}
			dstNum = dstNum + harf
		}
		gindex += num
	}

	return dst
}

/*
 * 1.compute order mapping when scatter;
 * 2.reverse mapping cards;
**/
func shuffle(cards []int) []int {
	sequence := make([]int, len(cards))
	for index := 0; index < len(cards); index++ {
		sequence[index] = index + 1
	}

	mapping := scatter(sequence)

	dst := make([]int, len(cards))
	for index := 0; index < len(cards); index++ {
		dst[mapping[index]-1] = cards[index]
	}

	return dst
}

/**
 * think about the scatter principle: 
 * jump 2 cards and move 1 card to desk until heap empty
 * so don't need to reverse mapping, just construct a sequence that match
 * the dest order when scatter, that is good gambler.
**/
func gamble(cards []int) []int {
	num := len(cards)
	dst := make([]int, num)

	dst[0] = cards[0] // init
	step := 2         // it's magic num
	dpos := 0
	for spos := 1; spos < num; spos++ {
		// find next pos
		next := dpos
		s := 0
		for s < step {
			next = (next + 1) % num
			if dst[next] == 0 {
				s++
			}
		}
		dpos = next
		dst[dpos] = cards[spos]
	}

	return dst
}

func main() {
	num := 10
	cards1 := make([]int, num)
	for index := 0; index < num; index++ {
		cards1[index] = index + 1
	}
	log.Printf("cards1: %+v", cards1)
	cards2 := shuffle(cards1)
	cards3 := shuffle(cards2)
	log.Printf("cards1: %+v, cards2: %+v, cards3: %+v", cards1, cards2, cards3)
}
