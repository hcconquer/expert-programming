package leetcode

import "testing"

func TestReverseString(t *testing.T) {
	var cases = []struct {
		input    string
		expected string
	}{
		{"", ""},
		{"a", "a"},
		{"aa", "aa"},
		{"aba", "aba"},
		{"abba", "abba"},
		{"hello", "olleh"},
	}

	for _, c := range cases {
		actual := reverseString(c.input)
		if actual != c.expected {
			t.Errorf("actual: %s, expected: %s, input: %s",
				actual, c.expected, c.input)
		}
	}
}

