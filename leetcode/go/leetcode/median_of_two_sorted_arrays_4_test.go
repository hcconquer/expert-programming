package leetcode

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFindMedianSortedArrays(t *testing.T) {
	var cases = []struct {
		array1 []int
		array2 []int
		median float64
	}{
		{[]int{}, []int{1, 2, 3, 4, 5, 6}, 3.5},
		{[]int{1, 1, 3, 3}, []int{1, 1, 3, 3}, 2},
		{[]int{1}, []int{}, 1},
		{[]int{1}, []int{1}, 1},
		{[]int{1, 1}, []int{1, 1}, 1},
		{[]int{3, 3}, []int{3, 3, 4}, 3},
		{[]int{1}, []int{1, 1, 1}, 1},
		{[]int{1, 1, 1}, []int{1, 1}, 1},
		{[]int{1, 2, 3}, []int{1, 2, 2}, 2},
		{[]int{1, 2}, []int{-1, 3}, 1.5},
		{[]int{3, 3, 3, 3}, []int{3, 3, 3, 3}, 3},
		{[]int{1, 3}, []int{2}, 2.0},
		{[]int{1, 2, 3, 4}, []int{2}, 2.0},
		{[]int{1, 2, 3, 4}, []int{5}, 3.0},
		{[]int{1, 2}, []int{3, 4}, 2.5},
		{[]int{1, 2}, []int{}, 1.5},
		{[]int{}, []int{1, 2}, 1.5},
		{[]int{1, 2, 3, 4, 5, 8, 9, 10}, []int{6, 9, 10}, 6},
		{[]int{6, 9, 10}, []int{1, 2, 3, 4, 5, 8, 9, 10}, 6},
		{[]int{1, 2, 3, 4, 5, 8, 9, 10}, []int{6, 7, 9, 10}, 6.5},
		{[]int{1, 2, 3, 4, 5}, []int{1, 2, 3, 4, 5}, 3},
		{[]int{1, 2, 3, 4, 5}, []int{2, 2, 2, 2, 2}, 2},
		{[]int{1, 2, 3, 4, 5}, []int{6, 6, 6, 6, 6}, 5.5},
		{[]int{5}, []int{1, 2, 3, 4, 6, 7, 8, 9}, 5},
		{[]int{5}, []int{1, 2, 3, 4, 5, 6, 7, 8, 9}, 5},
		{[]int{9}, []int{1, 2, 3, 4, 5, 6, 7, 8}, 5},
		{[]int{1, 3, 5, 7, 9}, []int{2, 4, 6, 8}, 5},
	}

	for _, c := range cases {
		median := findMedianSortedArrays(c.array1, c.array2)
		assert.Equalf(t, c.median, median, "case: %v", c)
	}
}
