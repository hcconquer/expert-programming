package leetcode

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestShuffle(t *testing.T) {
	var cases = []struct {
		num   int
		salt  int // salt make the cards chaos but not [1, n]
		cards []int
	}{
		{1, 1, nil},
		{2, 1, nil},
		{3, 1, nil},
		{4, 1, nil},
		{5, 1, []int{1, 5, 2, 4, 3}},
		{5, 3, nil},
		{6, 1, nil},
		{7, 1, nil},
		{10, 1, nil},
		{15, 1, nil},
		{20, 1, nil},
		{100, 1, nil},
	}

	for _, c := range cases {
		cards1 := make([]int, c.num)
		for index := 0; index < c.num; index++ {
			cards1[index] = (index + c.salt) * c.salt
		}
		cards2 := shuffle(cards1)
		cards3 := scatter(cards2)
		if c.cards != nil {
			assert.Equal(t, c.cards, cards2)
		}
		assert.Equal(t, cards1, cards3)
	}
}

func TestGamble(t *testing.T) {
	var cases = []struct {
		num   int
		salt  int
		cards []int
	}{
		{1, 1, nil},
		{2, 1, nil},
		{3, 1, nil},
		{4, 1, nil},
		{5, 1, []int{1, 5, 2, 4, 3}},
		{5, 3, nil},
		{6, 1, nil},
		{7, 1, nil},
		{10, 1, nil},
		{15, 1, nil},
		{20, 1, nil},
		{100, 1, nil},
	}

	for _, c := range cases {
		cards1 := make([]int, c.num)
		for index := 0; index < c.num; index++ {
			cards1[index] = (index + c.salt) * c.salt
		}
		cards2 := gamble(cards1)
		cards3 := shuffle(cards1)
		if c.cards != nil {
			assert.Equal(t, c.cards, cards2)
		}
		assert.Equal(t, cards2, cards3)
	}
}
