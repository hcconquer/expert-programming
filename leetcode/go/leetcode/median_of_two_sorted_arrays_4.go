package leetcode

import "log"

/*
 * There are two sorted arrays nums1 and nums2 of size m and n respectively.
 *
 * Find the median of the two sorted arrays. The overall run time complexity should be O(log(m+n)).
 *
 * Example 1:
 * nums1 = [1, 3]
 * nums2 = [2]
 * The median is 2.0
 *
 * Example 2:
 * nums1 = [1, 2]
 * nums2 = [3, 4]
 * The median is (2 + 3) / 2 = 2.5
**/

// nums1 is longer then nums2
func getKth(nums1, nums2 []int, kth int) int {
	log.Printf("num1: %+v, nums2: %+v, kth: %d", nums1, nums2, kth)
	len1 := len(nums1)
	len2 := len(nums2)
	if len1 < len2 {
		return getKth(nums2, nums1, kth)
	}
	if len2 == 0 {
		return nums1[kth]
	}
	if kth == 0 {
		if nums1[0] > nums2[0] {
			return nums2[0]
		}
		return nums1[0]
	}
	if len1+len2 == kth+1 {
		if nums1[len1-1] < nums2[len2-1] {
			return nums2[len2-1]
		}
		return nums1[len1-1]
	}
	// nums2 is short so index2 convergent quicker
	index2 := kth / 2
	if index2 >= len2 {
		index2 = len2 - 1
	}
	// index1 = kth - kth/2 - 1 = kth/2 - 1
	// so index1 is must be valid because nums1 is longer then nums2
	// -1 is very important
	// e.g. kth = 3, total 4 elem
	// index1 = 1 and index2 = 1, total 4 elem
	index1 := kth - index2 - 1
	log.Printf("index1: %d, index2: %d", index1, index2)
	if nums1[index1] == nums2[index2] {
		return nums1[index1]
	} else if nums1[index1] < nums2[index2] {
		return getKth(nums1[index1+1:], nums2, kth-index1-1)
	} else {
		return getKth(nums1, nums2[index2+1:], kth-index2-1)
	}
}

func findMedianSortedArrays(nums1 []int, nums2 []int) float64 {
	// log.Printf("new, num1: %+v, nums2: %+v", nums1, nums2)
	log.Printf("------------------------------------------")
	lens := len(nums1) + len(nums2)
	mid := (lens - 1) / 2
	if lens%2 == 0 {
		return float64(getKth(nums1, nums2, mid)+getKth(nums1, nums2, mid+1)) / 2
	}
	return float64(getKth(nums1, nums2, mid))
}
