package leetcode

import "testing"

func countBitsByMod(num uint64) int {
	count := 0
	bit := uint64(1)
	for i := 0; i < 64; i++ {
		if (num & bit) != 0 {
			count++
		}
		bit = bit << 1
	}
	return count
}

func TestCountBit(t *testing.T) {
	numMax := uint64(67000) // test up to 17bit

	bitNumArray := make([]int, numMax+1) // expected result, creat once
	for i := uint64(0); i <= numMax; i++ {
		bitNumArray[i] = countBitsByMod(i)
	}

	for num := uint64(0); num <= numMax; num++ {
		array := countBits(num)
		arrayLen := len(array)
		// t.Logf("num: %d, array: %v, arrayLen: %d", num, array, arrayLen)
		if arrayLen != int(num+1) {
			t.Errorf("array len error, expected: %d, actual: %d", num+1, arrayLen)
		}
		for i := uint64(0); i < num; i++ {
			if array[i] != bitNumArray[i] {
				t.Errorf("array[%d] bit count error, expected: %d, actual: %d", i, array[i], bitNumArray[i])
			}
		}
	}
}
