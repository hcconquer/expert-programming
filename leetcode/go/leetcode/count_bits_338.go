package leetcode

/*
 * Given a non negative integer number num. For every numbers i in the
 * range 0 ≤ i ≤ num calculate the number of 1's in their binary representation
 * and return them as an array.
 *
 * Example 1:
 * Input: 2
 * Output: [0,1,1]
 *
 * Example 2:
 * Input: 5
 * Output: [0,1,1,2,1,2]
 *
 * Follow up:
 * It is very easy to come up with a solution with run time O(n*sizeof(integer)).
 * But can you do it in linear time O(n) /possibly in a single pass?
 * Space complexity should be O(n).
 * Can you do it like a boss? Do it without using any builtin function
 * like __builtin_popcount in c++ or in any other language.
**/
func countBits(num uint64) []int {
	array := []int{0, 1, 1, 2}
	if num <= 3 {
		return array[:num+1]
	}

	array = append(array, make([]int, num-3)...)
	for k := uint64(4); k <= num; k = k * 2 {
		// kx = min(k, num - k + 1)
		// k+j<num --> k+j<=num+1 --> j<num-k+1
		kx := k
		if num < 2*k {
			kx = num - k + 1
		}
		for j := uint64(0); j < kx; j++ {
			array[k+j] = array[j] + 1
		}
	}

	return array
}
