/**
 * single_number_136.c
 *
 * Created: 2017/03/22
 *  Author: hanchen
 */

/**
 * Given an array of integers, every element appears twice except for one.
 * Find that single one.
 *
 * Note:
 * Your algorithm should have a linear runtime complexity. Could you
 * implement it without using extra memory?
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int singleNumber(int *nums, int numsSize) {
    int spec = 0;
    while (numsSize-- > 0) {
        spec = spec ^ nums[numsSize];
    }
    return spec;
}

typedef struct {
    int array[1024];
    int num;
    int spec;
} single_mumber_test_t;

static void single_mumber_test(int argc, char **argv) {
    single_mumber_test_t cases[] = {{{1}, 1, 1},
                                    {{0, 1, 0}, 3, 1},
                                    {{1, 0, 1}, 3, 0},
                                    {{1, 2, 3, 4, 3, 2, 1}, 7, 4}};
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        int *array = cases[k].array;
        int num = cases[k].num;
        int spec = cases[k].spec;
        int s = singleNumber(array, num);
        if (s != spec) {
            printf("test %d fail, %d != %d(expected), num: %d.\n", k, s, spec,
                   num);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    single_mumber_test(argc, argv);
    exit(EXIT_SUCCESS);
}
