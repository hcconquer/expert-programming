/**
 * power_of_four_342.c
 *
 * Created: 2016/05/13
 *  Author: hanchen
 */

/**
 * Given an integer (signed 32 bits), write a function to check whether it is a
 * power of 4. Example: Given num = 16, return true. Given num = 5, return
 * false. Follow up: Could you solve it without loops/recursion?
 */

#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 * p(x, n) - 1 = (x - 1) * (p(x, n - 1) + p(x, n - 2) + ... + p(x, 0))
 * it's easy to prove it by set-off
 * but I can't prove (p(2, 2n + 1) - 1) % 3 != 0
 *
 *    1 --- 0x01 --- true
 *    2 --- 0x02 --- false
 *    4 --- 0x04 --- true
 *    6 --- 0x06 --- false
 *    8 --- 0x08 --- false
 * 16 --- 0x10 --- true
 * 32 --- 0x20 --- false
 * 64 --- 0x40 --- true
 */
bool isPowerOfFour(int num) {
    if (num <= 0) {
        return false;
    }
    unsigned m = 1u << (sizeof(int) * 8 - 1);
    unsigned r = m % num;
    // return (r == 0) ? true : false;
    if (r != 0) {
        return false;
    }
    unsigned x = 0x55555555u;
    r = num | x;
    if (r == x) {
        return true;
    }
    return false;
}

typedef struct {
    int n;
    bool r;
} power_of_four_test_t;

static void is_power_of_four_test(int argc, char **argv) {
    power_of_four_test_t cases[] = {
        {0, false},   {1, true},   {2, false},      {4, true},
        {8, false},   {16, true},  {32, false},     {64, true},
        {128, false}, {256, true}, {1 << 30, true}, {1 << 31, false},
    };
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        int n = cases[k].n;
        bool r = cases[k].r;
        bool t = isPowerOfFour(n);
        if (r != t) {
            printf("test %d fail, %d != %d(expected).\n", k, t, r);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    is_power_of_four_test(argc, argv);
    exit(EXIT_SUCCESS);
}
