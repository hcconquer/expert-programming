/**
 * single_number_iii_260.c
 *
 * Created: 2017/04/07
 *  Author: hanchen
 */

/**
 * Given an array of numbers nums, in which exactly two elements appear only
 * once and all the other elements appear exactly twice. Find the two elements
 * that appear only once.
 *
 * For example:
 * Given nums = [1, 2, 1, 3, 2, 5], return [3, 5].
 *
 * Note:
 * 1.The order of the result is not important. So in the above example,
 * [5, 3] is also correct.
 * 2.Your algorithm should run in linear runtime complexity. Could you
 * implement it using only constant space complexity?
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ARRPRT(array, offset, len)         \
    ({                                     \
        typeof(offset) _o = offset;        \
        typeof(len) _l = len;              \
        typeof(len) _i = 0;                \
        for (_i = 0; _i < _l; _i++) {      \
            printf("%d ", array[_o + _i]); \
        }                                  \
        printf("\n");                      \
    })

int pivot(int *nums, int offset, int len) {
    int left = offset, right = offset + (len - 1);
    int piv = right;
    int val = nums[piv];
    while (left < right) {
        while (left < right) {
            if (nums[left] > val) {
                nums[right--] = nums[left];
                break;
            }
            left++;
        }
        while (left < right) {
            if (nums[right] <= val) {
                nums[left++] = nums[right];
                break;
            }
            right--;
        }
    }
    nums[left] = val;
    return left;
}

int single_mumber(int *nums, int offset, int len, int *exp) {
    int spec = 0, spex = 0;
    for (int i = 0; i < len; i++) {
        spec = spec ^ nums[offset + i];
        if (nums[offset + i] != *exp) {
            spex = spex ^ nums[offset + i];
        }
    }
    *exp = spex;
    return spec;
}

int *single_number_v(int *nums, int offset, int len, int *nlen) {
    int *array = NULL;
    int s, sx = -1;
    if (len <= 1) {
        array = (int *)malloc(1 * sizeof(int));
        array[0] = nums[offset];
        *nlen = 1;
        return array;
    } else if (len == 2) {
        if (nums[offset] == nums[offset + 1]) {
            *nlen = 0;
            return NULL;
        } else {
            array = (int *)malloc(2 * sizeof(int));
            array[0] = nums[offset];
            array[1] = nums[offset + 1];
            *nlen = 2;
            return array;
        }
    } else {
        s = single_mumber(nums, offset, len, &sx);
        if (s == 0) {
            *nlen = 0;
            return NULL;
        }
    }

    int pi = -1;

    // left and right shoud be even
    for (int i = 0; i < len; i++) {
        pi = pivot(nums, offset, len - i);
        if (pi % 2 == 0) {
            break;
        }
    }

    int ln = pi - offset, rn = len - ln;
    int sx1 = nums[pi], sx2 = nums[pi];
    int s1 = single_mumber(nums, offset, ln, &sx1);
    int s2 = single_mumber(nums, pi, rn, &sx2);

    int *sa1, *sa2;
    int sn1, sn2;
    if ((s1 == 0) && (sx1 == 0)) {
        sa2 = single_number_v(nums, pi, rn, &sn2);
        *nlen = sn2;
        return sa2;
    } else if ((s2 == 0) && (sx2 == 0)) {
        sa1 = single_number_v(nums, offset, ln, &sn1);
        *nlen = sn1;
        return sa1;
    } else {
        sn1 = 0;
        sn2 = 0;
        for (int i = 0; i < len; i++) {
            if (nums[offset + i] == s1) {
                sn1++;
            } else if (nums[offset + i] == s2) {
                sn2++;
            }
        }
        array = (int *)malloc(2 * sizeof(int));
        if ((sn1 % 2 == 1) && (sn2 % 2 == 1)) {
            array[0] = s1;
            array[1] = s2;
        } else {
            array[0] = sx1;
            array[1] = sx2;
        }
        *nlen = 2;
        return array;
    }

    *nlen = 0;
    return NULL;
}

/*
 * because of sum of xor can't split two numbers, so need to sum splited,
 * two different number must has 1 bit not match at least, xor has 1 bit
 * is not zero, e.g. 3(0011) xor 6(0110) = 5(0101)
 * so can use & to diff two numbers, and just 1 bit only because don't know
 * the bit 1 is come from which number
 * 3(0011) & 5(0101) = 0 and 6(0110) & 5(0101) = 0, fail
 * 3(0011) & 1(0001) = 1 and 6(0110) & 1(0001) = 0, good
 * 3(0011) & 4(0001) = 0 and 6(0110) & 4(0100) = 1, good
 *
 * there is only 1 bit different between true form and complement, which is
 * last bit 1
 */
int *single_mumber_x(int *nums, int numsSize, int *returnSize) {
    int spec = 0, i;
    for (i = 0; i < numsSize; i++) {
        spec ^= nums[i];
    }
    spec = (spec) & (-spec);
    int *array = (int *)calloc(2, sizeof(int));  // filled with zero
    for (i = 0; i < numsSize; i++) {
        if (nums[i] & spec) {
            array[0] ^= nums[i];
        } else {
            array[1] ^= nums[i];
        }
    }
    *returnSize = 2;
    return array;
}

/**
 * Return an array of size *returnSize.
 * Note: The returned array must be malloced, assume caller calls free().
 */
int *singleNumber(int *nums, int numsSize, int *returnSize) {
    //    return single_number_v(nums, 0, numsSize, returnSize);
    return single_mumber_x(nums, numsSize, returnSize);
}

typedef struct {
    int array[1024];
    int num;
    int specs[2];
} single_mumber_test_t;

static void single_mumber_test(int argc, char **argv) {
    single_mumber_test_t cases[] = {
        {{1, 2}, 2, {1, 2}},
        {{1, 2, 2, 3}, 4, {1, 3}},
        {{1, 2, 1, 3, 2, 5}, 6, {3, 5}},
        {{1, 3, 1, 2}, 4, {2, 3}},
        {{1, 2, 1, 3, 2, 5}, 6, {3, 5}},
        {{2, 2, 2, 2, 1, 3}, 6, {1, 3}},
        {{2, 2, 1, 2, 2, 3}, 6, {1, 3}},
        {{2, 2, 3, 2, 2, 1}, 6, {1, 3}},
        {{3, 2, 1, 1, 1, 1}, 6, {2, 3}},
        {{1, 2, 1, 1, 3, 1}, 6, {2, 3}},
        {{1, 2, 3, 4, 4, 5, 2, 1}, 8, {3, 5}},
        {{1, 1, 1, 1, 2, 3}, 6, {2, 3}},
    };
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        int *array = cases[k].array;
        int num = cases[k].num;
        int *specs = cases[k].specs;
        int n;
        int *sps = singleNumber(array, num, &n);
        //		ARRPRT(sps, 0, n);
        if ((n != 2) || (sps[0] * sps[1] != specs[0] * specs[1])) {
            printf("test %d fail, %d != %d(expected), num: %d.\n", k, n, 2,
                   num);
            ARRPRT(sps, 0, n);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    single_mumber_test(argc, argv);
    exit(EXIT_SUCCESS);
}
