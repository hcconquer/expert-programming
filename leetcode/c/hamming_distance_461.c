/**
 * hamming_distance_461.c
 *
 * Created: 2017/04/07
 *  Author: hanchen
 */

/**
 * The Hamming distance between two integers is the number of positions at
 * which the corresponding bits are different.
 * Given two integers x and y, calculate the Hamming distance.
 *
 * Note:
 * 0 ≤ x, y < 2^31.
 *
 * Example:
 * Input: x = 1, y = 4
 * Output: 2

 * Explanation:
 * 1     (0 0 0 1)
 * 4     (0 1 0 0)
 *                ↑     ↑
 *
 * The above arrows point to positions where the corresponding bits are
 different.
 */

#include <stdio.h>
#include <stdlib.h>

int hammingDistanceByMap(int x, int y) {
    const static int BIT_MAP[] = {
        [0] = 0,  [1] = 1,  [2] = 1,  [3] = 2,  [4] = 1,  [5] = 2,
        [6] = 2,  [7] = 3,  [8] = 1,  [9] = 2,  [10] = 2, [11] = 3,
        [12] = 2, [13] = 3, [14] = 3, [15] = 4,
    };
    int r = x ^ y;
    int d = 0;
    while (r != 0) {
        d += BIT_MAP[(r & 0x0F)];
        r = r >> 4;
    }
    return d;
}

int hammingDistanceNormal(int x, int y) {
    int r = x ^ y;
    int d = 0;
    while (r != 0) {
        d += (r & 0x01);
        r = r >> 1;
    }
    return d;
}

int hammingDistance(int x, int y) {
    return hammingDistanceByMap(x, y);
}

typedef struct {
    int x;
    int y;
    int d;
} hamming_distance_test_t;

static void hamming_distance_test(int argc, char **argv) {
    hamming_distance_test_t cases[] = {
        {0, 0, 0},
        {1, 1, 0},
        {1, 4, 2},
        {0, 1, 1},
        {111111111, 999999999, 13},
        {987654321, 123456789, 15},
        {2147483647, 2147483647, 0},
    };
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        int x = cases[k].x;
        int y = cases[k].y;
        int d = cases[k].d;
        int dis = hammingDistance(x, y);
        if (dis != d) {
            printf("test %d fail, %d != %d(expected), x: %d, y: %d.\n", k, dis,
                   d, x, y);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    hamming_distance_test(argc, argv);
    exit(EXIT_SUCCESS);
}
