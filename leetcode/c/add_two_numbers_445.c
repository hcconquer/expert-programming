/**
 * add_two_numbers_445.c
 *
 * Created: 2016/11/17
 *  Author: hanchen
 */

/**
 * You are given two linked lists representing two non-negative numbers.
 * The most significant digit comes first and each of their nodes contain a
 single digit.
 * Add the two numbers and return it as a linked list.
 * You may assume the two numbers do not contain any leading zero, except the
 number 0 itself.

 * Follow up:
 * What if you cannot modify the input lists? In other words, reversing the
 lists is not allowed.

 * Example: 7243 + 564 = 7807
 * Input: (7 -> 2 -> 4 -> 3) + (5 -> 6 -> 4)
 * Output: 7 -> 8 -> 0 -> 7
 *
 */

#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct ListNode {
    int val;
    struct ListNode *next;
};

static void list_print(struct ListNode *list) {
    while (list != NULL) {
        printf("%d ", list->val);
        list = list->next;
    }
    printf("\n");
}

static int list_cmp(struct ListNode *l1, struct ListNode *l2) {
    while ((l1 != NULL) && (l2 != NULL)) {
        if (l1->val != l2->val) {
            return l1->val < l2->val ? -1 : 1;
        }
        l1 = l1->next;
        l2 = l2->next;
    }
    return (l1 == l2) ? 0 : ((l1 == NULL) ? -1 : 1);
}

static struct ListNode *make_list_by_seq(int *seq, int num) {
    if ((seq == NULL) || (num <= 0)) {
        return NULL;
    }
    struct ListNode *head = NULL, *tail, *node;
    node = (struct ListNode *)malloc(sizeof(struct ListNode));
    node->val = seq[0];
    // node->next = NULL;
    head = node;
    tail = head;
    for (int i = 1; i < num; i++) {
        node = (struct ListNode *)malloc(sizeof(struct ListNode));
        node->val = seq[i];
        // node->next = NULL;
        tail->next = node;
        tail = node;
        // printf("node: %d, val: %d\n", i, node->val);
    }
    tail->next = NULL;
    return head;
}

static size_t list_length(struct ListNode *list) {
    size_t len = 0;
    while (list != NULL) {
        len++;
        list = list->next;
    }
    return len;
}

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *         int val;
 *         struct ListNode *next;
 * };
 */
struct ListNode *addTwoNumbers(struct ListNode *l1, struct ListNode *l2) {
    struct ListNode *head = NULL, *tail = NULL;

    size_t n1 = list_length(l1);
    size_t n2 = list_length(l2);
    struct ListNode *node = l1, *p1, *p2;
    size_t n = n1;
    // let l1 longer then l2
    if (n1 < n2) {
        l1 = l2;
        l2 = node;
        n1 = n2;
        n2 = n;
        n = n1;
    }
    p1 = l1;
    p2 = l2;

    // skip spare node
    size_t d = n1 - n2;
    for (int i = 0; i < d; i++) {
        node = (struct ListNode *)malloc(sizeof(struct ListNode));
        node->val = p1->val;
        if (head == NULL) {
            head = node;
            tail = head;
        }
        tail->next = node;
        tail = node;
        p1 = p1->next;
    }

    // merge list
    for (int i = d; i < n; i++) {
        node = (struct ListNode *)malloc(sizeof(struct ListNode));
        node->val = p1->val + p2->val;
        if (head == NULL) {
            head = node;
            tail = head;
        }
        if (node->val > 9) {
            if (tail != node) {
                tail->val += 1;
                node->val -= 10;
            }
        }
        tail->next = node;
        tail = node;
        p1 = p1->next;
        p2 = p2->next;
    }
    tail->next = NULL;

    // handle carry
    int carry = 1;
    while (carry != 0) {
        carry = 0;
        node = head;
        tail = head->next;
        while (tail != NULL) {
            if (tail->val > 9) {
                node->val += 1;
                tail->val -= 10;
                if (node->val > 9) {  // repeat carry
                    // printf("carry, tail: %d\n", tail->val);
                    carry = 1;
                    break;
                }
            }
            node = tail;
            tail = tail->next;
        }
    }

    if (head->val > 9) {
        node = (struct ListNode *)malloc(sizeof(struct ListNode));
        node->val = 1;
        head->val -= 10;
        node->next = head;
        head = node;
    }

    return head;
}

typedef struct {
    int seq1[65536];
    int len1;
    int seq2[65536];
    int len2;
    int sum[65536];
    int len;
} add_two_numbers_test_t;

static void add_two_numbers_test(int argc, char **argv) {
    add_two_numbers_test_t cases[] = {
        {{7, 2, 4, 3}, 4, {5, 6, 4}, 3, {7, 8, 0, 7}, 4},
        {{1, 2, 3, 4}, 4, {9, 8, 7, 6}, 4, {1, 1, 1, 1, 0}, 5},
        {{9, 9, 9}, 3, {9, 9, 9}, 3, {1, 9, 9, 8}, 4},
        {{9}, 1, {9, 9, 9}, 3, {1, 0, 0, 8}, 4},
        {{9, 9, 9}, 3, {9, 9, 9, 9}, 4, {1, 0, 9, 9, 8}, 5},
        {{9}, 1, {1, 9, 9, 9, 9}, 5, {2, 0, 0, 0, 8}, 5}};
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        struct ListNode *l1 = make_list_by_seq(cases[k].seq1, cases[k].len1);
        struct ListNode *l2 = make_list_by_seq(cases[k].seq2, cases[k].len2);
        struct ListNode *sum = make_list_by_seq(cases[k].sum, cases[k].len);
        list_print(l1);
        list_print(l2);
        list_print(sum);
        struct ListNode *s = addTwoNumbers(l1, l2);
        list_print(s);
        int r = list_cmp(s, sum);
        if (r != 0) {
            printf("test %d fail, ret: %d.\n", k, r);
            // list_print(s);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    add_two_numbers_test(argc, argv);
    exit(EXIT_SUCCESS);
}
