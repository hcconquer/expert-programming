/*
 * nim_game_292.c
 *
 * Created: 2016/5/11
 *  Author: hanchen
 */

/**
 * You are playing the following Nim Game with your friend: There is a heap of
 * stones on the table, each time one of you take turns to remove 1 to 3 stones.
 * The one who removes the last stone will be the winner. You will take the
 * first turn to remove the stones. Both of you are very clever and have optimal
 * strategies for the game. Write a function to determine whether you can win
 * the game given the number of stones in the heap. For example, if there are 4
 * stones in the heap, then you will never win the game: no matter 1, 2, or 3
 * stones you remove, the last stone will always be removed by your friend.
 * Hint:
 * If there are 5 stones in the heap, could you figure out a way to remove the
 * stones such that you will always be the winner?
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

bool canWinNimNum(int num, int max) {
    if (num <= max) {
        return true;
    }
    int rem = num % (max + 1);
    if (rem == 0) {
        return false;
    }
    return true;
}

bool canWinNim(int n) {
    return canWinNimNum(n, 3);
}

typedef struct {
    int num;
    bool result;
} nim_game_test_t;

static void excel_sheet_column_number_test(int argc, char **argv) {
    nim_game_test_t cases[] = {
        {0, true}, {1, true}, {2, true}, {3, true},  {4, false},
        {5, true}, {6, true}, {7, true}, {8, false},
    };

    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        int num = cases[k].num;
        bool result = cases[k].result;
        bool r = canWinNim(num);

        if (r != result) {
            printf("test %d fail, %d != %d(expected).\n", k, r, result);
            return;
        }

        printf("test %d pass.\n", k);
    }

    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    excel_sheet_column_number_test(argc, argv);
    exit(EXIT_SUCCESS);
}
