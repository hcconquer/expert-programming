/**
 * container_with_most_water_11.c
 *
 * Created: 2017/03/30
 *  Author: hanchen
 */

/**
 * Given n non-negative integers a1, a2, ..., an, where each represents
 * a point at coordinate (i, ai). n vertical lines are drawn such that
 * the two endpoints of line i is at (i, ai) and (i, 0). Find two lines,
 * which together with x-axis forms a container, such that the container
 * contains the most water.
 *
 * Note: You may not slant the container and n is at least 2.
 */

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int maxArea(int *height, int heightSize) {
    return 0;
}

typedef struct {
    int heights[256];
    int num;
    int area;
} max_area_test_t;

static void max_area_test(int argc, char **argv) {
    max_area_test_t cases[] = {
        {{1, 2, 3, 4}, 4, 4},
    };
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        int *heights = cases[k].heights;
        int num = cases[k].num;
        int area = cases[k].area;
        int ar = maxArea(heights, num);
        if (ar != area) {
            printf("test %d fail, %d != %d(expected).\n", k, ar, area);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    max_area_test(argc, argv);
    exit(EXIT_SUCCESS);
}
