/**
 * reverse_string_344.c
 *
 * Created: 2016/4/25
 *  Author: hanchen
 */

/**
 * Write a function that takes a string as input and returns the string
 * reversed. Example: Given s = "hello", return "olleh".
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *reverseString(char *s) {
    int i = 0, c = 0, len = 0, hlen = 0;
    len = strlen(s);
    hlen = len / 2;
    for (i = 0; i < hlen; i++) {
        c = s[i];
        s[i] = s[len - i - 1];
        s[len - i - 1] = c;
    }
    return s;
}

typedef struct {
    char ss[256];
    char ds[256];
} reverse_string_test_t;

void reverse_string_test(int argc, char **argv) {
    reverse_string_test_t cases[] = {{"", ""}, {"hello", "olleh"}};
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        char *ss = cases[k].ss;
        char *ds = cases[k].ds;
        ss = reverseString(ss);
        if (strcmp(ss, ds) != 0) {
            printf("test %d fail, %s != %s(expected).\n", k, ss, ds);
            return;
        }
        printf("test %d pass.\n", k);
    }

    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    reverse_string_test(argc, argv);
    exit(EXIT_SUCCESS);
}
