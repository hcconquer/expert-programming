/**
 * longest_common_prefix_14.c
 *
 * Created: 2017/03/30
 *  Author: hanchen
 */

/**
 * Write a function to find the longest common prefix string amongst an
 * array of strings.
 */

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *longestCommonPrefix(char **strs, int strsSize) {
    char *prefix = NULL;
    int len = 0;
    if (strsSize > 0) {
        prefix = strs[0];
        len = strlen(prefix);
    }
    for (int i = 1; i < strsSize; i++) {
        for (int j = 0; j < len; j++) {
            if (strs[i][j] != prefix[j]) {
                len = j;
                break;
            }
        }
    }
    prefix = (char *)malloc(len + 1);
    strncpy(prefix, strs[0], len);
    prefix[len] = '\0';
    return prefix;
}

typedef struct {
    char strs[16][256];
    int num;
    char prefix[256];
} longest_common_prefix_test_t;

static void longest_common_prefix_test(int argc, char **argv) {
    longest_common_prefix_test_t cases[] = {
        {{}, 0, ""},
        {{"abc", "ab", "a"}, 3, "a"},
        {{"a", "ab", "abc"}, 3, "a"},
        {{"a", "ab", "c"}, 3, ""},
    };
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        // char (*strs)[STR_LEN_MAX] = cases[k].strs;
        int num = cases[k].num;
        char **strs = (char **)malloc(num * sizeof(char *));
        for (int i = 0; i < num; i++) {
            strs[i] = (char *)malloc(strlen(cases[k].strs[i]) + 1);
            strcpy(strs[i], cases[k].strs[i]);
        }
        char *prefix = cases[k].prefix;
        char *p = longestCommonPrefix((char **)strs, num);
        if (strcmp(p, prefix) != 0) {
            printf("test %d fail, %s != %s(expected).\n", k, p, prefix);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    longest_common_prefix_test(argc, argv);
    exit(EXIT_SUCCESS);
}
