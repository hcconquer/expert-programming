/**
 * quick_sort.c
 *
 * Created: 2017/04/09
 *  Author: hanchen
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ARRPRT(array, len)              \
    ({                                  \
        typeof(len) _l = len;           \
        typeof(len) _i = 0;             \
        for (_i = 0; _i < _l; _i++) {   \
            printf("%d ", (array)[_i]); \
        }                               \
        printf("\n");                   \
    })

#define SWAP(m, n)    \
    ({                \
        typeof(m) _t; \
        _t = (m);     \
        (m) = (n);    \
        (n) = _t;     \
    })

int pivot2(int *nums, int offset, int len) {
    int left = offset, right = offset + (len - 1);
    int piv = left + (right - left) / 2;
    int val = nums[piv];
    while (left < right) {
        while (left < right) {
            if (nums[left] >= val) {
                break;
            }
            left++;
        }
        while (left < right) {
            if (nums[right] < val) {
                break;
            }
            right--;
        }
        if (left != right) {
            SWAP(nums[left], nums[right]);
            if (left == piv) {
                piv = right;
            } else if (right == piv) {
                piv = left;
            }
            left++;
            right--;
        }
    }
    if (left < piv) {
        if (nums[left] > nums[piv]) {
            SWAP(nums[left], nums[piv]);
        }
    } else if (right > piv) {
        if (nums[right] < nums[piv]) {
            SWAP(nums[right], nums[piv]);
        }
    }
    return left;
}

/*
 * pivot is quick 30% then pivot2, it's standard pivot method,
 * you need to use nums[0] or nums[len - 1] as pivot, even if use nums[rand],
 * just swap it
 */
int pivot(int *nums, int offset, int len) {
    int left = offset, right = offset + (len - 1);
    int piv = right;
    int val = nums[piv];
    while (left < right) {
        while (left < right) {
            if (nums[left] > val) {
                nums[right--] = nums[left];
                break;
            }
            left++;
        }
        while (left < right) {
            if (nums[right] < val) {
                nums[left++] = nums[right];
                break;
            }
            right--;
        }
    }
    nums[left] = val;
    return left;
}

void quick_sort_v(int *nums, int offset, int len) {
    if (len <= 1) {
        return;
    } else if (len == 2) {
        if (nums[offset] > nums[offset + 1]) {
            SWAP(nums[offset], nums[offset + 1]);
        }
    }
    int piv = pivot(nums, offset, len);
    quick_sort_v(nums, offset, piv - offset);
    quick_sort_v(nums, piv + 1, len - (piv - offset) - 1);
}

void quick_sort(int *nums, int numsSize) {
    quick_sort_v(nums, 0, numsSize);
}

typedef struct {
    int array[1024];
    int num;
    int sdarr[1024];
} single_mumber_test_t;

static void single_mumber_test(int argc, char **argv) {
    single_mumber_test_t cases[] = {
        {{}, 0, {}},
        {{1}, 1, {1}},
        {{1, 2}, 2, {1, 2}},
        {{2, 1}, 2, {1, 2}},
        {{1, 2, 3}, 3, {1, 2, 3}},
        {{3, 2, 1}, 3, {1, 2, 3}},
        {{1, 1, 1, 1, 1, 1}, 6, {1, 1, 1, 1, 1, 1}},
        {{2, 2, 2, 2, 2, 2}, 6, {2, 2, 2, 2, 2, 2}},
        {{2, 1, 1, 1, 1, 1}, 6, {1, 1, 1, 1, 1, 2}},
        {{1, 2, 1, 1, 1, 1}, 6, {1, 1, 1, 1, 1, 2}},
        {{1, 1, 2, 1, 1, 1}, 6, {1, 1, 1, 1, 1, 2}},
        {{1, 1, 1, 2, 1, 1}, 6, {1, 1, 1, 1, 1, 2}},
        {{1, 1, 1, 1, 2, 1}, 6, {1, 1, 1, 1, 1, 2}},
        {{1, 1, 1, 1, 1, 2}, 6, {1, 1, 1, 1, 1, 2}},
        {{2, 1, 1, 1, 1, 2}, 6, {1, 1, 1, 1, 2, 2}},
        {{1, 2, 1, 1, 2, 1}, 6, {1, 1, 1, 1, 2, 2}},
        {{1, 1, 2, 2, 1, 1}, 6, {1, 1, 1, 1, 2, 2}},
        {{1, 2, 3, 4, 5, 6}, 6, {1, 2, 3, 4, 5, 6}},
        {{6, 5, 4, 3, 2, 1}, 6, {1, 2, 3, 4, 5, 6}},
        {{5, 2, 3, 4, 6, 1}, 6, {1, 2, 3, 4, 5, 6}},
        {{1, 2, 3, 4, 5, 1}, 6, {1, 1, 2, 3, 4, 5}},
        {{1, 2, 1, 3, 4, 1}, 6, {1, 1, 1, 2, 3, 4}},
        {{2, 2, 2, 2, 1, 3}, 6, {1, 2, 2, 2, 2, 3}},
        {{2, 2, 1, 2, 2, 3}, 6, {1, 2, 2, 2, 2, 3}},
        {{2, 2, 3, 2, 2, 1}, 6, {1, 2, 2, 2, 2, 3}},
        {{2, 3, 3, 2, 2, 1}, 6, {1, 2, 2, 2, 3, 3}},
        {{2, 2, 3, 3, 2, 1}, 6, {1, 2, 2, 2, 3, 3}},
        {{1, 2, 3, 3, 2, 1}, 6, {1, 1, 2, 2, 3, 3}},
        {{2, 1, 3, 3, 2, 1}, 6, {1, 1, 2, 2, 3, 3}},
    };
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        int *array = cases[k].array;
        int num = cases[k].num;
        int *sdarr = cases[k].sdarr;
        quick_sort(array, num);
        for (int i = 0; i < num; i++) {
            if (array[i] != sdarr[i]) {
                printf("test %d fail, %d != %d(expected), num: %d.\n", k,
                       array[i], sdarr[i], num);
                ARRPRT(array, num);
                ARRPRT(sdarr, num);
                return;
            }
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    single_mumber_test(argc, argv);
    exit(EXIT_SUCCESS);
}
