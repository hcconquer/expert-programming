/**
 * two_sum_1.c
 *
 * Created: 2016/11/16
 *  Author: hanchen
 */

/**
 * Given an array of integers, return indices of the two numbers such that they
 * add up to a specific target. You may assume that each input would have
 * exactly one solution.
 *
 * Example:
 * Given nums = [2, 7, 11, 15], target = 9,
 *
 * Because nums[0] + nums[1] = 2 + 7 = 9,
 * return [0, 1].
 *
 * UPDATE (2016/2/13):
 * The return format had been changed to zero-based indices.
 * Please read the above updated description carefully.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ARRPRT(array, len)            \
    ({                                \
        typeof(len) _l = len;         \
        typeof(len) _i = 0;           \
        for (_i = 0; _i < _l; _i++) { \
            printf("%d ", array[_i]); \
        }                             \
        printf("\n");                 \
    })

int int_cmp(const void *m, const void *n) {
    int mi = *((int *)m);
    int ni = *((int *)n);
    return (mi == ni) ? 0 : ((mi < ni) ? -1 : 1);
}

// get index for m and n in 1 loop
void idxsch(int *idxs, int m, int n, const int *array, int len) {
    int cnt = 0;
    for (int i = 0; i < len; i++) {
        if ((array[i] == m) || (array[i] == n)) {
            idxs[cnt++] = i;
            if (cnt >= 2) {
                break;
            }
        }
    }
}

/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int *twoSum(int *nums, int numsSize, int target) {
    // need to return origin index, so can't sort origin array
    int *nums_sorted = (int *)malloc(numsSize * sizeof(nums[0]));
    if (nums_sorted == NULL) {
        return NULL;
    }
    memcpy(nums_sorted, nums, numsSize * sizeof(nums_sorted[0]));
    qsort(nums_sorted, numsSize, sizeof(nums_sorted[0]), int_cmp);

    int *idxs = (int *)malloc(2 * sizeof(int));
    int l = 0, r = numsSize - 1, s = 0;
    while (l < r) {
        s = nums_sorted[l] + nums_sorted[r];
        // printf("left: %d, right: %d, sum: %d, target: %d\n", l, r, s,
        // target);
        if (s == target) {
            idxsch(idxs, nums_sorted[l], nums_sorted[r], nums, numsSize);
            break;
        } else if (s < target) {
            l++;
        } else {
            r--;
        }
    }

    if (nums_sorted != NULL) {
        free(nums_sorted);
    }

    return idxs;
}

typedef struct {
    int nums[65536];
    int count;
    int target;
    int idxs[2];
} two_sum_test_t;

static void two_sum_test(int argc, char **argv) {
    two_sum_test_t cases[] = {{{2, 7, 11, 15}, 4, 9, {0, 1}},
                              {{3, 2, 4}, 3, 6, {1, 2}},
                              {{-3, 4, 3, 90}, 4, 0, {0, 2}},
                              {{0, 4, 3, 0}, 4, 0, {0, 3}},
                              {{-1, -2, -3, -4, -5}, 5, -8, {2, 4}}};
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        int *idxs = twoSum(cases[k].nums, cases[k].count, cases[k].target);
        int ret = memcmp(idxs, cases[k].idxs, 2 * sizeof(cases[k].nums[0]));
        if (ret != 0) {
            printf("test %d fail, [%d, %d] != [%d, %d](expected).\n", k,
                   idxs[0], idxs[1], cases[k].idxs[0], cases[k].idxs[1]);
            ARRPRT(cases[k].nums, cases[k].count);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    two_sum_test(argc, argv);
    exit(EXIT_SUCCESS);
}
