/*
 * sort_colors_75.c
 *
 * Created: 2016/04/27
 *  Author: hanchen
 */

/**
 * Given an array with n objects colored red, white or blue, sort them so that
 objects of the same color are adjacent, with the colors in the order red, white
 and blue.

 * Here, we will use the integers 0, 1, and 2 to represent the color red, white,
 and blue respectively.

 * Note:
 * You are not suppose to use the library's sort function for this problem.

 * Follow up:
 * A rather straight forward solution is a two-pass algorithm using counting
 sort.
 * First, iterate the array counting number of 0's, 1's, and 2's, then overwrite
 array with total number of 0's, then 1's and followed by 2's.

 * Could you come up with an one-pass algorithm using only constant space?
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ARRPRT(array, len)            \
    ({                                \
        typeof(len) _l = len;         \
        typeof(len) _i = 0;           \
        for (_i = 0; _i < _l; _i++) { \
            printf("%d ", array[_i]); \
        }                             \
        printf("\n");                 \
    })

void sortColors(int* nums, int numsSize) {
    int i = 0, j = 0, min = 0;
    while (i < numsSize - 1) {
        j = numsSize - 1;
        while (i < j) {
            while ((nums[i] == min) && (i < j)) {
                i++;
            }  // out if p(i) != min
            while ((nums[j] != min) && (i < j)) {
                j--;
            }  // out if p(j) == min
            if (i < j) {
                nums[j] = nums[i];
                nums[i] = min;
            }
        }
        min++;
    }
}

typedef struct {
    int array[256];
    int rn;
    int wn;
    int bn;
} sort_colors_test_t;

void test_spiral_matrix(int argc, char** argv) {
    sort_colors_test_t cases[] = {{{0}, 1, 0, 0},
                                  {{1}, 0, 1, 0},
                                  {{2}, 0, 0, 1},
                                  {{0, 0, 0}, 3, 0, 0},
                                  {{1, 1, 1}, 0, 3, 0},
                                  {{2, 2, 2}, 0, 0, 3},
                                  {{0, 1, 2}, 1, 1, 1},
                                  {{2, 1, 0}, 1, 1, 1},
                                  {{0, 1, 0, 2, 1, 0, 2, 1}, 3, 3, 2}};
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        int* nums = cases[k].array;
        int size = cases[k].rn + cases[k].wn + cases[k].bn;
        sortColors(nums, size);
        ARRPRT(nums, size);
        for (int i = 0; i < size; i++) {
            if (i < cases[k].rn) {
                if ((nums[i] != 0)) {
                    printf("test %d fail, %d != %d(expected).\n", k, nums[i],
                           0);
                    return;
                }
            } else if (i < cases[k].rn + cases[k].wn) {
                if (nums[i] != 1) {
                    printf("test %d fail, %d != %d(expected).\n", k, nums[i],
                           1);
                    return;
                }
            } else if (i < cases[k].rn + cases[k].wn + cases[k].bn) {
                if (nums[i] != 2) {
                    printf("test %d fail, %d != %d(expected).\n", k, nums[i],
                           2);
                    return;
                }
            }
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char* argv[]) {
    test_spiral_matrix(argc, argv);
    exit(EXIT_SUCCESS);
}
