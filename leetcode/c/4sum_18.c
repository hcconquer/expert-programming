/**
 * 4sum_18.c
 *
 * Created: 2017/04/01
 *  Author: hanchen
 */

/**
 * Given an array S of n integers, are there elements a, b, c, and d in S such
 * that a + b + c + d = target? Find all unique quadruplets in the array which
 * gives the sum of target.
 *
 * Note: The solution set must not contain duplicate quadruplets.
 *
 * For example, given array S = [1, 0, -1, 0, -2, 2], and target = 0.
 * A solution set is:
 * [
 *   [-1,  0, 0, 1],
 *   [-2, -1, 1, 2],
 *   [-2,  0, 0, 2]
 * ]
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ARRPRT(array, len)            \
    ({                                \
        typeof(len) _l = len;         \
        typeof(len) _i = 0;           \
        for (_i = 0; _i < _l; _i++) { \
            printf("%d ", array[_i]); \
        }                             \
        printf("\n");                 \
    })

#define ARRRPRT(array, idxs, len)           \
    ({                                      \
        typeof(len) _l = len;               \
        typeof(len) _i = 0;                 \
        for (_i = 0; _i < _l; _i++) {       \
            printf("%d ", array[idxs[_i]]); \
        }                                   \
        printf("\n");                       \
    })

int int_cmp(const void *m, const void *n) {
    int mi = *((int *)m);
    int ni = *((int *)n);
    return (mi == ni) ? 0 : ((mi < ni) ? -1 : 1);
}

int **anysum(int *nums, int numlen, int count, int target, int *alen) {
    int msize = numlen * sizeof(int);
    int *seqs = (int *)malloc(msize);
    memcpy(seqs, nums, msize);
    qsort(seqs, numlen, sizeof(int), int_cmp);

    int arrnum = 16, thnum = 0;
    int **arrs = (int **)malloc(arrnum * sizeof(int *));
    int *arr, *p;

    int *array = (int *)malloc(count * sizeof(int));
    memset(array, 0, count * sizeof(int));

    int lev = 0, sum, pval, djmp, i;
    for (;;) {
        djmp = 0;
        if (lev < count - 1) {
            if (lev > 0) {
                array[lev] = array[lev - 1] + 1;
            }
            lev++;
        } else {
            if (array[lev - 1] < numlen - 1) {
                sum = 0;
                for (int i = 0; i < count - 1; i++) {
                    sum += seqs[array[i]];
                }
                sum = target - sum;
                p = (int *)bsearch(&sum, seqs + array[lev - 1] + 1,
                                   numlen - array[lev - 1] - 1, sizeof(int),
                                   int_cmp);
                if (p != NULL) {
                    array[lev] = p - seqs;
                    if (thnum >= arrnum) {
                        arrnum *= 2;
                        arrs = realloc(arrs, arrnum * sizeof(int *));
                    }
                    arr = (int *)malloc(count * sizeof(int));
                    for (i = 0; i < count; i++) {
                        arr[i] = seqs[array[i]];
                    }
                    arrs[thnum++] = arr;
                }
            }
            if (array[lev - 1] < numlen) {
                pval = seqs[array[lev - 1]];
                array[lev - 1]++;
                djmp = 1;
            }
        }
        /*
         * back jump
         */
        if (array[lev - 1] >= numlen) {
            for (int l = count - 2; l >= 0; l--) {
                if (array[l] < numlen) {
                    lev = l;
                    pval = seqs[array[lev]];
                    array[lev]++;
                    lev++;
                    djmp = 1;
                    break;
                }
            }
        }
        if (djmp) {
            while (seqs[array[lev - 1]] == pval) {
                array[lev - 1]++;
                if (array[lev - 1] >= numlen) {
                    break;
                }
            }
        }
        if (array[0] >= numlen) {
            break;
        }
    }

    free(array);

    if (alen != NULL) {
        *alen = thnum;
    }

    return arrs;
}

/**
 * Return an array of arrays of size *returnSize.
 * Note: The returned array must be malloced, assume caller calls free().
 */
int **fourSum(int *nums, int numsSize, int target, int *returnSize) {
    return anysum(nums, numsSize, 4, target, returnSize);
}

typedef struct {
    int array[1024];
    int num;
    int target;
    int tharr[1024][4];
    int thnum;
} four_sum_test_t;

static void four_sum_test(int argc, char **argv) {
    four_sum_test_t cases[] = {
        {{1, 0, -1, 0, -2, 2},
         6,
         0,
         {{-1, 0, 0, 1}, {-2, -1, 1, 2}, {-2, 0, 0, 2}},
         3},
        {{1, 2, 3, 4, 5}, 5, 0, {}, 0},
        {{1, 2, 3, 4, 5, 6}, 6, 0, {}, 0},
        {{-1, 0, 1, 2, -1, -4}, 6, 0, {{-1, -1, 0, 2}}, 1},
        {{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, 10, 0, {}, 0},
        {{0, 0, 0, 0, 0}, 5, 0, {{0, 0, 0, 0}}, 1},
        {{1, 1, 1, 0, 0, 0}, 6, 0, {}, 0},
        {{-1, -1, -1, 0, 0, 0, 1, 1, 1}, 9, 0, {{0, 0, 0}, {-1, 0, 1}}, 2},
        {{3, 0, -2, -1, 1, 2}, 6, 0, {{-2, -1, 0, 3}, {-2, -1, 1, 2}}, 2},
        {{4, 3, 2, 1, 0, -1, -2, -3, -4},
         9,
         0,
         {{-4, -3, 3, 4},
          {-4, -2, 2, 4},
          {-4, -1, 1, 4},
          {-4, -1, 2, 3},
          {-4, 0, 1, 3},
          {-3, -2, 1, 4},
          {-3, -2, 2, 3},
          {-3, -1, 0, 4},
          {-3, -1, 1, 3},
          {-3, 0, 1, 2},
          {-2, -1, 0, 3},
          {-2, -1, 1, 2}},
         12},
    };
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        int *array = cases[k].array;
        int num = cases[k].num;
        int target = cases[k].target;
        // int **tharr = cases[k].tharr;
        int thnum = cases[k].thnum;
        int tn = 0;
        int **ta = fourSum(array, num, target, &tn);
        if ((ta == NULL) || (tn != thnum)) {
            printf("test %d fail, %d != %d(expected), num: %d, target: %d.\n",
                   k, tn, thnum, num, target);
            return;
        }
        int sum = 0;
        for (int i = 0; i < thnum; i++) {
            sum = 0;
            for (int j = 0; j < 4; j++) {
                sum += ta[i][j];
            }
            if (sum != target) {
                printf("test %d fail, %d != %d(expected).\n", k, sum, target);
                return;
            }
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    four_sum_test(argc, argv);
    exit(EXIT_SUCCESS);
}
