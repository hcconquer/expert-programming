/**
 * 3sum_15.c
 *
 * Created: 2017/03/24
 *  Author: hanchen
 */

/**
 * Given an array S of n integers, are there elements a, b, c in S such
 * that a + b + c = 0? Find all unique triplets in the array which gives the sum
 * of zero. Note: The solution set must not contain duplicate triplets.
 *
 * For example, given array S = [-1, 0, 1, 2, -1, -4],
 * A solution set is:
 * [
 *   [-1, 0, 1],
 *   [-1, -1, 2]
 * ]
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ARRPRT(array, len)            \
    ({                                \
        typeof(len) _l = len;         \
        typeof(len) _i = 0;           \
        for (_i = 0; _i < _l; _i++) { \
            printf("%d ", array[_i]); \
        }                             \
        printf("\n");                 \
    })

int int_cmp(const void *m, const void *n) {
    int mi = *((int *)m);
    int ni = *((int *)n);
    return (mi == ni) ? 0 : ((mi < ni) ? -1 : 1);
}

typedef struct node {
    int *array;
    struct node *next;
} node_t;

int **anysum(int *nums, int numlen, int count, int target, int *alen) {
    int msize = numlen * sizeof(int);
    int *seqs = (int *)malloc(msize);
    memcpy(seqs, nums, msize);
    qsort(seqs, numlen, sizeof(int), int_cmp);

    int arrnum = 16, thnum = 0;
    int **arrs = (int **)malloc(arrnum * sizeof(int *));
    int *arr, *p;

    int *array = (int *)malloc(count * sizeof(int));
    memset(array, 0, count * sizeof(int));

    int lev = 0, sum, pval, djmp, i;
    for (;;) {
        djmp = 0;
        if (lev < count - 1) {
            if (lev > 0) {
                array[lev] = array[lev - 1] + 1;
            }
            lev++;
        } else {
            if (array[lev - 1] < numlen - 1) {
                sum = 0;
                for (int i = 0; i < count - 1; i++) {
                    sum += seqs[array[i]];
                }
                sum = target - sum;
                p = (int *)bsearch(&sum, seqs + array[lev - 1] + 1,
                                   numlen - array[lev - 1] - 1, sizeof(int),
                                   int_cmp);
                if (p != NULL) {
                    array[lev] = p - seqs;
                    if (thnum >= arrnum) {
                        arrnum *= 2;
                        arrs = realloc(arrs, arrnum * sizeof(int *));
                    }
                    arr = (int *)malloc(count * sizeof(int));
                    for (i = 0; i < count; i++) {
                        arr[i] = seqs[array[i]];
                    }
                    arrs[thnum++] = arr;
                }
            }
            if (array[lev - 1] < numlen) {
                pval = seqs[array[lev - 1]];
                array[lev - 1]++;
                djmp = 1;
            }
        }
        if (array[lev - 1] >= numlen) {
            for (int l = count - 2; l >= 0; l--) {
                if (array[l] < numlen) {
                    lev = l;
                    pval = seqs[array[lev]];
                    array[lev]++;
                    lev++;
                    djmp = 1;
                    break;
                }
            }
        }
        if (djmp) {
            while (seqs[array[lev - 1]] == pval) {
                array[lev - 1]++;
                if (array[lev - 1] >= numlen) {
                    break;
                }
            }
        }
        if (array[0] >= numlen) {
            break;
        }
    }

    free(array);

    if (alen != NULL) {
        *alen = thnum;
    }

    return arrs;
}

/*
 * copy from 4sum_18.c
 * this is general method for any sum, but it's much slower then
 * threeSum2, about 2-3 times time
 */
int **threeSum3(int *nums, int numsSize, int *returnSize) {
    return anysum(nums, numsSize, 3, 0, returnSize);
}

/**
 * time less 10% then threeSum1 because use pre-malloc and realloc array instead
 * of list, reduce num of malloc for every node
 */
int **threeSum2(int *nums, int numsSize, int *returnSize) {
    int msize = numsSize * sizeof(int);
    int *seqs = (int *)malloc(msize);
    memcpy(seqs, nums, msize);
    qsort(seqs, numsSize, sizeof(int), int_cmp);
    // ARRPRT(seqs, numsSize);

    int arrnum = 16, thnum = 0, val = 0, i, j, ii, jj;
    int **tharr = (int **)malloc(arrnum * sizeof(int *));
    int *arr;
    int *p;
    for (i = 0, ii = i; i < numsSize - 2; i++) {
        if ((i > ii) && (seqs[i] == seqs[ii])) {
            continue;
        }
        ii = i;
        for (j = i + 1, jj = j; j < numsSize - 1; j++) {
            // printf("[%d]: %d, [%d]: %d\n", i, seqs[i], j,seqs[j]);
            if ((j > jj) && (seqs[j] == seqs[jj])) {
                continue;
            }
            jj = j;
            val = 0 - (seqs[i] + seqs[j]);
            p = (int *)bsearch(&val, seqs + j + 1, numsSize - j - 1,
                               sizeof(int), int_cmp);
            if (p == NULL) {
                continue;
            }
            // printf("thnum: %d, i: %d, j: %d, [%d, %d, %d]\n", thnum, i, j,
            // seqs[i], seqs[j], *p);
            if (thnum >= arrnum) {
                arrnum *= 2;
                tharr = realloc(tharr, arrnum * sizeof(int *));
            }
            arr = (int *)malloc(3 * sizeof(int));
            arr[0] = seqs[i];
            arr[1] = seqs[j];
            arr[2] = *p;
            tharr[thnum++] = arr;
        }
    }

    // printf("thnum: %d, idx: %d, [%d, %d, %d]\n", thnum, i, tharr[i][0],
    // tharr[i][1], tharr[i][2]);
    free(seqs);

    if (returnSize != NULL) {
        *returnSize = thnum;
    }

    return tharr;
}

int **threeSum1(int *nums, int numsSize, int *returnSize) {
    int msize = numsSize * sizeof(int);
    int *seqs = (int *)malloc(msize);
    memcpy(seqs, nums, msize);
    qsort(seqs, numsSize, sizeof(int), int_cmp);
    // ARRPRT(seqs, numsSize);

    node_t *head = NULL, *tail = NULL, *node;
    int thnum = 0, val = 0, i, j, ii, jj;
    int *p;
    for (i = 0, ii = i; i < numsSize - 2; i++) {
        if ((i > ii) && (seqs[i] == seqs[ii])) {
            continue;
        }
        ii = i;
        for (j = i + 1, jj = j; j < numsSize - 1; j++) {
            // printf("[%d]: %d, [%d]: %d\n", i, seqs[i], j,seqs[j]);
            if ((j > jj) && (seqs[j] == seqs[jj])) {
                continue;
            }
            jj = j;
            val = 0 - (seqs[i] + seqs[j]);
            p = (int *)bsearch(&val, seqs + j + 1, numsSize - j - 1,
                               sizeof(int), int_cmp);
            if (p == NULL) {
                continue;
            }
            // printf("thnum: %d, i: %d, j: %d, [%d, %d, %d]\n", thnum, i, j,
            // seqs[i], seqs[j], *p);
            node = (node_t *)malloc(sizeof(node_t));
            node->array = (int *)malloc(3 * sizeof(int));
            node->array[0] = seqs[i];
            node->array[1] = seqs[j];
            node->array[2] = *p;
            node->next = NULL;
            if (head == NULL) {
                head = node;
                tail = node;
            } else {
                tail->next = node;
                tail = node;
            }
            thnum++;
        }
    }

    int **tharr = (int **)malloc(thnum * sizeof(int *));
    for (i = 0, node = head; (i < thnum) && (node != NULL); i++) {
        tharr[i] = node->array;
        tail = node;
        node = node->next;
        free(tail);
        // printf("thnum: %d, idx: %d, [%d, %d, %d]\n", thnum, i, tharr[i][0],
        // tharr[i][1], tharr[i][2]);
    }
    free(seqs);

    if (returnSize != NULL) {
        *returnSize = thnum;
    }

    return tharr;
}

/**
 * Return an array of arrays of size *returnSize.
 * Note: The returned array must be malloced, assume caller calls free().
 */
int **threeSum(int *nums, int numsSize, int *returnSize) {
    return threeSum1(nums, numsSize, returnSize);
}

typedef struct {
    int array[1024];
    int num;
    int tharr[1024][3];
    int thnum;
} three_sum_test_t;

static void three_sum_test(int argc, char **argv) {
    three_sum_test_t cases[] = {
        {{-1, 0, 1, 2, -1, -4}, 6, {{-1, 0, 1}, {-1, -1, 2}}, 2},
        {{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, 10, {}, 0},
        {{0, 0, 0, 0, 0}, 5, {{0, 0, 0}}, 1},
        {{1, 1, 1, 0, 0, 0}, 6, {{0, 0, 0}}, 1},
        {{-1, -1, -1, 0, 0, 0, 1, 1, 1}, 9, {{0, 0, 0}, {-1, 0, 1}}, 2},
        {{3, 0, -2, -1, 1, 2}, 6, {{-2, -1, 3}, {-2, 0, 2}, {-1, 0, 1}}, 3},
        {{4, 3, 2, 1, 0, -1, -2, -3, -4},
         9,
         {{-4, 0, 4},
          {-4, 1, 3},
          {-3, -1, 4},
          {-3, 0, 3},
          {-3, 1, 2},
          {-2, -1, 3},
          {-2, 0, 2},
          {-1, 0, 1}},
         8},
        {{1, 4, 3, 2, 1, 0, 0, -1, -2, -3, -4, -1},
         12,
         {{-4, 0, 4},
          {-4, 1, 3},
          {-3, -1, 4},
          {-3, 0, 3},
          {-3, 1, 2},
          {-2, -1, 3},
          {-2, 0, 2},
          {-2, 1, 1},
          {-1, -1, 2},
          {-1, 0, 1}},
         10},
    };
    for (int t = 0; t < 1000000; t++) {
        for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
            int *array = cases[k].array;
            int num = cases[k].num;
            // int **tharr = cases[k].tharr;
            int thnum = cases[k].thnum;
            int tn = 0;
            int **ta = threeSum(array, num, &tn);
            if ((ta == NULL) || (tn != thnum)) {
                printf("test %d fail, %d != %d(expected), num: %d.\n", k, tn,
                       thnum, num);
                return;
            }
        }
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    three_sum_test(argc, argv);
    exit(EXIT_SUCCESS);
}
