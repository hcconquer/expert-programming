/**
 * power_of_two_231.c
 *
 * Created: 2016/05/12
 *  Author: hanchen
 */

/**
 * Given an integer, write a function to determine if it is a power of two.
 */

#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

bool isPowerOfTwoByDivide(int n) {
    if (n <= 0) {
        return false;
    }
    unsigned m = 1u << (sizeof(int) * 8 - 1);
    unsigned r = m % n;
    return (r == 0) ? true : false;
}

/*
 * pow(2, n) - 1 must be like 0b0011111
 */
// #define BUILD_BUG_ON_NOT_POWER_OF_2(n) BUILD_BUG_ON((n) == 0 || (((n) & ((n)
// - 1)) != 0))
bool isPowerOfTwoByMod(int n) {
    if (n <= 0) {
        return false;
    }
    int r = (n - 1) & n;
    return (r == 0) ? true : false;
}

bool isPowerOfTwo(int n) {
    return isPowerOfTwoByMod(n);
}

typedef struct {
    int n;
    bool r;
} power_of_two_test_t;

static void is_power_of_two_test(int argc, char **argv) {
    power_of_two_test_t cases[] = {
        {0, false},  {1, true},   {2, true},    {3, false},  {4, true},
        {81, false}, {99, false}, {100, false}, {128, true}, {1 << 31, false},
    };
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        int n = cases[k].n;
        bool r = cases[k].r;
        bool t = isPowerOfTwo(n);
        if (r != t) {
            printf("test %d fail, %d != %d(expected), num: %d.\n", k, t, r, n);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    is_power_of_two_test(argc, argv);
    exit(EXIT_SUCCESS);
}
