/**
 * add_digits_258.c
 *
 * Created: 2017/04/07
 *  Author: hanchen
 */

/**
 * Given a non-negative integer num, repeatedly add all its digits until
 * the result has only one digit.
 *
 * For example:
 * Given num = 38, the process is like: 3 + 8 = 11, 1 + 1 = 2. Since 2 has only
 * one digit, return it.
 *
 * Follow up:
 * Could you do it without any loop/recursion in O(1) runtime?
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int addDigitsByRecursion(int num) {
    if (num < 10) {
        return num;
    }
    int sum = 0;
    while (num > 0) {
        sum += num % 10;
        num /= 10;
    }
    return addDigitsByRecursion(sum);
}

int addDigitsByLaw(int num) {
    if (num == 0) {
        return 0;
    }
    num = num % 9;
    return (num > 0) ? num : 9;
}

int addDigits(int num) {
    return addDigitsByLaw(num);
}

typedef struct {
    int num;
    int sum;
} add_digits_test_t;

static void add_digits_test(int argc, char **argv) {
    add_digits_test_t cases[] = {
        {0, 0},  {10, 1}, {11, 2}, {12, 3}, {36, 9},  {37, 1},
        {38, 2}, {39, 3}, {98, 8}, {99, 9}, {100, 1}, {101, 2},
    };
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        int num = cases[k].num;
        int sum = cases[k].sum;
        int s = addDigits(num);
        if (s != sum) {
            printf("test %d fail, %d != %d(expected), num: %d.\n", k, s, sum,
                   num);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    add_digits_test(argc, argv);
    exit(EXIT_SUCCESS);
}
