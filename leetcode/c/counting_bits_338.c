/**
 * counting_bits_338.c
 *
 * Created: 2017/04/07
 *  Author: hanchen
 */

/**
 * Given a non negative integer number num. For every numbers i in the
 * range 0 ≤ i ≤ num calculate the number of 1's in their binary
 * representation and return them as an array.
 *
 * Example:
 * For num = 5 you should return [0,1,1,2,1,2].
 *
 * Follow up:
 * It is very easy to come up with a solution with run time
 * O(n*sizeof(integer)). But can you do it in linear time O(n) /possibly in a
 * single pass? Space complexity should be O(n). Can you do it like a boss? Do
 * it without using any builtin function like
 * __builtin_popcount in c++ or in any other language.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ARRPRT(array, len)             \
    ({                                 \
        typeof(len) _l = len;          \
        typeof(len) _i = 0;            \
        for (_i = 0; _i < _l; _i++) {  \
            printf("%d, ", array[_i]); \
        }                              \
        printf("\n");                  \
    })

/**
 * Return an array of size *returnSize.
 * Note: The returned array must be malloced, assume caller calls free().
 */
int *countBits(int num, int *returnSize) {
    const static int BIT_MAP[] = {
        [0] = 0,  [1] = 1,  [2] = 1,  [3] = 2,  [4] = 1,  [5] = 2,
        [6] = 2,  [7] = 3,  [8] = 1,  [9] = 2,  [10] = 2, [11] = 3,
        [12] = 2, [13] = 3, [14] = 3, [15] = 4,
    };
    // const static int BIT_MAP_POW = 4;
    const static int BIT_MAP_LEN = sizeof(BIT_MAP) / sizeof(BIT_MAP[0]);

    int *array = (int *)malloc((num + 1) * sizeof(int));
    int n = (num < BIT_MAP_LEN) ? (num + 1) : BIT_MAP_LEN;
    memcpy(array, BIT_MAP, n * sizeof(int));

    int pow = BIT_MAP_LEN;
    for (; n <= num;) {
        for (int i = 0; (i < pow) && (n <= num); i++) {
            array[n] = array[n - pow] + 1;
            // printf("n: %d, pow: %d, val: %d\n", n, pow, array[n]);
            n++;
            // ARRPRT(array, n);
        }
        pow = pow << 1;
    }

    if (returnSize != NULL) {
        *returnSize = n;
    }

    // ARRPRT(array, n);

    return array;
}

typedef struct {
    int num;
    int array[1024];
} count_bits_test_t;

static void count_bits_test(int argc, char **argv) {
    count_bits_test_t cases[] = {
        {0, {0}},
        {1, {0, 1}},
        {5, {0, 1, 1, 2, 1, 2}},
        {15, {0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4}},
        {16, {0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4, 1}},
        {32, {0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4, 1,
              2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 1}},
        {130, {0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4, 1, 2, 2, 3, 2, 3,
               3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4,
               3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 1, 2,
               2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5,
               3, 4, 4, 5, 4, 5, 5, 6, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5,
               5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 1, 2, 2}}};
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        int num = cases[k].num;
        int *array = cases[k].array;
        int n = -1;
        int *arr = countBits(num, &n);
        if ((arr == NULL) || (n != num + 1)) {
            printf("test %d fail, %d != %d(expected).\n", k, n, num);
            return;
        }
        for (int i = 0; i < n; i++) {
            if (arr[i] != array[i]) {
                printf(
                    "test %d fail, %d != %d(expected), index: %d, num: %d.\n",
                    k, arr[i], array[i], i, num);
                return;
            }
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    count_bits_test(argc, argv);
    exit(EXIT_SUCCESS);
}
