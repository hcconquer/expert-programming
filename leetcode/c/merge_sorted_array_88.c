/**
 * merge_sorted_array_88.c
 *
 * Created: 2016/05/12
 *  Author: hanchen
 */

/**
 * Given two sorted integer arrays nums1 and nums2, merge nums2 into nums1 as
 * one sorted array. Note: You may assume that nums1 has enough space (size that
 * is greater or equal to m + n) to hold additional elements from nums2. The
 * number of elements initialized in nums1 and nums2 are m and n respectively.
 */

#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void merge(int *nums1, int m, int *nums2, int n) {
    int len, i, j, k;
    len = m + n;
    i = m - 1;
    j = n - 1;
    k = len - 1;
    while (k >= 0) {
        if (nums1[i] >= nums2[j]) {
            nums1[k--] = nums1[i--];
        } else {
            nums1[k--] = nums2[j--];
        }
    }
}

#define ARRAY_LEN_MAX 256

typedef struct {
    int ma[ARRAY_LEN_MAX];
    int mn;
    int na[ARRAY_LEN_MAX];
    int nn;
    int ra[ARRAY_LEN_MAX];
} merge_sorted_array_test_t;

static void merge_sorted_array_test(int argc, char **argv) {
    merge_sorted_array_test_t cases[] = {
        {{1}, 1, {0}, 1, {0, 1}},
    };
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        // memcpy(cases[k].ra, cases[k].ma, sizeof(cases[k].ma));
        merge(cases[k].ma, cases[k].mn, cases[k].na, cases[k].nn);
        int rn = cases[k].mn + cases[k].nn;
        if (memcmp(cases[k].ma, cases[k].na, rn * sizeof(int))) {
            printf("test %d fail.\n", k);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    merge_sorted_array_test(argc, argv);
    exit(EXIT_SUCCESS);
}
