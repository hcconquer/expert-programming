/**
 * integer_to_roman_12.c
 *
 * Created: 2017/03/29
 *  Author: hanchen
 */

/**
 * Given an integer, convert it to a roman numeral.
 * Input is guaranteed to be within the range from 1 to 3999.
 */

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    int roman;
    int num;
    int times;
} roman_num_t;

char *intToRoman(int num) {
    static const roman_num_t ROMAN_MAP[] = {
        /*['I'] = */ {'I', 1, 3},
        /*['V'] = */ {'V', 5, 1},
        /*['X'] = */ {'X', 10, 3},
        /*['L'] = */ {'L', 50, 1},
        /*['C'] = */ {'C', 100, 3},
        /*['D'] = */ {'D', 500, 1},
        /*['M'] = */ {'M', 1000, 3},
    };
    // static const int ROMAN_TIMES_MAX = 3;
    char str[17] = {'\0'};
    int len = 0;
    int rms = sizeof(ROMAN_MAP) / sizeof(ROMAN_MAP[0]) - 1;
    while (num > 0) {
        if (num >= ROMAN_MAP[rms].num) {
            // printf("num: %d, rms: %d, times: %d\n", num, ROMAN_MAP[rms].num,
            // ROMAN_MAP[rms].times);
            if (ROMAN_MAP[rms].times > 1) {
                if ((num / ROMAN_MAP[rms].num) > ROMAN_MAP[rms].times) {
                    str[len++] = ROMAN_MAP[rms].roman;
                    str[len++] = ROMAN_MAP[rms + 1].roman;
                    num -= ROMAN_MAP[rms + 1].num - ROMAN_MAP[rms].num;
                    // printf("str: %s, num: %d\n", str, num);
                    continue;
                }
            } else {
                if (num >= ROMAN_MAP[rms + 1].num - ROMAN_MAP[rms - 1].num) {
                    str[len++] = ROMAN_MAP[rms - 1].roman;
                    str[len++] = ROMAN_MAP[rms + 1].roman;
                    num -= ROMAN_MAP[rms + 1].num - ROMAN_MAP[rms - 1].num;
                    continue;
                }
            }
            str[len++] = ROMAN_MAP[rms].roman;
            num -= ROMAN_MAP[rms].num;
        } else {
            rms--;
        }
    }

    char *rs = (char *)malloc(len + 1);
    strncpy(rs, str, len);
    rs[len] = '\0';
    return rs;
}

typedef struct {
    char string[1024];
    int num;
} int_to_roman_test_t;

static void int_to_roman_test(int argc, char **argv) {
    int_to_roman_test_t cases[] = {
        {"I", 1},
        {"II", 2},
        {"III", 3},
        {"IV", 4},
        {"V", 5},
        {"VI", 6},
        {"VII", 7},
        {"VIII", 8},
        {"IX", 9},
        {"X", 10},
        {"XI", 11},
        {"XII", 12},
        {"XIII", 13},
        {"XIV", 14},
        {"XL", 40},
        {"XLV", 45},
        {"XCIX", 99},
        {"CI", 101},
        {"CXIII", 113},
        {"CCLXXVIII", 278},
        {"CCCXXVII", 327},
        {"CCCLXXXVIII", 388},
        {"CDLXXXVIII", 488},
        {"CDXCVIII", 498},
        {"CDXCIX", 499},
        {"D", 500},
        {"DCXXI", 621},
        {"DCCC", 800},
        {"DCCCXCIX", 899},
        {"CMXCIX", 999},
        {"M", 1000},
        {"MDCCCLXXXVIII", 1888},
        {"MDCCCXCIX", 1899},
        {"MCMLXXVI", 1976},
        {"MCMLXXXIV", 1984},
        {"MCMXCVI", 1996},
        {"MCMXCIX", 1999},
        {"MMM", 3000},
        {"MMMCMXCIX", 3999},
    };
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        char *string = cases[k].string;
        int num = cases[k].num;
        char *s = intToRoman(num);
        if (strcmp(s, string)) {
            printf("test %d fail, %s != %s(expected), num: %d.\n", k, s, string,
                   num);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    int_to_roman_test(argc, argv);
    exit(EXIT_SUCCESS);
}
