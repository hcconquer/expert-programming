/**
 * longest_palindrome_409.c
 *
 * Created: 2017/04/07
 *  Author: hanchen
 */

/**
 * Given a string which consists of lowercase or uppercase letters, find the
 * length of the longest palindromes that can be built with those letters.
 *
 * This is case sensitive, for example "Aa" is not considered a palindrome here.
 *
 * Note:
 * Assume the length of given string will not exceed 1,010.
 *
 * Example:
 * Input:
 * "abccccdd"
 * Output:
 * 7
 *
 * Explanation:
 * One longest palindrome that can be built is "dccaccd", whose length is 7.
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ARRPRT(array, len)            \
    ({                                \
        typeof(len) _l = len;         \
        typeof(len) _i = 0;           \
        for (_i = 0; _i < _l; _i++) { \
            printf("%d ", array[_i]); \
        }                             \
        printf("\n");                 \
    })

int longestPalindrome(char *s) {
    int len = strlen(s);

    static const int MAP_LEN = 256;
    int map[MAP_LEN];
    memset(map, 0, sizeof(map));

    int pn = 0, ec = 0, n, i;

    for (i = 0; i < len; i++) {
        if (isalpha(s[i])) {
            map[(int)s[i]]++;
        }
    }
    // ARRPRT(map, (int) MAP_LEN);

    for (i = 0; i < MAP_LEN; i++) {
        n = map[i];
        if ((n % 2) == 0) {
            pn += n;
        } else {
            pn += n - 1;
            ec++;
        }
    }

    if (ec > 0) {
        pn++;
    }

    return pn;
}

typedef struct {
    char string[1024];
    int length;
} longest_palindromic_substring_test_t;

static void longest_palindromic_substring_test(int argc, char **argv) {
    longest_palindromic_substring_test_t cases[] = {
        {"", 0},
        {"a", 1},
        {"AAAaa", 5},
        {"AAABBB", 5},
        {"AAABBBab", 5},
        {"abccccdd", 7},
        {"longestPalindrome", 9},
        {"longest_palindromic_substring_test", 23},
    };
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        char *string = cases[k].string;
        int length = cases[k].length;
        int len = longestPalindrome(string);
        if (len != length) {
            printf("test %d fail, %d != %d(expected), string: %s.\n", k, len,
                   length, string);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    longest_palindromic_substring_test(argc, argv);
    exit(EXIT_SUCCESS);
}
