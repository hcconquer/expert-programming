/**
 * longest_palindromic_substring_5.c
 *
 * Created: 2017/03/22
 *  Author: hanchen
 */

/**
 * Given a string s, find the longest palindromic substring in s.
 * You may assume that the maximum length of s is 1000.
 *
 * Example:
 * Input: "babad"
 * Output: "bab"
 * Note: "aba" is also a valid answer.
 *
 * Example:
 * Input: "cbbd"
 * Output: "bb"
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MXP(mx, mn)                                      \
    do {                                                 \
        for (int _i = 0; _i < (mn); _i++) {              \
            for (int _j = 0; _j < (mn); _j++) {          \
                printf("%3d ", MXV(mx, mn, _i, _j + 1)); \
                if ((_j) >= 10) {                        \
                    break;                               \
                }                                        \
            }                                            \
            printf("\n");                                \
            if ((_i) >= 10) {                            \
                break;                                   \
            }                                            \
        }                                                \
    } while (0)

#define MXS(mn) ((mn) * (mn))

#define MXV(mx, mn, b, n) mx[(b) * (mn) + (n)-1]

void initmx(char *s, int *mx, int mn) {
    int smc = -1;
    for (int b = mn - 1; b >= 0; b--) {  // mn
        /* from long to short, return when found palindrome */
        for (int n = mn - b; n > 0; n--) {  // mn
            smc = -1;
            if (n == 1) {  // only 1 must be palindrome
                smc = 0;
            } else if (n == 2) {
                smc = (s[b] == s[b + 1]) ? 0 : 1;
            } else {
                if (s[b] == s[b + n - 1]) {
                    if (MXV(mx, mn, b + 1, n - 2) == 0) {
                        smc = 0;
                    }
                }
            }
            MXV(mx, mn, b, n) = smc;
        }
    }
}

char *longestPalindromeByMatrix(char *s) {
    int num = strlen(s);
    int mnum = MXS(num);
    int *mx = (int *)malloc(mnum * sizeof(int));
    for (int i = 0; i < mnum; i++) {
        mx[i] = -1;
    }
    initmx(s, mx, num);

    int b, mlen, mp = -1;
    for (mlen = num; mlen > 0; mlen--) {
        for (b = 0; b <= num - mlen; b++) {
            mp = MXV(mx, num, b, mlen);
            if (mp == 0) {
                break;
            }
        }
        if (mp == 0) {
            break;
        }
    }
    free(mx);

    char *p = (char *)malloc((mlen + 1) * sizeof(char));
    if (p == NULL) {
        return NULL;
    }
    strncpy(p, s + b, mlen);
    p[mlen] = '\0';

    return p;
}

int is_palindrome(char *s, int len) {
    int mid = len / 2;
    for (int b = 0; b < mid; b++) {
        if (s[b] != s[len - b - 1]) {
            return 0;
        }
    }
    return 1;
}

char *longestPalindromeByLoop(char *s) {
    char *p = NULL;

    int len = strlen(s);
    if (len <= 0) {
        p = (char *)malloc(1 * sizeof(char));
        p[0] = '\0';
        return p;
    }

    int *px = (int *)malloc(len * sizeof(int));
    if (px == NULL) {
        return NULL;
    }
    memset(px, 0, len * sizeof(int));

    int b, e;
    px[len - 1] = 1;
    for (b = len - 2; b >= 0; b--) {  // n
        e = b + px[b + 1] + 1;
        if (e < len) {
            if (s[b] == s[e]) {
                px[b] = px[b + 1] + 2;
            } else {
                e = e - 1;
            }
        } else {
            e = len - 1;
        }

        if (px[b] > 0) {  // all is palindrome
            continue;
        }

        for (; b <= e; e--) {
            if (s[b] == s[e]) {
                if (is_palindrome(s + b, e - b + 1)) {
                    px[b] = e - b + 1;
                    break;
                }
            }
        }
    }

    int mlen = 0;
    for (e = 0; e < len; e++) {
        // printf("%d ", px[e]);
        if (px[e] > mlen) {
            b = e;
            mlen = px[e];
        }
    }

    free(px);

    p = (char *)malloc((mlen + 1) * sizeof(char));
    if (p == NULL) {
        return NULL;
    }
    strncpy(p, s + b, mlen);
    p[mlen] = '\0';
    // printf("%s, %s, len: %d\n", p, s + b, mlen);

    return p;
}

/**
 * I don't know why by loop is quick then by matrix.
 */
char *longestPalindrome(char *s) {
    return longestPalindromeByLoop(s);
    //    return longestPalindromeByMatrix(s);
}

typedef struct {
    char string[1024];
    char substr[1024];
} longest_palindromic_substring_test_t;

static void longest_palindromic_substring_test(int argc, char **argv) {
    longest_palindromic_substring_test_t cases[] = {
        {"babad", "bab"},
        {"cbbd", "bb"},
        {"", ""},
        {"a", "a"},
        {"aa", "aa"},
        {"ab", "a"},
        {"abc", "a"},
        {"abbbb", "bbbb"},
        {"aaabccc", "aaa"},
        {"abbbcbbba", "abbbcbbba"},
        {"abbba", "abbba"},
        {"aabbaa", "aabbaa"},
        {"abacddcab", "bacddcab"},
        {"ciduhhuhbj", "uhhu"},
        {"jtofmboiyyrjzbonysurqfxylvhuzzrzqwcjxibhawifptuammlxstcjmcmfvjuphyyff"
         "lkcbwimmpehqrqcdqxglqciduhhuhbjnwaaywofljhwzuqsnhyhahtkilwggineoosnqh"
         "dluahhkkbcwbupjcuvzlbzocgmkkyhhglqsvrxsgcglfisbzbawitbjwycareuhyxnbvo"
         "unqdqdaixgqtljpxpyrccagrkdxsdtvgdjlifknczaacdwxropuxelvmcffiollbuekcf"
         "kxzdzuobkrgjedueyospuiuwyppgiwhemyhdjhadcabhgtkotqyneioqzbxviebbvqavt"
         "vwgyyrjhnlceyedhfechrbhugotqxkndwxukwtnfiqmstaadlsebfopixrkbvetaoycic"
         "sdndmztyqnaehnozchrakt",
         "uhhu"},
        {"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaabbaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaabbaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"}};
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        char *string = cases[k].string;
        char *substr = cases[k].substr;
        char *s = longestPalindrome(string);
        int ret = strcmp(s, substr);
        if (ret != 0) {
            printf("test %d fail, %s != %s(expected), string: %s.\n", k, s,
                   substr, string);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    longest_palindromic_substring_test(argc, argv);
    exit(EXIT_SUCCESS);
}
