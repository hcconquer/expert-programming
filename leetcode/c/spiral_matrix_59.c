/**
 * spiral_matrix_59.c
 *
 * Created: 2016/04/30
 *  Author: hanchen
 */

/**
 * Given an integer n, generate a square matrix filled with elements from 1 to
 n2 in spiral order.

 * For example,
 * Given n = 3,

 * You should return the following matrix:
 * [
 *   [ 1, 2, 3 ],
 *   [ 8, 9, 4 ],
 *   [ 7, 6, 5 ]
 * ]
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MXP(mx, len)                        \
    ({                                      \
        typeof(len) _l = len;               \
        typeof(len) _i = 0, _j = 0;         \
        for (_i = 0; _i < _l; _i++) {       \
            for (_j = 0; _j < _l; _j++) {   \
                printf("%3d ", mx[_i][_j]); \
            }                               \
            printf("\n");                   \
        }                                   \
    })

/**
 * Return an array of arrays.
 * Note: The returned array must be malloced, assume caller calls free().
 */
int **generateMatrix(int n) {
    int **mx = (int **)malloc(n * sizeof(int *));
    for (int i = 0; i < n; i++) {
        mx[i] = (int *)malloc(n * sizeof(int));
    }
    int i = 0, j = 0, p = 1, s = 1;
    for (p = 0; p < (n + 1) / 2; p++) {
        i = p, j = p;
        while (j < n - p) {
            mx[i][j++] = s++;
        }
        i++, j--;
        while (i < n - p) {
            mx[i++][j] = s++;
        }
        i--, j--;
        while (j >= p) {
            mx[i][j--] = s++;
        }
        i--, j++;
        while (i > p) {
            mx[i--][j] = s++;
        }
    }
    return mx;
}

typedef struct {
    int mx[256][256];
    int num;
} spiral_matrix_test_t;

static void spiral_matrix_test(int argc, char **argv) {
    spiral_matrix_test_t cases[] = {
        {{{1}}, 1},
        {{{1, 2}, {4, 3}}, 2},
        {{{1, 2, 3}, {8, 9, 4}, {7, 6, 5}}, 3},
        {{{1, 2, 3, 4}, {12, 13, 14, 5}, {11, 16, 15, 6}, {10, 9, 8, 7}}, 4}};
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        int(*mx)[256] = cases[k].mx;  // different with int **mx
        int num = cases[k].num;
        int **gmx = generateMatrix(num);
        for (int i = 0; i < num; i++) {
            for (int j = 0; j < num; j++) {
                if (mx[i][j] != gmx[i][j]) {
                    printf("test fail, %d != %d, i: %d, j: %d.\n", gmx[i][j],
                           mx[i][j], i, j);
                    return;
                }
            }
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    spiral_matrix_test(argc, argv);
    exit(EXIT_SUCCESS);
}
