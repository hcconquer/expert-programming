/**
 * single_number_ii_137.c
 *
 * Created: 2017/04/12
 *  Author: hanchen
 */

/**
 * Given an array of integers, every element appears three times except for
 * one, which appears exactly once. Find that single one.
 * Note:
 * Your algorithm should have a linear runtime complexity. Could you
 * implement it without using extra memory?
 */

#include <stdio.h>
#include <stdlib.h>

int singleNumber(int *nums, int numsSize) {
    static const int INT_BIT_LEN = sizeof(int) * 8;
    int spec = 0;
    int mask = 0x01, cnt;
    for (int i = 0; i < INT_BIT_LEN; i++) {
        mask = 0x01 << i;
        cnt = 0;
        for (int j = 0; j < numsSize; j++) {
            if (nums[j] & mask) {
                cnt = (cnt + 1) % 3;
            }
        }
        if (cnt == 1) {
            spec |= mask;
        }
    }
    return spec;
}

typedef struct {
    int array[1024];
    int num;
    int spec;
} single_mumber_test_t;

static void single_mumber_test(int argc, char **argv) {
    single_mumber_test_t cases[] = {
        {{1}, 1, 1},
        {{1, 2, 2, 2}, 4, 1},
        {{1, 2, 1, 2, 3, 1, 2}, 7, 3},
    };
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        int *array = cases[k].array;
        int num = cases[k].num;
        int spec = cases[k].spec;
        int s = singleNumber(array, num);
        if (s != spec) {
            printf("test %d fail, %d != %d(expected), num: %d.\n", k, s, spec,
                   num);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    single_mumber_test(argc, argv);
    exit(EXIT_SUCCESS);
}
