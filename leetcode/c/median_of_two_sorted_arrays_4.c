/**
 * median_of_two_sorted_arrays_4.c
 *
 * Created: 2017/03/22
 *  Author: hanchen
 */

/**
 * There are two sorted arrays nums1 and nums2 of size m and n respectively.
 * Find the median of the two sorted arrays. The overall run time complexity
 * should be O(log (m+n)).
 *
 * Example 1:
 * nums1 = [1, 3]
 * nums2 = [2]
 * The median is 2.0
 *
 * Example 2:
 * nums1 = [1, 2]
 * nums2 = [3, 4]
 * The median is (2 + 3)/2 = 2.5
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

double findMedianSortedArrays(int* nums1, int nums1Size, int* nums2,
                              int nums2Size) {
    return 0;
}

typedef struct {
    int seq1[65536];
    int len1;
    int seq2[65536];
    int len2;
    double median;
} median_of_two_sorted_arrays_test_t;

static void median_of_two_sorted_arrays_test(int argc, char** argv) {
    median_of_two_sorted_arrays_test_t cases[] = {{{1, 3}, 2, {2}, 1, 2.0},
                                                  {{1, 2}, 2, {3, 4}, 2, 2.5}};
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        double median = cases[k].median;
        double m = findMedianSortedArrays(cases[k].seq1, cases[k].len1,
                                          cases[k].seq2, cases[k].len2);
        if (m != median) {
            printf("test %d fail, %f != %f(expected).\n", k, m, median);
            // list_print(s);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char* argv[]) {
    median_of_two_sorted_arrays_test(argc, argv);
    exit(EXIT_SUCCESS);
}
