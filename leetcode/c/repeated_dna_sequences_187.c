/**
 * repeated_dna_sequences_187.c
 *
 * Created: 2016年4月27日
 *  Author: hanchen
 */

/**
 * All DNA is composed of a series of nucleotides abbreviated as A, C, G, and T,
 for example: "ACGAATTCCG". When studying DNA, it is sometimes useful to
 identify repeated sequences within the DNA.

 * Write a function to find all the 10-letter-long sequences (substrings) that
 occur more than once in a DNA molecule.

 * For example,

 * Given s = "AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT",

 * Return:
 * ["AAAAACCCCC", "CCCCCAAAAA"].
 */

#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const int SN = 10;

typedef struct {
    char* p;
    size_t sum;
    int cnt;
} str_desc_t;

size_t dsum(char* s, int len, int pw) {
    size_t sum = 0;
    for (int i = len - 1; i >= 0; i--) {
        sum = (sum * pw) + s[i];
    }
    return sum;
}

size_t csum(size_t sum, char oc, char nc, int sn, int pw) {
    size_t s1 = (sum - oc) / pw;
    size_t s2 = nc * (pow(pw, (sn - 1)));
    return s1 + s2;
}

int sdvcmp(const str_desc_t* sd1, const str_desc_t* sd2) {
    if (sd1->sum < sd2->sum) {
        return -1;
    } else if (sd1->sum > sd2->sum) {
        return 1;
    }
    return memcmp(sd1->p, sd2->p, SN);
}

int sdpcmp(const str_desc_t* sd1, const str_desc_t* sd2) {
    if (sd1->p < sd2->p) {
        return -1;
    } else if (sd1->p > sd2->p) {
        return 1;
    }
    return 0;
}

/**
 * Return an array of size *returnSize.
 * Note: The returned array must be malloced, assume caller calls free().
 */
char** findRepeatedDnaSequences(char* s, int* returnSize) {
    char** ss = NULL;

    int len = -1;
    len = strlen(s);
    if (len < 0) {
        *returnSize = -1;
        return ss;
    }

    int num = len - (SN - 1);
    if (num <= 0) {
        *returnSize = 0;
        return ss;
    }

    str_desc_t* sd = (str_desc_t*)malloc(num * sizeof(*sd));
    for (int i = 0; i < num; i++) {
        sd[i].p = s + i;
        if (i == 0) {
            sd[i].sum = dsum(s, SN, 2);
        } else {
            sd[i].sum = csum(sd[i - 1].sum, s[i - 1], s[i + SN - 1], SN, 2);
        }
    }

    qsort(sd, num, sizeof(*sd), (int (*)(const void*, const void*))sdvcmp);

    int cnt = 0;
    for (int i = 0; i < num; i++) {
        if (i == 0) {
            sd[i].cnt = 1;
        } else {
            if (sdvcmp(sd + i, sd + i - 1) == 0) {
                sd[i].cnt = sd[i - 1].cnt + 1;
            } else {
                sd[i].cnt = 1;
            }
        }
        if (sd[i].cnt == 2) {
            cnt++;
        }
    }

    qsort(sd, num, sizeof(*sd), (int (*)(const void*, const void*))sdpcmp);

    ss = (char**)malloc(cnt * sizeof(char*));
    char* rs = NULL;
    for (int i = 0, k = 0; i < num; i++) {
        if (sd[i].cnt == 2) {
            rs = malloc(SN + 1);
            strncpy(rs, sd[i].p, SN);
            rs[SN] = '\0';
            ss[k] = rs;
            k++;
        }
    }

    free(sd);

    *returnSize = cnt;

    return ss;
}

typedef struct {
    char s[4096];
    char ss[8][4096];
    int sn;
} repeated_dna_sequences_test_t;

void repeated_dna_sequences_test(int argc, char** argv) {
    repeated_dna_sequences_test_t cases[] = {
        {"", {""}, 0},
        {"AAAAAAAAAAA",
         {
             "AAAAAAAAAA",
         },
         1},
        {"AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT", {"AAAAACCCCC", "CCCCCAAAAA"}, 2},
        {"ADABDCAEBDCEBDCACAAAADBBCADABCBEABAADCBCAADDEBDBDDCBDACDBBAEDBDAAECAB"
         "DCEDDCCBDEEDDCCDAABBABBDEDAAABCDADBDABEACBEADBADDCBAACDBABCCCBACEEDBC"
         "CCEDBEECBCCAECADCCBDBDCCBCBAACCCBDDCCCBAEDBACDBCACCDCAADCBAEBEBCCEABB"
         "DCDEAABDBABADEAAAAEDBDBCEBCBDDEBCCACACDDEBECABCCBBDCBECBAEEDCDACDCBDB"
         "EBBACDDDDAABAEDABBAAABADDCDAADCCCDEEBCABACDADBAACDCCBECEDDEEBBBDBAAAA"
         "ABAEECCCAEBDEABDDACBEDEDEDEBDEBABDBCBDCBADBEECEECDCDBBDCBDBEEEBCDCABD"
         "EEACABDEAEDEBBCAACDADAECBCCBEDEDCECEABDCABDEABBCDECDEDADCAEBAABABEEDC"
         "AACDBDACBCCDBCECE",
         {},
         0},
    };
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        char* s = cases[k].s;
        char(*ss)[4096] = cases[k].ss;
        int sn = cases[k].sn;
        int rsn = -1;
        char** rs = findRepeatedDnaSequences(s, &rsn);
        if (rsn != sn) {
            printf("test %d fail, %d != %d(expected).\n", k, rsn, sn);
            return;
        }
        for (int i = 0; i < sn; i++) {
            if (strcmp(ss[i], rs[i]) != 0) {
                printf("test %d fail, %s != %s(expected).\n", k, rs[i], ss[i]);
                return;
            }
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char* argv[]) {
    repeated_dna_sequences_test(argc, argv);
    exit(EXIT_SUCCESS);
}
