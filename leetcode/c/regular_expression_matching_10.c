/**
 * regular_expression_matching_10.c
 *
 * Created: 2017/03/28
 *  Author: hanchen
 */

/**
 * Implement regular expression matching with support for '.' and '*'.

 * '.' Matches any single character.
 * '*' Matches zero or more of the preceding element.

 * The matching should cover the entire input string (not partial).

 * The function prototype should be:
 * bool isMatch(const char *s, const char *p)

 * Some examples:
 * isMatch("aa","a") → false
 * isMatch("aa","aa") → true
 * isMatch("aaa","aa") → false
 * isMatch("aa", "a*") → true
 * isMatch("aa", ".*") → true
 * isMatch("ab", ".*") → true
 * isMatch("aab", "c*a*b") → true
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

bool match(const char *str, int ss, int sn, const char *pat, int ps, int pn) {
    int si = ss + sn - 1, pi = ps + pn - 1;
    while (pi >= 0) {
        if (pat[pi] == '.') {
            if (si >= 0) {
                pi--;
                si--;
            } else {  // "." not match ""
                return false;
            }
        } else if (pat[pi] == '*') {
            if (pi > 0) {
                while (si >= 0) {
                    if ((pat[pi - 1] == '.') || (str[si] == pat[pi - 1])) {
                        // should notice (si + 1) because ".*" match with ""
                        if (match(str, 0, si + 1, pat, 0, pi - 1)) {
                            return true;
                        }
                        si--;
                    } else {
                        break;
                    }
                }
                pi -= 2;
            } else {
                return false;
            }
        } else {
            if (pat[pi] == str[si]) {
                pi--;
                si--;
            } else {
                return false;
            }
        }
    }

    if ((pi < 0) && (si < 0)) {
        return true;
    }

    return false;
}

bool isMatch(char *s, char *p) {
    return match(s, 0, strlen(s), p, 0, strlen(p));
}

typedef struct {
    char string[1024];
    char pattern[1024];
    bool match;
} is_match_test_t;

static void is_match_test(int argc, char **argv) {
    is_match_test_t cases[] = {
        {"aa", "a", false},
        {"aa", "aa", true},
        {"aaa", "aa", false},
        {"aa", "a*", true},
        {"aa", ".*", true},
        {"ab", ".*", true},
        {"abc", "a.*", true},
        {"abc", "b.*", false},
        {"aab", "c*a*b", true},
        {"", "", true},
        {"", ".*", true},
        {"", ".", false},
        {"", "*", false},
        {"aaa", "ab*a*c*a", true},
        {"aaa", "ab*a", false},
        {"aaa", ".*", true},
        {"abc", ".*", true},
        {"aasdf", "aasdf.*", true},
        {"aasdfasdfasdfasdf", "aasdf.*asdf.*asdf.*asdf", true},
        {"aasdfasdfasdfasdfas", "aasdf.*asdf.*asdf.*asdf.*s", true},
        {"ccababcccbbaaabbaa", "ab*.a*.*.*a*a*.*a*", false},
    };
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        char *string = cases[k].string;
        char *pattern = cases[k].pattern;
        bool match = cases[k].match;
        bool m = isMatch(string, pattern);
        if (m != match) {
            printf(
                "test %d fail, %d != %d(expected), string: [%s], pattern: "
                "[%s].\n",
                k, m, match, string, pattern);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    is_match_test(argc, argv);
    exit(EXIT_SUCCESS);
}
