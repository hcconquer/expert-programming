/*
 * 79_word_search.c
 *
 * Created: 2016/04/26
 *  Author: hanchen
 */

/*
 * Given a 2D board and a word, find if the word exists in the grid.
 * The word can be constructed from letters of sequentially adjacent cell,
 * where "adjacent" cells are those horizontally or vertically neighboring.
 * The same letter cell may not be used more than once.
 *
 * For example,
 * Given board =
 * [
 *    ['A','B','C','E'],
 *    ['S','F','C','S'],
 *    ['A','D','E','E']
 * ]
 * word = "ABCCED", -> returns true,
 * word = "SEE", -> returns true,
 * word = "ABCB", -> returns false.
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

bool exist(char** board, int boardRowSize, int boardColSize, char* word) {
    return true;
}

void exist_test(int argc, char** argv) {
}

int main(int argc, char* argv[]) {
    exist_test(argc, argv);
    exit(EXIT_SUCCESS);
}
