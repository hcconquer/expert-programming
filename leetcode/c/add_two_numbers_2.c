/**
 * add_two_numbers_2.c
 *
 * Created: 2017/03/22
 *  Author: hanchen
 */

/**
 * You are given two non-empty linked lists representing two non-negative
 integers.
 * The digits are stored in reverse order and each of their nodes contain a
 single digit.
 * Add the two numbers and return it as a linked list.
 *
 * You may assume the two numbers do not contain any leading zero,
 * except the number 0 itself.

 * Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
 * Output: 7 -> 0 -> 8
 */

#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct ListNode {
    int val;
    struct ListNode *next;
};

static void list_print(struct ListNode *list) {
    while (list != NULL) {
        printf("%d ", list->val);
        list = list->next;
    }
    printf("\n");
}

static int list_cmp(struct ListNode *l1, struct ListNode *l2) {
    while ((l1 != NULL) && (l2 != NULL)) {
        if (l1->val != l2->val) {
            return l1->val < l2->val ? -1 : 1;
        }
        l1 = l1->next;
        l2 = l2->next;
    }
    return (l1 == l2) ? 0 : ((l1 == NULL) ? -1 : 1);
}

static struct ListNode *make_list_by_seq(int *seq, int num) {
    if ((seq == NULL) || (num <= 0)) {
        return NULL;
    }
    struct ListNode *head = NULL, *tail, *node;
    node = (struct ListNode *)malloc(sizeof(struct ListNode));
    node->val = seq[0];
    // node->next = NULL;
    head = node;
    tail = head;
    for (int i = 1; i < num; i++) {
        node = (struct ListNode *)malloc(sizeof(struct ListNode));
        node->val = seq[i];
        // node->next = NULL;
        tail->next = node;
        tail = node;
        // printf("node: %d, val: %d\n", i, node->val);
    }
    tail->next = NULL;
    return head;
}

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *         int val;
 *         struct ListNode *next;
 * };
 */
struct ListNode *addTwoNumbers(struct ListNode *l1, struct ListNode *l2) {
    struct ListNode *head = NULL, *tail = NULL;
    struct ListNode *node, *p1 = l1, *p2 = l2;

    // merge list
    int carry = 0;
    // for (; idx < n2; idx++) {
    while ((p1 != NULL) && (p2 != NULL)) {
        node = (struct ListNode *)malloc(sizeof(struct ListNode));
        node->val = p1->val + p2->val + carry;
        carry = 0;
        if (head == NULL) {
            head = node;
            tail = head;
        }
        if (node->val > 9) {
            carry = 1;
            node->val -= 10;
        }
        tail->next = node;
        tail = node;
        p1 = p1->next;
        p2 = p2->next;
    }

    // copy spare nodes
    if (p1 == NULL) {
        p1 = p2;
    }
    while (p1 != NULL) {
        node = (struct ListNode *)malloc(sizeof(struct ListNode));
        node->val = p1->val + carry;
        carry = 0;
        if (head == NULL) {
            head = node;
            tail = head;
        }
        if (node->val > 9) {
            carry = 1;
            node->val -= 10;
        }
        tail->next = node;
        tail = node;
        p1 = p1->next;
    }
    tail->next = NULL;

    if (carry > 0) {
        node = (struct ListNode *)malloc(sizeof(struct ListNode));
        node->val = 1;
        node->next = NULL;
        tail->next = node;
        tail = node;
    }

    return head;
}

typedef struct {
    int seq1[65536];
    int len1;
    int seq2[65536];
    int len2;
    int sum[65536];
    int len;
} add_two_numbers_test_t;

static void add_two_numbers_test(int argc, char **argv) {
    add_two_numbers_test_t cases[] = {
        {{2, 4, 3}, 3, {5, 6, 4}, 3, {7, 0, 8}, 3},
        {{1, 2, 3, 4}, 4, {9, 8, 7, 6}, 4, {0, 1, 1, 1, 1}, 5},
        {{9, 9, 9}, 3, {9, 9, 9}, 3, {8, 9, 9, 1}, 4},
        {{9}, 1, {9, 9, 9}, 3, {8, 0, 0, 1}, 4},
        {{9, 9, 9}, 3, {9, 9, 9, 9}, 4, {8, 9, 9, 0, 1}, 5},
        {{9}, 1, {9, 9, 9, 9, 1}, 5, {8, 0, 0, 0, 2}, 5}};
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        struct ListNode *l1 = make_list_by_seq(cases[k].seq1, cases[k].len1);
        struct ListNode *l2 = make_list_by_seq(cases[k].seq2, cases[k].len2);
        struct ListNode *sum = make_list_by_seq(cases[k].sum, cases[k].len);
        list_print(l1);
        list_print(l2);
        list_print(sum);
        struct ListNode *s = addTwoNumbers(l1, l2);
        list_print(s);
        int r = list_cmp(s, sum);
        if (r != 0) {
            printf("test %d fail, ret: %d.\n", k, r);
            // list_print(s);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    add_two_numbers_test(argc, argv);
    exit(EXIT_SUCCESS);
}
