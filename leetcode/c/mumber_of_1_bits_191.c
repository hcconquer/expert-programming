/**
 * mumber_of_1_bits_191.c
 *
 * Created: 2017/03/22
 *  Author: hanchen
 */

/**
 * Write a function that takes an unsigned integer and
 * returns the number of ’1' bits it has (also known as the Hamming weight).
 *
 * For example, the 32-bit integer ’11' has binary
 * representation 00000000000000000000000000001011, so the function should
 * return 3.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int hammingWeight(uint32_t n) {
    int count = 0;
    int times = 8 * sizeof(n);
    for (int i = 0; i < times; i++) {
        if ((n & 0x01) != 0) {
            count++;
        }
        n = n >> 1;
    }
    return count;
}

typedef struct {
    uint32_t num;
    int count;
} mumber_of_1_bits_test_t;

static void mumber_of_1_bits_test(int argc, char **argv) {
    mumber_of_1_bits_test_t cases[] = {{0, 0}, {1, 1}, {2, 1},
                                       {3, 2}, {4, 1}, {11, 3}};
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        int num = cases[k].num;
        int count = cases[k].count;
        int cnt = hammingWeight(num);
        if (cnt != count) {
            printf("test %d fail, %d != %d(expected), num: %d.\n", k, cnt,
                   count, num);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    mumber_of_1_bits_test(argc, argv);
    exit(EXIT_SUCCESS);
}
