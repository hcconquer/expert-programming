/*
 * excel_sheet_column_number_171.c
 *
 * Created: 2016/5/12
 *  Author: hanchen
 */

/**
 * Related to question Excel Sheet Column Title
 * Given a column title as appear in an Excel sheet, return its corresponding
 * column number. For example: A -> 1 B -> 2 C -> 3
 *   ...
 *   Z -> 26
 *   AA -> 27
 *   AB -> 28
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define EXCEL_SHEET_COLUMN_TITLE_LENGTH_MAX 16

int titleToNumber(char *s) {
    int num = 0, c = 0, len = 0;
    len = strlen(s);
    for (int i = 0; i < len; i++) {
        c = s[i];
        num = num * 26 + (c - 'A' + 1);
    }
    return num;
}

typedef struct {
    char colstr[EXCEL_SHEET_COLUMN_TITLE_LENGTH_MAX];
    int num;
} title_to_number_test_t;

static void title_to_number_test(int argc, char **argv) {
    title_to_number_test_t cases[] = {
        {"A", 1},   {"Z", 26},    {"AA", 27},
        {"AB", 28}, {"AAA", 703}, {"ABCD", 19010},
    };

    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        int num = cases[k].num;
        int n = titleToNumber(cases[k].colstr);

        if (n != num) {
            printf("test %d fail, %d != %d(expected).\n", k, n, num);
            return;
        }

        printf("test %d pass.\n", k);
    }

    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    title_to_number_test(argc, argv);
    exit(EXIT_SUCCESS);
}
