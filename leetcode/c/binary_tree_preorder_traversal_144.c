/**
 * 144_binary_tree_preorder_traversal.c
 *
 * Created: 2016/04/27
 *  Author: hanchen
 */

/**
 * Given a binary tree, return the preorder traversal of its nodes' values.
 * For example:
 * Given binary tree {1,#,2,3},
 *     1
 *      \
 *       2
 *      /
 *     3
 * return [1,2,3].
 * Note: Recursive solution is trivial, could you do it iteratively?
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     struct TreeNode *left;
 *     struct TreeNode *right;
 * };
 */

struct TreeNode {
    int val;
    struct TreeNode* left;
    struct TreeNode* right;
};

struct TreeNodeTrave {
    struct TreeNode* node;
    struct TreeNodeTrave* next;
};

/**
 * Return an array of size *returnSize.
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* preorderTraversal(struct TreeNode* root, int* returnSize) {
#if 0
//    struct TreeNode** list = malloc(256 * sizeof(struct TreeNode*));
    struct TreeNode* node = NULL;
    int tnts = 256, tntn = 0, ptnt = 0, chn = 0;
    struct TreeNodeTrave* tntl = malloc(tnts * sizeof(struct TreeNodeTrave));
    tntl[ptnt] = root;
    while (tntl[ptnt] != NULL) {
        node = tntl[ptnt].node;
        tntl[tntn++].node = node;
        chn = 0;
        if (node->left != NULL) {
            tntl[tntn].node = node->left;
            tntl[ptnt].next = tntl[tntn].node;
            tntn++;
            chn++;
        }
        if (node->right != NULL) {
            tntl[tntn].node = node->right;
            if (tntl[ptnt].next == NULL) {
                tntl[ptnt].next = tntl[tntn].node;
            }
            tntn++;
            chn++;
        }
        if (chn == 0) {

        } else {
            ptnt = tntn - chn;
        }
    }
#endif
    return NULL;
}

typedef struct {
    char seqs[256];
    int num;
} preorder_traversal_test_t;

void preorder_traversal_test(int argc, char** argv) {
#if 0
    preorder_traversal_test_t cases[] = {
            { { 1, '#', 2, 3 }, 4 }
    };
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        char* seqs = cases[k].seqs;
        int num = cases[k].num;
        struct TreeNode *root = (struct TreeNode *) malloc(sizeof(struct TreeNode));
        struct TreeNode *node = NULL, *left = NULL, *right = NULL;
        for (int i = 0; i < num; /*i++*/) {
            if (seqs[i] == '#') {
                node = NULL;
            } else {
                node = (struct TreeNode*) malloc(sizeof(struct TreeNode));
                node->val = seqs[i];
            }
        }
    }
#endif
}

int main(int argc, char* argv[]) {
    preorder_traversal_test(argc, argv);
    exit(EXIT_SUCCESS);
}
