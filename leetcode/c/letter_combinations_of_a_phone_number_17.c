/**
 * letter_combinations_of_a_phone_number_17.c
 *
 * Created: 2017/04/05
 *  Author: hanchen
 */

/**
 * Given a digit string, return all possible letter combinations that the number
 * could represent. A mapping of digit to letters (just like on the telephone
 * buttons) is given below.
 *
 * 1()     2(abc) 3(def)
 * 4(ghi)  5(jkl) 6(mno)
 * 7(pqrs) 8(tuv) 9(wxyz)
 * *+      0()    #
 *
 * Input:Digit string "23"
 * Output: ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"].
 *
 * Note:
 * Although the above answer is in lexicographical order, your answer could be
 * in any order you want.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ARRPRT(array, len)            \
    ({                                \
        typeof(len) _l = len;         \
        typeof(len) _i = 0;           \
        for (_i = 0; _i < _l; _i++) { \
            printf("%d ", array[_i]); \
        }                             \
        printf("\n");                 \
    })

typedef struct {
    char key;
    char letters[4];
    int num;
} phone_key_entry_t;

/**
 * Return an array of size *returnSize.
 * Note: The returned array must be malloced, assume caller calls free().
 */
char **letterCombinations(char *digits, int *returnSize) {
    static const phone_key_entry_t PHONE_KEY_MAP[] = {
        ['1'] =
            {
                '1',
                {},
                0,
            },
        ['2'] = {'2', {'a', 'b', 'c'}, 3},
        ['3'] = {'3', {'d', 'e', 'f'}, 3},
        ['4'] = {'4', {'g', 'h', 'i'}, 3},
        ['5'] = {'5', {'j', 'k', 'l'}, 3},
        ['6'] = {'6', {'m', 'n', 'o'}, 3},
        ['7'] = {'7', {'p', 'q', 'r', 's'}, 4},
        ['8'] = {'8', {'t', 'u', 'v'}, 3},
        ['9'] = {'9', {'w', 'x', 'y', 'z'}, 4},
        ['*'] = {'*', {}, 0},
        ['0'] = {'0', {}, 0},
        ['#'] = {'#', {}, 0},
    };

    int len = strlen(digits);
    if (len <= 0) {
        *returnSize = 0;
        return NULL;
    }

    int n, s = 1, idx, i;
    for (i = 0; i < len; i++) {
        n = PHONE_KEY_MAP[(int)digits[i]].num;
        s *= n;
    }
    n = 0;

    char **combs = (char **)malloc(s * sizeof(char *));
    char *comb;

    int indi[12];
    memset(indi, 0, sizeof(indi));

    idx = len - 1;
    while (n < s) {
        // handle carry
        if (indi[idx] < PHONE_KEY_MAP[(int)digits[idx]].num) {
            // ARRPRT(array, len);
            comb = (char *)malloc((len + 1) * sizeof(char));
            for (i = 0; i < len; i++) {
                comb[i] = PHONE_KEY_MAP[(int)digits[i]].letters[indi[i]];
            }
            comb[len] = '\0';
            combs[n++] = comb;
            indi[idx]++;
        } else {
            // not need to check idx, because has been check by (n < s)
            while (indi[idx] >= PHONE_KEY_MAP[(int)digits[idx]].num) {
                idx--;
                indi[idx]++;
            }
            // reset pointer
            for (i = idx + 1; i < len; i++) {
                indi[i] = 0;
            }
            idx = len - 1;
        }
    }

    if (returnSize != NULL) {
        *returnSize = n;
    }

    return combs;
}

#define STR_LEN_MAX 256

typedef struct {
    char string[STR_LEN_MAX];
    char combs[256][STR_LEN_MAX];
    int num;
} letter_combinations_test_t;

static void letter_combinations_test(int argc, char **argv) {
    letter_combinations_test_t cases[] = {
        {"23", {"ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"}, 9},
        {"12", {}, 0},
        {"02", {}, 0},
        {"", {}, 0},
        //			{ "234", { "ad", "ae", "af", "bd", "be", "bf", "cd",
        //"ce", "cf" }, 9 },
    };
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        char *string = cases[k].string;
        // char (*combs)[STR_LEN_MAX] = cases[k].combs;
        int num = cases[k].num;
        int n = -1;
        char **cs = letterCombinations(string, &n);
        if (n != num) {
            printf("test %d fail, %d != %d(expected).\n", k, n, num);
            return;
        }
        int length = strlen(string);
        int len;
        for (int i = 0; i < n; i++) {
            len = strlen(cs[i]);
            if (len != length) {
                printf("test %d fail, len mismatch, %d != %d(expected).\n", k,
                       len, length);
                return;
            }
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    letter_combinations_test(argc, argv);
    exit(EXIT_SUCCESS);
}
