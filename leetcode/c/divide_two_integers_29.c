/**
 * divide_two_integers_29.c
 *
 * Created: 2017/04/06
 *  Author: hanchen
 */

/**
 * Divide two integers without using multiplication, division and mod operator.
 * If it is overflow, return MAX_INT.
 */

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

int divide(int dividend, int divisor) {
    // printf("dividend: %d, divisor: %d\n", dividend, divisor);

    static const int POW_2_MAX = 8 * sizeof(int) - 1;
    static const int INT_SIGN_MASK = (1 << POW_2_MAX);

    /*
     * can't change below orders
     */
    if (divisor == 0) {
        return INT_MAX;
    }
    if (dividend == 0) {
        return 0;
    }
    if (dividend == divisor) {
        return 1;
    }
    if (divisor == INT_MIN) {
        return 0;
    }
    if ((dividend == INT_MIN) && (divisor == -1)) {
        return INT_MAX;
    }

    int sign = (!!(dividend & INT_SIGN_MASK)) ^ (!!((divisor & INT_SIGN_MASK)));
    int spec = 0;

    if (dividend < 0) {
        if (dividend == INT_MIN) {
            dividend = INT_MAX;
            spec = 1;
        } else {
            dividend = -dividend;
        }
    }

    if (divisor < 0) {
        divisor = -divisor;
    }

    if (dividend < divisor) {
        return 0;
    }

    int result = 0, mask = 0, ddi = -1, dri = -1, dix;
    for (int i = POW_2_MAX - 1; i >= 0; i--) {
        mask = 1 << i;
        if ((ddi < 0) && (mask & dividend)) {
            ddi = i;
        }
        if ((dri < 0) && (mask & divisor)) {
            dri = i;
        }
        if ((ddi >= 0) && (dri >= 0)) {
            break;
        }
    }
    dix = (ddi - dri);

    int dd = dividend, dp, diff;
    if ((ddi >= 0) && (dri >= 0)) {
        if (dix > 0) {
            dix = dix - 1;
        }
        dp = divisor << dix;
        do {
            diff = dd - dp + spec;
            result += (1 << dix);
            dd -= dp;
        } while (dp < diff);
        result += divide(dd + spec, divisor);
    }

    if (sign != 0) {
        result = -result;
    }

    return result;
}

typedef struct {
    int dividend;
    int divisor;
    int result;
} divide_test_t;

static void divide_test(int argc, char **argv) {
    divide_test_t cases[] = {
        {1, 1, 1},
        {4, 2, 2},
        {100, 8, 12},
        {999999999, 5, 199999999},
        {2147483647, 1, 2147483647},
        {2147483647, 2, 1073741823},
        {2147483647, -2, -1073741823},
        {2147483647, -2147483648, 0},
        {-2147483648, 1, -2147483648},
        {-2147483648, 2, -1073741824},
        {-2147483648, 3, -715827882},
        {-2147483648, 4, -536870912},
        {-2147483648, 2147483647, -1},
        {-2147483648, -1, 2147483647},
        {-2147483648, -2, 1073741824},
        {-2147483648, -2147483648, 1},
        {0, 1, 0},
        {0, -1, 0},
        {2, 0, INT_MAX},
        {-2, 0, INT_MAX},
    };
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        int dividend = cases[k].dividend;
        int divisor = cases[k].divisor;
        int result = cases[k].result;
        int r = divide(dividend, divisor);
        if (result != r) {
            printf(
                "test %d fail, %d != %d(expected), dividend: %d, divisor: "
                "%d.\n",
                k, r, result, dividend, divisor);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    divide_test(argc, argv);
    exit(EXIT_SUCCESS);
}
