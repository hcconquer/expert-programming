/**
 * palindrome_partitioning_132.c
 *
 * Created: 2016/04/26
 *  Author: hanchen
 */

/**
 * Given a string s, partition s such that every substring of the partition is a
 * palindrome. Return the minimum cuts needed for a palindrome partitioning of
 * s. For example, given s = "aab", Return 1 since the palindrome partitioning
 * ["aa","b"] could be produced using 1 cut.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MXP(mx, mn)                                      \
    do {                                                 \
        for (int _i = 0; _i < (mn); _i++) {              \
            for (int _j = 0; _j < (mn); _j++) {          \
                printf("%3d ", MXV(mx, mn, _i, _j + 1)); \
                if ((_j) >= 10) {                        \
                    break;                               \
                }                                        \
            }                                            \
            printf("\n");                                \
            if ((_i) >= 10) {                            \
                break;                                   \
            }                                            \
        }                                                \
    } while (0)

#define MXS(mn) ((mn) * (mn))
// #define MXS(mn) (((num + 1) * num) / 2)

#define MXV(mx, mn, b, n) mx[(b) * (mn) + (n)-1]
// #define MXV(mx, mn, b, n) mx[((b) * (2 * (mn) - (b) + 1)) / 2 + (n) - 1]

/*
 * at this time has a important skill
 *
 * define f(0, n), f(1, n - 1), ..., f(n - 1, 1) are the best solution
 * f(x, n) = MIN(
 *             (f(x, 1) + f(x + 1, n - x - 1) + 1)
 *             (f(x, 2) + f(x + 2, n - x - 2) + 1)
 *             ...
 *             (f(x, n - 1) + f(n - x - 1, 1) + 1)
 *
 * ABS(f(0, n) - f(1, n)) <= 1
 * ABS(f(1, n - 1) - f(f2, n - 2)) <= 1
 *
 * and f(0, 1) = 0
 *
 * so, if f(0, x) is not palindrome, (f(0, x) + f(x, n - x)) must be not
 * the best solution, can jump over it
 */
void initmx(char *s, int *mx, int mn) {
    int mc = -1, mmc = -1, smc = -1;
    for (int b = mn - 1; b >= 0; b--) {  // mn
        mmc = -1;
        for (int n = 1; n <= mn - b; n++) {  // mn
            smc = -1;
            if (n == 1) {
                smc = 0;
            } else if (n == 2) {
                smc = (s[b] == s[b + 1]) ? 0 : 1;
            } else {
                if (s[b] == s[b + n - 1]) {
                    if (MXV(mx, mn, b + 1, n - 2) == 0) {
                        smc = 0;
                    }
                }
            }
            if (smc != 0) {
                continue;
            }
            MXV(mx, mn, b, n) = smc;  // can't remove because need for a[b]a
            if (mn - b - n <= 0) {
                mc = 0;
            } else {
                // MXV(mx, mn, b, n) == smc == 0
                mc =
                    /*MXV(mx, mn, b, n) + */ MXV(mx, mn, b + n, mn - b - n) + 1;
            }
            // printf("%d %d %d %d %d\n", smc, mmc, mc, b, n);
            if ((mmc > mc) || (mmc < 0)) {
                mmc = mc;
            }
        }
        if (mmc < 0) {
            printf("must be some error!\n");
        }
        MXV(mx, mn, b, mn - b) = mmc;
    }
}

int jump(char *s) {
    int num = strlen(s);
    if (num <= 0) {
        return 0;
    }
    int mnum = MXS(num);
    int *mx = (int *)malloc(mnum * sizeof(int));
    for (int i = 0; i < mnum; i++) {
        mx[i] = -1;
    }
    initmx(s, mx, num);
    // MXP(mx, num);
    int cut = MXV(mx, num, 0, num);
    free(mx);
    return cut;
}

typedef struct {
    char str[4096];
    int num;
} palindrome_partitioning_test_t;

static void palindrome_partitioning_test(int argc, char **argv) {
    palindrome_partitioning_test_t cases[] = {
        {"aab", 1},
        {"", 0},
        {"a", 0},
        {"ab", 1},
        {"abd", 2},
        {"abcd", 3},
        {"aabb", 1},
        {"abba", 0},
        {"abbba", 0},
        {"aabbaa", 0},
        {"aabcbaa", 0},
        {"aabcacb", 1},
        {"abcabba", 3},
        {"aabcabc", 5},
        {"aaabaa", 1},
        {"abacddcab", 1},
        {"eegiicgaeadbcfacfh", 13},
        {"eegiicgaeadbcfacfhifdbiehbgejcaeggcgbahfcajfhjjdgj", 42},
        {"adabdcaebdcebdcacaaaadbbcadabcbeabaadcbcaaddebdbddcbdacdbbaedbdaaecab"
         "dceddccbdeeddccdaabbabbdedaaabcdadbdabeacbeadbaddcbaacdbabcccbaceedbc"
         "ccedbeecbccaecadccbdbdccbcbaacccbddcccbaedbacdbcaccdcaadcbaebebcceabb"
         "dcdeaabdbabadeaaaaedbdbcebcbddebccacacddebecabccbbdcbecbaeedcdacdcbdb"
         "ebbacddddaabaedabbaaabaddcdaadcccdeebcabacdadbaacdccbeceddeebbbdbaaaa"
         "abaeecccaebdeabddacbedededebdebabdbcbdcbadbeeceecdcdbbdcbdbeeebcdcabd"
         "eeacabdeaedebbcaacdadaecbccbededceceabdcabdeabbcdecdedadcaebaababeedc"
         "aacdbdacbccdbcece",
         273},
        {"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
         "aaaaaaaaaaaaa",
         1}};
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        char *str = cases[k].str;
        int num = cases[k].num;
        int n = jump(str);
        if (n != num) {
            printf("test %d fail, %d != %d(expected), str: %s.\n", k, n, num,
                   str);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    palindrome_partitioning_test(argc, argv);
    exit(EXIT_SUCCESS);
}
