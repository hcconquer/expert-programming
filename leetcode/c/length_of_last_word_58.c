/**
 * length_of_last_word_58.c
 *
 * Created: 2016/05/20
 *  Author: hanchen
 */

/**
 * Given a string s consists of upper/lower-case alphabets and empty space
 * characters ' ', return the length of last word in the string. If the last
 * word does not exist, return 0. Note: A word is defined as a character
 * sequence consists of non-space characters only. For example, Given s = "Hello
 * World", return 5.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int lengthOfLastWord(char *s) {
    if (s == NULL) {
        return 0;
    }
    char *ps = s, *pe = s;
    int len = 0;
    while (*pe != '\0') {
        while ((*pe != '\0') && (*pe != ' ')) {
            pe++;
        }
        if (pe != ps) {
            len = pe - ps;
        }
        if (*pe == '\0') {
            break;
        }
        pe++;
        ps = pe;
    }
    return len;
}

typedef struct {
    char str[65536];
    int len;
} length_of_last_word_test_t;

static void length_of_last_word_test(int argc, char **argv) {
    length_of_last_word_test_t cases[] = {{"", 0},
                                          {" ", 0},
                                          {"a", 1},
                                          {"a\0\0", 1},
                                          {"a\0\1", 1},
                                          {"a ", 1},
                                          {" a", 1},
                                          {" a ", 1},
                                          {"     a     ", 1},
                                          {"Hello World", 5},
                                          {"Length of Last Word", 4}};
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        char *str = cases[k].str;
        int len = cases[k].len;
        int lw = lengthOfLastWord(str);
        if (lw != len) {
            printf("test %d fail, %d != %d(expected), str: %s.\n", k, lw, len,
                   str);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    length_of_last_word_test(argc, argv);
    exit(EXIT_SUCCESS);
}
