/**
 * sort_characters_by_frequency_451.c
 *
 * Created: 2017/04/17
 *  Author: hanchen
 */

/**
 * Given a string, sort it in decreasing order based on the frequency of
 * characters.
 *
 * Example 1:
 * Input:
 * "tree"
 * Output:
 * "eert"
 *
 * Explanation:
 * 'e' appears twice while 'r' and 't' both appear once.
 * So 'e' must appear before both 'r' and 't'. Therefore "eetr" is also a valid
 * answer.
 *
 * Example 2:
 * Input:
 * "cccaaa"
 * Output:
 * "cccaaa"
 *
 * Explanation:
 * Both 'c' and 'a' appear three times, so "aaaccc" is also a valid answer.
 * Note that "cacaca" is incorrect, as the same characters must be together.
 *
 * Example 3:
 * Input:
 * "Aabb"
 * Output:
 * "bbAa"
 *
 * Explanation:
 * "bbaA" is also a valid answer, but "Aabb" is incorrect.
 * Note that 'A' and 'a' are treated as two different characters.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    char c;
    int freq;
} char_freq_t;

int cmp_by_freq_desc(const void *m, const void *n) {
    char_freq_t *mt = (char_freq_t *)m;
    char_freq_t *nt = (char_freq_t *)n;
    if (mt->freq == nt->freq) {
        return 0;
    }
    return mt->freq > nt->freq ? -1 : 1;
}

char *frequencySort(char *s) {
    static const int CHAR_LEN = 128;
    char_freq_t freqs[CHAR_LEN];
    memset(freqs, 0, sizeof(freqs));
    int i, j;
    for (i = 0; i < CHAR_LEN; i++) {
        freqs[i].c = i;
    }
    int len = strlen(s);
    for (i = 0; i < len; i++) {
        freqs[(int)s[i]].freq++;
    }
    qsort(freqs, CHAR_LEN, sizeof(freqs[0]), cmp_by_freq_desc);
    char *str = (char *)malloc((len + 1) * sizeof(char));
    int idx = 0;
    for (i = 0; i < CHAR_LEN; i++) {
        for (j = 0; j < freqs[i].freq; j++) {
            str[idx++] = freqs[i].c;
            if (idx >= len) {
                break;
            }
        }
        if (idx >= len) {
            break;
        }
    }
    str[idx] = '\0';
    return str;
}

typedef struct {
    char string[1024];
    char srstr[1024];
} frequency_sort_test_t;

static void frequency_sort_test(int argc, char **argv) {
    frequency_sort_test_t cases[] = {
        {"tree", /*"eert"*/ "eetr"},
        {"cccaaa", /*"cccaaa"*/ "aaaccc"},
        {"Aabb", "bbAa"},
    };
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        char *string = cases[k].string;
        char *srstr = cases[k].srstr;
        char *s = frequencySort(string);
        if (strcmp(s, srstr) != 0) {
            printf("test %d fail, %s != %s(expected), string: %s.\n", k, s,
                   srstr, string);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    frequency_sort_test(argc, argv);
    exit(EXIT_SUCCESS);
}
