/*
 * combinations_77.c
 *
 *  Created: 2016/5/1
 *   Author: hanchen
 */
/**
 * Given two integers n and k, return all possible combinations of k numbers out
 of 1 ... n.

 * For example,
 * If n = 4 and k = 2, a solution is:

 * [
 *   [2,4],
 *   [3,4],
 *   [2,3],
 *   [1,2],
 *   [1,3],
 *   [1,4],
 * ]
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int A(int n, int k) {
    int a = 1, i = 0;
    for (i = 0; i < k; i++) {
        a = a * (n - i);
    }
    return a;
}

/**
 * Return an array of arrays of size *returnSize.
 * The sizes of the arrays are returned as *columnSizes array.
 * Note: Both returned array and *columnSizes array must be malloced, assume
 * caller calls free().
 *
 * C(n, k) = A(n, k) / A(k, k)
 */
int** combine(int n, int k, int** columnSizes, int* returnSize) {
    //	int rsz = A(n, k) / A(k, k);
    //	int **table = (int **) malloc(rsz * sizeof(int *));
    //	int *row = (int *) malloc(k * sizeof(int));
    //	for (int i = 0; i < k; i++) {
    //		for (int j = 1; j <= n; j++) {
    //			row[i] = j;
    //		}
    //	}
    return NULL;
}

int main(int argc, char* argv[]) {
    //	number_of_digit_one_test(argc, argv);
    exit(EXIT_SUCCESS);
}
