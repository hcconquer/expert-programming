/**
 * zigzag_conversion_6.c
 *
 * Created: 2017/03/23
 *  Author: hanchen
 */

/**
 * The string "PAYPALISHIRING" is written in a zigzag pattern
 * on a given number of rows like this: (you may want to
 * display this pattern in a fixed font for better legibility)
 *
 * P   A   H   N
 * A P L S I I G
 * Y   I   R
 *
 * And then read line by line: "PAHNAPLSIIGYIR"
 * Write the code that will take a string and make this conversion given a
 * number of rows: string convert(string text, int nRows);
 * convert("PAYPALISHIRING", 3) should return "PAHNAPLSIIGYIR".
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * 0    8    16    24
 * 1 7  9 15 17 23 25
 * 2 6 10 14 18 22
 * 3 5 11 13 19 21
 * 4   12    20
 *
 * if row as row, column is col, when row >= 3, should:
 * 1.col * (row - 1) <= seq
 * 2.diff is ((row - 1) * 2) interlacing
 */
char *convert(char *s, int numRows) {
    int len = strlen(s);
    char *zigzag = (char *)malloc((len + 1) * sizeof(char));
    zigzag[len] = '\0';

    int column = 0;
    if (numRows < 3) {
        column = (len - 1) / numRows + 1;
    } else {
        // not completely correct but very simple
        column = (len - 1) / (numRows - 1) + 1;
    }

    int row = 0, col = 0, m = 0, n = 0;
    for (row = 0; row < numRows; row++) {
        for (col = 0; col < column; col++) {
            if (numRows < 3) {
                m = row + col * numRows;
            } else {
                if ((row > 0) && (row < (numRows - 1))) {
                    if (col & 0x01) {  // odd
                        m = col * (numRows - 1) + (numRows - row - 1);
                    } else {
                        m = row + col * (numRows - 1);
                    }
                } else {
                    m = row + col * (numRows - 1);
                    col++;
                }
            }
            if (m < len) {
                zigzag[n++] = s[m];
            }
            if (n >= len) {
                break;
            }
        }
        if (n >= len) {
            break;
        }
    }
    return zigzag;
}

typedef struct {
    char string[1024];
    int rownum;
    char zigzag[2048];
} convert_test_t;

static void convert_test(int argc, char **argv) {
    convert_test_t cases[] = {
        {"PAYPALISHIRING", 3, "PAHNAPLSIIGYIR"},
        {"", 1, ""},
        {"ABCDEFGHIJKLMNOPQRSTUVWXYZ", 1, "ABCDEFGHIJKLMNOPQRSTUVWXYZ"},
        {"ABCDEFGHIJKLMNOPQRSTUVWXYZ", 2, "ACEGIKMOQSUWYBDFHJLNPRTVXZ"},
        {"ABCDEFGHIJKLMNOPQRSTUVWXYZ", 3, "AEIMQUYBDFHJLNPRTVXZCGKOSW"},
        {"ABCDEFGHIJKLMNOPQRSTUVWXYZ", 4, "AGMSYBFHLNRTXZCEIKOQUWDJPV"},
        {"ABCDEFGHIJKLMNOPQRSTUVWXYZ", 5,
         "AIQYBHJPRXZCGKOSWDFLNTVEMU"},                       // column = 7
        {"ABCDEFGHIJKLMNOPQRST", 5, "AIQBHJPRCGKOSDFLNTEM"},  // column == 5
        {"ABCDEFGHIJKLMNOPQRSTUVW", 5,
         "AIQBHJPRCGKOSWDFLNTVEMU"},  // column = 6
    };
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        char *string = cases[k].string;
        int rownum = cases[k].rownum;
        char *zigzag = cases[k].zigzag;
        char *z = convert(string, rownum);
        int ret = strcmp(z, zigzag);
        if (ret != 0) {
            printf("test %d fail, %s != %s(expected), string: %s.\n", k, z,
                   zigzag, string);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    convert_test(argc, argv);
    exit(EXIT_SUCCESS);
}
