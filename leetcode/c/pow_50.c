/**
 * pow_50.c
 *
 * Created: 2017/04/05
 *  Author: hanchen
 */

/**
 * Implement pow(x, n).
 */

#include <float.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define FLT_EPSILON_S (1.19209290E-07F)
#define DBL_EPSILON_S (1.084202172485504E-19)
#define DBL_EPSILON_V (0.00001)

double myPow(double x, int n) {
    if (x == 0) {
        return 0;
    }
    if (n == 0) {
        return 1;
    }

    /*
     * can conv x to signed but not n,
     * because int INT_MIN != -INT_MAX
     */
    double xn = x > 0 ? x : -x;
    int step = 1;
    if (n < 0) {
        xn = 1 / xn;
        step = -1;
    }

    double p = 1, pn, d;
    for (int i = 0; i != n; i += step) {
        pn = p * xn;
        d = pn > p ? pn - p : p - pn;
        p = pn;
        if (d < DBL_EPSILON_S) {
            break;
        }
    }

    if ((x < 0) && (n % 2 != 0)) {
        p = -p;
    }

    return p;
}

typedef struct {
    double x;
    double n;
    double p;
} my_pow_test_t;

static void my_pow_test(int argc, char **argv) {
    my_pow_test_t cases[] = {
        {0, 1, 0},
        {0, -1, 0},
        {0, -2, 0},
        {2, 0, 1},
        {-2, 0, 1},
        {4, 0.5, 1},
        {4, 0.8, 1},
        {4, 2.5, 16},
        {-4, 2.5, 16},
        {-4, 3.5, -64},
        {-4, -3.5, -0.015625},
        {999999999, 5, 999999994999999962849382769179798031801778176.00000},
        {0.00001, 2147483647, 0},
        {1.00000, 2147483647, 1},
        {1.00001, 123456, 3.436845},
        {-0.99999, 221620, 0.10902},
        {0.99999, 948688, 0.00008},
        {2.00000, -2147483648, 0},
        {-1.00000, 2147483647, -1},
    };
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        double x = cases[k].x;
        double n = cases[k].n;
        double p = cases[k].p;
        double pow = myPow(x, n);
        double d = fabs(pow - p);
        if (d >= DBL_EPSILON_V) {
            printf("test %d fail, %f != %f(expected), x: %f, n: %f, d: %f.\n",
                   k, pow, p, x, n, d);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    my_pow_test(argc, argv);
    exit(EXIT_SUCCESS);
}
