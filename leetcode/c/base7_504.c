/**
 * base7_504.c
 *
 * Created: 2017/04/07
 *  Author: hanchen
 */

/**
 * Given a non-negative integer num, repeatedly add all its digits until
 * the result has only one digit.
 *
 * For example:
 * Given num = 38, the process is like: 3 + 8 = 11, 1 + 1 = 2. Since 2 has only
 * one digit, return it.
 *
 * Follow up:
 * Could you do it without any loop/recursion in O(1) runtime?
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *convertToBase7(int num) {
    static const int LEN_MAX = 20;
    char string[LEN_MAX];
    int sign = num < 0 ? -1 : 1;
    int len = 0, rem;
    while (num != 0) {
        rem = (num % 7) * sign;
        string[LEN_MAX - len - 1] = '0' + rem;
        len++;
        // printf("rem: %d, len: %d\n", rem, len);
        num = num / 7;
    }
    if (sign < 0) {
        string[LEN_MAX - len - 1] = '-';
        len++;
    }
    char *str = (char *)malloc((len + 1) * sizeof(char));
    if (len > 0) {
        memcpy(str, string + (LEN_MAX - len), len);
    } else {
        str[len++] = '0';
    }
    str[len] = '\0';
    // printf("str: %s\n", s);
    return str;
}

typedef struct {
    int num;
    char str[32];
} convert_to_base7_test_t;

static void convert_to_base7_test(int argc, char **argv) {
    convert_to_base7_test_t cases[] = {{100, "202"},
                                       {-7, "-10"},
                                       {0, "0"},
                                       {1, "1"},
                                       {-1, "-1"},
                                       {2147483647, "104134211161"},
                                       {-2147483648, "-104134211162"}};
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        int num = cases[k].num;
        char *str = cases[k].str;
        char *s = convertToBase7(num);
        if (strcmp(s, str) != 0) {
            printf("test %d fail, %s != %s(expected), num: %d.\n", k, s, str,
                   num);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    convert_to_base7_test(argc, argv);
    exit(EXIT_SUCCESS);
}
