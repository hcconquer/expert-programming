/**
 * longest_substring_without_repeating_characters_3.c
 *
 * Created: 2017/03/22
 *  Author: hanchen
 */

/**
 * Given a string, find the length of the longest substring without repeating
 * characters. Examples: Given "abcabcbb", the answer is "abc", which the length
 * is 3. Given "bbbbb", the answer is "b", with the length of 1. Given "pwwkew",
 * the answer is "wke", with the length of 3. Note that the answer must be a
 * substring, "pwke" is a subsequence and not a substring.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int lengthOfLongestSubstring(char *s) {
    char *subx = s, *sub = s;
    char subxmap[128] = {0}, submap[128] = {0};
    int subxlen = 0, sublen = 0;
    int slen = strlen(s);
    int ci = 0, i, j;
    char c;

    for (i = 0; i < slen; i++) {
        memset(submap, 0, sizeof(submap));
        sublen = 0;
        sub = s + i;
        for (j = i; j < slen; j++) {
            c = s[j];
            ci = (int)c;
            // printf("sub: %s, len: %d, char: %c, set: %d\n", sub, sublen, c,
            // submap[ci]);
            if (submap[ci] != 0) {
                break;
            }
            submap[ci] = 1;
            sublen++;
        }
        if (sublen > subxlen) {
            // printf("reset\n");
            memcpy(subxmap, submap, sizeof(subxmap));
            subxlen = sublen;
            subx = sub;
        }
        // jump over repeate char
        if (j < slen) {  // not complete inner loop because find repeate char
            for (; i < slen; i++) {
                if (s[i] == c) {
                    break;
                }
            }
        }
        // printf("subx: %s, len: %d\n", subx, subxlen);
    }

    return subxlen;
}

typedef struct {
    char string[256];
    char substr[256];
} longest_substring_without_repeating_characters_test_t;

static void longest_substring_without_repeating_characters_test(int argc,
                                                                char **argv) {
    longest_substring_without_repeating_characters_test_t cases[] = {
        {"abcabcbb", "abc"},
        {"bbbbb", "b"},
        {"pwwkew", "wke"},
        {"", ""},
        {"c", "c"},
        {"aaa", "a"},
        {"abc", "abc"},
        {"longestsubstringwithoutrepeatingcharacters", "ubstringw"}};
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        char *string = cases[k].string;
        char *substr = cases[k].substr;
        int len = lengthOfLongestSubstring(string);
        int slen = strlen(substr);
        if (len != slen) {
            printf("test %d fail, %d != %d(expected), string: %s.\n", k, len,
                   slen, string);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    longest_substring_without_repeating_characters_test(argc, argv);
    exit(EXIT_SUCCESS);
}
