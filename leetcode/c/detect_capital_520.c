/**
 * detect_capital_520.c
 *
 * Created: 2017/04/07
 *  Author: hanchen
 */

/**
 * Given a word, you need to judge whether the usage of capitals in it is right
 * or not. We define the usage of capitals in a word to be right when one of the
 * following cases holds: 1.All letters in this word are capitals, like "USA".
 * 2.All letters in this word are not capitals, like "leetcode".
 * 3.Only the first letter in this word is capital if it has more than one
 * letter, like "Google".
 *
 * Otherwise, we define that this word doesn't use capitals in a right way.
 *
 * Example 1:
 * Input: "USA"
 * Output: True
 *
 * Example 2:
 * Input: "FlaG"
 * Output: False
 *
 * Note: The input will be a non-empty word consisting of uppercase and
 * lowercase latin letters.
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

bool detectCapitalUse(char *word) {
    int len = strlen(word);
    int u = 0;
    for (int i = 0; i < len; i++) {
        if ((word[i] >= 'A') && (word[i] <= 'Z')) {
            u++;
        }
    }
    if ((u == 0) || (u == len)) {
        return true;
    }
    if ((u == 1) && ((word[0] >= 'A') && (word[0] <= 'Z'))) {
        return true;
    }
    return false;
}

typedef struct {
    char string[1024];
    bool result;
} mumber_of_1_bits_test_t;

static void mumber_of_1_bits_test(int argc, char **argv) {
    mumber_of_1_bits_test_t cases[] = {
        {"USA", true},
        {"leetcode", true},
        {"Google", true},
        {"FlaG", false},
    };
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        char *string = cases[k].string;
        bool result = cases[k].result;
        bool r = detectCapitalUse(string);
        if (r != result) {
            printf("test %d fail, %d != %d(expected), string: %s.\n", k, r,
                   result, string);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    mumber_of_1_bits_test(argc, argv);
    exit(EXIT_SUCCESS);
}
