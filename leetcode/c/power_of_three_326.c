/**
 * power_of_three_326.c
 *
 * Created: 2016/04/27
 *  Author: hanchen
 */

/**
 * Given an integer, write a function to determine if it is a power of three.

 * Follow up:
 * Could you do it without using any loop / recursion?
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

bool isPowerOfThree(int n) {
    if (n <= 0) {
        return false;
    }
    int r = 1162261467 % n;
    return r == 0 ? true : false;
}

typedef struct {
    int n;
    bool r;
} power_of_three_test_t;

void is_power_of_three_test(int argc, char **argv) {
    power_of_three_test_t cases[] = {
        {0, false}, {1, true},  {2, false},  {3, true},
        {4, false}, {81, true}, {99, false}, {100, false},
    };
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        int n = cases[k].n;
        bool r = cases[k].r;
        bool t = isPowerOfThree(n);
        if (r != t) {
            printf("test %d fail, %d != %d(expected).\n", k, t, r);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    is_power_of_three_test(argc, argv);
    exit(EXIT_SUCCESS);
}
