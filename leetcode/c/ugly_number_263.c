/*
 * ugly_number_263.c
 *
 * Created: 2016/09/15
 *  Author: hanchen
 */
/**
 * Write a program to check whether a given number is an ugly number.

 * Ugly numbers are positive numbers whose prime factors only include 2, 3, 5.
 For example, 6, 8 are ugly while 14 is not ugly since it includes another prime
 factor 7.

 * Note that 1 is typically treated as an ugly number.
 */
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 * every number can divide by some prime factor
 */
bool isUgly(int num) {
    static const int factors[] = {2, 3, 5};
    static const int factor_num = 3;
    if (num <= 0) {
        return false;
    } else if (num < factors[factor_num - 1]) {
        return true;
    }
    int rem = 0, i = 0;
    for (i = 0; i < factor_num; i++) {
        for (;;) {
            rem = num % factors[i];
            if (rem != 0) {
                break;
            }
            num = num / factors[i];
        }
    }
    if (num > factors[factor_num - 1]) {
        return false;
    }
    return true;
}

typedef struct {
    int num;
    bool ret;
} is_ugly_test_t;

static void is_ugly_test(int argc, char **argv) {
    is_ugly_test_t cases[] = {
        {-2147483648, false},
        {-4, false},
        {0, false},
        {1, true},
        {2, true},
        {3, true},
        {4, true},
        {5, true},
        {6, true},
        {7, false},
        {8, true},
        {9, true},
        {10, true},
        {11, false},
        {12, true},
        {13, false},
        {14, false},
        {28, false},
    };
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        int num = cases[k].num;
        bool ret = cases[k].ret;
        int r = isUgly(num);
        if (r != ret) {
            printf("test %d fail, %d != %d(expected), num: %d.\n", k, r, ret,
                   num);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    is_ugly_test(argc, argv);
    exit(EXIT_SUCCESS);
}
