/*
 * remove_duplicate_letters_316.c
 *
 * Created: 2016/5/11
 *  Author: hanchen
 */

/**
 * Given a string which contains only lowercase letters, remove duplicate
 * letters so that every letter appear once and only once. You must make sure
 * your result is the smallest in lexicographical order among all possible
 * results. Example: Given "bcabc" Return "abc" Given "cbacdcbc" Return "acdb"
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void moveZeroes(int *nums, int numsSize) {
    int i = 0, j = 0;
    while ((i < numsSize) && (j < numsSize)) {
        while (i < numsSize) {
            if (nums[i] == 0) {
                break;
            }
            i++;
        }
        j = i + 1;
        while (j < numsSize) {
            if (nums[j] != 0) {
                break;
            }
            j++;
        }
        if ((i >= numsSize) || (j >= numsSize)) {
            break;
        }
        nums[i] = nums[j];
        nums[j] = 0;
        i++;
        j++;
    }
}

typedef struct {
    int s[256];
    int d[256];
    int num;
} move_zeroes_test_t;

static void excel_sheet_column_number_test(int argc, char **argv) {
    move_zeroes_test_t cases[] = {{{0, 1, 0, 3, 12}, {1, 3, 12, 0, 0}, 5},
                                  {{0, 1, 0, 3, 12, 0}, {1, 3, 12, 0, 0, 0}, 6},
                                  {{0}, {0}, 1},
                                  {{1}, {1}, 1},
                                  {{1, 0}, {1, 0}, 2},
                                  {{0, 1}, {1, 0}, 2},
                                  {{1, 0, 1}, {1, 1, 0}, 3},
                                  {{1, 1, 0}, {1, 1, 0}, 3},
                                  {{0, 1, 0}, {1, 0, 0}, 3},
                                  {{0, 0, 1}, {1, 0, 0}, 3}};

    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        int *s = cases[k].s;
        int *d = cases[k].d;
        int num = cases[k].num;

        moveZeroes(s, num);

        for (int i = 0; i < num; i++) {
            if (s[i] != d[i]) {
                printf("test fail, %d != %d, index: %d.\n", s[i], d[i], i);
                return;
            }
        }

        printf("test %d pass.\n", k);
    }

    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    excel_sheet_column_number_test(argc, argv);
    exit(EXIT_SUCCESS);
}
