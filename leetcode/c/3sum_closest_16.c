/**
 * 3sum_closest_16.c
 *
 * Created: 2017/04/01
 *  Author: hanchen
 */

/**
 * Given an array S of n integers, find three integers in S such that the
 * sum is closest to a given number, target. Return the sum of the three
 * integers. You may assume that each input would have exactly one solution.
 *
 * For example, given array S = {-1 2 1 -4}, and target = 1.
 * The sum that is closest to the target is 2. (-1 + 2 + 1 = 2).
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ARRPRT(array, len)            \
    ({                                \
        typeof(len) _l = len;         \
        typeof(len) _i = 0;           \
        for (_i = 0; _i < _l; _i++) { \
            printf("%d ", array[_i]); \
        }                             \
        printf("\n");                 \
    })

int int_cmp(const void *m, const void *n) {
    int mi = *((int *)m);
    int ni = *((int *)n);
    return (mi == ni) ? 0 : ((mi < ni) ? -1 : 1);
}

ssize_t bsearchp(const void *key, const void *data, size_t num, size_t size,
                 int (*cmp)(const void *, const void *)) {
    ssize_t l = 0, r = num - 1, m = 0;
    int ret = -1;
    if (data == NULL || cmp == NULL) {
        return -1;
    }
    while (l <= r) {
        m = (l + r) / 2;
        ret = cmp(key, data + m * size);
        if (ret < 0) {
            r = m - 1;
        } else if (ret > 0) {
            l = m + 1;
        } else {
            break;
        }
    }
    return m;
}

int threeSumClosest(int *nums, int numsSize, int target) {
    static const int CMP_SCOPE[] = {0, -1, 1};
    int msize = numsSize * sizeof(int);
    int *seqs = (int *)malloc(msize);
    memcpy(seqs, nums, msize);
    qsort(seqs, numsSize, sizeof(int), int_cmp);

    int c, d, c1, d1, val, i, j, p, ps;

    // init and assume has a solution
    c = seqs[0] + seqs[1] + seqs[2];
    d = c < target ? target - c : c - target;

    for (i = 0; i < numsSize - 2; i++) {
        for (j = i + 1; j < numsSize - 1; j++) {
            val = target - (seqs[i] + seqs[j]);
            p = bsearchp(&val, seqs + j + 1, numsSize - j - 1, sizeof(int),
                         int_cmp);
            p += j + 1;

            for (int k = 0; k < sizeof(CMP_SCOPE) / sizeof(CMP_SCOPE[0]); k++) {
                ps = p + CMP_SCOPE[k];
                if ((ps <= j) || (ps >= numsSize)) {
                    continue;
                }
                c1 = seqs[i] + seqs[j] + seqs[ps];
                d1 = c1 < target ? target - c1 : c1 - target;
                //                printf("target: %d, s1: %d, s2: %d, key: %d,
                //                find: %d, closet: %d, diff: %d\n",
                //                        target, seqs[i], seqs[j], val, seqs[p
                //                        + k], c1, d1);
                if (d1 < d) {
                    c = c1;
                    d = d1;
                    if (d == 0) {
                        return c;
                    }
                }
            }
        }
    }

    return c;
}

typedef struct {
    int array[1024];
    int num;
    int target;
    int closest;
} three_sum_closest_test_t;

static void three_sum_closest_test(int argc, char **argv) {
    three_sum_closest_test_t cases[] = {
        {{-1, 2, 1, -4}, 4, 1, 2},
        {{-1, -2, -3, -4}, 4, 1, -6},
        {{1, 2, 3, 4}, 4, 1, 6},
        {{3, 0, -2, -1, 1, 2}, 6, 0, 0},
        {{4, 3, 2, 1, 0, -1, -2, -3, -4}, 9, 0, 0},
        {{1, 4, 3, 2, 1, 0, 0, -1, -2, -3, -4, -1}, 12, 0, 0},
        {{1, 3, 5, 9}, 4, 10, 9},
        {{1, 3, 5, 9}, 4, 11, 9},
        {{1, 3, 5, 9}, 4, 12, 13},
        {{-1, 0, 1, 1, 55}, 5, 3, 2},
        {{1, 1, 1, 1, 2, 2, 2, 2}, 8, 4, 4},
    };
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        int *array = cases[k].array;
        int num = cases[k].num;
        int target = cases[k].target;
        int closest = cases[k].closest;
        int c = threeSumClosest(array, num, target);
        if (c != closest) {
            printf("test %d fail, %d != %d(expected), num: %d.\n", k, c,
                   closest, num);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    three_sum_closest_test(argc, argv);
    exit(EXIT_SUCCESS);
}
