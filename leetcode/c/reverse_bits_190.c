/**
 * reverse_bits_190.c
 *
 * Created: 2016/05/13
 *  Author: hanchen
 */

/**
 * Reverse bits of a given 32 bits unsigned integer.
 * For example, given input 43261596 (represented in binary as
 * 00000010100101000001111010011100), return 964176192 (represented in binary as
 * 00111001011110000010100101000000). Follow up: If this function is called many
 * times, how would you optimize it?
 */

#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

uint32_t reverseBits(uint32_t n) {
    uint8_t map[] = {
        0x00,          0x08 /*0x01*/, 0x04 /*0x02*/, 0x0C /*0x03*/,
        0x02 /*0x04*/, 0x0A /*0x05*/, 0x06 /*0x06*/, 0x0E /*0x07*/,
        0x01 /*0x08*/, 0x09 /*0x09*/, 0x05 /*0x0A*/, 0x0D /*0x0B*/,
        0x03 /*0x0C*/, 0x0B /*0x0D*/, 0x07 /*0x0E*/, 0x0F /*0x0F*/,
    };
    uint32_t r = 0;
    int b = sizeof(n) * 8;
    for (int i = 0; i < b; i += 4) {
        r = r | (map[((n >> i) & 0x0F)] << (b - 4 - i));
        // printf("%d, %x, %x, %x\n", i, map[((n >> i) & 0x0F)], (map[((n >> i)
        // & 0x0F)] << (b - 4 - i)), r);
    }
    return r;
}

typedef struct {
    uint32_t num;
    uint32_t rbn;
} reverse_bits_test_t;

static void reverset_bits_test(int argc, char **argv) {
    reverse_bits_test_t cases[] = {
        {0, 0},
        {1, 1 << 31},
        {1 << 31, 1},
        {2, 1 << 30},
        {1 << 30, 2},
        {0xFFFFFFFF, 0xFFFFFFFF},
        {0x000000FF, 0xFF000000},
        {43261596, 964176192},
        {964176192, 43261596},
    };
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        uint32_t n = cases[k].num;
        uint32_t r = cases[k].rbn;
        uint32_t t = reverseBits(n);
        if (r != t) {
            printf("test %d fail, 0x%x != 0x%x/%d(expected), num: 0x%x/%d.\n",
                   k, t, r, r, n, n);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    reverset_bits_test(argc, argv);
    exit(EXIT_SUCCESS);
}
