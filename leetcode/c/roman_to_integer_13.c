/**
 * roman_to_integer_13.c
 *
 * Created: 2017/03/29
 *  Author: hanchen
 */

/**
 * Given a roman numeral, convert it to an integer.
 * Input is guaranteed to be within the range from 1 to 3999.
 */

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    int roman;
    int num;
} roman_num_t;

int romanToInt(char *s) {
    static const roman_num_t ROMAN_MAP[128] = {
        ['I'] = {'I', 1},    ['V'] = {'V', 5},   ['X'] = {'X', 10},
        ['L'] = {'L', 50},   ['C'] = {'C', 100}, ['D'] = {'D', 500},
        ['M'] = {'M', 1000},
    };
    int num = 0, idx = 0, left, right, third;
    int len = strlen(s);
    while (idx < len - 1) {
        left = ROMAN_MAP[(int)s[idx]].num;
        right = ROMAN_MAP[(int)s[idx + 1]].num;
        if (left < right) {
            num += right - left;
            idx += 2;
        } else if (left > right) {
            if (idx < len - 2) {
                third = ROMAN_MAP[(int)s[idx + 2]].num;
                if ((third > right) && (third <= left)) {
                    num += left + (third - right);
                    idx += 3;
                    continue;
                }
            }
            num += left + right;
            idx += 2;
        } else {
            num += left + right;
            idx += 2;
            if (idx < len) {  // max can three times
                right = ROMAN_MAP[(int)s[idx]].num;
                if (left == right) {
                    num += right;
                    idx++;
                }
            }
        }
        // printf("num: %d, left: %d, right: %d, idx: %d\n", num, left, right,
        // idx);
    }
    if (idx < len) {
        left = ROMAN_MAP[(int)s[idx]].num;
        num += left;
    }
    return num;
}

typedef struct {
    char string[1024];
    int num;
} roman_to_int_test_t;

static void roman_to_int_test(int argc, char **argv) {
    roman_to_int_test_t cases[] = {
        {"I", 1},
        {"II", 2},
        {"III", 3},
        {"IV", 4},
        {"V", 5},
        {"VI", 6},
        {"VII", 7},
        {"VIII", 8},
        {"IX", 9},
        {"X", 10},
        {"XI", 11},
        {"XII", 12},
        {"XIII", 13},
        {"XIV", 14},
        {"XL", 40},
        {"XLV", 45},
        {"XCIX", 99},
        {"CI", 101},
        {"CXIII", 113},
        {"CCLXXVIII", 278},
        {"CCCXXVII", 327},
        {"CCCLXXXVIII", 388},
        {"CDLXXXVIII", 488},
        {"CDXCVIII", 498},
        {"CDXCIX", 499},
        {"D", 500},
        {"DCXXI", 621},
        {"DCCC", 800},
        {"DCCCXCIX", 899},
        {"CMXCIX", 999},
        {"M", 1000},
        {"MDCCCLXXXVIII", 1888},
        {"MDCCCXCIX", 1899},
        {"MCMLXXVI", 1976},
        {"MCMLXXXIV", 1984},
        {"MCMXCVI", 1996},
        {"MCMXCIX", 1999},
        {"MMM", 3000},
        {"MMMCMXCIX", 3999},
    };
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        char *string = cases[k].string;
        int num = cases[k].num;
        int n = romanToInt(string);
        if (n != num) {
            printf("test %d fail, %d != %d(expected), string: %s.\n", k, n, num,
                   string);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    roman_to_int_test(argc, argv);
    exit(EXIT_SUCCESS);
}
