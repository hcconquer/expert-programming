/**
 * number_of_digit_one_233.c
 *
 * Created: 2016/04/30
 *  Author: hanchen
 */

/**
 * Given an integer n, count the total number of digit 1 appearing
 * in all non-negative integers less than or equal to n.

 * For example:
 * Given n = 13,
 * Return 6, because digit 1 occurred in the following numbers: 1, 10, 11,
 12, 13.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int count_digit(int num, int digit) {
    int cnt = 0, n = num, r = 0;
    if (n < 0) {
        n = -n;
    }
    while (n > 0) {
        r = n % 10;
        if (r == digit) {
            cnt++;
        }
        n = n / 10;
    }
    return cnt;
}

int countDigitOne_z(int n) {
    int cnt = 0;
    for (int i = 0; i <= n; i++) {
        cnt += count_digit(i, 1);
    }
    return cnt;
}

long pow_z(int x, int y) {
    long map[] = {
        1,
        10,
        100,
        1000,
        10000,
        100000,
        1000000,
        10000000,
        100000000,
        1000000000,
        100000000000,
        1000000000000,
        10000000000000,
        100000000000000,
        1000000000000000,
        10000000000000000,
        100000000000000000,
        1000000000000000000,
    };  // pow(10, 21) == pow(1000, 7) == pow(2, 70)
    return map[y];
}

long count_digit_as(int u) {
    return u * pow_z(10, u - 1);
}

int countDigitOne(int n) {
    int d = n, p = 0, v = 0;
    long cnt = 0, sl = 0, sh = 0, c = 0, ec = 0;
    int array[32];
    while (d > 0) {
        for (d = n; d > 0; d = d / 10) array[p++] = d % 10;
    }
    sh = count_digit_as(p);  // f(999999) = 600000
    cnt = sh;
    for (int u = p - 1; u >= 0; u--) {
        v = array[u];
        sl = count_digit_as(u);
        c = (9 - v) * sl;
        if (v == 0) {
            ec = pow_z(10, u);
        } else if (v == 1) {
            ec = pow_z(10, u) - (n % (int)pow_z(10, u)) - 1;
        } else {
            ec = 0;
        }
        cnt = cnt - c - ec;
    }
    return cnt;
}

typedef struct {
    int num;
    int cnt;
} number_of_digit_one_test_t;

static void number_of_digit_one_test(int argc, char **argv) {
    number_of_digit_one_test_t cases[] = {
        {1, 1},
        {4, 1},
        {9, 1},
        {10, 2},
        {13, 6},
        {14, 7},
        {19, 12},
        {20, 12},
        {49, 15},
        {54, 16},
        {79, 18},
        {80, 18},
        {89, 19},  // f(89) - f(79) = 1
        {99, 20},
        {100, 21},
        {110, 33},
        {114, 42},
        {120, 53},
        {121, 55},
        {125, 59},
        {129, 63},
        {130, 64},
        {190, 130},
        {199, 140},
        {200, 140},
        {201, 141},
        {299, 160},
        {399, 180},
        {654, 236},
        {799, 260},
        {800, 260},
        {899, 280},  // f(899) - f(799) = 20
        {999, 300},  // f(999): f(99): f(9) ==> 300: 20: 1
        {1000, 301},
        {1111, 448},
        {1999, 1600},
        {2999, 1900},
        {7000, 3100},
        {7654, 3336},
        {7999, 3400},
        {8000, 3400},
        {8999, 3700},  // f(8999) - f(7999) = 300
        {9999, 4000},  // f(9999): f(999): f(99): f(9) ==> 4000: 300: 20: 1
        {10000, 4001},
        {19999, 18000},  // f(19999): f(1999): f(199): f(19) ==>18000: 1600:
                         // 140: 12
        {70000, 38000},
        {80000, 42000},
        {77654, 41336},
        {87654, 45336},
        {99999, 50000},
        {987654, 595336},
        {999999, 600000},
        {1410065408, 1737167499},
    };
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        int num = cases[k].num;
        int cnt = cases[k].cnt;
        int c = countDigitOne(num);
        if (c != cnt) {
            printf("test %d fail, %d != %d(expected), num: %d.\n", k, c, cnt,
                   num);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    number_of_digit_one_test(argc, argv);
    exit(EXIT_SUCCESS);
}
