/**
 * reverse_integer_7.c
 *
 * Created: 2017/03/24
 *  Author: hanchen
 */

/**
 * Reverse digits of an integer.
 * Example1: x = 123, return 321
 * Example2: x = -123, return -321
 *
 * click to show spoilers.
 *
 * Note:
 * The input is assumed to be a 32-bit signed integer.
 * Your function should return 0 when the reversed integer overflows.
 */

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * reverse2 is quick 10% then reverse1,
 * because less then a compare
 */
int reverse2(int x) {
    // static const int INT_MAX = ~(0x01 << (sizeof(x) * 8 - 1));
    int rev = 0, rem;
    int num = (x > 0) ? x : (-x);
    while (num != 0) {
        rem = num % 10;
        if ((INT_MAX - rem) / 10 < rev) {
            return 0;
        }
        rev = rev * 10 + rem;
        num = num / 10;
        // printf("rem: %d, rev: %d, num: %d\n", rem, rev, x);
    }
    return (x > 0) ? rev : (-rev);
}

int reverse1(int x) {
    int rev = 0, r, num = x, rem;
    while (num != 0) {
        rem = num % 10;
        r = rev * 10 + rem;
        if (((r / 10) != rev) && ((r % 10) != rem)) {
            return 0;
        }
        rev = r;
        num = num / 10;
        // printf("rem: %d, rev: %d, num: %d\n", rem, rev, x);
    }
    return rev;
}

int reverse(int x) {
    return reverse2(x);
}

typedef struct {
    int num;
    int rev;
} reverse_test_t;

static void reverse_test(int argc, char **argv) {
    reverse_test_t cases[] = {
        {123, 321},       {-123, -321},    {0, 0},
        {-2147483648, 0}, {2147483647, 0}, {1, 1},
        {10, 1},          {11, 11},        {123456789, 987654321},
    };
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        int num = cases[k].num;
        int rev = cases[k].rev;
        int r = reverse(num);
        if (r != rev) {
            printf("test %d fail, %d != %d(expected), num: %d.\n", k, r, rev,
                   num);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    reverse_test(argc, argv);
    exit(EXIT_SUCCESS);
}
