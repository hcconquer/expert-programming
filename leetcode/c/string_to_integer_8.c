/**
 * string_to_integer_8.c
 *
 * Created: 2017/03/24
 *  Author: hanchen
 */

/**
 * Implement atoi to convert a string to an integer.
 * Hint: Carefully consider all possible input cases. If you want a challenge,
 * please do not see below and ask yourself what are the possible input cases.
 * Notes: It is intended for this problem to be specified vaguely (ie, no given
 * input specs). You are responsible to gather all the input requirements up
 * front.
 *
 * Requirements for atoi:
 *
 * The function first discards as many whitespace characters as necessary
 * until the first non-whitespace character is found.
 * Then, starting from this character, takes an optional initial plus
 * or minus sign followed by as many numerical digits as possible,
 * and interprets them as a numerical value.
 *
 * The string can contain additional characters after those that form
 * the integral number, which are ignored and have no effect on the
 * behavior of this function.
 *
 * If the first sequence of non-whitespace characters in str is not a valid
 * integral number, or if no such sequence exists because either str is empty
 * or it contains only whitespace characters, no conversion is performed.
 * If no valid conversion could be performed, a zero value is returned.
 *
 * If the correct value is out of the range of representable values,
 * INT_MAX (2147483647) or INT_MIN (-2147483648) is returned.
 */

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int myAtoi(char *str) {
    int num = 0, sign = 0, n = 0;
    int len = strlen(str);
    for (int i = 0; i < len; i++) {
        n = str[i] - '0';
        if ((n >= 0) && (n <= 9)) {
            if (sign == 0) {
                sign = 1;
            }
            if (sign > 0) {
                if ((INT_MAX - n) / 10 < num) {
                    return INT_MAX;
                }
            } else {
                if ((INT_MIN + n) / 10 > num) {
                    // printf("%d, %d, %d\n", n, num, (INTMIN + n));
                    return INT_MIN;
                }
            }
            num = num * 10 + sign * n;
        } else {
            if (sign != 0) {
                return num;
            }
            switch (str[i]) {
                case '-':
                    if (sign == 0) {
                        sign = -1;
                    }
                    break;
                case '+':
                    if (sign == 0) {
                        sign = 1;
                    }
                    break;
                case ' ':
                    // nothing to do, jump over
                    break;
                default:
                    return 0;
            }
        }
        // printf("sign: %d, num: %d, n: %d, str: [%s]\n", sign, num, n, str);
    }
    // printf("sign: %d, num: %d, str: [%s]\n", sign, num, str);
    return num;
}

typedef struct {
    char string[32];
    int num;
} my_atoi_test_t;

static void my_atoi_test(int argc, char **argv) {
    my_atoi_test_t cases[] = {
        {"0", 0},
        {"1", 1},
        {"10", 10},
        {"11", 11},
        {"123", 123},
        {"-123", -123},
        {"+123", 123},
        {"0123", 123},
        {"a123", 0},
        {"a0123", 0},
        {"-a0123", 0},
        {"+a0123", 0},
        {"-1a2b3c", -1},
        {"-123+", -123},
        {" -1234+ ", -1234},
        {" -0123+", -123},
        {" -0 1 2 ", 0},
        {"-1-2-3", -1},
        {"-+1+2-3", 0},
        {"+-+1+2-3", 0},
        {" -123", -123},
        {" +0123", 123},
        {" -0123a", -123},
        {"123456789", 123456789},
        {"2147483646", 2147483646},
        {"2147483647", 2147483647},
        {"2147483648", 2147483647},
        {"-2147483647", -2147483647},
        {"-2147483648", -2147483648},
        {"-2147483649", -2147483648},
        {"9999999999", 2147483647},
        {" b11228552307", 0},
    };
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        char *string = cases[k].string;
        int num = cases[k].num;
        int n = myAtoi(string);
        if (n != num) {
            printf("test %d fail, %d != %d(expected), str: [%s].\n", k, n, num,
                   string);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    my_atoi_test(argc, argv);
    exit(EXIT_SUCCESS);
}
