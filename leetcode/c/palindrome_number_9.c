/**
 * palindrome_number_9.c
 *
 * Created: 2017/03/25
 *  Author: hanchen
 */

/**
 * Determine whether an integer is a palindrome. Do this without extra space.
 *
 * Some hints:
 * Could negative integers be palindromes? (ie, -1)
 * If you are thinking of converting the integer to string,
 * note the restriction of using extra space.
 *
 * You could also try reversing an integer. However, if you have solved
 * the problem "Reverse Integer", you know that the reversed integer
 * might overflow. How would you handle such case?
 *
 * There is a more generic way of solving this problem.
 */

#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

bool isPalindrome(int x) {
    int rev = 0;
    int num = x > 0 ? x : -x;
    while (num != 0) {
        rev = rev * 10 + num % 10;
        num = num / 10;
    }
    return (rev == x);
}

typedef struct {
    int num;
    bool ispal;
} is_palindrome_test_t;

static void is_palindrome_test(int argc, char **argv) {
    is_palindrome_test_t cases[] = {
        {0, true},    {1, true},
        {11, true},   {101, true},
        {111, true},  {10, false},
        {100, false}, {-1, false},
        {-11, false}, {-2147483648, false},
    };
    for (int k = 0; k < sizeof(cases) / sizeof(cases[0]); k++) {
        int num = cases[k].num;
        bool ispal = cases[k].ispal;
        bool ip = isPalindrome(num);
        if (ip != ispal) {
            printf("test %d fail, %d != %d(expected), num: %d.\n", k, ip, ispal,
                   num);
            return;
        }
        printf("test %d pass.\n", k);
    }
    printf("test pass.\n");
}

int main(int argc, char *argv[]) {
    is_palindrome_test(argc, argv);
    exit(EXIT_SUCCESS);
}
