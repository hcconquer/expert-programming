/**
 * 132_palindrome_partitioning_2.c
 *
 * Created on: 2016/04/26 Author: hanchen
 */

/**
 * Given a string s, partition s such that every substring of the partition is 
 * a palindrome. Return the minimum cuts needed for a palindrome partitioning 
 * of s. For example, given s = "aab", Return 1 since the palindrome 
 * partitioning ["aa","b"] could be produced using 1 cut.
 */

package com.zheezes.leetcode;

public class PalindromePartitioning132 {
  private int mxk(int[] mx, int mn, int b, int n) {
    return ((b) * (2 * (mn) - (b) + 1)) / 2 + (n) - 1;
  }

  private int mxv(int[] mx, int mn, int b, int n) {
    return mx[mxk(mx, mn, b, n)];
  }

  private void mxv(int[] mx, int mn, int b, int n, int value) {
    mx[mxk(mx, mn, b, n)] = value;
  }

  @SuppressWarnings("unused")
  private void mxp(int[] mx, int mn) {
    for (int _i = 0; _i < (mn); _i++) {
      for (int _j = 0; _j < (mn) - _i; _j++) {
        System.out.printf("%3d ", mxv(mx, mn, _i, _j + 1));
      }
      System.out.printf("\n");
    }
  }

  private void initmx(String s, int[] mx, int mn) {
    int mc = -1, mmc = -1, smc = -1;
    for (int b = mn - 1; b >= 0; b--) { // mn
      mmc = -1;
      for (int n = 1; n <= mn - b; n++) { // mn
        smc = -1;
        if (n == 1) {
          smc = 0;
        } else if (n == 2) {
          smc = (s.charAt(b) == s.charAt(b + 1)) ? 0 : 1;
        } else {
          if (s.charAt(b) == s.charAt(b + n - 1)) {
            if (mxv(mx, mn, b + 1, n - 2) == 0) {
              smc = 0;
            }
          }
        }
        mxv(mx, mn, b, n, smc);
        if (smc == 0) {
          if (mn - b - n <= 0) {
            mc = 0;
          } else {
            mc = mxv(mx, mn, b + n, mn - b - n) + 1;
          }
          if ((mmc > mc) || (mmc < 0)) {
            mmc = mc;
          }
        }
      }
      if (mmc < 0) {
        System.out.printf("must be some error!\n");
      }
      mxv(mx, mn, b, mn - b, mmc);
    }
  }

  public int minCut(String s) {
    int num = s.length();
    if (num <= 0) {
      return 0;
    }
    int mnum = ((num + 1) * num) / 2;
    int[] mx = new int[mnum];
    for (int i = 0; i < mnum; i++) {
      mx[i] = -1;
    }
    initmx(s, mx, num);
    return mxv(mx, num, 0, num);
  }
}
