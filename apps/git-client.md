# git-client

## Overview

| app                                    | platform      | price   | recommend |
| -------------------------------------- | ------------- | ------- | --------- |
| SourceTree                             | Windows/macOS | free    | 4         |
| Fork                                   | Windows/macOS | free    | 4         |
| [GitKraken](https://www.gitkraken.com) | Windows/macOS | $4.08/m | 3         |

## Features

| features       | SourceTree | Fork | GitKraken |
| -------------- | ---------- | ---- | --------- |
| inline compare | no         | yes  | yes       |
| interaction    | 5          | 5    | 3         |
| stability      | 5          | 5    | 3         |
