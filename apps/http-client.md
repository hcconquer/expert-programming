# http-client

## Overview

| app                      | platform      | price  | recommend |
| ------------------------ | ------------- | ------ | --------- |
| Postman                  | Windows/macOS | free   | 4         |
| [Paw](https://paw.cloud) | Windows/macOS | $49.99 | 2         |

## Features

| features   | Postman | Paw     |
| ---------- | ------- | ------- |
| Extensions | no      | yes     |
| Trail      | -       | 30(day) |
| Price      | free    | 49.99   |
